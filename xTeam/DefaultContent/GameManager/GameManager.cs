﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Network.Managers.GameManager.Modules;
using XTeam.Network.Managers.GameManager.Modules.Utilities;
using XTeam.Resources.GamePlay.GameManager.GameSpawnManager;
using XTeam.Resources.GamePlay.Network.Server;

namespace Resources.GamePlay.GameManager
{
    /// <summary>
    ///     Class managing the game process.
    ///     A new GameManager is spawned for each map.
    /// </summary>
    public class GameManager : NetworkBehaviour
    {
        //Gameobjects
        public static GameManager instance;

        //Gamesettings
        private GameSettings _gameSettings;
        public GameSpawnManager gameSpawnManager;

        //Gamemanager modules.
        public GameStateManager gameStateManager;
        public GameTeamManager gameTeamManager;
        public GameUiManager gameUiManager;

        //SERVER commands\\
        /// <summary>
        ///     Initializes the game.
        /// </summary>
        [Server]
        public static void InitializeGame()
        {
            Server.instance.StartCoroutine(instance.InitializeGameCoroutine());
        }

        private void RegisterHandlers() {}

        /// <summary>
        ///     Initializes the game.
        /// </summary>
        [Server]
        private IEnumerator InitializeGameCoroutine()
        {
            yield return new WaitForSeconds(3);
            Debug.Log("Initializing the GameManager...");

            //Caching the map objects.
            _gameSettings = FindObjectOfType<GameSettings>();
            if (_gameSettings == null)
            {
                Debug.LogError("Couldn't find GameSettings object on the map! Aborting the game initialization.");

                //Closing the server... //TODO
                NetworkManager.singleton.StopServer();
                //TODO: LinkManager.instance.OnClientDisconnectedFromServer(null);
                yield break;
            }

            //Initializing GameManager components.
            gameTeamManager.Initialize();
            gameSpawnManager.Initialize();
            gameSpawnManager.spawnWavesDelay = _gameSettings.GetSpawnWavesDelay();

            //Starting the pregame game state.
            StartGameState(GameState.Pregame);

            //Setting the StartNextGameState method to StateEnd event.
            GameStateManager.onGameStateEndCallback.Add(CmdStartNextGameState);
        }
        /// <summary>
        /// Switches GameStates.
        /// </summary>
        [Command]
        public void CmdStartNextGameState(GameState lastGameState)
        {
            Debug.Log("Starting next GameState.");
            //Getting the next apropriate GameState.
            switch (lastGameState)
            {
                case GameState.Pregame:
                    //The previous GameState was PreGame.
                    if (_gameSettings.GetWarmupEnabled())
                    {
                        StartGameState(GameState.Warmup);
                    }
                    else
                    {
                        StartGameState(GameState.Ingame);
                    }
                    break;
                case GameState.Warmup:
                    StartGameState(GameState.Ingame);
                    break;
                case GameState.Ingame:
                    StartGameState(GameState.Endgame);
                    break;
                case GameState.Endgame:
                    //The previous GameState was EndGame.
                    //Starting a new game!
                    Debug.Log("Starting a new game!");
                    //TODO: Start a new game.
                    break;
                case GameState.Paused: //TODO!!!
                    //The previous GameState was Paused.
                    //Resuming the game.
                    Debug.Log("Resuming the game!");
                    gameStateManager.CmdStartGameState(GameState.Ingame, 1800);
                    //TODO: Resuming to the moment of pause.
                    break;
                default:
                    Debug.LogError("Cannot get the previous GameState");
                    //TODO: Error for now ;D
                    break;
            }
        }
        /// <summary>
        /// Starts a game state.
        /// </summary>
        [Server]
        public void StartGameState(GameState state)
        {
            switch (state)
            {
                case GameState.Pregame:
                    //Starting the Pregame state.
                    gameStateManager.CmdStartGameState(GameState.Pregame, _gameSettings.GetPregameStateLenght());

                    //Spawning management.
                    gameSpawnManager.currentPlayerSpawnMode = PlayerSpawnMode.SpawningBlocked;
                    break;
                case GameState.Warmup:
                    //Starting the Warmup state.
                    gameStateManager.CmdStartGameState(GameState.Warmup, _gameSettings.GetWarmupStateLenght());

                    //Spawning management.
                    gameSpawnManager.currentPlayerSpawnMode = PlayerSpawnMode.SpawningPlayermanaged;
                    gameSpawnManager.SpawnDroppedItems();
                    break;
                case GameState.Ingame:
                    //Starting the first round.
                    Debug.Log("Starting the game!");
                    //gameRoundManager.CmdStartRound(1, gameSettings.GetRoundLenght());
                    gameSpawnManager.currentPlayerSpawnMode = PlayerSpawnMode.SpawningServermanaged;
                    break;
                case GameState.Endgame:
                    //Ending the game.
                    gameSpawnManager.currentPlayerSpawnMode = PlayerSpawnMode.SpawningBlocked;
                    break;
            }
        }

        //Non-networked mothods.
        private void Awake()
        {
            instance = this;
        }
    }
}