﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.GameplayObjects.Entities.Living;
using XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Scripts;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.GamePlay.Utilities;
using XTeam.Resources.Utilities.InputManager.cMonkeys;
using XTeam.Resources.Utilities.UiManager.Notification;

namespace XTeam.Resources.GamePlay.GameplayObjects.Items
{
    public class ItemManager : NetworkBehaviour
    {
        //Static variables.
        public static Dictionary<ItemInfo, ItemObjectHolder> registeredItems =
            new Dictionary<ItemInfo, ItemObjectHolder>();

        //Drop button variables.
        private Timer _dropToThrowSwitchTimer;
        public Camera characterCamera;
        public Inventory inventory;

        //References.
        public MovementController movementController;

        //Item pickup properties.
        public float pickupDistance;

        #region ObjectMethods

        //Checking whether we can pickup an object.
        private void Update()
        {
            if (movementController.isActiveAndEnabled)
            {
                //Checking whether we can pickup an object.
                var dropped = GetDroppedItemInFront();
                if (dropped != null)
                {
                    if (!string.IsNullOrEmpty(dropped.itemCustomName))
                    {
                        //The item has a custom name.
                        //This means it also has an owner.
                        //Showing the pickup prompt.
                        NotificationsManager.instance
                            .ShowSmallNotification(
                                "Press Interact button to pickup " +
                                LinkManager.GetPlayer(dropped.ownersGuid).playerName + "'s " + dropped.itemCustomName,
                                3f);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dropped.ownersGuid))
                        {
                            //This item has its owner.
                            //Showing the pickup prompt.
                            NotificationsManager.instance
                                    .ShowSmallNotification(
                                    "Press Interact button to pickup " +
                                    LinkManager.GetPlayer(dropped.ownersGuid).playerName + "'s " + dropped.itemName, 3f);
                        }
                        else
                        {
                            //This item doesn't have an owner.
                            //Showing the pickup prompt.
                            NotificationsManager.instance
                                    .ShowSmallNotification("Press Interact button to pickup " + dropped.itemName, 3f);
                        }
                    }
                }

                //Checking whether player wants to pickup an object.
                if (CInput.GetButtonDown("Interact"))
                {
                    //Picking up the object.
                    CmdPickupItemInFront();
                }
            }
        }

        public DroppedItem GetDroppedItemInFront()
        {
            RaycastHit hitInfo;

            if (!Physics.Raycast(characterCamera.transform.position, characterCamera.transform.forward, out hitInfo,
                pickupDistance)) return null;

            //Checking if object in front of us is a pickupable object.
            var item = hitInfo.transform.GetComponent<DroppedItem>();

            return item ?? null;
        }

        public void CheckDropButton()
        {
            if (CInput.GetButton("Drop"))
            {
                //The drop key is being pressed.
                if (_dropToThrowSwitchTimer != null) return;

                //The drop/throw timer is not running.
                //Starting the timer.
                _dropToThrowSwitchTimer = gameObject.AddComponent<Timer>();
                _dropToThrowSwitchTimer.endNumber = 2;
                _dropToThrowSwitchTimer.currentCountingMode = CountingMode.Up;
                _dropToThrowSwitchTimer.SetActive(true);
            }
            else
            {
                //The drop key isn't pressed.
                if (!_dropToThrowSwitchTimer.active) return;
                //Stopping the timer.
                _dropToThrowSwitchTimer.End();
                //Press of drop button wasn't long enought to start throwing.
                //Droping the item.
                CmdDropCurrentItem();
            }
        }

        #endregion

        #region Commands

        [Command]
        public void CmdPickupItemInFront()
        {
            //Caching the dropped item Item variable.
            var itemToPickup = GetDroppedItemInFront();
            if (itemToPickup == null)
            {
                return;
            }

            //Making a temp copy of item.
            var temp = new ItemInfo
            {
                name = itemToPickup.itemName,
                customName = itemToPickup.itemCustomName,
                ownersGuid = itemToPickup.ownersGuid
            };

            //Destroying the dropped item.
            NetworkServer.Destroy(itemToPickup.gameObject);

            //Giving player a picked up item.
            inventory.CmdAddItemToInventory(temp);
        }

        [Command]
        public void CmdDropCurrentItem()
        {
            //Removing item from player's inventory.
            //TODO
            //inventory.CmdRemoveItemFromInventory();
        }

        [Command]
        public void CmdThrowCurrentItem() {}

        #endregion

        #region StaticMethods

        /// <summary>
        ///     Spawns useable item.
        /// </summary>
        /// <returns>Spawned useable item</returns>
        public static IItem SpawnUseableItem(string itemName)
        {
            foreach (var item in registeredItems.Keys.Where(item => item.name == itemName))
            {
                //Desired item found.
                //Spawning item and returning a value.
                ItemObjectHolder searchedItemPrefab;
                registeredItems.TryGetValue(item, out searchedItemPrefab);

                var searchedItem = (GameObject) Instantiate(searchedItemPrefab.useableItem);
                return searchedItem.GetComponent<IItem>();
            }
            return null;
        }

        /// <summary>
        ///     Spawns dropped item.
        /// </summary>
        /// <returns>Spawned dropped item</returns>
        public static DroppedItem SpawnDroppedItem(string itemName)
        {
            foreach (var item in registeredItems.Keys.Where(item => item.name == itemName))
            {
                //Desired item found.
                //Spawning item and returning a value.
                ItemObjectHolder searchedItemPrefab;
                registeredItems.TryGetValue(item, out searchedItemPrefab);

                if (searchedItemPrefab == null) continue;
                var searchedItem = (GameObject) Instantiate(searchedItemPrefab.droppedItem);

                var droppedItem = searchedItem.GetComponent<DroppedItem>();
                return droppedItem;
            }
            return null;
        }

        /// <summary>
        ///     Registers new item into the game.
        /// </summary>
        public static void RegisterItem(ItemInfo info, Object useableItem, Object droppedItem)
        {
            //Creating new ItemObjectHolder
            var itemObjectHolder = new ItemObjectHolder
            {
                droppedItem = droppedItem,
                useableItem = useableItem
            };

            //Adding new item to registered items.
            registeredItems.Add(info, itemObjectHolder);
        }

        #endregion
    }
}