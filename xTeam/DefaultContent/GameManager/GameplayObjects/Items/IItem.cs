﻿using UnityEngine;

namespace XTeam.Resources.GamePlay.GameplayObjects.Items
{
    public interface IItem {
        string GetItemName();
        string GetCustomItemName();
        string GetItemOwnersGuid();

        GameObject GetGameObject();

        //Use methods.
        void Use1();
        void Use2();
        //Throw methods.
        void ThrowingStart();
        void ThrowingAbort();

        //Set methods.
        void SetCustomName(string name);
    }
    /// <summary>
    /// Holds all information about an item.
    /// </summary>
    [System.Serializable]
    public struct ItemInfo
    {
        public string name;
        public ItemType itemType;
        public string ownersGuid;
        public string customName;
    }
    /// <summary>
    /// Enum descibing the type of an item.
    /// </summary>
    public enum ItemType
    {
        MainWeapon, SecondaryWeapon, HelperWeapon, Grenade, Gadget
    }
    /// <summary>
    /// Holds all objects associated with specific item.
    /// </summary>
    public class ItemObjectHolder
    {
        public Object useableItem;
        public Object droppedItem;
    }
}