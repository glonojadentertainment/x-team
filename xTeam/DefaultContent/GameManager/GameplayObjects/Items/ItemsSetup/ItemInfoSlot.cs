﻿using UnityEngine;

namespace XTeam.Resources.GamePlay.GameplayObjects.Items.ItemsSetup
{
    public class ItemInfoSlot : MonoBehaviour {
        public string itemName;
        public ItemType itemType;
        public Object droppedItemObject;
        public Object useableItemObject;
    }
}
