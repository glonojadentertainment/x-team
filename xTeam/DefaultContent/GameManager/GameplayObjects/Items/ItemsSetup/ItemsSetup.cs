﻿using UnityEngine;
using UnityEngine.Networking;

namespace XTeam.Resources.GamePlay.GameplayObjects.Items.ItemsSetup
{
    public class ItemsSetup : MonoBehaviour {
        public ItemInfoSlot[] itemInfoSlots;

        void Awake()
        {
            //Registering all the items.
            foreach (ItemInfoSlot slot in itemInfoSlots)
            {
                //Creating the itemInfo for object.
                ItemInfo itemInfo = new ItemInfo();
                itemInfo.name = slot.itemName;
                itemInfo.itemType = slot.itemType;

                ItemManager.RegisterItem(itemInfo, slot.useableItemObject, slot.droppedItemObject);

                //Registering useable and dropped item prefabs.
                ClientScene.RegisterPrefab((GameObject)slot.useableItemObject);
                ClientScene.RegisterPrefab((GameObject)slot.droppedItemObject);

                Debug.Log("Item Setup: Registred item " + slot.itemName);
            }
        }
    }
}
