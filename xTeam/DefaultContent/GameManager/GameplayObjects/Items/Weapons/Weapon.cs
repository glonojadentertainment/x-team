﻿namespace XTeam.Resources.GamePlay.GameplayObjects.Items.Weapons
{
    public interface IWeapon : IItem
    {
        WeaponType getType();
    }
    public enum WeaponType
    {
        Melee, Gun
    }
}