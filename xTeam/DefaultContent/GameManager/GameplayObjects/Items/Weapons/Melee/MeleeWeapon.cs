﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.GameplayObjects.Entities.Living;

namespace XTeam.Resources.GamePlay.GameplayObjects.Items.Weapons.Melee
{
    public class MeleeWeapon : NetworkBehaviour, IWeapon {

        //Item variables.
        public string itemName;
        public string customItemName;
        [HideInInspector]
        public string itemOwnersGuid;

        //Melee weapon damages.
        public float attack1Damage;
        public float attack2Damage;
        //Damage cooldowns.
        public float attack1Cooldown;
        public float attack2Cooldown;
        //Attack ranges.
        public float attack1Range;
        public float attack2Range;

        //Damage multipliers.
        public float headAttackMultiplier;
        public float throatAttackMultiplier;
        public float torsoAttackMultiplier;
        public float armAttackMultiplier;
        public float anusAttackMultiplier;
        public float legAttackMultiplier;
        public float backAttackMultiplier;

        //Animations.
        public Animator animator;

        #region WeaponMethods
        public WeaponType getType()
        {
            return WeaponType.Melee;
        }

        public string GetItemName()
        {
            return itemName;
        }

        public string GetCustomItemName()
        {
            return customItemName;
        }

        public string GetItemOwnersGuid()
        {
            return itemOwnersGuid;
        }

        //Attack1
        public void Use1()
        {
            //Playing the attack1 animation.
            animator.SetTrigger("Attack1");
            //Calling the Attack1 method.
            CmdAttack1();
        }

        //Attack2
        public void Use2()
        {
            //Playing the attack2 animation.
            animator.SetTrigger("Attack2");
            //Calling the Attack2 method.
            CmdAttack2();
        }

        //Throwing
        public void ThrowingStart()
        {
            //Calling the throwingStart method.
            CmdThrowingStart();
        }

        public void ThrowingAbort()
        {
            CmdThrowingAbort();
        }
        #endregion

        #region NetworkedMethods
        [Command]
        public void CmdAttack1()
        {
            //TODO: Playing player animation.
            //Giving damage to victim. //TODO: Multipliers.
            //getTarget(attack1Range).CmdTakeDamage(attack1Damage);
        }
        [Command]
        public void CmdAttack2()
        {
            //TODO: Playing player animation.
            //(attack2Range).CmdTakeDamage(attack2Damage);
        }
        [Command]
        public void CmdThrowingStart()
        {
            throw new NotImplementedException();
        }
        [Command]
        public void CmdThrowingAbort()
        {
            throw new NotImplementedException();
        }
        [Server]
        public ILivingEntity GetTarget(float distance)
        {
            //Hit information.
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distance))
            {
                Transform victimTransform = hit.transform;

                ILivingEntity victim = victimTransform.GetComponent<ILivingEntity>();
                //Checking if thing we hit is a LivingEntity.
                if(victim != null)
                {
                    //The victim is a living entity.
                    //Retutning the results.
                    return victim;
                }
            }
            return null;
        }

        public GameObject GetGameObject()
        {
            return this.gameObject;
        }

        public void SetCustomName(string name)
        {
            this.customItemName = name;
        }
        #endregion
    }
}