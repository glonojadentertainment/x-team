﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.MapMaker.Prefabs.Required.GameSetup.Team;
using Object = UnityEngine.Object;

namespace XTeam.Resources.GamePlay.GameplayObjects.Teams
{
    public class TeamManager : NetworkBehaviour
    {
        [Tooltip("Prefab of the TeamManager.")]
        public static Object teamManagerPrefab;
        [Tooltip("Prefab of a Team object.")]
        public Object teamPrefab;
        [Tooltip("Holder object of all the spawned Team objects.")]
        public GameObject teamsHolder;
        /// <summary>
        /// Instance of this TeamManager.
        /// </summary>
        public static TeamManager instance;
        /// <summary>
        /// Called when the teamList changes.
        /// </summary>
        public Action onTeamsListChange;

        /// <summary>
        /// Initializes the default TeamManager.
        /// </summary>
        [Server]
        public static void Initialize()
        {
            Debug.Log("Initializing the default TeamManager...");

            //Spawning a new TeamManager instance.
            instance = ((GameObject) Instantiate(teamManagerPrefab)).GetComponent<TeamManager>();
            //TODO:
            NetworkServer.Spawn(instance.gameObject);
        }
        /// <summary>
        /// Caches teams from the map.
        /// </summary>
        [Server]
        public void CacheTeamsFromMap()
        {
            //Clearing current teams list.
            ClearTeamList();

            Debug.Log("Caching teams from the current map...");
            foreach (TeamInfo t in GameObject.FindObjectsOfType<TeamInfo>())
            {
                //Creating Team component out of the TeamInfo data.
                Team.Team newTeam = CreateTeam(t);

                Debug.Log("Added team " + newTeam.teamName + " to the teams list.");
            }
            RpcOnTeamListChanged();
        }
        /// <summary>
        /// Caches created teams.
        /// </summary>
        [Client]
        private void CacheCreatedTeams()
        {
            //Clearing current teams list.
            ClearTeamList();

            Debug.Log("Caching the spawned teams");
            foreach (var t in GameObject.FindObjectsOfType<Team.Team>())
            {
                //Setting parents.
                t.transform.SetParent(teamsHolder.transform);

                Debug.Log("Added team " + t.name + " to the teams list.");
            }
        }
        /// <summary>
        /// Called on all of the clients, when a teams list is refreshed.
        /// </summary>
        [ClientRpc]
        public void RpcOnTeamListChanged()
        {
            //Caching the new teamsList.
            CacheCreatedTeams();
            //Calling the onTeamsListChange.
            onTeamsListChange();
        }
        /// <summary>
        /// Creates a new team.
        /// </summary>
        /// <param name="info">Team information.</param>
        /// <returns>Created team.</returns>
        [Server]
        public Team.Team CreateTeam(TeamInfo info)
        {
            if(GetTeam(info.teamName) == null)
            {
                Team.Team newTeam = ((GameObject)Instantiate(teamPrefab)).GetComponent<Team.Team>();

                //Setting teamName.
                newTeam.teamName = string.IsNullOrEmpty(info.teamName) ? "Unknown Team" : info.teamName;

                //Setting teamColor.
                newTeam.teamColor = info.teamColor;

                //Setting teamSize.
                newTeam.teamSize = info.teamSize == 0 ? 10 : info.teamSize;

                //Setting teamAvatar.
                newTeam.teamAvatar = info.teamPlayerAvatar.ToString();

                //Setting parents and spawning the team.
                newTeam.transform.position = Vector3.zero;
                newTeam.transform.SetParent(teamsHolder.transform);

                NetworkServer.Spawn(newTeam.gameObject);
                RpcOnTeamListChanged();

                //Returning the new team.
                return newTeam;
            } else
            {
                //Team with this name already exists.
                Debug.LogWarning("Team " + info.teamName + " already exists!");
                return GetTeam(info.teamName);
            }
        }
        /// <summary>
        /// Removes a team.
        /// </summary>
        /// <param name="teamToRemove">Team to be removed.</param>
        [Server]
        public void RemoveTeam(Team.Team teamToRemove)
        {
            //Destroying the team gameObject.
            NetworkServer.Destroy(teamToRemove.gameObject);
            RpcOnTeamListChanged();
        }
        /// <summary>
        /// Clears the current teamsList.
        /// </summary>
        private void ClearTeamList()
        {
            Debug.Log("Clearing the current team list...");
            //Lopping through all teams on the list and deleting them.
            foreach (var t in GetTeamList())
            {
                Destroy(t.gameObject);
            }
        }
        public IEnumerable<Team.Team> GetTeamList()
        {
            return teamsHolder.GetComponentsInChildren<Team.Team>();
        }
        public Team.Team GetTeam(string teamName)
        {
            return GetTeamList().FirstOrDefault(t => t.teamName == teamName);
        }

        void Start()
        {
            //ClientConnector just joined the server.
            //Caching the spawned teams.
            CacheCreatedTeams();
        }
    }
}