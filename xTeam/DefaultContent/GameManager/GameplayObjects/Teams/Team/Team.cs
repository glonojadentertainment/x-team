﻿using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.MapMaker;
using XTeam.Resources.GamePlay.Network.Client.Player;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.Utilities.LevelLoader;

namespace XTeam.Resources.GamePlay.GameplayObjects.Teams.Team
{
    public class Team : NetworkBehaviour {
        [SyncVar]
        public string teamName;
        [SyncVar]
        public Color teamColor;
        [SyncVar]
        public int teamSize;
        [SyncVar]
        public string teamAvatar;

        //Not networked methods.
        public Player[] GetMembers() {
            //Checking which player belongs to this team.
            return LinkManager.GetPlayers().Where(p => p.currentTeamName == teamName).ToArray();
        }
        public bool HasMember(Player p)
        {
            return GetMembers().Any(member => member == p);
        }

        //Networked methods.
        [Server]
        public void AddPlayer(Player toAdd)
        {
            //Assigning player to this team.
            toAdd.currentTeamName = teamName;
            Debug.Log (toAdd.playerName + " joined team " + teamName);

            //Closing the team selection menu for this player.
            toAdd.networkIdentity.clientAuthorityOwner.Send(MyMsgTypes.ServerMsgCloseTeamSelectionMenu, new Map());
        }
    }
}
