﻿namespace XTeam.Resources.GamePlay.GameplayObjects.Entities
{
    public interface IEntity {
        string GetName();
    }
}
