﻿using UnityEngine;
using UnityEngine.Networking;
using XTeam.MapMaker.Prefabs.Required.GameSetup.Team;
using XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Scripts;
using Avatar = XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Avatars.Avatar;

namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters
{
    /// <summary>
    ///     Class for synchronizing character information.
    /// </summary>
    public class Character : NetworkBehaviour, ILivingEntity
    {
        /// <summary>
        ///     Reference to the character, that is currently playing.
        /// </summary>
        public static Character currentlyUsedCharacter;

        [Tooltip("Reference to the game object containing avatars.")] public GameObject avatarsHolder;

        [Tooltip("Teference to the main camera of the character.")] public Camera characterCamera;

        [Tooltip("Reference to the currently enabled avatar.")] public Avatar currentAvatar;

        [SyncVar (hook = "SetAvatar")] [Tooltip("Current type of an avatar.")] public PlayerAvatarType currentAvatarType =
            PlayerAvatarType.Guard;

        [Tooltip("Health component of this living entity.")] public Health health;

        [Tooltip("Inventory component of this living entity.")] public Inventory inventory;

        [SyncVar] [Tooltip("GUID of this character's owner.")] public string ownersGuid;

        public MovementController movementController;

        /// <summary>
        ///     Sets character's current avatar.
        /// </summary>
        /// <param name="type">Type of new character avatar.</param>
        private void SetAvatar(PlayerAvatarType type)
        {
            //Disabling previous avatar.
            currentAvatar.gameObject.SetActive(false);

            //Getting the new avatar.
            var newAvatar = Avatar.GetAvatar(type);

            //Setting up required avatar.
            currentAvatar = newAvatar;
            currentAvatarType = newAvatar.avatarType;

            //Enabling current avatar.
            currentAvatar.gameObject.SetActive(true);
            characterCamera.gameObject.GetComponent<CameraHeadBinder>().leftEye =
                currentAvatar.transform.FindChild("python")
                    .FindChild("hips")
                    .FindChild("spine")
                    .FindChild("chest")
                    .FindChild("neck")
                    .FindChild("head")
                    .FindChild("eye.L");
            characterCamera.gameObject.GetComponent<CameraHeadBinder>().rightEye =
                currentAvatar.transform.FindChild("python")
                    .FindChild("hips")
                    .FindChild("spine")
                    .FindChild("chest")
                    .FindChild("neck")
                    .FindChild("head")
                    .FindChild("eye.R");

            //Setting proper avatar layer.
            if (ownersGuid == LinkManager.clientGuid)
            {
                //Setting avatar invisible for ourselves.
                SetLayerRecursively(currentAvatar.gameObject, 9);
            }
        }
        /// <summary>
        ///     Enabling required componenets on the character.
        /// </summary>
        [ClientRpc]
        public void RpcEnableComponents()
        {
            Debug.Log("Enabling character components...");
            if (IsLocal())
            {
                GetComponent<MovementController>().controlsActive = true;
                transform.FindChild("Camera").gameObject.SetActive(true);
            }
        }

        /// <summary>
        ///     Sets layer for a GameObject and its children.
        /// </summary>
        /// <param name="obj">Target of layer changing.</param>
        /// <param name="layer">New layer for target object.</param>
        private void SetLayerRecursively(GameObject obj, int layer)
        {
            obj.layer = layer;

            foreach (Transform child in obj.transform)
            {
                SetLayerRecursively(child.gameObject, layer);
            }
        }

        #region Gets
        public string GetName()
        {
            return LinkManager.GetPlayer(ownersGuid).playerName;
        }

        public Health GetHealth()
        {
            return health;
        }

        public Inventory GetInventory()
        {
            return inventory;
        }

        /// <summary>
        ///     Checks whether the character is controlled by a local player.
        /// </summary>
        /// <returns></returns>
        public bool IsLocal()
        {
            if (ownersGuid == LinkManager.clientGuid)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region MonoBehaviour
        private void Start()
        {
            if (IsLocal())
            {
                currentlyUsedCharacter = this;
            }
        }

        public override void OnStartServer()
        {
            RpcEnableComponents();
        }

        /// <summary>
        ///     Checkes whether the character was spawned on a server or a single player world.
        /// </summary>
        private void Awake()
        {
            if (NetworkManager.singleton == null || !NetworkManager.singleton.client.isConnected)
            {
                movementController.controlsActive = true;
                characterCamera.gameObject.SetActive(true);
            }
        }
        #endregion
    }
}