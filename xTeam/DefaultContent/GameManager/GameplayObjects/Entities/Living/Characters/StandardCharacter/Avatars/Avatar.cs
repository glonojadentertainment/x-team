﻿using UnityEngine;
using XTeam.MapMaker.Prefabs.Required.GameSetup.Team;

namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Avatars
{
    /// <summary>
    /// Class holding the info about a player avatar.
    /// </summary>
    public class Avatar : MonoBehaviour {
        [Tooltip ("Reference to the Character root.")]
        public GameObject character;
        [Tooltip("Type of the avatar.")]
        public PlayerAvatarType avatarType;
        private Animator _sourceAnimator;
        private Animator _thisAnimator;
	
        //Start
        void Start()
        {
            //Caching this animator.
            _thisAnimator = GetComponent<Animator>();
            //Caching the source animator.
            _sourceAnimator = character.GetComponent<Animator>();
        }
        // Update is called once per frame
        void Update () {
            //Setting Horizontal.
            _thisAnimator.SetFloat("Horizontal", _sourceAnimator.GetFloat("Horizontal"));
            //Setting Vertical.
            _thisAnimator.SetFloat("Vertical", _sourceAnimator.GetFloat("Vertical"));
            //Setting State.
            _thisAnimator.SetInteger("State", _sourceAnimator.GetInteger("State"));
            //Setting Jump.
            _thisAnimator.SetBool("Jump", _sourceAnimator.GetBool("Jump"));
            //Setting InAir.
            _thisAnimator.SetBool("InAir", _sourceAnimator.GetBool("InAir"));
        }
        /// <summary>
        /// Searches for specified avatar type.
        /// </summary>
        /// <param name="type">Character of specified type.</param>
        /// <returns></returns>
        public static Avatar GetAvatar(PlayerAvatarType type)
        {
            foreach(Transform a in Character.currentlyUsedCharacter.avatarsHolder.transform)
            {
                if(a.GetComponent<Avatar>().avatarType == type)
                {
                    return a.GetComponent<Avatar>();
                }
            }
            return null;
        }
    }
}