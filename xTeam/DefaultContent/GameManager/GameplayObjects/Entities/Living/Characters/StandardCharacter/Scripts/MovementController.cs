﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.Utilities.InputManager.cMonkeys;
using XTeam.Resources.Utilities.UiManager;

namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Scripts
{
    [RequireComponent(typeof (Rigidbody))]
    [RequireComponent(typeof (CapsuleCollider))]
    public class MovementController : NetworkBehaviour
    {
        //Components
        public Animator characterAnimator;
        public CapsuleCollider collider;
        public Rigidbody rigidbody;
        public CameraHeadBinder cameraHeadBinder;
        public bool controlsActive;

        //Positions settings.
        [SerializeField] public PositionSettings walkSettings = new PositionSettings();
        [SerializeField] public PositionSettings crouchSettings = new PositionSettings();
        [SerializeField] public PositionSettings flySettings = new PositionSettings();
        [SerializeField] public PositionSettings proneSettings = new PositionSettings();
        public bool canSprint = true;

        [SyncVar] public PlayerPosition currentPlayerPosition;


        public float heightChangeTime = 0.5f;
        public float airControl = 0.5f;
        public float jumpForce = 2.0f;
        public float maxVelocityChange = 10.0f;

        public float slideLimit = 70f;
        public float speed = 10.0f;
        public float sprintMultiplier = 1.5f;

        private Vector3 _contactPoint;
        private float _rayDistance;

        /// <summary>
        /// Runs every server tick.
        /// </summary>
        private void FixedUpdate()
        {
            //Works only on local player.
            if (controlsActive)
            {
                //Input getting.
                var horizontalInput = CInput.GetAxis("Horizontal Movement");
                var verticalInput = CInput.GetAxis("Vertical Movement");

                //Getting the speed.
                var currentSpeed = speed;

                //Temp Animator variables.
                var animatorHorizontal = horizontalInput;
                var animatorVertical = verticalInput;

                //Checking whether character is grounded.
                if (CheckGrounded())
                {
                    //Telling character animator, that player is not in air.
                    characterAnimator.SetBool("InAir", false);
                    characterAnimator.SetBool("Jump", false);

                    //Calculating the slide.
                    // If sliding block any player movement.
                    if (CheckSliding())
                    {
                        return;
                    }

                    //Proccessing character's movement based on current position.
                    switch (currentPlayerPosition)
                    {
                        case PlayerPosition.Standing:
                            //Character is standing on the ground - normal control.
                            animatorVertical = animatorVertical/5;

                            //Checking if we're sprinting.
                            if (canSprint && CInput.GetButton("Sprint"))
                            {
                                currentSpeed *= sprintMultiplier;
                                animatorVertical = 1;
                            }

                            // Set the movement input to be the force to apply to the player every frame
                            horizontalInput *= currentSpeed;
                            verticalInput *= currentSpeed;

                            rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                                ForceMode.VelocityChange);

                            //Setting characteranimator values.
                            characterAnimator.SetFloat("Horizontal", animatorHorizontal);
                            characterAnimator.SetFloat("Vertical", animatorVertical);
                            break;
                        case PlayerPosition.Crouching:
                            //Character is crouching on the ground - crouching control.
                            currentSpeed *= crouchSettings.movementMultiplier;

                            // Set the movement input to be the force to apply to the player every frame
                            horizontalInput *= currentSpeed;
                            verticalInput *= currentSpeed;

                            rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                                ForceMode.VelocityChange);

                            //Setting characteranimator values.
                            characterAnimator.SetFloat("Horizontal", animatorHorizontal);
                            characterAnimator.SetFloat("Vertical", animatorVertical);
                            break;
                        case PlayerPosition.Proning:
                            //Character is crouching on the ground - crouching control.
                            currentSpeed *= proneSettings.movementMultiplier;

                            // Set the movement input to be the force to apply to the player every frame
                            horizontalInput *= currentSpeed;
                            verticalInput *= currentSpeed;

                            rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                                ForceMode.VelocityChange);

                            //Setting characteranimator values.
                            characterAnimator.SetFloat("Horizontal", animatorHorizontal);
                            characterAnimator.SetFloat("Vertical", animatorVertical);
                            break;
                    }
                }
                else
                {
                    //Character is not on the ground - air control.

                    //Set the movement input to be the force to apply to the player every frame
                    horizontalInput = horizontalInput*currentSpeed*rigidbody.mass*airControl;
                    verticalInput = verticalInput*currentSpeed*rigidbody.mass*airControl;
                    //Applying the inAir force to the player.
                    rigidbody.AddForce(CalculateAcceleration(horizontalInput, verticalInput), ForceMode.Acceleration);
                    //Setting the InAir bool for Animator.
                    characterAnimator.SetBool("InAir", true);

                    //Proccessing character's movement based on current position.
                    //Setting characteranimator values.
                    characterAnimator.SetFloat("Horizontal", animatorHorizontal);
                    characterAnimator.SetFloat("Vertical", animatorVertical);
                }
            }
        }

        #region ColliderMessages

        // Store point that we're in contact with for use in FixedUpdate if needed
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            _contactPoint = hit.point;
        }

        #endregion

        #region MonoBehaviour
        /// <summary>
        ///     Setting up the NetworkAnimator.
        /// </summary>
        private void Start()
        {
            //Setting up the network animator.
            GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
            GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
            GetComponent<NetworkAnimator>().SetParameterAutoSend(2, true);
            GetComponent<NetworkAnimator>().SetParameterAutoSend(3, true);
            GetComponent<NetworkAnimator>().SetParameterAutoSend(4, true);
        }

        private void Update()
        {
            //Checking current character height.
            CheckHeight();
            //Checking current character position.
            CheckPosition();
            //Checking current character rotation.
            //CheckRotation();

            //Works only on local player.
            if (controlsActive)
            {
                //Setting proper character position, based on player input.
                if (CInput.GetButtonDown("Jump"))
                {
                    if (CheckGrounded())
                    {
                        Jump();
                        return;
                    }
                }
                if (CInput.GetButton("Crouch"))
                {
                    currentPlayerPosition = PlayerPosition.Crouching;
                    return;
                }
                if (CInput.GetButton("Prone"))
                {
                    currentPlayerPosition = PlayerPosition.Proning;
                    return;
                }

                currentPlayerPosition = PlayerPosition.Standing;
            }
        }

        #endregion

        #region Jumping

        private void Jump()
        {
            if (flySettings.canUse)
            {
                currentPlayerPosition = PlayerPosition.Flying;
                rigidbody.AddForce(new Vector3(0, CalculateJumpVerticalSpeed(), 0), ForceMode.Impulse);
                characterAnimator.SetBool("Jump", true);
            }
        }

        private void Stand()
        {
            if (walkSettings.canUse && CheckSpaceForPosition(PlayerPosition.Standing))
            {
                currentPlayerPosition = PlayerPosition.Standing;
                characterAnimator.SetInteger("State", (int) PlayerPosition.Standing);
                _rayDistance = walkSettings.rayDistance;
            }
        }

        private void Crouch()
        {
            if (crouchSettings.canUse && CheckSpaceForPosition(PlayerPosition.Crouching))
            {
                currentPlayerPosition = PlayerPosition.Crouching;
                characterAnimator.SetInteger("State", (int) PlayerPosition.Crouching);
                _rayDistance = crouchSettings.rayDistance;
            }
        }

        private void Prone()
        {
            if (proneSettings.canUse && CheckSpaceForPosition(PlayerPosition.Proning))
            {
                currentPlayerPosition = PlayerPosition.Proning;
                characterAnimator.SetInteger("State", (int) PlayerPosition.Proning);
                _rayDistance = proneSettings.rayDistance;
            }
        }

        private float CalculateJumpVerticalSpeed()
        {
            // From the jump height and gravity we deduce the upwards speed 
            // for the character to reach at the apex.
            return Mathf.Sqrt(2*jumpForce*-Physics.gravity.y);
        }

        private Vector3 CalculateVelocityChange(float horizontalInput, float verticalInput)
        {
            var horizontal = transform.rotation*Vector3.right*horizontalInput;
            var vertical = transform.rotation*Vector3.forward*verticalInput;

            //Calculating the velocity change.
            var targetVelocity = horizontal + vertical;
            var velocity = rigidbody.velocity;
            var velocityChange = targetVelocity - velocity;
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;

            return velocityChange;
        }

        private Vector3 CalculateAcceleration(float horizontalInput, float verticalInput)
        {
            var horizontal = transform.rotation*Vector3.right*horizontalInput;
            var vertical = transform.rotation*Vector3.forward*verticalInput;

            //Calculating the velocity change.
            var targetVelocity = horizontal + vertical;
            var velocity = rigidbody.velocity;
            var velocityChange = targetVelocity - velocity;
            velocityChange.y = 0;

            return velocityChange;
        }

        #endregion

        #region Checks

        private bool CheckSliding()
        {
            if (currentPlayerPosition == PlayerPosition.Standing)
            {
                // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
                // because that interferes with step climbing amongst other annoyances
                RaycastHit hit;
                if (Physics.Raycast(transform.position, -Vector3.up, out hit, _rayDistance))
                {
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    {
                        return true;
                    }
                }
                // However, just raycasting straight down from the center can fail when on steep slopes
                // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
                else
                {
                    Physics.Raycast(_contactPoint + Vector3.up, -Vector3.up, out hit);
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool CheckGrounded()
        {
            var hits = Physics.RaycastAll(transform.position + collider.center, -Vector3.up, _rayDistance);

            return hits.Any(hit => hit.collider.tag != "Character");
        }

        private void CheckHeight()
        {
            switch (currentPlayerPosition)
            {
                case PlayerPosition.Standing:
                    if (collider.height != walkSettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.height, walkSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, 0, collider.center.z), heightChangeTime,
                            value => collider.center = value));
                        StartCoroutine(UiManager.Smooth(collider.radius, walkSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 1;
                    }
                    break;
                case PlayerPosition.Crouching:
                    if (collider.height != crouchSettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, crouchSettings.characterColliderCenter, collider.center.z),
                            heightChangeTime, value => collider.center = value));
                        StartCoroutine(UiManager.Smooth(collider.height, crouchSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        StartCoroutine(UiManager.Smooth(collider.radius, crouchSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 1;
                    }
                    break;
                case PlayerPosition.Proning:
                    if (collider.height != proneSettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, proneSettings.characterColliderCenter, collider.center.z),
                            heightChangeTime, value => collider.center = value));
                        StartCoroutine(UiManager.Smooth(collider.height, proneSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        StartCoroutine(UiManager.Smooth(collider.radius, proneSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 2;
                    }
                    break;
                case PlayerPosition.Flying:
                    if (collider.height != flySettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.height, flySettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                    }
                    break;
            }
        }

        private void CheckPosition()
        {
            switch (currentPlayerPosition)
            {
                case PlayerPosition.Standing:
                    Stand();
                    cameraHeadBinder.OnCharacterChangeState();
                    break;
                case PlayerPosition.Crouching:
                    Crouch();
                    cameraHeadBinder.OnCharacterChangeState();
                    break;
                case PlayerPosition.Proning:
                    Prone();
                    cameraHeadBinder.OnCharacterChangeState();
                    break;
            }
        }

        private bool CheckSpaceForPosition(PlayerPosition newPosition)
        {
            RaycastHit[] hits;

            switch (newPosition)
            {
                case PlayerPosition.Standing:
                    hits = Physics.RaycastAll(transform.position + collider.center, Vector3.up, walkSettings.rayDistance);

                    foreach (var hit in hits)
                    {
                        if (hit.collider.tag != "Character")
                        {
                            return false;
                        }
                    }
                    return true;
                case PlayerPosition.Crouching:
                    hits = Physics.RaycastAll(transform.position + collider.center, Vector3.up, crouchSettings.rayDistance);

                    foreach (var hit in hits)
                    {
                        if (hit.collider.tag != "Character")
                        {
                            return false;
                        }
                    }
                    return true;
            }
            return true;
        }

        private void CheckRotation()
        {
            if (currentPlayerPosition == PlayerPosition.Proning)
            {
                //Firing a ray to the ground and rotating player to lie straight.
                foreach (var hit in Physics.RaycastAll(transform.position, -Vector3.up, proneSettings.rayDistance))
                {
                    transform.rotation = Quaternion.LookRotation(hit.normal);
                }
            }
        }

        #endregion
    }

    /// <summary>
    ///     Enum for identifying current player's position.
    /// </summary>
    public enum PlayerPosition
    {
        Standing,
        Crouching,
        Proning,
        Flying
    }

    /// <summary>
    ///     Class for storing different per-position settings.
    /// </summary>
    [Serializable]
    public class PositionSettings
    {
        public bool canUse = true;
        public float characterColliderCenter = 0f;
        public float characterColliderHeight = 0.7f;
        public float characterColliderRadius = 0.3f;
        public float movementMultiplier = 0.6f;
        public float rayDistance = 0.3f;
    }
}