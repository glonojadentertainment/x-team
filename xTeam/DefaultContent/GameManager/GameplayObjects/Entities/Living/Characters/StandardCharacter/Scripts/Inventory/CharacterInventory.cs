﻿using UnityEngine;
using XTeam.Resources.GamePlay.GameplayObjects.Items;

namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Scripts.Inventory
{
    /// <summary>
    /// Component for managing items stored in the inventory as a character.
    /// </summary>
    public class CharacterInventory : MonoBehaviour
    {
        [Tooltip("The actual inventory, that synchronizes with other clients.")]
        public Living.Inventory characterInventory;

        public ItemInfo itemInHand;

        /// <summary>
        /// Updating the characterInvenotry's info about the inventory.
        /// </summary>
        void Update()
        {

        }

        /// <summary>
        /// Changes player's item in hand to the specified one.
        /// </summary>
        /// <param name="info">New item in hand.</param>
        public void SetHeldItem(ItemInfo info)
        {

        }
        /// <summary>
        /// Adds specified item to displayed item list.
        /// </summary>
        /// <param name="info">Item to be added to list.</param>
        public void AddItemToList(ItemInfo info)
        {

        }
        /// <summary>
        /// Removes specified item from displayed item list.
        /// </summary>
        /// <param name="info">Item to be removed.</param>
        public void RemoveItemFromList(ItemInfo info)
        {

        }
    }
}
