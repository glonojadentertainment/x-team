﻿using UnityEngine;

namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters.StandardCharacter.Scripts
{
    /// <summary>
    /// Component keeping camera in thew character's head.
    /// </summary>
    public class CameraHeadBinder : MonoBehaviour {
        [Tooltip("Left eye position of the character.")]
        public Transform leftEye;
        [Tooltip("Right eye position of the character.")]
        public Transform rightEye;

        // Update is called once per frame
        public void OnCharacterChangeState () {
            this.transform.position = Vector3.Lerp(leftEye.position, rightEye.position, 0.5f);
        }
    }
}
