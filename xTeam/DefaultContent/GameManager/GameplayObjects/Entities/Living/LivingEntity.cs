﻿namespace XTeam.Resources.GamePlay.GameplayObjects.Entities.Living
{
    /// <summary>
    /// Interface for all living entities.
    /// </summary>
    public interface ILivingEntity : IEntity
    {
        Health GetHealth();
        Inventory GetInventory();
    }
}
