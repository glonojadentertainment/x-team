﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;
using XTeam.Network.Managers.GameManager.Modules.Utilities;

namespace XTeam.Network.Managers.GameManager.Modules
{
    /// <summary>
    /// Class managing different game states.
    /// </summary>
    public class GameStateManager : NetworkBehaviour
    {
        [Tooltip("Currently running game state.")] [SyncVar]
        public GameState currentGameState;
        [Tooltip("Amount of seconds for the current game state to end.")] [SyncVar]
        public int secondsToStateEnd;

        /// <summary>
        /// Callbacks for OnGameStateEnd event.
        /// </summary>
        public static List<Action<GameState>> onGameStateEndCallback = new List<Action<GameState>>();
        /// <summary>
        /// Callbacks for OnGameStateCountdownTick event.
        /// </summary>
        public static List<Action<GameState, int>> onGameStateCountdownTickCallback = new List<Action<GameState, int>>();

        //Server methods.
        /// <summary>
        /// Starts a new game state.
        /// </summary>
        /// <param name="gameState">State to start.</param>
        /// <param name="timeInSeconds">How long should the started game state last.</param>
        [Command]
        public void CmdStartGameState(GameState gameState, int timeInSeconds)
        {
            //Debugging
            Debug.Log("Starting the " + gameState.ToString() + " game state");
            Debug.Log(gameState.ToString() + " will take " + timeInSeconds + " seconds");
            //The game state will change for the desired time and then invoke OnGameStateEnd.
            currentGameState = gameState;

            //Setting up the timer.
            secondsToStateEnd = timeInSeconds;
            Timer stateCountdown = this.gameObject.AddComponent<Timer>();

            stateCountdown.SetTime(timeInSeconds);
            stateCountdown.onTimerTick += OnStateCountdownTick;
            stateCountdown.onTimerEnd += OnStateCountdownEnd;
            //Begining the countdown.
            stateCountdown.SetActive(true);
        }
        /// <summary>
        /// Called when the state countdown ticks.
        /// </summary>
        public static void OnStateCountdownTick()
        {
            //Decreasing the secondsToStateEnd.
            GameManager.instance.gameStateManager.secondsToStateEnd--;
            //Calling the OnStateCountdownTick callbacks.
            foreach (Action<GameState, int> callback in onGameStateCountdownTickCallback)
            {
                callback(global::Resources.GamePlay.GameManager.GameManager.instance.gameStateManager.currentGameState, global::Resources.GamePlay.GameManager.GameManager.instance.gameStateManager.secondsToStateEnd);
            }
        }
        /// <summary>
        /// Called when the state countdown ends.
        /// </summary>
        public static void OnStateCountdownEnd()
        {
            Debug.Log(global::Resources.GamePlay.GameManager.GameManager.instance.gameStateManager.currentGameState + " game state ended!");

            //Calling the OnStateCountdownEnd callbacks.
            foreach (Action<GameState> callback in onGameStateEndCallback)
            {
                callback(global::Resources.GamePlay.GameManager.GameManager.instance.gameStateManager.currentGameState);
            }
        }
    } 
}