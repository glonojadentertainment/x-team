﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using XTeam.Network.Managers.GameManager.Modules.Utilities;

namespace XTeam.Network.Managers.GameManager.Modules
{
    public class GameUiManager : MonoBehaviour
    {
        //UI elements.
        public GameObject teamSelectionMenu;
        public NotificationsManager notifications;

        //Active indicators.
        public bool isTeamSelectionMenuActive = false;

        public void OpenTeamSelectionMenu(NetworkMessage msg)
        {
            //Setting the teamSelection menu active.
            StartCoroutine(Smooth(0f, 1f, 0.5f, value => teamSelectionMenu.transform.FindChild("Canvas").GetComponent<CanvasGroup>().alpha = value));
            teamSelectionMenu.SetActive(true);
            isTeamSelectionMenuActive = true;
        }
        public void CloseTeamSelectionMenu(NetworkMessage msg)
        {
            //Deactivating the teamSelection menu.
            StartCoroutine(Smooth(1f, 0f, 0.5f, value => teamSelectionMenu.transform.FindChild("Canvas").GetComponent<CanvasGroup>().alpha = value));
            teamSelectionMenu.SetActive(false);
            isTeamSelectionMenuActive = false;
        }

        public void UpdateTeamList()
        {
            //Listing all available teams.
            teamSelectionMenu.transform.FindChild("Canvas").transform.FindChild("TeamList").GetComponent<TeamsLister>().ClearTeamsList();
            teamSelectionMenu.transform.FindChild("Canvas").transform.FindChild("TeamList").GetComponent<TeamsLister>().ListTeams(GameTeamManager.GetTeamList());
        }
    } 
}