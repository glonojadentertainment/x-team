using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using XTeam.Network.Managers.GameManager.Modules.Utilities;
using XTeam.Resources.GamePlay.GameManager.GameTeam.Team;
using XTeam.Resources.GamePlay.Network.LinkManager;

namespace XTeam.Resources.GamePlay.GameManager.GameUI.LobbyUIState.TeamsList.TeamPanel
{
    public class TeamPanel : MonoBehaviour
    {
        //The team associated with this panel.
        private Team _representedTeam;

        //Gameobjects within the teamPanel;
        public GameObject label;
        public GameObject playersHolder;

        //Prefabs.
        public Object playerListSlot;

        public void SetupTeamPanel(Team representedTeam)
        {
            //Setting the represented team variable.
            this._representedTeam = representedTeam;

            //Setting the teamPanel's properties.
            name = representedTeam.teamName;
            label.GetComponentInChildren<Text>().text = representedTeam.teamName;
            //label.GetComponent<Image>().color = representedTeam.teamColor;

            //Listing players.
            ListPlayers();
        }

        public void ListPlayers()
        {
            //Listing players onto the teamPanel.
            foreach (Player p in _representedTeam.GetMembers())
            {
                //Spawning new player list slot and setting it as a child of playersHolder.
                GameObject playerSlot = (GameObject)Instantiate(playerListSlot, Vector3.zero, Quaternion.identity);
                playerSlot.GetComponent<RectTransform>().SetParent(playersHolder.transform, true);
                playerSlot.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                //Setting teams panel's properties.
                playerSlot.GetComponent<TeamPanelPlayerListSlot>().SetupPlayerListSlot(p);
            }
        }
        public void JoinRepresentedTeam()
        {
            //Player wants to join the team represented by this panel.
            //Sending message to the server.
            StringMessage teamNameMessage = new StringMessage();
            teamNameMessage.content = _representedTeam.teamName;

            NetworkManager.singleton.client.Send(MyMsgTypes.ClientMsgAddToTeam, teamNameMessage);
        }
    } 
}