﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace XTeam.Network.Managers.GameManager.Modules.Utilities
{
    public class TeamPanelPlayerListSlot : MonoBehaviour
    {

        //Represented Player.
        private Player _representedPlayer;

        //Gameobjects within PlayerListSlot.
        public GameObject playerNameText;
        public GameObject playerAvatarImage;

        public void SetupPlayerListSlot(Player representedPlayer)
        {
            //Setting te represented player.
            this._representedPlayer = representedPlayer;

            //Setting the playerSlot's properties.
            name = representedPlayer.playerName;
            playerNameText.GetComponent<Text>().text = representedPlayer.playerName;
            StartCoroutine(SetAvatar());
        }

        public IEnumerator SetAvatar()
        {
            //Refreshing maps list.
            WWW avatarWww = new WWW(_representedPlayer.playerAvatarUrl);
            yield return avatarWww;
            if (avatarWww.error != null)
            {
                //There was an error downloading the avatar file.
                Debug.LogError("There was an error downloading " + _representedPlayer.name + "'s avatar: " + avatarWww.error);

                //Trying again.
                StartCoroutine(SetAvatar());
                yield break;
            }
            else
            {
                //Enabling the icon element.
                StartCoroutine(GameUiManager.Smooth(0f, 1f, 0.5f, value => playerAvatarImage.GetComponent<CanvasGroup>().alpha = value));
                RawImage avatar = playerAvatarImage.GetComponent<RawImage>();

                //Successfully downloaded image file!
                //Setting it to the icon texture.
                avatar.texture = avatarWww.texture;
            }
        }
    }
}