﻿using UnityEngine;
using XTeam.Resources.GamePlay.GameManager.GameTeam.Team;
using XTeam.Resources.GamePlay.GameManager.GameUI.LobbyUIState.TeamsList.TeamPanel;

namespace XTeam.Network.Managers.GameManager.Modules.Utilities
{
    public class TeamsLister : MonoBehaviour
    {
        public GameObject teamsHolder;
        public Object teamPanel;

        public void ListTeams(Team[] teams)
        {
            Debug.Log("Listing teams...");
            foreach (Team t in teams)
            {
                //Spawning new team panel and setting it as a child of teamsHolder.
                GameObject newTeamPanel = (GameObject)Instantiate(teamPanel);
                newTeamPanel.GetComponent<RectTransform>().SetParent(teamsHolder.transform, false);

                //Setting teams panel's properties.
                newTeamPanel.GetComponent<TeamPanel>().SetupTeamPanel(t);
            }
        }
        public void ClearTeamsList()
        {
            //Clearing the teams list.
            foreach (Transform teamPanel in teamsHolder.transform)
            {
                Destroy(teamPanel.gameObject);
            }
        }
    } 
}
