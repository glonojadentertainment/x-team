﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.GameplayObjects.Entities.Living.Characters;
using XTeam.Resources.GamePlay.GameplayObjects.Items;
using XTeam.Resources.GamePlay.Network.LinkManager;
using Random = System.Random;

namespace XTeam.Resources.GamePlay.GameManager.GameSpawnManager
{
    public class GameSpawnManager : NetworkBehaviour
    {
        [SyncVar] public PlayerSpawnMode currentPlayerSpawnMode;

        //Dropped item spawning variables.
        public List<DroppedItemSpawnpoint> droppedItemsSpawnPoints;
        public Object playerCharacterPrefab;
        public List<PlayerSpawnPoint> playerSpawnPoints;

        //Player spawning variables.
        public float spawnWavesDelay = 5f;

        public void Initialize()
        {
            //Caching all the playerSpawnPoints.
            foreach (var p in FindObjectsOfType<PlayerSpawnPoint>())
            {
                playerSpawnPoints.Add(p);
            }

            //Caching all the droppedItemSpawnPoints.
            foreach (var p in FindObjectsOfType<DroppedItemSpawnpoint>())
            {
                droppedItemsSpawnPoints.Add(p);
            }

            //Setting the AutoPlayerSpawn countdown.
            var timer = gameObject.AddComponent<Timer>();
            timer.tickInterval = 1;
            timer.SetTime(spawnWavesDelay);
            timer.autoReset = true;
            timer.onTimerEnd += OnAutoPlayerSpawnTick;
            //Starting the countdown.
            timer.SetActive(true);
        }

        //Automatic spawning - Player mode.
        private void OnAutoPlayerSpawnTick()
        {
            if (isServer && currentPlayerSpawnMode == PlayerSpawnMode.SpawningPlayermanaged)
            {
                SpawnReadyPlayers();
            }
        }

        #region PlayerSpawning

        [Server]
        public void SpawnReadyPlayers()
        {
            //Making sure, that player spawning isn't currently blocked.
            if (currentPlayerSpawnMode != PlayerSpawnMode.SpawningBlocked)
            {
                //Debug.Log("Spawning player characters.");
                foreach (var p in LinkManager.GetPlayers())
                {
                    SpawnPlayer(p);
                }
            }
        }

        [Server]
        public void SpawnPlayer(Player p)
        {
            if (p.readyToSpawn && !CheckPlayerAlreadySpawned(p))
            {
                //Getting the spawnpoint.
                var spawnPointToSpawn = GetFreeSpawnPointForTeam(p.currentTeamName);

                Debug.Log("Spawning " + p.playerName + "'s character");

                //Getting spawn coordinates offset.
                var r = new Random();
                float offsetX = r.Next(-spawnPointToSpawn.spawnRadius, spawnPointToSpawn.spawnRadius);
                float offsetZ = r.Next(-spawnPointToSpawn.spawnRadius, spawnPointToSpawn.spawnRadius);

                //Spawning player character.
                var playerCharacter =
                    (GameObject)
                        Instantiate(playerCharacterPrefab,
                            new Vector3(spawnPointToSpawn.point.transform.position.x + offsetX,
                                spawnPointToSpawn.point.transform.position.y,
                                spawnPointToSpawn.point.transform.position.z + offsetZ), Quaternion.identity);
                playerCharacter.transform.SetParent(p.transform);
                playerCharacter.GetComponent<Character>().ownersGuid = p.playerGuid;
                p.readyToSpawn = false;

                NetworkServer.SpawnWithClientAuthority(playerCharacter, LinkManager.GetConnection(p.connectionId));

                //Enabling components on character for its owner.
                NetworkServer.SendToClient(p.connectionId, MyMsgTypes.ServerMsgEnableCharacterComponents,
                    new StringMessage());

                //Setting proper character avatar depending on player's team.
                var stringMessage = new StringMessage();
                stringMessage.content = p.GetTeam().teamAvatar;
                NetworkServer.SendToClient(p.connectionId, MyMsgTypes.ServerMsgSetCharacterAvatar, stringMessage);
            }
        }

        public PlayerSpawnPoint GetFreeSpawnPointForTeam(string teamName)
        {
            PlayerSpawnPoint selectedSpawnPoint = null;
            var playersOnSelectedSpawnPoint = 0;

            foreach (var s in playerSpawnPoints)
            {
                if (selectedSpawnPoint == null)
                {
                    selectedSpawnPoint = s;
                    playersOnSelectedSpawnPoint = CountPlayersOnSpawnPoint(s);
                }
                if (selectedSpawnPoint.teamName == teamName)
                {
                    //If we don't have any spawnpoint selected, selecting first found spawnPoint.
                    if (CountPlayersOnSpawnPoint(s) < playersOnSelectedSpawnPoint)
                    {
                        selectedSpawnPoint = s;
                    }
                }
            }

            return selectedSpawnPoint;
        }

        public void EnableCharacterComponents(NetworkMessage msg)
        {
            //Searching for a character with our GUID.
            foreach (var character in GameObject.FindGameObjectsWithTag("Character"))
            {
                var c = character.GetComponent<Character>();
                if (c.ownersGuid == LinkManager.clientGuid)
                {
                    //This is a character, that belongs to this player.
                    //Enabling required components.
                    c.EnableComponents();
                }
            }
        }

        public void SetCharacterAvatar(NetworkMessage msg)
        {
            var type = msg.reader.ReadMessage<StringMessage>().content;
            //Searching for a character with our GUID.
            foreach (var character in GameObject.FindGameObjectsWithTag("Character"))
            {
                var c = character.GetComponent<Character>();
                if (c.ownersGuid == LinkManager.clientGuid)
                {
                    //This is a character, that belongs to this player.
                    c.SetAvatar(type);
                }
            }
        }

        public int CountPlayersOnSpawnPoint(PlayerSpawnPoint s)
        {
            var playersOnSpawnPoint = 0;
            foreach (var o in GameObject.FindGameObjectsWithTag("Character"))
            {
                var distanceSqr = (s.transform.position - o.transform.position).sqrMagnitude;
                if (distanceSqr < s.spawnRadius)
                {
                    playersOnSpawnPoint++;
                }
            }

            return playersOnSpawnPoint;
        }

        public bool CheckPlayerAlreadySpawned(Player p)
        {
            //Checking whether player already has a character assigned.
            foreach (Transform t in p.transform)
            {
                if (t.gameObject.GetComponent<Character>() != null)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region DroppedItemsSpawning

        [Server]
        public void SpawnDroppedItems()
        {
            Debug.Log("Spawning dropped items.");
            foreach (var p in droppedItemsSpawnPoints)
            {
                SpawnDroppedItem(p);
            }
        }

        [Server]
        public void SpawnDroppedItem(DroppedItemSpawnpoint p)
        {
            //Spawning dropped item.
            var item = ItemManager.SpawnDroppedItem(p.itemName);

            //Setting item's position.
            item.gameObject.transform.position = new Vector3(p.transform.position.x, p.transform.position.y,
                p.transform.position.z);

            //Setting droppedItem's information.
            item.itemCustomName = p.customItemName;
            item.ownersGuid = "";

            NetworkServer.Spawn(item.gameObject);
        }

        #endregion
    }

    public enum PlayerSpawnMode
    {
        SpawningBlocked,
        SpawningPlayermanaged,
        SpawningServermanaged
    }
}