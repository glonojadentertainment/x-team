﻿using XTeam.Resources.GamePlay.Network.Client.Player;

namespace xTeam.API.Event.PlayerEvent
{
    public class PlayerLoginEvent : PlayerEvent {
        public PlayerLoginEvent(Player player, string connectionAddress, string joinMessage) : base(player)
        {
            this.connectionAddress = connectionAddress;
            this.joinMessage = joinMessage;
            result = Result.ALLOWED; //Allowing login by default.
        }
        public override string GetEventName()
        {
            return "Player Login Event";
        }
        /// <summary>
        /// Results of the login process.
        /// </summary>
        public enum Result
        {
            ALLOWED, KICK_BANNED, KICK_FULL, KICK_OTHER, KICK_WHITELIST
        }
        /// <summary>
        /// Address that the player used to connect to the server.
        /// </summary>
        protected string connectionAddress;
        /// <summary>
        /// The current result of the login.
        /// </summary>
        protected Result result;
        /// <summary>
        /// The kick message to display if getResult() != Result.ALLOWED
        /// </summary>
        protected string kickMessage;
        /// <summary>
        /// The join message to display if the player logged in successfully.
        /// </summary>
        protected string joinMessage;

        /// <summary>
        /// Allows the player to log in.
        /// </summary>
        public void AllowLogin()
        {
            SetResult(Result.ALLOWED);
        }
        /// <summary>
        /// Dissalows the player from logging in, with the given reason.
        /// </summary>
        /// <param name="result">Result for disallowing the player to log in.</param>
        /// <param name="reason">Reason for disallowing the player to log in.</param>
        public void DisallowLogin(Result result, string reason)
        {
            if (result == Result.ALLOWED) return;
            SetResult(result);
            SetKickMessage(reason);
        }
        /// <summary>
        /// Gets the connection address that the player used to connect to the server.
        /// </summary>
        /// <returns></returns>
        public string GetConnectionAddress()
        {
            return connectionAddress;
        }
        /// <summary>
        /// Gets the current join message that will be sent to all online players.
        /// </summary>
        /// <returns></returns>
        public string GetJoinMessage()
        {
            return joinMessage;
        }
        /// <summary>
        /// Gets the current kick message that will be used if getResult() != Result.ALLOWED.
        /// </summary>
        /// <returns></returns>
        public string GetKickMessage()
        {
            return kickMessage;
        }
        /// <summary>
        /// Gets the current result of the login.
        /// </summary>
        /// <returns></returns>
        public Result GetResult()
        {
            return result;
        }
        /// <summary>
        /// Sets the kick message to display if getResult() != Result.ALLOWED.
        /// </summary>
        /// <param name="message"></param>
        public void SetKickMessage(string message)
        {
            kickMessage = message;
        }
        /// <summary>
        /// Sets the join message to send to all online players.
        /// </summary>
        /// <param name="message"></param>
        public void SetJoinMessage(string message)
        {
            joinMessage = message;
        }
        /// <summary>
        /// Sets the new result of the login.
        /// </summary>
        /// <param name="result"></param>
        public void SetResult(Result result)
        {
            this.result = result;
        }
    }
}
