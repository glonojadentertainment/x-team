﻿namespace xTeam.API.Event
{
    /// <summary>
    /// Makes an event cancellable.
    /// </summary>
    public interface ICancellable
    {
        /// <summary>
        /// Sets the cancellation state of this event. 
        /// A cancelled event will not be executed.
        /// </summary>
        /// <param name="cancel"></param>
        void SetCancelled(bool cancel);

        /// <summary>
        /// Gets the cancellation state of this event. 
        /// A cancelled event will not be executed.
        /// </summary>
        /// <returns></returns>
        bool IsCanceled();
    }
}
