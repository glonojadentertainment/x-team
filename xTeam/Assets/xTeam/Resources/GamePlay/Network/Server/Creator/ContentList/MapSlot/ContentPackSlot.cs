﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using XTeam.Resources.GamePlay.ContentManager;

namespace XTeam.Resources.GamePlay.Network.Server.Creator.MapList.MapSlot
{
    /// <summary>
    /// Class for managing a single contentPack slot.
    /// </summary>
    public class ContentPackSlot : MonoBehaviour {
        /// <summary>
        /// Map selection button.
        /// </summary>
        public Button mapButton;
        /// <summary>
        /// GameObject of the contentPack icon.
        /// </summary>
        public GameObject iconGo;
        /// <summary>
        /// Map represented by this ContentPackSlot.
        /// </summary>
        public ContentPack contentPack;
        /// <summary>
        /// Type of this ContentPackSlot.
        /// </summary>
        public ContentSlotType type;

        /// <summary>
        /// Whether content in this ContentPackSlot should be enabled.
        /// </summary>
        public bool contentEnabled = false;

        /// <summary>
        /// Called when the contentPack slot is selected.
        /// </summary>
        public void Select() {
            switch (type)
            {
                case ContentSlotType.LOCAL_CONTENT:
                    SwitchEnabled();
                    break;
                case ContentSlotType.ONLINE_CONTENT:
                    //StartCoroutine(LevelDownloader.DownloadMap(contentPack.name, contentPack.downloadUrl, null));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SwitchEnabled()
        {
            contentEnabled = !contentEnabled;

            if (contentEnabled)
            {
                Server.instance.enabledContentPacks.Add(contentPack);
            }
            else
            {
                Server.instance.enabledContentPacks.Remove(contentPack);
            }
        }

        /// <summary>
        /// Sets up the contentPack slot to server given contentPack data.
        /// </summary>
        /// <param name="representedContentPack">A contentPack represented by this contentPack slot.</param>
        /// <param name="type">A type of this contentPack slot.</param>
        public void Setup(ContentPack representedContentPack, ContentSlotType type)
        {
            //Setting a contentPack object to contentPack slot.
            contentPack = representedContentPack;
            //Setting contentPack slot type.
            this.type = type;

            //Setting the properties of contentPack slot.
            //Name
            transform.FindChild("Name").GetComponent<Text>().text = representedContentPack.info.name;
            //Type
            //transform.FindChild("Type").GetComponent<Text>().text = representedContentPack.type;
            //Version
            transform.FindChild("Version").GetComponent<Text>().text = "v" + representedContentPack.info.version;

            switch (type)
            {
                case ContentSlotType.ONLINE_CONTENT:
                    //Checking whether the contentPack is already downloaded.
                    ContentBrowser.IsContentDownloadedAsync(representedContentPack.info.name, representedContentPack.info.version, SetMapDownloaded);
                    break;
            }
        }

        /// <summary>
        /// Sets whether this contentPack slot should be active.
        /// </summary>
        /// <param name="active">Whether this contentPack slot should be active.</param>
        private void SetMapDownloaded(bool downloaded)
        {
            mapButton.interactable = !downloaded;
        }

        private IEnumerator SetIcon(string url)
        {
            Debug.Log("Downloading icon from: " + url);
            //Downloading contentPack icon.
            WWW iconWww = new WWW(url);
            yield return iconWww;
            if (iconWww.error != null)
            {
                //There was an error downloading the contentPack list file.
                Debug.LogError("There was an error downloading the contentPack list file. " + iconWww.error);
                yield break;
            }
            else
            {
                //Enabling the icon element.
                iconGo.SetActive(true);
                var icon = iconGo.GetComponent<RawImage>();
                //Successfully downloaded image file!
                Debug.Log("Downloaded the icon!");
                //Setting it to the icon texture.
                icon.texture = iconWww.texture;
            }
        }

        private void SetIcon(Texture2D icon)
        {
            Debug.Log("Setting icon...");
            //Enabling the icon element.
            iconGo.SetActive(true);
            RawImage iconComponent = iconGo.GetComponent<RawImage>();
            //Setting it to the icon texture.
            iconComponent.texture = icon;
        }
    }

    public enum ContentSlotType
    {
        LOCAL_CONTENT,
        ONLINE_CONTENT
    }
}