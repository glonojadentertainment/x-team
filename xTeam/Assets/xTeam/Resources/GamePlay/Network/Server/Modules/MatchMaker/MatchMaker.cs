﻿using System;
using System.Collections;
using GameJolt.API;
using UnityEngine;
using UnityEngine.Networking;

namespace XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker
{
    /// <summary>
    /// Class utilizing the MatchMaking service.
    /// </summary>
    internal static class MatchMaker
    {
        /// <summary>
        ///     Registers a new ServerInfo into the MatchMaker.
        /// </summary>
        /// <param name="info">ServerInfo of server to register.</param>
        /// <param name="callback">Called when the server has been registered.</param>
        internal static void RegisterServer(ServerInfo info, Action<MatchRegisterResponse> callback)
        {
            Debug.Log("Registering server: " + info.name + " to the MatchMaker.");
            DataStore.Set("_MATCHMAKING-" + info.name, SerializeServerInfo(info), true,
                (onMatchRegistered) =>
                {
                    if (onMatchRegistered)
                    {
                        //Match registration was successful.
                        Debug.Log("Successfully registered server: " + info.name);
                        callback(new MatchRegisterResponse
                        {
                            server = info,
                            success = true
                        });
                    }
                    else
                    {
                        //Match registration failed.
                        Debug.Log("Couldn't register server: " + info.name + " to the MatchMaker.");
                        callback(new MatchRegisterResponse
                        {
                            server = info,
                            success = false
                        });
                    }
                });
        }
        /// <summary>
        ///     Unregisters a specified server from the MatchMaker.
        /// </summary>
        /// <param name="info">ServerInfo of a server to unregister.</param>
        /// <param name="callback">Called when the server is unregistered.</param>
        internal static void UnregisterServer(ServerInfo info, Action<MatchUnregisterResponse> callback)
        {
            Debug.Log("Unregistering server: " + info.name + " from the MatchMaker.");
            DataStore.Delete("_MATCHMAKING-" + info.name, true,
                (onMatchUnregistered) =>
                {
                    if (onMatchUnregistered)
                    {
                        //Match registration was successful.
                        Debug.Log("Successfully unregistered server: " + info.name);
                        callback(new MatchUnregisterResponse
                        {
                            server = info,
                            success = true
                        });
                    }
                    else
                    {
                        //Match registration failed.
                        Debug.Log("Couldn't unregister server: " + info.name + " to the MatchMaker.");
                        callback(new MatchUnregisterResponse
                        {
                            server = info,
                            success = false
                        });
                    }
                });
        }
        /// <summary>
        /// Updates current server status with the provided ServerInfo.
        /// </summary>
        /// <param name="info">ServerInfo of a server to update.</param>
        /// <param name="callback">Called when the server is updated.</param>
        internal static void UpdateServerStatus(ServerInfo info, Action<MatchUpdateResponse> callback)
        {
            DataStore.Set("_MATCHMAKING-" + info.name, SerializeServerInfo(info), true,
                (onMatchUpdated) =>
                {
                    if (onMatchUpdated)
                    {
                        //Match registration was successful.
                        Debug.Log("Successfully updated server: " + info.name);
                        callback(new MatchUpdateResponse
                        {
                            server = info,
                            success = true
                        });
                    }
                    else
                    {
                        //Match registration failed.
                        Debug.Log("Couldn't update server: " + info.name + " on the MatchMaker.");
                        callback(new MatchUpdateResponse
                        {
                            server = info,
                            success = false
                        });
                    }
                });
        }
        /// <summary>
        /// Gets server list from the MasterServer.
        /// </summary>
        /// <param name="callback">Called when a new ServerInfo is fetched.</param>
        internal static void ListServers(Action<MatchGetResponse> callback)
        {
            //Requesting all the global keys.
            DataStore.GetKeys(true, results =>
            {
                foreach (var result in results)
                {
                    //Checking whether this item is a MatchMaking record.
                    if (result.StartsWith("_MATCHMAKING-"))
                    {
                        DataStore.Get(result, true, s =>
                        {
                            callback(new MatchGetResponse
                            {
                                server = DeserializeServerInfo(s),
                                serverNetworkType = ServerNetworkType.PUBLIC,
                                success = true
                            });
                        });
                    }
                }
            });
        }

        #region ServerInfoManagement
        /// <summary>
        ///     Deserializes a ServerInfo from the specified string.
        /// </summary>
        /// <param name="serverInfoString">String to deserialize ServerInfo from.</param>
        /// <returns></returns>
        public static ServerInfo DeserializeServerInfo(string serverInfoString)
        {
            return JsonUtility.FromJson<ServerInfo>(serverInfoString);
        }

        /// <summary>
        ///     Serializes a ServerInfo object into string.
        /// </summary>
        /// <param name="serverInfo">ServerInfo to serialized.</param>
        /// <returns></returns>
        public static string SerializeServerInfo(ServerInfo serverInfo)
        {
            return JsonUtility.ToJson(serverInfo);
        }
        /// <summary>
        /// Creates ServerInfo object based on currently running server data.
        /// </summary>
        /// <returns>Created ServerInfo</returns>
        public static ServerInfo CreateCurrentServerInfo()
        {
            return new ServerInfo
            {
                publicAddress = IPUtility.GetPublicIp().ToString(),
                localAddress = IPUtility.GetLocalIp().ToString(),
                port = Server.properties.serverPort,
                description = Server.properties.serverDescription,
                name = Server.properties.serverName,
                playerCount = NetworkServer.connections.Count,
                size = Server.properties.serverSize
            };
        }
        #endregion
    }
    /// <summary>
    /// Used for updating current ServerInfo on the local network and on the MasterServer.
    /// </summary>
    public static class MatchUpdater
    {
        /// <summary>
        /// Whether the updating process should _stop.
        /// </summary>
        private static bool _stop = false;

        /// <summary>
        /// Starts MatchMaker updating of the currently running server.
        /// </summary>
        internal static void StartUpdating(bool updateMatchMaker, bool updateLan)
        {
            //Cancelling the stop flag.
            _stop = false;
            //Starting the Coroutines.
            if (updateMatchMaker)
            {
                MatchMaker.RegisterServer(MatchMaker.CreateCurrentServerInfo(), response =>
                {
                    if (response.success)
                    {
                        Server.instance.StartCoroutine(MatchMakerUpdateLoop());
                    }
                });
            }
            if (updateLan)
            {
                Server.instance.StartCoroutine(LanUpdateLoop());
            }
        }
        /// <summary>
        /// Stops updating of the server.
        /// </summary>
        internal static void StopUpdating()
        {
            //Setting the stop flag.
            _stop = true;
        }

        #region UpdateLoops
        /// <summary>
        /// Updates the MatchMaker info of the server every 5 seconds.
        /// </summary>
        /// <returns></returns>
        private static IEnumerator MatchMakerUpdateLoop()
        {
            while (!_stop)
            {
                MatchMaker.UpdateServerStatus(MatchMaker.CreateCurrentServerInfo(), response => { });
                yield return new WaitForSeconds(Server.instance.networkDiscovery.broadcastInterval);
            }
            MatchMaker.UnregisterServer(MatchMaker.CreateCurrentServerInfo(), response => {});
        }
        /// <summary>
        /// Starts broadcasting server info to the local network.
        /// </summary>
        private static IEnumerator LanUpdateLoop()
        {
            //Stops any currently running networkDiscovery broadcasts.
            if (Server.instance.networkDiscovery.running)
            {
                Server.instance.networkDiscovery.StopBroadcast();
            }
            Server.instance.networkDiscovery.Initialize();
            Server.instance.networkDiscovery.broadcastData = MatchMaker.SerializeServerInfo(MatchMaker.CreateCurrentServerInfo());
            Server.instance.networkDiscovery.StartAsServer();
            
            while (!_stop)
            {
                Server.instance.networkDiscovery.broadcastData = MatchMaker.SerializeServerInfo(MatchMaker.CreateCurrentServerInfo());
                yield return new WaitForSeconds(Server.instance.networkDiscovery.broadcastInterval);
            }

            //Stopping the networkDiscovery after update stopped.
            Server.instance.networkDiscovery.StopBroadcast();
        }
        #endregion
    }

    /// <summary>
    ///     Class holding information about the server.
    /// </summary>
    [Serializable]
    public class ServerInfo
    {
        public string name;
        public string description;
        public string publicAddress;
        public string localAddress;
        public int port;

        public int playerCount;
        public int size;
    }

    /// <summary>
    /// Base class for handling the MatchResponses.
    /// </summary>
    public class MatchResponse
    {
        public ServerInfo server;
        public ServerNetworkType serverNetworkType;
        public bool success;
    }

    /// <summary>
    /// Class containing information about ServerInfo registeration result.
    /// </summary>
    public class MatchRegisterResponse : MatchResponse
    {
        
    }
    /// <summary>
    /// Class containing information about ServerInfo unregisteration result.
    /// </summary>
    public class MatchUnregisterResponse : MatchResponse
    {
        
    }
    /// <summary>
    /// Class containing information about ServerInfo update result.
    /// </summary>
    public class MatchUpdateResponse : MatchResponse
    {

    }
    /// <summary>
    /// Class containing inforation about ServerInfo list result.
    /// </summary>
    public class MatchGetResponse : MatchResponse
    {

    }

    /// <summary>
    /// Domain numbers for MatchMaking.
    /// </summary>
    public enum MatchMakingDomain
    {
        OFFICIAL = 0,
        COMMUNITY = 1,
        HOSTED = 2
    }
    /// <summary>
    /// Type of the server slot.
    /// </summary>
    public enum ServerNetworkType
    {
        LAN, PUBLIC
    }
}