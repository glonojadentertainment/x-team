﻿using UnityEngine;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Characters.StandardCharacter.Scripts
{
    /// <summary>
    /// A straightforward C# conversion of the JavaScript smartCrosshair (tentatively named)
    /// Works similarly to original
    /// </summary>
    public class SmartCrosshair : MonoBehaviour
    {
        #region Fields
        public bool drawCrosshair = true;
        public Color crosshairColor = Color.white;
        public float width = 1;
        public float height = 3;

        [System.Serializable]
        public class Spreading
        {
            public float sSpread = 20;
            public float maxSpread = 60;
            public float minSpread = 20;
            public float spreadPerSecond = 30;
            public float decreasePerSecond = 25;
        }

        public Spreading spread = new Spreading();

        Texture2D _tex;
        float _newHeight;
        GUIStyle _lineStyle;

        #endregion

        #region Functions
        void Awake()
        {
            _tex = new Texture2D(1, 1);
            _lineStyle = new GUIStyle();
            _lineStyle.normal.background = _tex;
        }

        void OnGui()
        {
            Vector2 centerPoint = new Vector2(Screen.width / 2, Screen.height / 2);
            float screenRatio = Screen.height / 100;

            _newHeight = height * screenRatio;

            if (drawCrosshair)
            {
                GUI.Box(new Rect(centerPoint.x - (width / 2), centerPoint.y - (_newHeight + spread.sSpread), width, _newHeight), GUIContent.none, _lineStyle);
                GUI.Box(new Rect(centerPoint.x - (width / 2), (centerPoint.y + spread.sSpread), width, _newHeight), GUIContent.none, _lineStyle);
                GUI.Box(new Rect((centerPoint.x + spread.sSpread), (centerPoint.y - (width / 2)), _newHeight, width), GUIContent.none, _lineStyle);
                GUI.Box(new Rect(centerPoint.x - (_newHeight + spread.sSpread), (centerPoint.y - (width / 2)), _newHeight, width), GUIContent.none, _lineStyle);
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                spread.sSpread += spread.spreadPerSecond * Time.deltaTime;
                Fire();
            }

            spread.sSpread -= spread.decreasePerSecond * Time.deltaTime;
            spread.sSpread = Mathf.Clamp(spread.sSpread, spread.minSpread, spread.maxSpread);
        }

        void Fire() { }

        void SetColor(Texture2D myTexture, Color myColor)
        {
            for (int y = 0; y < myTexture.height; y++)
            {
                for (int x = 0; x < myTexture.width; x++)
                    myTexture.SetPixel(x, y, myColor);
                myTexture.Apply();
            }
        }
        #endregion
    }
}