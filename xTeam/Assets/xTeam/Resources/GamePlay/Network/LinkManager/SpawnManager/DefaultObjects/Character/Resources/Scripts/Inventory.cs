﻿using UnityEngine;
using UnityEngine.Networking;
using XTeam.API.Objects.Items;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Characters.StandardCharacter.Scripts
{
    /// <summary>
    /// Class synchronizing inventory information of living enities.
    /// </summary>
    public class Inventory : NetworkBehaviour {
        [Tooltip("List of items in this inventory.")]
        public SyncListItemInfo itemsInInventory = new SyncListItemInfo();
	
        /// <summary>
        /// Adds specified item to the inventory.
        /// </summary>
        /// <param name="info">Item to be added to the inventory.</param>
        [Command]
        public void CmdAddItemToInventory(ItemInfo info)
        {
            itemsInInventory.Add(info);
        }
        /// <summary>
        /// Removes specified item from the inventory.
        /// </summary>
        /// <param name="info">Item to be removed fromt the inventory.</param>
        [Command]
        public void CmdRemoveItemFromInventory(ItemInfo info)
        {
            itemsInInventory.Remove(info);
        }
    }
    /// <summary>
    /// List class for ItemInfo.
    /// </summary>
    public class SyncListItemInfo : SyncListStruct<ItemInfo> { };
}