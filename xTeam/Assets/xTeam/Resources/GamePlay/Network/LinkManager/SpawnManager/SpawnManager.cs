﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using xTeam.Resources.Utilities.FileManager.XLoad;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.Utilities.FileManager;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager
{
    public class SpawnManager : NetworkBehaviour
    {
        /// <summary>
        /// Networked list of currently registered spawn prefabs.
        /// </summary>
        [SerializeField]
        private SyncListRegisteredSpawnPrefab syncListRegisteredSpawnPrefab = new SyncListRegisteredSpawnPrefab();
        /// <summary>
        /// Local list of currently registered spawn prefabs.
        /// </summary>
        [SerializeField]
        public List<RegisteredSpawnPrefabInfo> registeredSpawnPrefabs = new List<RegisteredSpawnPrefabInfo>(); 
        /// <summary>
        /// Instance of the SpawnManager.
        /// </summary>
        internal static SpawnManager instance;

        /// <summary>
        /// Spawns object from a specified content relativePath.
        /// Will register object to the network if needed.
        /// </summary>
        /// <param name="path">Path to the spawned object.</param>
        /// <param name="position">Position on which to spawn an object.</param>
        /// <param name="rotation">Rotation of the spawned object.</param>
        /// <param name="callback">A callback.</param>
        [Server]
        public static void Spawn(string path, Vector3 position, Quaternion rotation, Action<GameObject> callback)
        {
            //Spawns specified object locally.
            var spawnedObject = SpawnLocally(path, position, rotation);
            var hash128 = NetworkHash128.Parse(spawnedObject.GetHashCode().ToString());
            //Checking for NetworkIdentity on the object.
            if (spawnedObject.GetComponents<NetworkIdentity>() == null)
            {
                spawnedObject.AddComponent<NetworkIdentity>();
            }

            //Registering the object.
            RegisterObject(path, hash128);

            //Registers spawned object within the UNET spawning system.
            NetworkServer.Spawn(spawnedObject, hash128);

            //Calling the callback.
            if (callback.Method.GetParameters()[0].IsDefined(typeof (GameObject), false))
            {
                callback(spawnedObject);
            }
        }

        /// <summary>
        /// Spawns a default object.
        /// Will register object to the network if needed.
        /// </summary>
        /// <param name="defaultObjectType">Default object type to spawn.</param>
        /// <param name="position">Position on which to spawn an object.</param>
        /// <param name="rotation">Rotation of the spawned object.</param>
        /// <param name="callback">A callback.</param>
        [Server]
        public static void Spawn(global::xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.DefaultObjects.DefaultObjectType defaultObjectType, Vector3 position, Quaternion rotation, Action<GameObject> callback)
        {
            //Spawns specified object locally.
            GameObject spawnedObject = Instantiate(global::xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.DefaultObjects.GetDefaultObject(defaultObjectType), position, rotation) as GameObject;
            
            //Checking for NetworkIdentity on the object.
            if (spawnedObject.GetComponents<NetworkIdentity>() == null)
            {
                spawnedObject.AddComponent<NetworkIdentity>();
            }

            //Registers spawned object within the UNET spawning system.
            NetworkServer.Spawn(spawnedObject);

            //Calling the callback.
            if (callback.Method.GetParameters()[0].IsDefined(typeof(GameObject), false))
            {
                callback(spawnedObject);
            }
        }

        /// <summary>
        /// Spawns object from a specified content path locally.
        /// This object will not be visible to any other players on the server.
        /// </summary>
        /// <param name="path">Path of the object to spawn.</param>
        public static GameObject SpawnLocally(string path, Vector3 position, Quaternion rotation)
        {
            Debug.Log("Spawning object from path: " + path + " locally...");

            //Loading object from file.
            var spawnedObject = new GameObject(new FileInfo(path).Name);
            Load.GameObjectFromFile(path, position, rotation, o =>
            {
                spawnedObject = o;
            });

            return spawnedObject;
        }

        /// <summary>
        /// Registers an object into the spawning system.
        /// </summary>
        /// <param name="path">Path of the object in the content folder.</param>
        /// <param name="hash128">NetworkHash128 of this Object</param>
        [Server]
        private static void RegisterObject(string path, NetworkHash128 hash128)
        {
            //Getting the relative relativePath.
            var relativePath = path.Remove(0, FileManager.GetApplicationContentPath().Length);

            //Checking whether the object is already registered.
            if (instance.syncListRegisteredSpawnPrefab.Any(registeredPrefab => registeredPrefab.relativePath == relativePath)) {
                return;
            }

            //Adds object to the syncListRegisteredSpawnPrefab.
            instance.syncListRegisteredSpawnPrefab.Add(new RegisteredSpawnPrefab
            {
                networkHash128 = hash128,
                relativePath = relativePath
            });
        }
        /// <summary>
        /// Called when a new GameObject is spawned on the server.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        internal static GameObject SpawnHandler(Vector3 position, NetworkHash128 assetId)
        {
            //Spawning the object locally.
            RegisteredSpawnPrefab registeredSpawnPrefab = new RegisteredSpawnPrefab();
            registeredSpawnPrefab.relativePath = "UNKNOWN";

            foreach (var networkRegisteredPrefab in instance.syncListRegisteredSpawnPrefab.Where(networkRegisteredPrefab => networkRegisteredPrefab.networkHash128.ToString() == assetId.ToString())) {
                registeredSpawnPrefab = networkRegisteredPrefab;
            }

            if (registeredSpawnPrefab.relativePath == "UNKNOWN")
            {
                foreach (var locallyRegisteredPrefab in instance.registeredSpawnPrefabs)
                {
                    if (locallyRegisteredPrefab.networkHash128.ToString() == assetId.ToString())
                    {
                        registeredSpawnPrefab = new RegisteredSpawnPrefab
                        {
                            networkHash128 = locallyRegisteredPrefab.networkHash128,
                            relativePath = locallyRegisteredPrefab.relativePath
                        };
                    }
                }
            }

            return SpawnLocally(FileManager.GetApplicationContentPath() + registeredSpawnPrefab.relativePath, position, Quaternion.identity);
        }

        /// <summary>
        /// Called when a GameObject is despawed from the server.
        /// </summary>
        /// <param name="spawned"></param>
        internal static void UnSpawnHandler(GameObject spawned)
        {
            //Destroying the unspawned object.
            Destroy(spawned);
        }
        /// <summary>
        /// Registers a spawn handler for a single object.
        /// </summary>
        internal static void RegisterClientSpawnHandler(NetworkMessage msg)
        {
            RegisteredSpawnPrefabInfo registeredSpawnPrefabInfo =
                msg.ReadMessage<RegisteredSpawnPrefabInfo>();

            //Adding to the local registered spawning object list.
            instance.registeredSpawnPrefabs.Add(registeredSpawnPrefabInfo);
            //Registering spawn handlers.
            ClientScene.RegisterSpawnHandler(registeredSpawnPrefabInfo.networkHash128, SpawnHandler, UnSpawnHandler);
        }
        /// <summary>
        /// Registers client spawn handlers.
        /// </summary>
        internal static void RegisterObjectListCallbacks()
        {
            //Registering the syncListRegisteredSpawnPrefab callback.
            instance.syncListRegisteredSpawnPrefab.Callback += (op, index) =>
            {
                if (op == SyncList<RegisteredSpawnPrefab>.Operation.OP_ADD)
                {
                    ClientScene.RegisterSpawnHandler(instance.syncListRegisteredSpawnPrefab.GetItem(index).networkHash128, SpawnHandler, UnSpawnHandler);
                }
                else if (op == SyncList<RegisteredSpawnPrefab>.Operation.OP_REMOVE || op == SyncList<RegisteredSpawnPrefab>.Operation.OP_REMOVEAT)
                {
                    ClientScene.UnregisterSpawnHandler(instance.syncListRegisteredSpawnPrefab.GetItem(index).networkHash128);
                }
            };
        }
        /// <summary>
        /// Sends all registered spawned objects to the client.
        /// </summary>
        /// <param name="playerConnection"></param>
        [Server]
        public static void SendSpawnedObjects(NetworkConnection playerConnection)
        {
            foreach (var registeredObject in instance.syncListRegisteredSpawnPrefab)
            {
                RegisteredSpawnPrefabInfo info = new RegisteredSpawnPrefabInfo
                {
                    networkHash128 = registeredObject.networkHash128,
                    relativePath = registeredObject.relativePath
                };

                playerConnection.Send(XMsgTypes.ServerMsgSpawnedObj, info);
            }
        }
        #region ClientSetup
        /// <summary>
        /// Setting the instance.
        /// </summary>
        private void Awake()
        {
            instance = this;
        }

        public override void OnStartClient()
        {
            RegisterObjectListCallbacks();
        }
        /// <summary>
        /// Registers client handlers.
        /// </summary>
        public static void RegisterClientHandlers()
        {
            XTeam.Resources.GamePlay.Network.Shared.Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgSpawnedObj, RegisterClientSpawnHandler);
        }
        #endregion
    }

    public struct RegisteredSpawnPrefab
    {
        public NetworkHash128 networkHash128;
        public string relativePath;
    }
    public class RegisteredSpawnPrefabInfo :MessageBase
    {
        public NetworkHash128 networkHash128;
        public string relativePath;
    }

    public class SyncListRegisteredSpawnPrefab : SyncListStruct<RegisteredSpawnPrefab>
    {
    }
}