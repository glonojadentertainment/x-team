﻿using UnityEngine;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts.AvatarManager
{
    /// <summary>
    /// Class holding the info about a player avatar.
    /// </summary>
    public class Avatar : MonoBehaviour
    {
        [Tooltip ("Animator of the avatar.")]
        public Animator animator;
	
        /// <summary>
        /// Updates avatar's animation values based on the AvatarAnimationValues.
        /// </summary>
        public void UpdateAnimationValues (AvatarAnimationValues avatarAnimationValues)
        {
            if (animator != null)
            {
                //Setting Horizontal.
                animator.SetFloat("Horizontal", avatarAnimationValues.horizontal);
                //Setting Vertical.
                animator.SetFloat("Vertical", avatarAnimationValues.vertical);
                //Setting State.
                animator.SetInteger("State", (int) avatarAnimationValues.playerPosition);
                //Setting Jump.
                animator.SetBool("IsJumping", avatarAnimationValues.isJumping);
                //Setting IsSprinting.
                animator.SetBool("IsSprinting", avatarAnimationValues.isSprinting);
                //Setting InAir.
                animator.SetBool("InAir", avatarAnimationValues.inAir);
            }
        }
    }
    /// <summary>
    /// Holds information about all the required avatar animation values.
    /// </summary>
    public class AvatarAnimationValues
    {
        /// <summary>
        /// The horizontal value of the avatar animation.
        /// </summary>
        public float horizontal;
        /// <summary>
        /// The vertical value of the avatar animation.
        /// </summary>
        public float vertical;
        /// <summary>
        /// PlayerInfo position of the avatar.
        /// </summary>
        public PlayerPosition playerPosition;
        /// <summary>
        /// Whether avatar is jumping.
        /// </summary>
        public bool isJumping;
        /// <summary>
        /// Whether avatar is sprinting.
        /// </summary>
        public bool isSprinting;
        /// <summary>
        /// Whether avatar is in air.
        /// </summary>
        public bool inAir;
    }
}