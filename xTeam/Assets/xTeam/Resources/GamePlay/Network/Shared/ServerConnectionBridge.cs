﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.Client.Managers;
using XTeam.Resources.GamePlay.Network.Client.Player;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.GamePlay.Network.Server;
using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;
using XTeam.Resources.Utilities.Threading;

namespace XTeam.Resources.GamePlay.Network.Shared
{
    /// <summary>
    ///     Class for receiving ServerInfo from server when connecting.
    ///     Only useable on the client.
    /// </summary>
    internal static class Client
    {
        internal static ActionThread connThr;

        private static Stream _strLocal;

        private static NetworkStream _strRemote;

        private static TcpListener _tcpServer;

        /// <summary>
        /// UNET client used by the Client.
        /// </summary>
        public static NetworkClient unetClient;

        /// <summary>
        /// PlayerInfo of this client.
        /// </summary>
        public static Player player;
        /// <summary>
        /// GUID of this client.
        /// </summary>
        public static string guid;

        /// <summary>
        /// Initiates the server connection process.
        /// </summary>
        internal static void Connect(string address, int port)
        {
            connThr = UnityThreadHelper.CreateThread(() =>
            {
                //Parsing the IPAddress.
                IPAddress ipAddress;

                if (!IPAddress.TryParse(address, out ipAddress) && address != "localhost")
                {
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.LogError("Invalid IP address!");
                    });
                    return;
                }

                // Connecting
                var client = new TcpClient();
                try
                {
                    //Checking whether the send ip is our ip in order to send to local host.
                    if (address == "localhost" || address == IPUtility.GetPublicIp().ToString() || address == IPUtility.GetLocalIp().ToString())
                    {
                        UnityThreadHelper.Dispatcher.Dispatch(() =>
                        {
                            Debug.Log("Connecting to the local host:" + (port + 2));
                        });
                        client.Connect("localhost", port + 2);
                    }
                    else
                    {
                        UnityThreadHelper.Dispatcher.Dispatch(() =>
                        {
                            Debug.Log("Connecting to " + address + ":" + (port + 2));
                        });
                        client.Connect(address, port + 2);
                    }
                }
                catch (Exception e)
                {
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.LogError("Error connecting to the server: " + e);
                    });
                    return;
                }
                var ns = client.GetStream();

                //Checking whether the server accepted the connection.
                if (ns.ReadByte() == 1)
                {
                    //Reading the ServerInfo from the NetworkStream.
                    string serverInfoString;
                    {
                        var serverInfoLenghtBytes = new byte[4]; //int32
                        ns.Read(serverInfoLenghtBytes, 0, 4); // int32
                        var serverInfoNameBytes = new byte[BitConverter.ToInt32(serverInfoLenghtBytes, 0)];
                        ns.Read(serverInfoNameBytes, 0, serverInfoNameBytes.Length);

                        serverInfoString = Encoding.ASCII.GetString(serverInfoNameBytes);
                    }
                    var serverInfo = MatchMaker.DeserializeServerInfo(serverInfoString);
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.Log("Received ServerInfo, getting the ServerProperties...");
                    });

                    //Reading the ServerProperties from the NetworkStream.
                    string serverPropertiesString;
                    {
                        var serverPropertiesLenghtBytes = new byte[4]; //int32
                        ns.Read(serverPropertiesLenghtBytes, 0, 4); // int32
                        var serverPropertiesNameBytes = new byte[BitConverter.ToInt32(serverPropertiesLenghtBytes, 0)];
                        ns.Read(serverPropertiesNameBytes, 0, serverPropertiesNameBytes.Length);

                        serverPropertiesString = Encoding.ASCII.GetString(serverPropertiesNameBytes);
                    }
                    Debug.Log(serverPropertiesString);
                    Server.Server.properties = JsonUtility.FromJson<ServerProperties>(serverPropertiesString);
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.Log("Received ServerProperties, getting the ConnectionConfig...");
                    });


                    //Reading the ConnectionConfig from the NetworkStream.
                    string connectionConfigString;
                    {
                        var connectionConfigLenghtBytes = new byte[4]; //int32
                        ns.Read(connectionConfigLenghtBytes, 0, 4); // int32
                        var connectionConfigNameBytes = new byte[BitConverter.ToInt32(connectionConfigLenghtBytes, 0)];
                        ns.Read(connectionConfigNameBytes, 0, connectionConfigNameBytes.Length);

                        connectionConfigString = Encoding.ASCII.GetString(connectionConfigNameBytes);
                    }
                    var connectionConfig = JsonUtility.FromJson<ConnectionConfig>(connectionConfigString);
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.Log("Received server ConnectionConfig, initializing UNET connection...");
                    });

                    //Applying the ConnectionConfig of the server.
                    ConfigureUNETClient(serverInfo, connectionConfig);

                    //Checking public IP adresses.
                    if (serverInfo.publicAddress == IPUtility.GetPublicIp().ToString())
                    {
                        //Server is hosted on localhost or other device in local network.
                        //Checking local IPs.
                        if (serverInfo.localAddress == IPUtility.GetLocalIp().ToString())
                        {
                            UnityThreadHelper.Dispatcher.Dispatch(() =>
                            {
                                //Connecting to the server with UNET.
                                Debug.Log("UNET connecting to server: " + serverInfo.name + " - " + "localhost" + ":" + port);
                                unetClient.Connect("localhost", port);
                            });
                        }
                    }
                    else
                    {
                        UnityThreadHelper.Dispatcher.Dispatch(() =>
                        {
                            //Connecting to the server with UNET.
                            Debug.Log("UNET connecting to server: " + serverInfo.name + " - " + address + ":" + port);
                            unetClient.Connect(address, port);
                        });
                    }
                }
                else
                {
                    Debug.LogError("Server didn't accept the connection...");
                }

                client.Close();
            }, true);
        }
        /// <summary>
        /// Sets up a new NetworkClient.
        /// </summary>
        internal static void ConfigureUNETClient(ServerInfo serverInfo, ConnectionConfig connectionConfig)
        {
            //Spawning new NetworkClient object.
            if (unetClient == null)
            {
                unetClient = new NetworkClient();
            }
            //Configuring NetworkClient connection config.
            unetClient.Configure(connectionConfig, serverInfo.size);

            //Registering client handlers.
            RegisterClientHandlers(unetClient);
            NetworkContentManager.RegisterClientHandlers();
            SpawnManager.RegisterClientHandlers();
            //Registering spawn prefabs.
            UnityThreadHelper.Dispatcher.Dispatch(RegisterSpawnPrefabs);
        }
        /// <summary>
        /// Registers basic client handlers essential for a successfull connection.
        /// </summary>
        /// <param name="client"></param>
        private static void RegisterClientHandlers(NetworkClient client)
        {
            Debug.Log("Registering default clientg prefabs...");
            client.RegisterHandler(MsgType.Connect, CallbackManager.OnClientConnect);
            client.RegisterHandler(MsgType.Disconnect, CallbackManager.OnClientDisconnect);
            client.RegisterHandler(MsgType.Error, CallbackManager.OnErrorConnecting);
            client.RegisterHandler(XMsgTypes.ServerMsgKick, CallbackManager.OnClientKick);
            client.RegisterHandler(XMsgTypes.ServerMsgGuid, CallbackManager.OnGuidInfoGetFromServer);
        }
        /// <summary>
        /// Registers basic spawn prefabs.
        /// </summary>
        private static void RegisterSpawnPrefabs()
        {
            foreach (var prefab in NetworkManager.singleton.spawnPrefabs)
            {
                ClientScene.RegisterPrefab(prefab);
            }
        }
        /// <summary>
        /// Disconnects from the server.
        /// </summary>
        public static void Disconnect()
        {
            //Stopping the BRIDGE client if running.
            connThr.Exit();
            //Stopping the UNET client.
            unetClient.Disconnect();
            unetClient.Shutdown();
        }
    }

    /// <summary>
    ///     Class for sending current ServerInfo to the connecting client.
    ///     Runs on the server.
    /// </summary>
    internal static class ServerInfoSender
    {
        internal static ActionThread thrListen;
        internal static bool stop = false;

        private static TcpListener _tcpServer;

        internal static void StartListeningForConnections()
        {
            // Initialize new thread for client communications
            thrListen = UnityThreadHelper.CreateThread(() =>
            {
                while (!stop)
                {
                    //If the TCP listener object was not created before, create it.
                    if (_tcpServer == null)
                    {
                        // Create the TCP listener object using the IP of the server and the specified port
                        _tcpServer = new TcpListener(IPAddress.Any, Server.Server.properties.connectionBridgePort);
                    }
                    //Start the TCP listener and listen for connections.
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.Log("ConnectionBridge server listening on: " + IPUtility.GetLocalIp() + ":" +
                                    Server.Server.properties.connectionBridgePort);
                    });
                    _tcpServer.Start();
                    //Getting a pending connection.
                    var client = _tcpServer.AcceptTcpClient();
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.Log("A client connected to the server, sending server info...");
                    });

                    //Sending a positive byte to the client to accept the connection.
                    var ns = client.GetStream();
                    ns.WriteByte(1);

                    //Sending ServerInfo to the client.
                    var serverInfoName = Encoding.ASCII.GetBytes(MatchMaker.SerializeServerInfo(MatchMaker.CreateCurrentServerInfo()));
                    var serverInfoLenght = BitConverter.GetBytes(serverInfoName.Length);
                    ns.Write(serverInfoLenght, 0, serverInfoLenght.Length);
                    ns.Write(serverInfoName, 0, serverInfoName.Length);

                    //Sending ServerProperties to the client.
                    var serverPropertiesName = Encoding.ASCII.GetBytes(JsonUtility.ToJson(Server.Server.properties));
                    var serverPropertiesLenght = BitConverter.GetBytes(serverPropertiesName.Length);
                    ns.Write(serverPropertiesLenght, 0, serverPropertiesLenght.Length);
                    ns.Write(serverPropertiesName, 0, serverPropertiesName.Length);

                    //Sending ConnectionConfig to the client.
                    var connectionConfigName = Encoding.ASCII.GetBytes(JsonUtility.ToJson(Server.Server.connectionConfig));
                    var connectionConfigLenght = BitConverter.GetBytes(connectionConfigName.Length);
                    ns.Write(connectionConfigLenght, 0, connectionConfigLenght.Length);
                    ns.Write(connectionConfigName, 0, connectionConfigName.Length);

                    //Closing the connection.
                    client.Close();
                    _tcpServer.Stop();
                }

            }, true);
        }

        public static void StopListeningForConnections()
        {
            thrListen.Exit();

            if (_tcpServer != null)
            {
                _tcpServer.Stop();
                _tcpServer = null;
            }
        }
    }
}