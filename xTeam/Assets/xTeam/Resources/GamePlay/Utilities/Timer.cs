﻿using System;
using UnityEngine;

namespace XTeam.Resources.GamePlay.Utilities
{
    /// <summary>
    /// Universal timer for various uses.
    /// </summary>
    public class Timer : MonoBehaviour {

        public bool active;
        public bool autoReset = false;

        public float time;
        private float _timeCache;

        public int tickInterval = 1;
        public int endNumber = 0;

        public Action onTimerTick;
        public Action onTimerEnd;

        public CountingMode currentCountingMode = CountingMode.Down;

        // Update is called once per frame
        void Update () {
            if(active)
            {
                //Subtracting deltaTime from time.
                switch (currentCountingMode)
                {
                    case CountingMode.Up:
                        time += Time.deltaTime;
                        break;
                    case CountingMode.Down:
                        time -= Time.deltaTime;
                        break;
                }

                //Checking if there is a tick in this frame.
                if ((int)time > (int)(time - Time.deltaTime))
                {
                    //There is a tick at this frame.
                    Tick();
                }

                //Checking whether countdown should end.
                switch (currentCountingMode)
                {
                    case CountingMode.Up:
                        if ((int)time >= endNumber)
                        {
                            End();
                        }
                        break;
                    case CountingMode.Down:
                        if ((int)time <= endNumber)
                        {
                            End();
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Called on every timer tick.
        /// </summary>
        void Tick()
        {
            if(onTimerTick != null)
            {
                //Running the onTimerTick method.
                onTimerTick();
            }
        }
        /// <summary>
        /// Ends current countdown.
        /// </summary>
        public void End()
        {
            if(onTimerEnd != null)
            {
                //Running the onTimerEnd method.
                onTimerEnd();
            }

            if(autoReset)
            {
                //Resetting the countdown.
                time = _timeCache;
            } else
            {
                //Ending the countdown.
                active = false;
                Destroy(this);
            }
        }
        /// <summary>
        /// Sets new timer start time.
        /// </summary>
        /// <param name="time">Start time to set.</param>
        public void SetTime(float time)
        {
            //Setting the time value.
            this.time = time;
            //Setting the time cache value.
            _timeCache = time;
        }
        /// <summary>
        /// Sets timer active state.
        /// </summary>
        /// <param name="active">New timer active state.</param>
        public void SetActive(bool active)
        {
            //Setting the active state.
            this.active = active;
        }
    }
    /// <summary>
    /// Timer work mode. 
    /// UP - Counting up. 
    /// Down - Counting down.
    /// </summary>
    public enum CountingMode
    {
        Up, Down
    }
}