﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using Ionic.Zip;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.Resources.XEditor.MapExporter;
using xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup;
using xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup.AutomaticallyGenerated;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;
using Object = UnityEngine.Object;

namespace xTeam.Resources.Utilities.FileManager.XLoad.Editor
{
    public class Save
    {
        /// <summary>
        /// Saves s pecified GameObject to the file.
        /// </summary>
        public static void GameObjectToFile(GameObject gameObject, string path)
        {
            //Clear the assetbundle build database.
            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Creating prefab...
            var tempPrefabPath = "Assets" + "/" + gameObject.name + ".prefab";
            PrefabUtility.CreatePrefab(tempPrefabPath, gameObject, ReplacePrefabOptions.Default);

            path = new FileInfo(path).DirectoryName;
            if (path.StartsWith(Application.dataPath))
            {
                path = "Assets" + path.Substring(Application.dataPath.Length);
            }

            //Exporting...
            AssetImporter assetImporter = AssetImporter.GetAtPath(tempPrefabPath);
            assetImporter.assetBundleName = gameObject.name;
            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows);

            //Clear the assetbundle build database.
            names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Delete temp files.
            File.Delete(tempPrefabPath);
            File.Delete(path + "/" + new DirectoryInfo(path).Name);
            File.Delete(path + "/" + new DirectoryInfo(path).Name + ".manifest");
            File.Delete(path + "/" + gameObject.name.ToLower() + ".manifest");
            File.Move(path + "/" + gameObject.name.ToLower(), path + "/" + gameObject.name + ".xto");
        }
        public static void SceneToFile(Scene scene, string path)
        {
            //Load the scene to save and set it as active.
            if (SceneManager.GetActiveScene() != scene)
            {
                SceneManager.LoadScene(scene.name);
                SceneManager.SetActiveScene(scene);
            }

            //Making sure that the MapSettings object is present on the map.
            var mapSettings = Object.FindObjectOfType<MapSettings>();
            if (mapSettings == null)
            {
                mapSettings = new GameObject("MapSettings", new Type[]
                {
                    typeof(MapSettings)
                }).GetComponent<MapSettings>();
            }
            if (string.IsNullOrEmpty(mapSettings.mapName))
            {
                mapSettings.mapName = SceneManager.GetActiveScene().name;
            }

            //Getting the paths needed for export.
            var iconFilePath = EditorUtility.OpenFilePanel("Select map icon", "", "png");
            var projectExportDir = new FileInfo(path).DirectoryName;
            if (projectExportDir.StartsWith(Application.dataPath))
            {
                projectExportDir = "Assets" + projectExportDir.Substring(Application.dataPath.Length);
            }

            //Checking whether export was cancelled.
            if (iconFilePath == "" || projectExportDir == "")
            {
                Debug.Log("Export cancelled.");
                return;
            }

            //Begining the export process...
            Debug.Log("Cleaning up map " + mapSettings.mapName);

            //Cleaning up!
            CleanUpCurrentScene();

            //Begining the export process...
            Debug.Log("Exporting map " + mapSettings.mapName + " to " + projectExportDir);

            //Saving the scene in the editor.
            EditorSceneManager.SaveOpenScenes();

            //Moving the saved scene file to the default assets level path.
            string defaultLevelAssetsPath = "Assets/" + mapSettings.mapName + ".unity";
            if (SceneManager.GetActiveScene().path != defaultLevelAssetsPath)
            {
                //Moving saved scene to a default maps directory.
                FileUtil.ReplaceFile(SceneManager.GetActiveScene().path, defaultLevelAssetsPath);
            }

            //Clear the assetbundle build database.
            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Exporting...
            AssetImporter assetImporter = AssetImporter.GetAtPath(defaultLevelAssetsPath);
            assetImporter.assetBundleName = mapSettings.mapName;
            BuildPipeline.BuildAssetBundles(projectExportDir, BuildAssetBundleOptions.ForceRebuildAssetBundle, BuildTarget.StandaloneWindows);
            //Getting rid of CustomLevelSettings.
            var cls = Object.FindObjectOfType<CustomLevelSettings>().gameObject;
            if (cls != null)
            {
                Object.DestroyImmediate(cls.gameObject);
            }

            //Delete temp files.
            File.Delete(projectExportDir + "/" + new DirectoryInfo(projectExportDir).Name);
            File.Delete(projectExportDir + "/" + new DirectoryInfo(projectExportDir).Name + ".manifest");
            File.Delete(projectExportDir + "/" + mapSettings.mapName.ToLower() + ".manifest");
            File.Move(projectExportDir + "/" + mapSettings.mapName.ToLower(), projectExportDir + "/" + mapSettings.mapName + ".xto");

            try
            {
                using (var zip = new ZipFile())
                {
                    //Adding map file to the zip.
                    Debug.Log("Adding map file to map zip.");
                    var mapFileZipEntry = zip.AddEntry("mapData.xto", File.ReadAllBytes(projectExportDir + "/" + mapSettings.mapName + ".xto"));
                    mapFileZipEntry.Comment = "The map file itself. Can be opened directly through X-Team";

                    //Adding map info document to the zip.
                    var mapInfo = CreateMapInfoDocument(mapSettings);
                    var mapInfoPath = projectExportDir + "/mapInfo.xml";
                    mapInfo.Save(mapInfoPath);
                    var mapInfoZipEntry = zip.AddEntry("mapInfo.xml",
                        File.ReadAllBytes(projectExportDir + "/mapInfo.xml"));
                    mapInfoZipEntry.Comment = "Document containing all information about the map.";

                    //Adding map icon file to the zip.
                    var mapIconZipEntry = zip.AddEntry("mapIcon.png", File.ReadAllBytes(iconFilePath));
                    mapIconZipEntry.Comment = "An image of the map.";

                    zip.Comment = "X-Team map " + mapSettings.mapName + " file.";

                    zip.Save(projectExportDir + "/" + new FileInfo(path).Name);

                    //Cleaning up the non-xtmed files.
                    File.Delete(projectExportDir + "/" + mapSettings.mapName + ".xto");
                    File.Delete(projectExportDir + "/mapInfo.xml");
                    File.Delete(projectExportDir + "/contentInfo.xml");
                    File.Delete(defaultLevelAssetsPath);
                }
            }
            catch (Exception ex1)
            {
                Console.Error.WriteLine("Exception creating a ZIP file: " + ex1);
            }

            //Exporting completed!
            Debug.Log("Exporting complete!");

            //Clear the assetbundle build database.
            names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Refreshing the asset database.
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }
        /// <summary>
        /// Sleans up the currently loaded scene.
        /// </summary>
        /// <param name="scene"></param>
        private static void CleanUpCurrentScene()
        {
            //Creating new CustomLevelSettings object and setting its values.
            var cls = Object.FindObjectOfType<CustomLevelSettings>();
            if (cls != null)
            {
                cls.SetSettings();
            }
            else
            {
                var clsgo = new GameObject { name = "CustomLevelSettings" };
                cls = clsgo.AddComponent<CustomLevelSettings>();
                cls.SetSettings();
            }

            //Removing unnecesery cameras.
            var cams = Object.FindObjectsOfType<Camera>();
            var camCount = 0;
            foreach (var cam in cams.Where(cam => cam.enabled).Where(cam => cam.targetTexture == null))
            {
                cam.enabled = false;
                camCount++;
            }
            if (camCount > 0)
            {
                Debug.Log("You had " + camCount + " extra cameras enabled, they were disabled for export.");
            }

            //Removing unnecesery audiolisteners.
            var listeners = Object.FindObjectsOfType<AudioListener>();
            var listenerCount = 0;
            foreach (var t in listeners.Where(t => t.enabled))
            {
                t.enabled = false;
                listenerCount++;
            }
            if (listenerCount > 0)
                Debug.Log("You had " + listenerCount + " extra AudioListeners enabled, they were disabled for export.");
        }
        /// <summary>
        /// Creates a map info .xml document based on the specified MapSettings.
        /// </summary>
        /// <param name="mapSettings"></param>
        /// <returns></returns>
        private static XmlDocument CreateMapInfoDocument(MapSettings mapSettings)
        {
            Map mapInfo = new Map
            {
                name = mapSettings.mapName,
                description = mapSettings.mapDescription,
                type = mapSettings.mapType,
                version = mapSettings.mapVersion
            };

            return XmlUtil.Serialize(mapInfo);
        }
    }
}
