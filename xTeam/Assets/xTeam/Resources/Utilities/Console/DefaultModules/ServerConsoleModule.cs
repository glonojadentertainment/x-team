﻿using UnityEngine;
using UnityEngine.Networking;
using XTeam.API.Console;
using XTeam.Resources.GamePlay.Network.Server;
using XTeam.Resources.GamePlay.Network.Shared;

namespace XTeam.Resources.Utilities.Console.DefaultModules
{
    public class ServerConsoleModule : ConsoleModule
    {
        public override string GetName()
        {
            return "Server Management";
        }
        public override string[] GetBaseCommands()
        {
            return new string[] { "server", "sv" };
        }
        public override bool OnCommandRun(ConsoleCommand command)
        {
            //Checking command args.
            if(command.arguments.Length == 0)
            {
                //There are no arguments on the command.
                //Showing server status.
                Debug.Log("Server status:");
                //UNET
                Debug.Log(" UNET:");
                Debug.Log("  Active: " + NetworkServer.active);
                Debug.Log("  Connected clients: " + NetworkServer.connections.Count);
                Debug.Log("  FileTransfers: " + "TODO");

                //Bridge.
                Debug.Log(" BRIDGE:");
                if (ServerInfoSender.thrListen == null)
                {
                    Debug.Log("  Active: " + null);
                }
                else
                {
                    Debug.Log("  Active: " + ServerInfoSender.thrListen.IsAlive);
                }
                return true;
            }

            //Checking the set arg.
            if(command.arguments[0] == "start")
            {
                //Starting the server.
                Server.StartServer();
                return true;
            }
            //Checking the set arg.
            if (command.arguments[0] == "stop")
            {
                //Stopping the server.
                Server.StopServer();
                return true;
            }
            return false;
        }
    }
}
