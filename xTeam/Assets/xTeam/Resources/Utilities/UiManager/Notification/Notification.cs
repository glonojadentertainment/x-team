﻿using UnityEngine;
using UnityEngine.UI;
using XTeam.Resources.GamePlay.Utilities;

namespace XTeam.Resources.Utilities.UiManager.Notification
{
    public class Notification : MonoBehaviour
    {

        //UI components.
        public Text notificationText;
        public CanvasGroup notificationCanvasGroup;

        public void Initialize(string notificationText, float notificationTime)
        {
            //Setting the notification text.
            this.notificationText.text = notificationText;

            //Setting the autodestruction timer.
            Timer disableTimer = gameObject.AddComponent<Timer>();
            disableTimer.SetTime(notificationTime);
            disableTimer.onTimerEnd += Remove;
            disableTimer.SetActive(true);

            //Enabling the notification.
            StartCoroutine(UiManager.Smooth(0f, 1f, 0.1f, value => notificationCanvasGroup.alpha = value));
        }
        public void Remove()
        {
            //Disabling the notification.
            StartCoroutine(UiManager.Smooth(1f, 0f, 0.1f, value => notificationCanvasGroup.alpha = value));
            //Destroying the notification.
            Destroy(gameObject);
        }
    }
}