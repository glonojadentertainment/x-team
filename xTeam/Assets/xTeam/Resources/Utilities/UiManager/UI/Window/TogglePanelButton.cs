﻿using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager.UI.Window
{
    public class TogglePanelButton : MonoBehaviour
    {

        public void TogglePanel(GameObject panel)
        {
            panel.SetActive(!panel.activeSelf);
        }
    }
}