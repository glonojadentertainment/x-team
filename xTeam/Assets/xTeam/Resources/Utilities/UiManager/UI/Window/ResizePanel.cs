﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace XTeam.Resources.Utilities.UiManager.UI.Window
{
    public class ResizePanel : MonoBehaviour, IPointerDownHandler, IDragHandler
    {

        public Vector2 minSize;
        public Vector2 maxSize;

        private RectTransform _rectTransform;
        private Vector2 _currentPointerPosition;
        private Vector2 _previousPointerPosition;

        void Awake()
        {
            _rectTransform = transform.parent.GetComponent<RectTransform>();
        }

        public void OnPointerDown(PointerEventData data)
        {
            _rectTransform.SetAsLastSibling();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, data.position, data.pressEventCamera, out _previousPointerPosition);
        }

        public void OnDrag(PointerEventData data)
        {
            if (_rectTransform == null)
                return;

            Vector2 sizeDelta = _rectTransform.sizeDelta;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, data.position, data.pressEventCamera, out _currentPointerPosition);
            Vector2 resizeValue = _currentPointerPosition - _previousPointerPosition;

            sizeDelta += new Vector2(resizeValue.x, -resizeValue.y);
            sizeDelta = new Vector2(
                Mathf.Clamp(sizeDelta.x, minSize.x, maxSize.x),
                Mathf.Clamp(sizeDelta.y, minSize.y, maxSize.y)
                );

            _rectTransform.sizeDelta = sizeDelta;

            _previousPointerPosition = _currentPointerPosition;
        }
    }
}