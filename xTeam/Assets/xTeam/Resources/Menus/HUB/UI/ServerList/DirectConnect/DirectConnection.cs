﻿using UnityEngine;
using UnityEngine.UI;
using XTeam.Resources.GamePlay.Network.Shared;

namespace XTeam.Resources.Menus.Hub.UI.ServerList.DirectConnect
{
    public class DirectConnection : MonoBehaviour
    {
        public InputField address;
        public InputField port;

        public void Connect()
        {
            Client.Connect(address.text, int.Parse(port.text));
        }
    }
}
