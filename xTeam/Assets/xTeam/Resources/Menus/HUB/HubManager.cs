﻿using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.Resources.Utilities.FileManager.LevelLoader;
using XTeam.Resources.Utilities.UiManager;

namespace XTeam.Resources.Menus.Hub
{
    public class HubManager : MonoBehaviour
    {

        public CanvasGroup mainMenuCanvasGroup;
        public CanvasGroup playMenuCanvasGroup;
        public CanvasGroup inventoryCanvasGroup;
        public CanvasGroup shopCanvasGroup;

        public CanvasGroup currentlyOpenCanvasGroup;

        public void OpenMainMenu()
        {
            OpenSection(HubSection.MAIN_MENU);
        }

        public void OpenPlayMenu()
        {
            OpenSection(HubSection.PLAY_MENU);
        }

        public void OpenInventory()
        {
            OpenSection(HubSection.INVENTORY);
        }

        public void OpenShop()
        {
            OpenSection(HubSection.MAIN_MENU);
        }

        public void OpenSection(HubSection section)
        {
            CanvasGroup targetCanvasGroup = null;

            switch (section)
            {
                case HubSection.MAIN_MENU:
                    targetCanvasGroup = mainMenuCanvasGroup;
                    break;
                case HubSection.PLAY_MENU:
                    targetCanvasGroup = playMenuCanvasGroup;
                    break;
                case HubSection.INVENTORY:
                    targetCanvasGroup = inventoryCanvasGroup;
                    break;
                case HubSection.SHOP:
                    targetCanvasGroup = shopCanvasGroup;
                    break;
            }

            //Closing the currently open section.
            if (currentlyOpenCanvasGroup != null)
            {
                StartCoroutine(UiManager.Smooth(1f, 0f, 0.5f, value => currentlyOpenCanvasGroup.alpha = value));
                currentlyOpenCanvasGroup.gameObject.SetActive(false);
            }

            targetCanvasGroup.gameObject.SetActive(true);
            StartCoroutine(UiManager.Smooth(0f, 1f, 0.5f, value => targetCanvasGroup.alpha = value));
            currentlyOpenCanvasGroup = targetCanvasGroup;
        }

        public void OpenServerSetup()
        {
            StartCoroutine(LevelLoader.LoadScene("Server", LoadSceneMode.Additive, null));
        }
    }

    public enum HubSection
    {
        MAIN_MENU,
        PLAY_MENU,
        INVENTORY,
        SHOP
    }
}
