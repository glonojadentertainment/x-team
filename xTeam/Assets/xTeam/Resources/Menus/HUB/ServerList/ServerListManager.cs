﻿using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;
using XTeam.Resources.Utilities.UiManager;

namespace XTeam.Resources.Menus.HUB.ServerList
{
    public class ServerListManager : MonoBehaviour
    {
        /// <summary>
        ///     Instance of this component.
        /// </summary>
        public static ServerListManager instance;

        [Tooltip("NetworkDiscovery used for findng LAN games.")] public NetworkDiscovery networkDiscovery;

        /// <summary>
        ///     Current mode of server listing.
        /// </summary>
        public ServerListMode serverListMode = ServerListMode.OFFICIAL;

        [Tooltip("Prefab of the server list slot.")] public Object serverSlotPrefab;

        public CanvasGroup directConnect;

        /// <summary>
        ///     Called when a new server is found.
        /// </summary>
        /// <param name="listInfo"></param>
        public void OnServerFound(MatchGetResponse serverGetResponse)
        {
            //Duplicate checking.
            if (GetComponentsInChildren<Menus.ServerList.ServerSlot.ServerSlot>().Any(slot => slot.representedServer.name == serverGetResponse.server.name)) {
                return;
            }

            Debug.Log("Found server: " + serverGetResponse.server.name);

            //Spawning a ServerList slot.
            var processedMatchSlot = (GameObject) Instantiate(serverSlotPrefab);
            processedMatchSlot.transform.SetParent(transform, false);

            //Setting the properties.
            processedMatchSlot.GetComponent<Menus.ServerList.ServerSlot.ServerSlot>().representedServer = serverGetResponse.server;
            processedMatchSlot.GetComponent<Menus.ServerList.ServerSlot.ServerSlot>().serverNetworkType =
                serverGetResponse.serverNetworkType;

            processedMatchSlot.transform.FindChild("Name").GetComponent<Text>().text =
                serverGetResponse.server.name;
            processedMatchSlot.transform.FindChild("Description").GetComponent<Text>().text =
                serverGetResponse.server.description;
            processedMatchSlot.transform.FindChild("Slots").GetComponent<Text>().text = serverGetResponse.server.playerCount +
                                                                                        "/" +
                                                                                        serverGetResponse.server.size;
        }

        #region MonoBehaviour

        /// <summary>
        ///     Setting the instance variable.
        /// </summary>
        private void Awake()
        {
            instance = this;
        }

        // Use this for initialization
        private void Start()
        {
            //Refreshing the list for the first time.
            Refresh();
        }

        #endregion

        #region ListManagement
        /// <summary>
        /// Sets a new server list mode.
        /// </summary>
        /// <param name="mode">Mode to be set.</param>
        public void SetServerListType(int mode)
        {
            serverListMode = (ServerListMode)mode;

            if (serverListMode == ServerListMode.DIRECT_CONNECT)
            {
                directConnect.gameObject.SetActive(true);
                UiManager.instance.StartCoroutine(UiManager.Smooth(0f, 1f, 0.05f, value => directConnect.alpha = value));
            } else if (directConnect.alpha != 0f)
            {
                directConnect.gameObject.SetActive(false);
                UiManager.instance.StartCoroutine(UiManager.Smooth(1f, 0f, 0.05f, value => directConnect.alpha = value));
            }

            Refresh();
        }
        /// <summary>
        ///     Refreshses the server list.
        /// </summary>
        public void Refresh()
        {
            //Refreshing the serverlist.
            Debug.Log("Refreshing serverlist...");
            ClearServerList();
            if (networkDiscovery.running || networkDiscovery.isClient)
            {
                networkDiscovery.StopBroadcast();
                networkDiscovery.enabled = false;
            }

            switch (serverListMode)
            {
                case ServerListMode.OFFICIAL:
                    // Request new list of matches.
                    //listMatchRequest.domain = (int)MatchMakingDomain.OFFICIAL; TODO
                    MatchMaker.ListServers(OnServerFound);
                    break;
                case ServerListMode.COMMUNITY:
                    // Request new list of matches.
                    //listMatchRequest.domain = (int)MatchMakingDomain.COMMUNITY; TODO
                    MatchMaker.ListServers(OnServerFound);
                    break;
                case ServerListMode.HOSTED:
                    // Request new list of matches.
                    //listMatchRequest.domain = (int)MatchMakingDomain.HOSTED; TODO
                    MatchMaker.ListServers(OnServerFound);
                    break;
                case ServerListMode.LAN:
                    //Starts listening for servers on local network.
                    networkDiscovery.enabled = true;
                    networkDiscovery.Initialize();
                    networkDiscovery.StartAsClient();
                    break;
            }
        }

        /// <summary>
        ///     Clears current server list.
        /// </summary>
        public void ClearServerList()
        {
            //Destroying outdated server slots.
            foreach (var serverSlot in GetComponentsInChildren<Menus.ServerList.ServerSlot.ServerSlot>())
            {
                Destroy(serverSlot.gameObject);
            }
        }

        #endregion
    }

    /// <summary>
    ///     Mode of server listing.
    /// </summary>
    public enum ServerListMode
    {
        OFFICIAL,
        COMMUNITY,
        HOSTED,
        LAN,
        FRIENDS,
        FAVOURITES,
        HISTORY,
        DIRECT_CONNECT
    }
}