﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.Resources.Utilities.FileManager.XLoad;
using xTeam.Resources.Utilities.FileManager.XLoad.Editor;

namespace xTeam.Resources.XEditor.MapExporter.Editor
{
    public class MapExporter : MonoBehaviour
    {
        public static void ExportCurrentMap()
        {
            Save.SceneToFile(SceneManager.GetActiveScene(), EditorUtility.SaveFilePanel("Save map file", Application.dataPath, String.Empty, "xtm"));
        }
        [MenuItem("Assets/Load xTeam map")]
        public static void LoadObject()
        {
            //Loading and spawning the selected object.
            string loadedScene = Load.SceneFromFile(AssetDatabase.GetAssetPath(Selection.activeObject));
            SceneManager.LoadScene(loadedScene);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(loadedScene));
            Debug.Log(loadedScene + " has been loaded successfully!");
            UnityEngine.Resources.UnloadUnusedAssets();
        }
        [MenuItem("Assets/Load xTeam map", true)]
        public static bool ValidateXTM()
        {
            if (new FileInfo(AssetDatabase.GetAssetPath(Selection.activeObject)).Extension == ".xtm")
            {
                return true;
            }
            return false;
        }
    }
}