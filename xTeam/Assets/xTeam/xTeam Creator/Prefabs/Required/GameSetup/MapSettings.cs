﻿using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup
{
    /// <summary>
    /// Class containing contentPack settings.
    /// </summary>
    public class MapSettings : MonoBehaviour {
        [Tooltip ("Name of this contentPack.")]
        public string mapName;
        [Tooltip("Description of this contentPack.")]
        public string mapDescription;
        [Tooltip("Type of this contentPack.")]
        public string mapType;
        [Tooltip("Version of this contentPack.")]
        public int mapVersion;
    }
}