﻿//No need to attach this to any object

using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup.AutomaticallyGenerated
{
    public class CustomLevelSettings : MonoBehaviour
    {
        private Color _ambientLight;
        private float _flareFadeSpeed;
        private float _flareStrength;

        private bool _fog;
        private Color _fogColor;
        private float _fogDensity;
        private float _fogEndDistance;
        private FogMode _fogMode;
        private float _fogStartDistance;
        private float _haloStrength;
        //Texture2D haloTexture;
        //Texture2D spotCookie;

        private LightProbes _lightProbes;
        private Material _skybox;

        private void Start()
        {
            LoadSettings();
        }

        public void SetSettings()
        {
            _fog = RenderSettings.fog;
            _fogColor = RenderSettings.fogColor;
            _fogMode = RenderSettings.fogMode;
            _fogDensity = RenderSettings.fogDensity;
            _fogStartDistance = RenderSettings.fogStartDistance;
            _fogEndDistance = RenderSettings.fogEndDistance;
            _ambientLight = RenderSettings.ambientLight;
            _skybox = RenderSettings.skybox;
            _haloStrength = RenderSettings.haloStrength;
            _flareStrength = RenderSettings.flareStrength;
            _flareFadeSpeed = RenderSettings.flareFadeSpeed;
            // haloTexture = RenderSettings.haloTexture;
            // spotCookie = RenderSettings.spotCookie;
            _lightProbes = LightmapSettings.lightProbes;
        }

        public void LoadSettings()
        {
            RenderSettings.fog = _fog;
            RenderSettings.fogColor = _fogColor;
            RenderSettings.fogMode = _fogMode;
            RenderSettings.fogDensity = _fogDensity;
            RenderSettings.fogStartDistance = _fogStartDistance;
            RenderSettings.fogEndDistance = _fogEndDistance;
            RenderSettings.ambientLight = _ambientLight;
            RenderSettings.skybox = _skybox;
            RenderSettings.haloStrength = _haloStrength;
            RenderSettings.flareStrength = _flareStrength;
            RenderSettings.flareFadeSpeed = _flareFadeSpeed;
            LightmapSettings.lightProbes = _lightProbes;
            //Self-destructing.
            Destroy(gameObject);
        }
    }
}