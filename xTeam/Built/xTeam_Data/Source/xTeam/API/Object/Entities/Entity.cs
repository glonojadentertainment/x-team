﻿namespace XTeam.API.Objects.Entities
{
    public interface IEntity {
        string GetName();
    }
}
