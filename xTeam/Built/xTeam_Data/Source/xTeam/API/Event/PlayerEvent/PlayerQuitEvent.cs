﻿using XTeam.Resources.GamePlay.Network.Client.Player;

namespace xTeam.API.Event.PlayerEvent
{
    public class PlayerQuitEvent : PlayerEvent
    {
        public PlayerQuitEvent(Player player, string quitMessage) : base(player)
        {
            playerQuitMessage = quitMessage;
        }
        public override string GetEventName()
        {
            return "Player Quit Event";
        }
        /// <summary>
        /// The quit message to send to all online players.
        /// </summary>
        protected string playerQuitMessage;
        /// <summary>
        /// Gets the quit message to send to all online players.
        /// </summary>
        /// <returns></returns>
        public string GetQuitMessage()
        {
            return playerQuitMessage;
        }
        /// <summary>
        /// Sets the quit message to send to all online players.
        /// </summary>
        /// <returns></returns>
        public void SetQuitMessage(string message)
        {
            playerQuitMessage = message;
        }
    }
}
