﻿using System;

namespace xTeam.API.Event
{
    /// <summary>
    /// Handler for calling the events.
    /// </summary>
    public class EventHandler
    {
        /// <summary>
        /// Method called when the event is called.
        /// </summary>
        public Action<Event> callback;
        /// <summary>
        /// Type of the handled event.
        /// </summary>
        public Type handledEventType;

        public EventHandler(Type eventTypeToHandle, Action<Event> callback)
        {
            this.callback = callback;
            handledEventType = eventTypeToHandle;
        }
    }
}