﻿namespace xTeam.API.Event
{
    /// <summary>
    /// Base class for all the events.
    /// </summary>
    public abstract class Event
    {
        /// <summary>
        /// Name of this event.
        /// </summary>
        protected string name;

        /// <summary>
        /// Returns name of this event.
        /// </summary>
        /// <returns>Name of this event.</returns>
        public abstract string GetEventName();
    }
}
