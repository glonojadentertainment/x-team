﻿using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Optional.Camera_Setup.Lobby_Camera
{
    public class LobbyCamera : MonoBehaviour {
        [Tooltip("If you place more than one LobbyCamera on the contentPack, game will switch between cameras based on their IDs.")]
        public int cameraId;
        [Tooltip("This value determines, how long will this camera be active, before switching to the next camera.")]
        public float singleActivationLenght;
        [Tooltip("This value determines, whether the mouse sway effect is enabled on this camera.")]
        public bool mouseSwayEnabled;
    }
}
