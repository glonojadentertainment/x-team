﻿using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Optional.Utilities.Scripts
{
    public class DontDestroyOnLoad : MonoBehaviour {

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
