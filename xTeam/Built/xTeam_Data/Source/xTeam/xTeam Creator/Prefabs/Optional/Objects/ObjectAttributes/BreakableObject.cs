﻿using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Optional.Objects.ObjectAttributes
{
    public class BreakableObject : MonoBehaviour {

        [Tooltip ("A broken version of this object.")]
        public GameObject brokenModel;
        [Tooltip ("A healthy version of this object.")]
        public GameObject healthyModel;
        [Tooltip ("Durability of this object.")]
        public float durability;
        [Tooltip ("Used for calculations. Recommended to leave default.")]
        public float massMultiplier = 0.2f;
        [Tooltip ("Should the break velocity be inverted?")]
        public bool inverted = false;

        Vector3 _velocity;

        // Use this for initialization
        void OnCollisionEnter (Collision collision) {
            Rigidbody body = collision.rigidbody;
            GameObject go = collision.gameObject;
            while(body == null && go.transform.parent) {
                go = go.transform.parent.gameObject;
                body = go.GetComponent<Rigidbody>();
            }
            if(body == null) {
                return;
            }
            if (collision.relativeVelocity.magnitude >= durability - (body.mass * collision.relativeVelocity.magnitude) * massMultiplier) {
                if(inverted){
                    _velocity = collision.relativeVelocity;
                }
                else{
                    _velocity = -collision.relativeVelocity;
                }
                Break(_velocity);
            }
        }
        public void Break(Vector3 breakVelocity) {
            brokenModel.transform.parent = null;
            Destroy(healthyModel);
            brokenModel.SetActive(true);
            Rigidbody[] bodies = brokenModel.GetComponentsInChildren<Rigidbody>();
            if(bodies.Length != 0) {
                foreach(Rigidbody body in bodies) {
                    body.velocity = breakVelocity;
                }
            }
        }
    }
}
