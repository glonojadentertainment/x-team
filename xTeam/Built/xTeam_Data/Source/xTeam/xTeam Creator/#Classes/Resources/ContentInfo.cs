﻿using UnityEngine.Networking;

namespace xTeam.xTeam_Creator_Classes.Resources
{
    /// <summary>
    ///     Class holding information about sending content.
    /// </summary>
    public class ContentInfo : MessageBase
    {
        public string name;
        public int version;
    }
}