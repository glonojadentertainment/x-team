﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using xTeam.xTeam_Creator.Core.Editor;
using System.Linq;

namespace xTeam.Resources.XEditor.Editor
{
    [ExecuteInEditMode]
    public class XEditorCore : MonoBehaviour
    {
        /// <summary>
        /// Path to the game's folder.
        /// </summary>
        private static string _gamePath;

        /// <summary>
        /// Path to the game's folder.
        /// </summary>
        public static string gameDir
        {
            get
            {
                if(string.IsNullOrEmpty(_gamePath))
                {
                    _gamePath = PlayerPrefs.GetString("XCreator.xTeamGameDir");

                    if (!string.IsNullOrEmpty(_gamePath))
                    {
                        if (CheckForGameInGameDir(_gamePath))
                        {
                            return _gamePath;
                        }
                        else
                        {
                            PlayerPrefs.DeleteKey("XCreator.xTeamGameDir");
                            PlayerPrefs.Save();
                        }
                    }

                    _gamePath = Application.dataPath;
                    while (!CheckForGameInGameDir(_gamePath))
                    {
                        _gamePath = EditorUtility.OpenFolderPanel("Select the game folder", _gamePath, "");

                        if (string.IsNullOrEmpty(_gamePath))
                        {
                            throw new Exception("Couldn't find game folder!");
                        }
                    }
                    PlayerPrefs.SetString("XCreator.xTeamGameDir", _gamePath);
                    PlayerPrefs.Save();

                    return _gamePath;
                } else
                {
                    return _gamePath;
                }
            }
            set
            {
                _gamePath = Application.dataPath;
                while (!CheckForGameInGameDir(_gamePath))
                {
                    _gamePath = EditorUtility.OpenFolderPanel("Select the game folder", _gamePath, "");

                    if (string.IsNullOrEmpty(_gamePath))
                    {
                        throw new Exception("Couldn't find game folder!");
                    }
                }
                PlayerPrefs.SetString("XCreator.xTeamGameDir", _gamePath);
                PlayerPrefs.Save();
            }
        }
        /// <summary>
        /// Checks whether gameDir contains the game executable.
        /// </summary>
        /// <returns>Whether gameDir contains the game executable.</returns>
        public static bool CheckForGameInGameDir(string dir)
        {
        #if UNITY_EDITOR_WIN
            return new DirectoryInfo(dir).GetFiles().Any(file => file.Extension == ".exe");
        #endif
        #if UNITY_EDITOR_OSX
            return new DirectoryInfo(dir).GetFiles().Any(file => file.Extension == ".app");
        #endif
            return false;
        }
        [MenuItem("xTeam/Creator")]
        public static void ShowCreatorWindow()
        {
            EditorWindow.GetWindow<XTeamCreator>().Show();
        }
    }
    /// <summary>
    /// Main Creator window.
    /// </summary>
    public class XTeamCreator : EditorWindow
    {
        /// <summary>
        /// Called whenever the GUI layer is drawn.
        /// </summary>
        void OnGUI()
        {
            //Drawing a general settings label.
            GUILayout.Label("General", EditorStyles.boldLabel);

            //Drawing game path select button.
            if (GUILayout.Button("Choose game path"))
            {
                //Prompting user to find the game directory.
                XEditorCore.gameDir = null;
            }

            //Drawing source import button.
            if (GUILayout.Button("Reimport game source"))
            {
                //Gethering game assemblies.
                GameSourceImporter.ImportGameSource();
            }
            GUILayout.Label("Reimport xTeam source to access ingame classes.", EditorStyles.boldLabel);
            //Drawing source import button.
            if (GUILayout.Button("Delete game source"))
            {
                //Gethering game assemblies.
                new DirectoryInfo(GameSourceImporter.GetSourceProjectPath()).Delete(true);
            }
            GUILayout.Label("Delete the game source only when your content is finished to save disk space!", EditorStyles.boldLabel);

            //Drawing a space.
            GUILayout.Space(20f);

            //Drawing an export current map button.
            if (GUILayout.Button("Export current map"))
            {
                xTeam_Creator.MapExporter.Editor.MapExporter.ExportCurrentMap();
            }

            //Drawing a space.
            GUILayout.Space(20f);

            if (GUILayout.Button("Test in game"))
            {
                GameTester.Editor.GameTester.RunContentInGame();
            }

            //Drawing a space.
            GUILayout.Space(20f);

            //Drawing game path label.
            GUILayout.Label("Game path: " + XEditorCore.gameDir);
        }
    }
}