﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace XTeam.Resources.Utilities.Threading
{
	public class Channel<T> : IDisposable
	{
		private List<T> _buffer = new List<T>();
		private object _setSyncRoot = new object();
		private object _getSyncRoot = new object();
		private object _disposeRoot = new object();
		private ManualResetEvent _setEvent = new ManualResetEvent(false);
		private ManualResetEvent _getEvent = new ManualResetEvent(true);
		private ManualResetEvent _exitEvent = new ManualResetEvent(false);
		private bool _disposed = false;

		public int BufferSize { get; private set; }

		public Channel()
			: this(1)
		{
		}

		public Channel(int bufferSize)
		{
			if (bufferSize < 1)
				throw new ArgumentOutOfRangeException("bufferSize", "Must be greater or equal to 1.");

			BufferSize = bufferSize;
		}

		~Channel()
		{
			Dispose();
		}

		public void Resize(int newBufferSize)
		{
			if (newBufferSize < 1)
				throw new ArgumentOutOfRangeException("newBufferSize", "Must be greater or equal to 1.");

			lock (_setSyncRoot)
			{
				if (_disposed)
					return;

				var result = WaitHandle.WaitAny(new WaitHandle[] { _exitEvent, _getEvent });
				if (result == 0)
					return;

				_buffer.Clear();

				if (newBufferSize != BufferSize)
					BufferSize = newBufferSize;
			}
		}

		public bool Set(T value)
		{
			return Set(value, int.MaxValue);
		}

		public bool Set(T value, int timeoutInMilliseconds)
		{
			lock (_setSyncRoot)
			{
				if (_disposed)
					return false;
			
				var result = WaitHandle.WaitAny(new WaitHandle[] { _exitEvent, _getEvent }, timeoutInMilliseconds);
				if (result == WaitHandle.WaitTimeout || result == 0)
					return false;

				_buffer.Add(value);
				if (_buffer.Count == BufferSize)
				{
					_setEvent.Set();
					_getEvent.Reset();
				}

				return true;
			}
		}

		public T Get()
		{
			return Get(int.MaxValue, default(T));
		}

		public T Get(int timeoutInMilliseconds, T defaultValue)
		{
			lock (_getSyncRoot)
			{
				if (_disposed)
					return defaultValue;

				var result = WaitHandle.WaitAny(new WaitHandle[] { _exitEvent, _setEvent }, timeoutInMilliseconds);
				if (result == WaitHandle.WaitTimeout || result == 0)
					return defaultValue;

				var value = _buffer[0];
				_buffer.RemoveAt(0);
				if (_buffer.Count == 0)
				{
					_getEvent.Set();
					_setEvent.Reset();
				}

				return value;
			}
		}

		public void Close()
		{
			lock (_disposeRoot)
			{
				if (_disposed)
					return;

				_exitEvent.Set();
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			if (_disposed)
				return;

			lock (_disposeRoot)
			{
				_exitEvent.Set();

				lock (_getSyncRoot)
				{
					lock (_setSyncRoot)
					{
						_setEvent.Close();
						_setEvent = null;

						_getEvent.Close();
						_getEvent = null;

						_exitEvent.Close();
						_exitEvent = null;

						_disposed = true;
					}
				}
			}
		}

		#endregion
	}

	public class Channel : Channel<object>
	{
	}
}
