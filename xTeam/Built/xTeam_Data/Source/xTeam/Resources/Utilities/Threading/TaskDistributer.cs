using System;
using System.Collections;
using System.Linq;
using System.Threading;

namespace XTeam.Resources.Utilities.Threading
{
    public class TaskDistributor : DispatcherBase
	{
        private TaskWorker[] _workerThreads;

        internal WaitHandle NewDataWaitHandle { get { return dataEvent; } }

		private static TaskDistributor _mainTaskDistributor;

		/// <summary>
		/// Returns the first created TaskDistributor instance. When no instance has been created an exception will be thrown.
		/// </summary>
		public static TaskDistributor Main
		{
			get
			{
				if (_mainTaskDistributor == null)
					throw new InvalidOperationException("No default TaskDistributor found, please create a new TaskDistributor instance before calling this property.");

				return _mainTaskDistributor;
			}
		}

		/// <summary>
		/// Returns the first created TaskDistributor instance.
		/// </summary>
		public static TaskDistributor MainNoThrow
		{
			get
			{
				return _mainTaskDistributor;
			}
		}

		/// <summary>
		/// Creates a new instance of the TaskDistributor with ProcessorCount x2 worker threads.
		/// The task distributor will auto start his worker threads.
		/// </summary>
        public TaskDistributor(string name)
			: this(name, 0)
        {
        }

        public override int TaskCount
        {
            get
            {
                var count = base.TaskCount;
                lock (_workerThreads)
                {
                    for (var i = 0; i < _workerThreads.Length; ++i)
                    {
                        count += _workerThreads[i].dispatcher.TaskCount;
                    }
                }
                return count;
            }
        }

		/// <summary>
		/// Creates a new instance of the TaskDistributor.
		/// The task distributor will auto start his worker threads.
		/// </summary>
		/// <param name="workerThreadCount">The number of worker threads, a value below one will create ProcessorCount x2 worker threads.</param>
		public TaskDistributor(string name, int workerThreadCount)
			: this(name, workerThreadCount, true)
		{
		}

		/// <summary>
		/// Creates a new instance of the TaskDistributor.
		/// </summary>
        /// <param name="workerThreadCount">The number of worker threads, a value below one will create ProcessorCount x2 worker threads.</param>
		/// <param name="autoStart">Should the instance auto start the worker threads.</param>
		public TaskDistributor(string name, int workerThreadCount, bool autoStart)
			: base()
		{
			_name = name;
            if (workerThreadCount <= 0)
				workerThreadCount = ThreadBase.AvailableProcessors * 2;

			_workerThreads = new TaskWorker[workerThreadCount];
			lock (_workerThreads)
			{
				for (var i = 0; i < workerThreadCount; ++i)
					_workerThreads[i] = new TaskWorker(name, this);
			}

			if (_mainTaskDistributor == null)
				_mainTaskDistributor = this;

			if (autoStart)
				Start();
		}

		/// <summary>
		/// Starts the TaskDistributor if its not currently running.
		/// </summary>
		public void Start()
		{
			lock (_workerThreads)
			{
				for (var i = 0; i < _workerThreads.Length; ++i)
				{
					if (!_workerThreads[i].IsAlive)
					{
						_workerThreads[i].Start();
					}
				}
			}
		}

        public void SpawnAdditionalWorkerThread()
        {
            lock (_workerThreads)
            {
                Array.Resize(ref _workerThreads, _workerThreads.Length + 1);
                _workerThreads[_workerThreads.Length - 1] = new TaskWorker(_name, this);
				_workerThreads[_workerThreads.Length - 1].Priority = _priority;
                _workerThreads[_workerThreads.Length - 1].Start();
            }
        }

        /// <summary>
        /// Amount of additional spawnable worker threads.
        /// </summary>
		public int maxAdditionalWorkerThreads = 0;

		private string _name;

        internal void FillTasks(Dispatcher target)
        {
			target.AddTasks(IsolateTasks(1));
        }

		protected override void CheckAccessLimitation()
		{
			if (maxAdditionalWorkerThreads > 0 || !allowAccessLimitationChecks)
                return;

			if (ThreadBase.CurrentThread != null &&
				ThreadBase.CurrentThread is TaskWorker &&
				((TaskWorker)ThreadBase.CurrentThread).TaskDistributor == this)
			{
				throw new InvalidOperationException("Access to TaskDistributor prohibited when called from inside a TaskDistributor thread. Dont dispatch new Tasks through the same TaskDistributor. If you want to distribute new tasks create a new TaskDistributor and use the new created instance. Remember to dispose the new instance to prevent thread spamming.");
			}
		}

        internal override void TasksAdded()
        {
			if (maxAdditionalWorkerThreads > 0 &&
				(_workerThreads.All(worker => worker.dispatcher.TaskCount > 0 || worker.IsWorking) || taskList.Count > _workerThreads.Length))
            {
				Interlocked.Decrement(ref maxAdditionalWorkerThreads);
                SpawnAdditionalWorkerThread();
            }

			base.TasksAdded();
        }

        #region IDisposable Members

		private bool _isDisposed = false;
		/// <summary>
		/// Disposes all TaskDistributor, worker threads, resources and remaining tasks.
		/// </summary>
        public override void Dispose()
        {
			if (_isDisposed)
				return;

			while (true)
			{
				Task currentTask;
                lock (taskListSyncRoot)
                {
                    if (taskList.Count != 0)
						currentTask = taskList.Dequeue();
                    else
                        break;
                }
				currentTask.Dispose();
			}

			lock (_workerThreads)
			{
				for (var i = 0; i < _workerThreads.Length; ++i)
					_workerThreads[i].Dispose();
				_workerThreads = new TaskWorker[0];
			}

			dataEvent.Close();
			dataEvent = null;

			if (_mainTaskDistributor == this)
				_mainTaskDistributor = null;

			_isDisposed = true;
        }

        #endregion

		private ThreadPriority _priority = ThreadPriority.BelowNormal;
		public ThreadPriority Priority
		{
			get { return _priority; }
			set
			{
				_priority = value;
				foreach (var worker in _workerThreads)
					worker.Priority = value;
			}
		}
	}

    internal sealed class TaskWorker : ThreadBase
    {
		public Dispatcher dispatcher;
		public TaskDistributor TaskDistributor { get; private set; }

		public bool IsWorking
		{
			get
			{
				return dispatcher.IsWorking;
			}
		}

		public TaskWorker(string name, TaskDistributor taskDistributor)
            : base(name, false)
        {
			TaskDistributor = taskDistributor;
			dispatcher = new Dispatcher(false);
		}

        protected override IEnumerator Do()
        {
            while (!exitEvent.InterWaitOne(0))
            {
                if (!dispatcher.ProcessNextTask())
                {
					TaskDistributor.FillTasks(dispatcher);
                    if (dispatcher.TaskCount == 0)
                    {
						var result = WaitHandle.WaitAny(new WaitHandle[] { exitEvent, TaskDistributor.NewDataWaitHandle });
						if (result == 0)
                            return null;
						TaskDistributor.FillTasks(dispatcher);
                    }
                }
            }
            return null;
        }

        public override void Dispose()
        {
            base.Dispose();
			if (dispatcher != null)
				dispatcher.Dispose();
			dispatcher = null;
        }
	}
}

