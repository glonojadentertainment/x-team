﻿using UnityEngine;
using UnityEngine.UI;

namespace XTeam.Resources.Utilities.UiManager.ProgressBar.ProgressBar
{
    public class ProgressBar : MonoBehaviour {
        /// <summary>
        /// Operation to show progress of.
        /// </summary>
        public AsyncOperation Operation
        {
            get
            {
                return _operation;
            }
            set
            {
                _www = null;
                _operation = value;
            }
        }
        private AsyncOperation _operation;
        /// <summary>
        /// Download to show progress of.
        /// </summary>
        public WWW Www
        {
            get
            {
                return _www;
            }
            set
            {
                _operation = null;
                _www = value;
            }
        }
        private WWW _www;
        /// <summary>
        /// UI element to show progress on.
        /// </summary>
        public Slider progressBar;
        public CanvasGroup canvasGroup;
        #region MonoBehaviour
        /// <summary>
        /// Updating the progressbar.
        /// </summary>
        void Update()
        {
            if(_operation != null)
            {
                SetProgress(_operation.progress);
            }
            if(_www != null)
            {
                SetProgress(_www.progress);
            }
        }
        #endregion
        public void SetProgress(float progress)
        {
            //Checking whether progress value changed.
            if(progressBar.value != progress)
            {
                //Smoothly changing to new value.
                StartCoroutine(UiManager.Smooth(progressBar.value, progress, 0.05f, value => progressBar.value = value));
            }
        }
        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
