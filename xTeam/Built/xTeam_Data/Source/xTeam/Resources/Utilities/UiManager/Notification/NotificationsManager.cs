﻿using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager.Notification
{
    public class NotificationsManager : MonoBehaviour
    {
        /// <summary>
        /// Instance of this component.
        /// </summary>
        public static NotificationsManager instance;

        //Notifications prefabs.
        public Object smallNotificationPrefab;
        public Object mediumNotificationPrefab;
        public Object screenTitlePrefab;

        //References.
        public GameObject notificationCanvas;
        public GameObject smallNotificationsArea;
        public GameObject mediumNotificationsArea;
        public GameObject screenTitlesArea;

        #region MonoBehaviour
        void Awake()
        {
            instance = this;
        }
#endregion

        public void ShowSmallNotification(string text, float time)
        {
            if (CheckNotificationDuplicate(text))
            {
                return;
            }
            //Spawning a new notification object.
            GameObject newSmallNotification = (GameObject)Instantiate(smallNotificationPrefab);

            //Setting notification position.
            newSmallNotification.GetComponent<RectTransform>().SetParent(smallNotificationsArea.GetComponent<RectTransform>());

            //Activating the notification.
            newSmallNotification.GetComponent<Notification>().Initialize(text, time);
        }
        public void ShowMediumNotification(string text, float time)
        {
            if (CheckNotificationDuplicate(text))
            {
                return;
            }
            //Spawning a new notification object.
            GameObject newMediumNotification = (GameObject)Instantiate(mediumNotificationPrefab);

            //Setting notification position.
            newMediumNotification.GetComponent<RectTransform>().SetParent(mediumNotificationsArea.GetComponent<RectTransform>());

            //Activating the notification.
            newMediumNotification.GetComponent<Notification>().Initialize(text, time);
        }
        public void ShowScreenTitle(string text, float time)
        {
            if (CheckNotificationDuplicate(text))
            {
                return;
            }
            //Spawning a new notification object.
            GameObject newScreenTitle = (GameObject)Instantiate(screenTitlePrefab);

            //Setting notification position.
            newScreenTitle.GetComponent<RectTransform>().SetParent(screenTitlesArea.GetComponent<RectTransform>());

            //Activating the notification.
            newScreenTitle.GetComponent<Notification>().Initialize(text, time);
        }
        public bool CheckNotificationDuplicate(string notificationText)
        {
            foreach (Transform notificationArea in notificationCanvas.transform)
            {
                foreach (Notification notification in notificationArea.GetComponentsInChildren<Notification>())
                {
                    if (notification.notificationText.text == notificationText)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }

}