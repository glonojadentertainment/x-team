﻿using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager.ProgressBar
{
    /// <summary>
    /// Manages display of progressbars.
    /// </summary>
    public class ProgressbarManager : MonoBehaviour
    {
        /// <summary>
        /// Instance of this component.
        /// </summary>
        public static ProgressbarManager instance;

        [Tooltip("Progressbar prefab.")]
        public Object progressBarPrefab;
        [Tooltip("Holder for all spawned progress bars.")]
        public Transform progressBarHolder;

        #region MonoBehaviour
        public void Awake()
        {
            instance = this;
        }
#endregion

        /// <summary>
        /// Creates a new progress bar, attached to a AsyncOperation.
        /// </summary>
        /// <param name="operationToTrack">AsyncOperation to be tracked by the new progress bar.</param>
        /// <returns></returns>
        public static ProgressBar.ProgressBar NewProgressBar(AsyncOperation operationToTrack)
        {
            var newProgressbar = SpawnProgressBar();
            newProgressbar.Operation = operationToTrack;
            return newProgressbar;
        }
        /// <summary>
        /// Creates a new progress bar, attached to a WWW download process.
        /// </summary>
        /// <param name="downloadToTrack">WWW download process to be tracked by the new progress bar.</param>
        /// <returns></returns>
        public static ProgressBar.ProgressBar NewProgressBar(WWW downloadToTrack)
        {
            var newProgressbar = SpawnProgressBar();
            newProgressbar.Www = downloadToTrack;
            return newProgressbar;
        }
        /// <summary>
        /// Creates a new progress bar.
        /// </summary>
        /// <returns></returns>
        public static ProgressBar.ProgressBar NewProgressBar()
        {
            return SpawnProgressBar();
        }
        private static ProgressBar.ProgressBar SpawnProgressBar()
        {
            var newProgressBarGameObject = (GameObject)Instantiate(instance.progressBarPrefab);
            DontDestroyOnLoad(newProgressBarGameObject);
            newProgressBarGameObject.transform.SetParent(instance.progressBarHolder);
            var newProgressBar = newProgressBarGameObject.GetComponent<ProgressBar.ProgressBar>();

            UiManager.instance.StartCoroutine(UiManager.Smooth(0f, 1f, 0.05f, value => newProgressBar.canvasGroup.alpha = value));

            return newProgressBar;
        }
    }
}
