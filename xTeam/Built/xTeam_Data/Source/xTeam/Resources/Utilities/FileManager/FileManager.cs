﻿using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace XTeam.Resources.Utilities.FileManager
{
    public class FileManager : MonoBehaviour
    {
        /// <summary>
        ///     Data relativePath of the application.
        /// </summary>
        private static string _applicationDataPath;

        /// <summary>
        ///     Cache relativePath of the application.
        /// </summary>
        private static string _applicationTemporaryCachePath;

        #region MonoBehaviour

        private void Start()
        {
            CacheApplicationDataPath();
            _applicationTemporaryCachePath = Application.temporaryCachePath;
        }

        #endregion

        private static void CacheApplicationDataPath()
        {
#if UNITY_EDITOR
            if (PlayerPrefs.HasKey("TestGameDataPath"))
            {
                _applicationDataPath = PlayerPrefs.GetString("TestGameDataPath");
            }
            else
            {
                _applicationDataPath = EditorUtility.OpenFolderPanel("Set test X-Team data directory.", "", "");
                PlayerPrefs.SetString("TestGameDataPath", _applicationDataPath);
            }

            if (!Directory.Exists(_applicationDataPath))
            {
                Directory.CreateDirectory(_applicationDataPath);
            }

            return;
#endif
            _applicationDataPath = Application.dataPath;

            if (!Directory.Exists(_applicationDataPath))
            {
                Directory.CreateDirectory(_applicationDataPath);
            }
        }

        /// <summary>
        ///     Returns the relativePath of game's data directory.
        /// </summary>
        public static string GetApplicationDataPath()
        {
            return _applicationDataPath;
        }

        /// <summary>
        ///     Returns the relativePath of game's cache directory.
        /// </summary>
        public static string GetApplicationCachePath()
        {
            return _applicationTemporaryCachePath;
        }

        /// <summary>
        ///     Returns the relativePath of game's content directory.
        /// </summary>
        public static string GetApplicationContentPath()
        {
            var contentDirectoryPath = GetApplicationDataPath() + "/Content";

            if (!Directory.Exists(contentDirectoryPath))
            {
                Directory.CreateDirectory(contentDirectoryPath);
            }

            return contentDirectoryPath;
        }
        /// <summary>
        /// Replaces all the forbidden characters in the file or folder name with an _ sign.
        /// </summary>
        /// <param name="input">Name of the file/folder.</param>
        /// <returns></returns>
        public static string RemoveForbiddenChars(string input)
        {
            //Caching all the forbidden characters.
            char[] forbiddenChars = new char[]
            {
                '/', ':', '*', '?', '"', '<', '>', '|'
            };
            char replaceTo = '_';

            //Replacing the forbidden characters.
            string output = input;
            foreach (var forbiddenChar in forbiddenChars)
            {
                output = output.Replace(forbiddenChar, replaceTo);
            }

            return output;
        }
    }
}