﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace xTeam.Resources.Utilities.FileManager.XLoad
{
    public class Save
    {
        /// <summary>
        /// Saves s pecified GameObject to the file.
        /// </summary>
        public static void GameObjectToFile(GameObject gameObject, string path)
        {
            //Clear the assetbundle build database.
            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Creating prefab...
            var tempPrefabPath = "Assets" + "/" + gameObject.name + ".prefab";
            PrefabUtility.CreatePrefab(tempPrefabPath, gameObject, ReplacePrefabOptions.Default);

            path = new FileInfo(path).DirectoryName;
            if (path.StartsWith(Application.dataPath))
            {
                path = "Assets" + path.Substring(Application.dataPath.Length);
            }

            //Exporting...
            AssetImporter assetImporter = AssetImporter.GetAtPath(tempPrefabPath);
            assetImporter.assetBundleName = gameObject.name;
            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows);

            //Clear the assetbundle build database.
            names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Delete temp files.
            File.Delete(tempPrefabPath);
            File.Delete(path + "/" + new DirectoryInfo(path).Name);
            File.Delete(path + "/" + new DirectoryInfo(path).Name + ".manifest");
            File.Delete(path + "/" + gameObject.name.ToLower() + ".manifest");
            File.Move(path + "/" + gameObject.name.ToLower(), path + "/" + gameObject.name + ".xto");
        }
    }
}
