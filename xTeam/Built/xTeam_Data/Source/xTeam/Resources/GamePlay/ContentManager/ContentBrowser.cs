﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using xTeam.xTeam_Creator_Classes.Resources;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;
using XTeam.API;
using XTeam.Resources.Utilities.FileManager;
using XTeam.Resources.Utilities.Threading;

namespace XTeam.Resources.GamePlay.ContentManager
{
    /// <summary>
    /// Class for browsing content on disk.
    /// </summary>
    public class ContentBrowser
    {
        /// <summary>
        /// Gets all downloaded ContentPacks.
        /// </summary>
        /// <returns>All downloaded ContentPacks.</returns>
        public static ContentPack[] GetAllContentPacks()
        {
            List<ContentPack> contentPacks = new List<ContentPack>();

            foreach (var contentPackFolder in new DirectoryInfo(FileManager.GetApplicationContentPath()).GetDirectories())
            {
                contentPacks.Add(GetContentPack(contentPackFolder.ToString()));
            }

            return contentPacks.ToArray();
        }
        /// <summary>
        /// Gets all downloaded ContentPacks.
        /// </summary>
        /// <returns>All downloaded ContentPacks.</returns>
        public static void GetAllContentPacksAsync(Action<ContentPack[]> callback)
        {
            UnityThreadHelper.CreateThread(() =>
            {
                List<ContentPack> contentPacks = new List<ContentPack>();

                foreach (var contentPackFolder in new DirectoryInfo(FileManager.GetApplicationContentPath()).GetDirectories())
                {
                    contentPacks.Add(GetContentPack(contentPackFolder.ToString()));
                }

                callback(contentPacks.ToArray());
            }, true);
        }
        /// <summary>
        /// Gets ContentPack at the specified path.
        /// </summary>
        /// <param name="contentPackPath">Path of the searched ContentPack</param>
        /// <returns>Found ContentPack.</returns>
        public static ContentPack GetContentPack(string contentPackPath)
        {
            Debug.Log("Searching for content.xml" +
                      " in " + contentPackPath);
            //Searching for content.xml file inside the contentpack folder.
            DirectoryInfo contentPack = new DirectoryInfo(contentPackPath);
            if (!contentPack.Exists) return null;
            foreach (var file in contentPack.GetFiles())
            {
                if (file.Name == "content.xml")
                {
                    XmlDocument contentPackXml = new XmlDocument();
                    contentPackXml.Load(file.FullName);

                    var contentPackInfo = XmlUtil.Deserialize(contentPackXml, typeof (ContentInfo)) as ContentInfo;

                    return new ContentPack
                    {
                        info = contentPackInfo,
                        path = contentPackPath
                    };
                }
            }

            Debug.LogWarning("Couldn't find content.xml file in ContentPack: " + contentPackPath);
            return null;
        }

        /// <summary>
        /// Gets ContentPack at the specified relativePath.
        /// </summary>
        /// <param name="contentPackPath">Path of the searched ContentPack</param>
        /// <param name="callback">Called when searching finishes.</param>
        /// <returns>Found ContentPack.</returns>
        public static void GetDownloadedContentPackAsync(string contentPackPath, Action<ContentPack> callback)
        {
            Debug.Log("Searching for content.xml in " + contentPackPath);
            UnityThreadHelper.CreateThread(() =>
            {
                //Searching for content.xml file inside the contentpack folder.
                DirectoryInfo contentPack = new DirectoryInfo(contentPackPath);
                if (!contentPack.Exists) callback(null);
                foreach (var file in contentPack.GetFiles())
                {
                    if (file.Name == "content.xml")
                    {
                        XmlDocument contentPackXml = new XmlDocument();
                        contentPackXml.Load(file.FullName);

                        var contentPackInfo = XmlUtil.Deserialize(contentPackXml, typeof(ContentInfo)) as ContentInfo;

                        callback(new ContentPack
                        {
                            info = contentPackInfo,
                            path = contentPackPath
                        });
                    }
                }

                Debug.LogWarning("Couldn't find content.xml file in ContentPack: " + contentPackPath);
                callback(null);
            });
        }

        /// <summary>
        ///     Checks whther a specified content is already downloaded.
        /// </summary>
        /// <param name="contentPackPath">Path of the content.</param>
        /// <param name="contentVersion">Version of the content.</param>
        /// <returns>Whether the specified content is already downloaded.</returns>
        public static bool IsContentDownloaded(string contentPackName, int contentVersion)
        {
            string contentPackPath = GetContentPackPath(contentPackName, ContentExecutor.CLIENT);

            Debug.Log("Checking for ContentPack: " + contentPackName + " existance in " +
                new DirectoryInfo(FileManager.GetApplicationContentPath()) + " folder.");

            var contentPack = GetContentPack(contentPackPath);
            if (contentPack != null && contentPack.info.version == contentVersion)
            {
                Debug.Log("Successfully found ContentPack: " + contentPack.info.name + " with version: " + contentPack.info.version);
                return true;
            }
            else if (contentPack != null)
            {
                Debug.Log("Found ContentPack: " + contentPack.info.name + " but it doesn't have the required version: " + contentPack.info.version + "/" + contentVersion);
                return false;
            }
            else
            {
                Debug.LogWarning("Couldn't find ContentPack: " + contentPackName + " at: " + contentPackPath);
                return false;
            }
        }

        /// <summary>
        ///     Checks whther a specified content is already downloaded asynchronously.
        /// </summary>
        /// <param name="contentName">Name of the content.</param>
        /// <param name="contentVersion">Version of the content.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <returns></returns>
        public static void IsContentDownloadedAsync(string contentPackName, int contentVersion, Action<bool> callback)
        {
            string contentPackPath = GetContentPackPath(contentPackName, ContentExecutor.CLIENT);

            Debug.Log("Checking for ContentPack: " + contentPackName + " existance in " +
                new DirectoryInfo(FileManager.GetApplicationContentPath()) + " folder.");

            UnityThreadHelper.CreateThread(() =>
            {
                var contentPack = GetContentPack(contentPackPath);
                if (contentPack != null && contentPack.info.version == contentVersion)
                {
                    Debug.Log("Successfully found ContentPack: " + contentPack.info.name + " with version: " + contentPack);
                    callback(true);
                }
                else if (contentPack != null)
                {
                    Debug.Log("Found ContentPack: " + contentPack.info.name + " but it doesn't have the required version: " + contentPack.info.version + "/" + contentVersion);
                    callback(false);
                }
                else
                {
                    Debug.LogWarning("Couldn't find ContentPack: " + contentPackName + " at: " + contentPackPath);
                    callback(false);
                }
            }, true);
        }
        /// <summary>
        /// Gets root 
        /// path of the specified contentPackName.
        /// WARNING: Doesn't check whether content exists!
        /// </summary>
        /// <param name="contentPackName">Name of the ContentPack.</param>
        /// <returns>Path of the ContentPack.</returns>
        public static string GetContentPackPath(string contentPackName, ContentExecutor executor)
        {
            switch (executor)
            {
                case ContentExecutor.CLIENT:
                    return FileManager.GetApplicationContentPath() + "/" + FileManager.RemoveForbiddenChars(contentPackName);
                case ContentExecutor.SERVER:
                    foreach (var loadedContent in ContentManager.loadedContent)
                    {
                        if (loadedContent.Key.info.name == contentPackName)
                        {
                            return loadedContent.Key.path;
                        }
                    }
                    break;
            }
            return null;
        }
        /// <summary>
        /// Checks whether content info exists in the specified directory.
        /// </summary>
        /// <param name="contentPath"></param>
        /// <returns></returns>
        public static bool CheckContentXml(string contentPath)
        {
            foreach (var contentInfoFile in Directory.GetFiles(contentPath, "content.xml"))
            {
                return true;
            }
            return false;
        }
    }
}
