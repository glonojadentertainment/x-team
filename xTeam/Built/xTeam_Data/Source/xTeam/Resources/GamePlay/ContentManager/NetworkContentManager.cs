﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager;
using xTeam.xTeam_Creator_Classes.Resources;
using XTeam.API;
using XTeam.Resources.GamePlay.Network.Client.Managers;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.GamePlay.Network.LinkManager.PlayerManager;
using XTeam.Resources.GamePlay.Network.Shared;
using XTeam.Resources.Utilities.FileManager;

namespace XTeam.Resources.GamePlay.ContentManager
{
    /// <summary>
    /// Class managing content over the network.
    /// </summary>
    internal static class NetworkContentManager {
        /// <summary>
        /// Class managing content downloading over the network.
        /// </summary>
        internal class ContentTransferManager
        {
            /// <summary>
            /// Client's content download queue.
            /// </summary>
            public static List<ContentInfo> contentDownloadQueue = new List<ContentInfo>();
            /// <summary>
            /// Target downloaded content count.
            /// </summary>
            public static int contentToDownload;

            /// <summary>
            /// Sends content info of every contentpack on the server to a specified connection.
            /// First sends a TotalContentMessage so client knows how many contentPacks are yet to be downloaded.
            /// </summary>
            /// <param name="playerConnection">Connection to send infos to.</param>
            internal static void SendContentInfos(NetworkConnection playerConnection)
            {
                //Sending content count.
                playerConnection.Send(XMsgTypes.ServerMsgTotalContentCount,
                    new IntegerMessage(ContentManager.loadedContent.Count));

                //Sending content infos.
                foreach (var contentInfo in ContentManager.loadedContent)
                {
                    //Sending current contentPack info.
                    SendContentInfo(contentInfo.Key.info, playerConnection);
                }
            }

            /// <summary>
            /// Sends a single contentInfo to a specified connection.
            /// </summary>
            /// <param name="contentInfo">ContentInfo to send.</param>
            /// <param name="playerConnection">Connection to send the ContentInfo to.</param>
            internal static void SendContentInfo(ContentInfo contentInfo, NetworkConnection playerConnection)
            {
                //Sending content info to player.
                playerConnection.Send(XMsgTypes.ServerMsgContentInfo, contentInfo);
            }

            /// <summary>
            /// Called when a single ContentInfo is received from the server.
            /// </summary>
            /// <param name="msg"></param>
            internal static void OnContentInfoGetFromServer(NetworkMessage msg)
            {
                //Reading the fetched ContentInfo.
                var contentInfo = msg.ReadMessage<ContentInfo>();
                Debug.Log("Fetched content info for " + contentInfo.name + " version: " +
                          contentInfo.version);

                //Checking whether content is already in the local content folder.
                if (ContentBrowser.IsContentDownloaded(contentInfo.name, contentInfo.version))
                {
                    //The content is already downloaded.
                    Debug.Log("Server content: " + contentInfo.name + " version: " + contentInfo.version +
                              " is already downloaded. Loading content from disk.");

                    //Adding content to the load queue.
                    ContentInitializeManager.contentInitializeQueue.Add(contentInfo);
                    //Checking whether all the content was successfully downloaded.
                    if (CheckAllContentDownloaded())
                    {
                        Client.unetClient.Send(XMsgTypes.ClientMsgAllContentDownloaded, new EmptyMessage());
                        return;
                    }
                }
                else
                {
                    //The content hasn't been downloaded yet.
                    Debug.Log("Server content: " + contentInfo.name + " version: " + contentInfo.version +
                              " isn't downloaded yet.");

                    //Adding file to the download queue.
                    contentDownloadQueue.Add(contentInfo);
                }

                //Starting downloading content from the download queue.
                DownloadNextFromQueue();
            }
            /// <summary>
            /// Called when a client sent a content request.
            /// </summary>
            /// <param name="msg"></param>
            internal static void OnClientSentContentRequest(NetworkMessage msg)
            {
                //Reading the message.
                var contentInfo = msg.ReadMessage<ContentInfo>();
                var player = PlayerManager.GetPlayer(msg.conn.connectionId);

                //PlayerInfo wants to get a content file.
                Debug.Log("Player " + player.playerName + " sent content: " + contentInfo.name +
                          " file request. Sending content file.");

                //Getting the contentPack file from local storage and sending it to the client.
                FileSender.SendFile(contentInfo, new DirectoryInfo(ContentBrowser.GetContentPack(ContentBrowser.GetContentPackPath(contentInfo.name, ContentExecutor.SERVER)).path), player);
            }
            /// <summary>
            /// Request specific content pack from the server.
            /// </summary>
            /// <param name="contentInfo">ContentInfo of the requested ContentPack.</param>
            private static void RequestContent(ContentInfo contentInfo)
            {
                Debug.Log("Requesting content: " + contentInfo.name + " version: " + contentInfo.version +
                          " from the server.");
                //Sending the content request.
                Client.unetClient.Send(XMsgTypes.ClientMsgContentRequest, contentInfo);
            }
            /// <summary>
            /// Checks whether all content has been downloaded.
            /// </summary>
            /// <returns></returns>
            private static bool CheckAllContentDownloaded()
            {
                return ContentInitializeManager.contentInitializeQueue.Count == contentToDownload;
            }
            /// <summary>
            /// Called when the FileTransferClient finished the download process and is ready to start another one.
            /// </summary>
            internal static void DownloadNextFromQueue()
            {
                //Requesting next content from the download queue.
                if (contentDownloadQueue.Count != 0)
                {
                    var toRequest = contentDownloadQueue[0];
                    RequestContent(toRequest);
                    contentDownloadQueue.Remove(toRequest);
                    return;
                }

                //Checking whether all content is downloaded.
                if (CheckAllContentDownloaded())
                {
                    Client.unetClient.Send(XMsgTypes.ClientMsgAllContentDownloaded,
                        new EmptyMessage());
                }
            }

            internal static void OnDownloadContentPack(ContentInfo contentInfo)
            {
                //Adding the content to the initialize queue.
                ContentInitializeManager.contentInitializeQueue.Add(contentInfo);

                //Downloading next item from the queue.
                DownloadNextFromQueue();
            }
        }
        /// <summary>
        /// Class managing content initializing over the network.
        /// </summary>
        internal class ContentInitializeManager
        {
            /// <summary>
            /// Client's content initialize queue.
            /// </summary>
            public static List<ContentInfo> contentInitializeQueue = new List<ContentInfo>();

            /// <summary>
            /// Called when server sent LoadAllContent message.
            /// </summary>
            /// <param name="msg"></param>
            public static void OnServerSentLoadAllRequest(NetworkMessage msg)
            {
                //Checking whether there is any content to initialize.
                if (contentInitializeQueue.Count == 0)
                {
                    //Sending the AllContentLoaded message.
                    Client.unetClient.Send(XMsgTypes.ClientMsgAllContentLoaded, new EmptyMessage());
                }
                //Initializing all content in the queue.
                ContentInfo[] toLoad = contentInitializeQueue.ToArray();
                foreach (var contentPack in toLoad)
                {
                    InitializeContent(contentPack, () =>
                    {
                        contentInitializeQueue.Remove(contentPack);

                        //Checking whether all the content has been loaded.
                        if (contentInitializeQueue.Count == 0)
                        {
                            //Sending the AllContentLoaded message.
                            Client.unetClient.Send(XMsgTypes.ClientMsgAllContentLoaded, new EmptyMessage());
                        }
                    });
                }
            }

            /// <summary>
            /// Loads specified content.
            /// </summary>
            /// <param name="contentInfo"></param>
            /// <param name="OnContentInitialized"></param>
            public static void InitializeContent(ContentInfo contentInfo, Action OnContentInitialized)
            {
                Debug.Log("Initializing content: " + contentInfo.name + " version: " + contentInfo.version + "...");
                ContentManager.LoadContentPack(ContentBrowser.GetContentPackPath(contentInfo.name, ContentExecutor.CLIENT), ContentExecutor.CLIENT, OnContentInitialized);
            }
        }

        /// <summary>
        /// Registers client handlers for NetworkContentManager.
        /// </summary>
        public static void RegisterClientHandlers()
        {
            //ContentTransfer handlers.
            Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgContentInfo, ContentTransferManager.OnContentInfoGetFromServer);
            Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgTotalContentCount, msg =>
            {
                ContentTransferManager.contentToDownload = msg.ReadMessage<IntegerMessage>().value;
                Debug.Log("Starting download process for " + ContentTransferManager.contentToDownload + " server ContentPacks...");

                if (ContentTransferManager.contentToDownload == 0)
                {
                    //No content on the server.
                    //Sending all downloaded message.
                    Client.unetClient.Send(XMsgTypes.ClientMsgAllContentDownloaded, new EmptyMessage());
                }
            });
            //ContentInitialize handlers.
            Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgLoadAllContent, ContentInitializeManager.OnServerSentLoadAllRequest);
            Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgAllLoaded, CallbackManager.OnClientLoadServerData);
            //FileTransfer handlers.
            Client.unetClient.RegisterHandler(XMsgTypes.ServerMsgFileTransferPrepare, FileReceiver.OnFileTransferIncoming);
        }
        /// <summary>
        /// Registers server handlers for NetworkContentManager.
        /// </summary>
        public static void RegisterServerHandlers()
        {
            //ContentTransfer handlers.
            NetworkServer.RegisterHandler(XMsgTypes.ClientMsgContentRequest, ContentTransferManager.OnClientSentContentRequest);
            NetworkServer.RegisterHandler(XMsgTypes.ClientMsgAllContentDownloaded, msg =>
            {
                //Sending player a LoadAllContent message.
                msg.conn.Send(XMsgTypes.ServerMsgLoadAllContent, new EmptyMessage());
            });
            //ContentInitialize handlers.
            NetworkServer.RegisterHandler(XMsgTypes.ClientMsgAllContentLoaded, msg =>
            {
                Debug.Log("PlayerInfo " + PlayerManager.GetPlayer(msg.conn.connectionId) + " loaded all the server content.");

                //Sending spawned objects to the client.
                SpawnManager.SendSpawnedObjects(msg.conn);

                //Sending allloaded message to the client.
                msg.conn.Send(XMsgTypes.ServerMsgAllLoaded, new EmptyMessage());
            });
        }
    }
}
