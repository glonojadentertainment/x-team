﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class IPUtility : MonoBehaviour
{
    private static IPAddress _localIp;
    public static IPAddress _publicIp;

    /// <summary>
    ///     Returns local IP adress.
    /// </summary>
    /// <returns></returns>
    public static IPAddress GetLocalIp()
    {
        if (_localIp == null)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)) {
                _localIp = ip;
                Debug.Log("Local IP address found: " + _localIp);
                return _localIp;
            }
            Debug.LogError("Local IP Address not found!");
            return null;
        }
        else
        {
            return _localIp;
        }
    }

    public static IPAddress GetPublicIp()
    {
        if (_publicIp == null)
        {
            Debug.Log("Getting the public IP address...");

            //Using web service to get public IP address.
            try
            {
                var direction = "";
                var request = WebRequest.Create("http://checkip.dyndns.org/");
                using (var response = request.GetResponse())
                using (var stream = new StreamReader(response.GetResponseStream()))
                {
                    direction = stream.ReadToEnd();
                }

                //Search for the ip in the html
                var first = direction.IndexOf("Address: ", StringComparison.Ordinal) + 9;
                var last = direction.LastIndexOf("</body>", StringComparison.Ordinal);
                direction = direction.Substring(first, last - first);

                _publicIp = IPAddress.Parse(direction);
            }
            catch (Exception e)
            {
                //Repeating the IP checking.
                Debug.LogWarning("There was a problem with getting the public IP address: " + e.Message);
                return GetPublicIp();
            }

            Debug.Log("Public IP address found: " + _publicIp);
            return _publicIp;
        }
        else
        {
            return _publicIp;
        }
    }
}