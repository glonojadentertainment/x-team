﻿using UnityEngine;
using UnityEngine.UI;

namespace XTeam.Resources.GamePlay.Network.Server.Creator.MaxPlayers
{
    public class MaxPlayersSlider : MonoBehaviour
    {
        public InputField maxPlayerIndicator;
        public ServerSetup serverSetup;
        public Slider slider;

        /// <summary>
        ///     Called when the slider value has changed.
        /// </summary>
        public void SetValueSlider()
        {
            //Setting new slider value.
            maxPlayerIndicator.text = slider.value.ToString();
            //Setting server's max players value.
            serverSetup.SetServerMaxPlayers(slider.value);
        }
        /// <summary>
        ///     Called when the input value has changed.
        /// </summary>
        public void SetValueInput()
        {
            if(maxPlayerIndicator.text == "") return;

            var value = float.Parse(maxPlayerIndicator.text);
            //Setting slider's value.
            slider.value = value;
            //Setting server's max players value.
            serverSetup.SetServerMaxPlayers(value);
        }
    }
}