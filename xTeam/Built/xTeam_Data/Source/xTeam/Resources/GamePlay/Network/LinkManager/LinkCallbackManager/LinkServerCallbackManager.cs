﻿using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;

namespace XTeam.Resources.GamePlay.Network.LinkManager.LinkCallbackManager
{
    internal class LinkServerCallbackManager : MonoBehaviour
    {
        #region ServerHandlers
        internal void OnServerReady()
        {
            Debug.Log("Server is now ready to join!");
            Debug.Log("");
            Debug.Log("");
            Debug.Log("");
            //Starting local client if server is not dedicated.
            if (!Server.Server.properties.isDedicated)
            {
                Debug.Log("Starting the hosted client...");
                Shared.Client.unetClient = ClientScene.ConnectLocalServer();
                Shared.Client.ConfigureUNETClient(MatchMaker.CreateCurrentServerInfo(), Server.Server.connectionConfig);
            }
            //Starting the MatchUpdater based on the Server properties.
            MatchUpdater.StartUpdating(Server.Server.properties.useMatchmaker, true);
        }
        #endregion
    }

}