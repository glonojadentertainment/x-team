﻿using UnityEngine;
using UnityEngine.Networking;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts.AvatarManager
{
    /// <summary>
    /// Class managing player's current avatar.
    /// </summary>
    public class AvatarManager : NetworkBehaviour
    {
        [Tooltip("Currently used avatar.")]
        public Avatar currentAvatar;
    }
}
