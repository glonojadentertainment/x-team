﻿using UnityEngine;

namespace XTeam.Resources.GamePlay.Network.Server.Creator
{
    /// <summary>
    /// Class for setting up the server.
    /// </summary>
    public class ServerSetup : MonoBehaviour
    {   
        /// <summary>
        /// Sets server name.
        /// </summary>
        /// <param name="name">Name of the server.</param>
        public void SetServerName(string name)
        {
            Server.properties.serverName = name;
        }
        /// <summary>
        /// Sets server description.
        /// </summary>
        /// <param name="description"></param>
        public void SetServerDescription(string description)
        {
            Server.properties.serverDescription = description;
        }
        /// <summary>
        /// Sets server address.
        /// </summary>
        /// <param name="address"></param>
        public void SetServerAddress(string address)
        {
            Server.properties.serverAddress = address;
        }
        /// <summary>
        /// Sets server port.
        /// </summary>
        /// <param name="port"></param>
        public void SetServerPort(string port)
        {
            Server.properties.serverPort = int.Parse(port);
        }
        /// <summary>
        /// Sets server maxplayers.
        /// </summary>
        /// <param name="players"></param>
        public void SetServerMaxPlayers(float players)
        {
            Server.properties.serverSize = (int)players;
        }
        /// <summary>
        /// Sets whether the server is public.
        /// </summary>
        /// <param name="isPublic">Whether the server is public.</param>
        public void SetServerPublic(bool isPublic)
        {
            Server.properties.useMatchmaker = isPublic;
        }
        /// <summary>
        /// Starts the server.
        /// </summary>
        public void StartServer()
        {
            Server.StartServer();
        }
    }
}
