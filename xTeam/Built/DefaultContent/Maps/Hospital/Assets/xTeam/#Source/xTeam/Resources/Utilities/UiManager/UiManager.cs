﻿using System.Collections;
using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager
{
    public class UiManager : MonoBehaviour {
        /// <summary>
        /// Instance of the UiManager.
        /// </summary>
        public static UiManager instance;

        #region Monobehaviour
        void Start()
        {
            instance = this;
        }
        #endregion
        #region Smooths
        /// <summary>
        /// Smoothly changes the value of result float, from source float to target float over specified time.
        /// </summary>
        /// <param name="source">Beginning balue of the transition.</param>
        /// <param name="target">Target value of the transition.</param>
        /// <param name="overTime">Time of the transition in seconds.</param>
        /// <param name="result">Affected float.</param>
        /// <returns></returns>
        public static IEnumerator Smooth(float source, float target, float overTime, System.Action<float> result)
        {
            float startTime = Time.time;
            while (Time.time < startTime + overTime && result.Method.GetParameters()[0].IsDefined(typeof(float), false))
            {
                result(Mathf.Lerp(source, target, (Time.time - startTime) / overTime));
                yield return null;
            }
            result(target);
            yield break;
        }
        /// <summary>
        /// Smoothly changes the value of result Vector3, from source Vector3 to target Vector3 over specified time.
        /// </summary>
        /// <param name="source">Beginning value of the transition.</param>
        /// <param name="target">Target value of the transition.</param>
        /// <param name="overTime">Time of the transition in seconds.</param>
        /// <param name="result">Affected Vector3.</param>
        /// <returns></returns>
        public static IEnumerator Smooth(Vector3 source, Vector3 target, float overTime, System.Action<Vector3> result)
        {
            float startTime = Time.time;
            while (Time.time < startTime + overTime && result.Method.GetParameters()[0].IsDefined(typeof(Vector3), false))
            {
                result(Vector3.Lerp(source, target, (Time.time - startTime) / overTime));
                yield return null;
            }
            result(target);
            yield break;
        }
        /// <summary>
        /// Smoothly changes the value of result Quaternion, from source Quaternion to target Quaternion over specified time.
        /// </summary>
        /// <param name="source">Beginning value of the transition.</param>
        /// <param name="target">Target value of the transition.</param>
        /// <param name="overTime">Time of the transition in seconds.</param>
        /// <param name="result">Affected Quaternion.</param>
        /// <returns></returns>
        public static IEnumerator Smooth(Quaternion source, Quaternion target, float overTime, System.Action<Quaternion> result)
        {
            var startTime = Time.time;
            while (Time.time < startTime + overTime && result.Method.GetParameters()[0].IsDefined(typeof(Quaternion), false))
            {
                result(Quaternion.Lerp(source, target, (Time.time - startTime) / overTime));
                yield return null;
            }
            result(target);
            yield break;
        }
        #endregion
    }
}
