﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using XTeam.Resources.Utilities;

namespace XTeam.Resources.Menus.Hub.UI.TopBar.User_Data
{
    internal class UserDataWindow : MonoBehaviour {
        [Tooltip("Username of the current user.")]
        public Text userName;
        [Tooltip("Account type of the current user.")]
        public Text userType;
        [Tooltip("Avatar of the current user.")]
        public RawImage userAvatar;

        void Awake()
        {
            PlayerIdentity.OnPlayerIdentityChange = OnPlayerDataChanged;
        }
        /// <summary>
        /// Called when player data is changed.
        /// </summary>
        private void OnPlayerDataChanged()
        {
            userName.text = PlayerIdentity.playerName;
            userType.text = PlayerIdentity.playerType.ToString();
            StartCoroutine(DownloadAvatar(PlayerIdentity.playerAvatarUrl));
        }
        /// <summary>
        /// Called when the user clicks player data box.
        /// </summary>
        public void OnUserBoxClick()
        {
            //Opening GameJolt login menu.
            GameJolt.UI.Manager.Instance.ShowSignIn(PlayerIdentity.OnGameJoltLogin);
        }

        private IEnumerator DownloadAvatar(string avatarUrl)
        {
            var www = new WWW(avatarUrl);
            yield return www;
            userAvatar.material.mainTexture = www.texture;
        }
    }
}
