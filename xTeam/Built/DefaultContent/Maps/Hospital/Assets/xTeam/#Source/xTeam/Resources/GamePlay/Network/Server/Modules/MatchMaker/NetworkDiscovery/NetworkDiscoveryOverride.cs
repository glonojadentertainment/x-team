﻿using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;
using XTeam.Resources.Menus.HUB.ServerList;

namespace XTeam.Resources.GamePlay.Network.Server.MatchMaker.NetworkDiscovery
{
    public class NetworkDiscoveryOverride : UnityEngine.Networking.NetworkDiscovery {
        /// <summary>
        ///     Called when a new server is found on the local network.
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="data"></param>
        public override void OnReceivedBroadcast(string fromAddress, string data)
        {
            var foundMatch = Modules.MatchMaker.MatchMaker.DeserializeServerInfo(data);

            //Getting the ServerInfo class.
            ServerListManager.instance.OnServerFound(new MatchGetResponse {
                server = foundMatch,
                serverNetworkType = ServerNetworkType.LAN,
                success = true
                });
        }
    }
}
