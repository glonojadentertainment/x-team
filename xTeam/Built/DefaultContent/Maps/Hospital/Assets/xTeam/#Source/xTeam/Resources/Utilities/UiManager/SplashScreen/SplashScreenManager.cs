﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager.SplashScreen
{
    public class SplashScreenManager : MonoBehaviour
    {
        /// <summary>
        /// Instance of this component.
        /// </summary>
        public static SplashScreenManager instance;

        [Tooltip("Registered splash screens.")]
        public List<SplashScreen.SplashScreen> splashScreens = new List<SplashScreen.SplashScreen>();
        [Tooltip("Holder object of all the splash screens.")]
        public GameObject splashScreenHolder;

        #region Monobehaviour
        void Awake()
        {
            instance = this;
        }
#endregion
        /// <summary>
        /// Shows a specified splash screen for a specified amount of seconds.
        /// </summary>
        /// <param name="splashScreenName">Splash screen to be displayed.</param>
        /// <param name="seconds">How long in seconds, should the splash screen stay at the screen.</param>
        public static IEnumerator ShowSplashScreen(string splashScreenName, float seconds)
        {
            //Searching for the specified splashscreen.
            foreach (var splashScreen in instance.splashScreens)
            {
                if (splashScreen.screenName == splashScreenName)
                {
                    //Spawning the splash screen.
                    var newSplashScreenGo = (GameObject)Instantiate(splashScreen.gameObject);
                    DontDestroyOnLoad(newSplashScreenGo);
                    var newSplashScreen = newSplashScreenGo.GetComponent<SplashScreen.SplashScreen>();

                    //Making the spawned splash screen visible.
                    instance.StartCoroutine(UiManager.Smooth(0, 1f,
                        0.2f, value => newSplashScreen.canvasGroup.alpha = value));

                    yield return new WaitForSeconds(seconds);

                    newSplashScreen.Destroy();
                }
            }
        }
        /// <summary>
        /// Spawns a specified splashscreen.
        /// </summary>
        /// <param name="splashScreenName">Splash screen to be spawned.</param>
        /// <returns></returns>
        public static SplashScreen.SplashScreen ShowSplashScreen(string splashScreenName)
        {
            //Searching for the specified splashscreen.
            foreach (var splashScreen in instance.splashScreens)
            {
                if (splashScreen.screenName == splashScreenName)
                {
                    //Spawning the splash screen.
                    var newSplashScreenGo = (GameObject)Instantiate(splashScreen.gameObject);
                    DontDestroyOnLoad(newSplashScreenGo);
                    var newSplashScreen = newSplashScreenGo.GetComponent<SplashScreen.SplashScreen>();

                    //Making the spawned splash screen visible.
                    instance.StartCoroutine(UiManager.Smooth(0, 1f,
                        0.2f, value => newSplashScreen.canvasGroup.alpha = value));

                    return newSplashScreen;
                }
            }

            return null;
        }
    }
}
