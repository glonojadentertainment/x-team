﻿using UnityEngine;
using UnityEngine.Networking;

namespace XTeam.API.Objects.Items
{
    public class DroppedItem : NetworkBehaviour {

        [SyncVar] [Tooltip ("Name of the in-game item. This is already set up on the prefabs.")]
        public string itemName;

        [SyncVar] [HideInInspector]
        public string itemCustomName;

        [SyncVar] [HideInInspector]
        public string ownersGuid;
    }
}
