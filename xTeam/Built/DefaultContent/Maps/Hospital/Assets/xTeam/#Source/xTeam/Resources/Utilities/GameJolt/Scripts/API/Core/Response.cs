﻿using UnityEngine;
using GameJolt.External.SimpleJSON;

namespace GameJolt.API.Core
{
	public enum ResponseFormat { Dump, Json, Raw, Texture }

	public class Response
	{			
		public readonly ResponseFormat format;
		public readonly bool success = false;
		public readonly byte[] bytes = null;
		public readonly string dump = null;
		public readonly JSONNode json = null;
		public readonly Texture2D texture = null;

		public Response(string errorMessage) {
			success = false;
			Debug.LogWarning(errorMessage);
		}
		
		public Response(WWW www, ResponseFormat format = ResponseFormat.Json)
		{
			if (www.error != null)
			{
				success = false;
				Debug.LogWarning(www.error);
				return;
			}

			this.format = format;

			switch (format)
			{
			case ResponseFormat.Dump:
				success = www.text.StartsWith("SUCCESS");

				var returnIndex = www.text.IndexOf ('\n');
				if (returnIndex != -1)
				{
					dump = www.text.Substring(returnIndex + 1);
				}

				if (!success)
				{
					Debug.LogWarning(dump);
					dump = null;
				}

				break;
				
			case ResponseFormat.Json:
				json = JSON.Parse(www.text)["response"];
				success = json["success"].AsBool;

				if (!success)
				{
					Debug.LogWarning(json["message"]);
					json = null;
				}

				break;
			
			case ResponseFormat.Raw:
				success = true;
				bytes = www.bytes;

				break;

			case ResponseFormat.Texture:
				success = true;
				texture = www.texture;

				break;

			default:
				success = false;
				Debug.LogWarning("Unknown format. Cannot process response.");

				break;
			}
		}
	}
}
