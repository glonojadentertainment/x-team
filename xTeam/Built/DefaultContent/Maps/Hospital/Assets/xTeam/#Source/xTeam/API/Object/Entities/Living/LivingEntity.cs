﻿using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Characters.StandardCharacter.Scripts;

namespace XTeam.API.Objects.Entities.Living
{
    /// <summary>
    /// Interface for all living entities.
    /// </summary>
    public interface ILivingEntity : IEntity
    {
        Health GetHealth();
        Inventory GetInventory();
    }
}
