﻿using System.IO;
using UnityEditor;
using UnityEngine;
using Vexe.FastSave;

namespace xTeam.xTeam_Creator.ObjectExporter.Editor
{
    public class ObjectExporter : MonoBehaviour
    {
        [MenuItem("xTeam/Object/Bake selected object")]
        public static void BakeSelectedObject()
        {
            //Making a working copy of the object and passing it to the BakeGameObject.
            BakeGameObject(Selection.activeGameObject, EditorUtility.SaveFilePanelInProject("Save object file", Selection.activeGameObject.name, "xto", ""));
        }
        [MenuItem("Assets/Load xTeam object")]
        public static void LoadObject()
        {
            //Loading and spawning the selected object.
            Load.HierarchyFromFile(AssetDatabase.GetAssetPath(Selection.activeObject), new GameObject(Selection.activeObject.name));
        }
        /// <summary>
        /// Bakes the specified GameObject to file.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="dir"></param>
        public static void BakeGameObject(GameObject gameObject, string path)
        {
            //Saving the object.
            Save.HierarchyToFile(path, gameObject);

            //Refreshing the assets.
            AssetDatabase.Refresh();

            Debug.Log("Baking complete!");
        }
        [MenuItem("Assets/Load xTeam object", true)]
        public static bool ValidateXTO()
        {
            if (new FileInfo(AssetDatabase.GetAssetPath(Selection.activeObject)).Extension == ".xto")
            {
                return true;
            }
            return false;
        }
    }
}