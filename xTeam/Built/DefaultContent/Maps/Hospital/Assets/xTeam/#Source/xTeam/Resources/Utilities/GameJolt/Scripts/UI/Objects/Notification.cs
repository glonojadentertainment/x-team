﻿using UnityEngine;

namespace GameJolt.UI.Objects
{
	public class Notification
	{
		#region Fields & Properties
		public string Text { get; set; }
		public Sprite Image { get; set; }
		#endregion Fields & Properties
		
		#region Constructors
		public Notification(string text)
		{
			Text = text;

			var tex = Resources.Load(API.Constants.DEFAULT_NOTIFICATION_ASSET_PATH) as Texture2D;
			Image = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(.5f, .5f), tex.width);
		}

		public Notification(string text, Sprite image)
		{
			Text = text;
			Image = image;
		}
		#endregion Constructors
	}
}
