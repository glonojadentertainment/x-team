﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ionic.Zip;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using xTeam.xTeam_Creator.Resources;
using xTeam.xTeam_Creator_Classes.Resources;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.Client.Player;
using XTeam.Resources.GamePlay.Network.LinkManager;
using XTeam.Resources.GamePlay.Network.Server;
using XTeam.Resources.Utilities.Threading;
using XTeam.Resources.Utilities.UiManager.ProgressBar;
using XTeam.Resources.Utilities.UiManager.ProgressBar.ProgressBar;

namespace XTeam.Resources.Utilities.FileManager
{
    /// <summary>
    ///     Class for receiving files using the Socket method.
    /// </summary>
    internal static class FileReceiver
    {
        /// <summary>
        ///     Progress bar showing file receiving progress.
        /// </summary>
        private static ProgressBar _progressBar;

        /// <summary>
        ///     The TCP listener that will listen for connections.
        /// </summary>
        private static TcpListener _tcpServer;
        /// <summary>
        ///     Thread on which the listening script is located.
        /// </summary>
        internal static ActionThread thrListen;

        /// <summary>
        /// Whether the client is currently receiving content.
        /// </summary>
        public static bool isReceiving = false;

        /// <summary>
        /// Called when the server wants to send the client a file.
        /// </summary>
        /// <param name="msg"></param>
        internal static void OnFileTransferIncoming(NetworkMessage msg)
        {
            //Starting the FileTransfer server.
            Debug.Log("FileTransfer incoming...");
            StartReceiving();
        }
        /// <summary>
        ///     Starts listening for incoming file transfers.
        /// </summary>
        private static void StartReceiving()
        {
            //Creating a progressbar.
            _progressBar = ProgressbarManager.NewProgressBar();

            thrListen = UnityThreadHelper.CreateThread(() =>
            {
                //Start the TCP listener and listen for connections.
                _tcpServer.Start();
                //Accepting pending connections.
                var client = _tcpServer.AcceptTcpClient();
                var ns = client.GetStream();

                //Getting the sent content info.
                long contentLenght;
                string contentInfoString;
                {
                    var fileNameLengthBytes = new byte[4]; //int32
                    var fileLengthBytes = new byte[8]; //int64

                    ns.Read(fileLengthBytes, 0, 8); // int64
                    ns.Read(fileNameLengthBytes, 0, 4); // int32
                    var fileNameBytes = new byte[BitConverter.ToInt32(fileNameLengthBytes, 0)];
                    ns.Read(fileNameBytes, 0, fileNameBytes.Length);

                    contentLenght = BitConverter.ToInt64(fileLengthBytes, 0);
                    contentInfoString = Encoding.ASCII.GetString(fileNameBytes);
                }
                var contentInfo = JsonUtility.FromJson<ContentInfo>(contentInfoString);
                UnityThreadHelper.Dispatcher.Dispatch(() =>
                {
                    Debug.Log("Downloading server content: " + contentInfo.name + " version: " +
                        contentInfo.version + "...");
                });

                //Getting the file save location.
                var downloadDirectory =
                    new DirectoryInfo(FileManager.GetApplicationContentPath());
                //Creating a new FileStream in the selected location.
                var f = new FileInfo(downloadDirectory.FullName + "/" + FileManager.RemoveForbiddenChars(contentInfo.name) + ".xtm");

                //Checking whether same file already exist.
                if (f.Exists)
                {
                    //Deleting the duplicate file.
                    File.Delete(f.FullName);
                }
                //Opening a new filestream.
                var fileStream = File.Open(f.FullName, FileMode.Create);

                //Giving server permission to start the transfer.
                ns.WriteByte(1);
                UnityThreadHelper.Dispatcher.Dispatch(() =>
                {
                    Debug.Log("Receiving file: " + contentInfo.name + " from the server...");
                });

                //Receiving a fragmented file data.
                int read;
                var totalRead = 0;
                var buffer = new byte[32*1024]; // 32k chunks
                while ((read = ns.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, read);
                    totalRead += read;
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        _progressBar.SetProgress(totalRead/(float) contentLenght);
                    });
                }
                fileStream.Dispose();

                //Extracting the file.
                using (ZipFile zip = ZipFile.Read(f.FullName))
                {
                    zip.ExtractAll(FileManager.GetApplicationContentPath(), ExtractExistingFileAction.OverwriteSilently);
                };
                f.Delete();

                //Initializing downloaded content
                UnityThreadHelper.Dispatcher.Dispatch(() =>
                {
                    Debug.Log("Content: " + contentInfo.name + " version: " + contentInfo.version + "(" + contentLenght +
                    "b) successfully received from the server and saved in: " + f.FullName);
                    //Setting the isReceiving flag.
                    isReceiving = false;
                    //Calling the OnFinishedDownloadingLastContent event on the ContentTransferManager.
                    NetworkContentManager.ContentTransferManager.OnDownloadContentPack(contentInfo);

                    _progressBar.Destroy();
                });

                //Closing all the streams and connections.
                client.Close();
            }, false);

            // If the TCP listener object was not created before, create it.
            if (_tcpServer == null)
            {
                // Create the TCP listener object using the IP of the server and the specified port
                _tcpServer = new TcpListener(IPUtility.GetLocalIp(), Server.properties.fileTransferPort);
            }
            //Setting the isReceiving flag.
            isReceiving = true;
            //Start client listening thread.
            thrListen.Start();
            Debug.Log("A FileTransfer server has started on port: " + Server.properties.fileTransferPort);
        }
    }

    /// <summary>
    ///     Class for sending files using the Socket method.
    /// </summary>
    internal static class FileSender
    {
        internal static ActionThread thrSend;

        public static void SendFile(ContentInfo info, DirectoryInfo contentPackToSend, Player player)
        {
            //Initializing a sending thread.
            thrSend = UnityThreadHelper.CreateThread(() =>
            {
                var sendAddress = player.GetConnectionAddress();
                Debug.Log("Preparing a new file transfer for: " + sendAddress);
                //Parsing receiver's IP address.
                IPAddress address;
                if (!IPAddress.TryParse(sendAddress, out address))
                {
                    Debug.LogError("Error with IP Address");
                    return;
                }
                address = IPAddress.Parse(sendAddress);

                FileInfo fileToSend = null;
                //Zipping the contentPack.
                try
                {
                    using (var zip = new ZipFile())
                    {
                        //Adding content folder to the zip.
                        Debug.Log("Zipping the content pack: " + contentPackToSend);
                        var contentPackEntry = zip.AddDirectory(contentPackToSend.ToString(), FileManager.RemoveForbiddenChars(info.name));
                        contentPackEntry.Comment = "The content folder itself.";

                        zip.Comment = "X-Team compressed content file.";

                        fileToSend = new FileInfo(FileManager.GetApplicationCachePath() + "/" + FileManager.RemoveForbiddenChars(info.name) + ".xtm");
                        if (fileToSend.Exists)
                        {
                            fileToSend.Delete();
                        }
                        zip.Save(fileToSend.FullName);
                    }
                }
                catch (Exception ex1)
                {
                    Debug.LogError("Exception creating a ZIP file: " + ex1);
                }

                if (fileToSend == null || !fileToSend.Exists)
                {
                    Debug.LogError("Error when finding zipped content file.");
                    return;
                }

                //Creating a new filestream to pull data off.
                FileStream fileStream;
                try
                {
                    fileStream = fileToSend.OpenRead();
                }
                catch
                {
                    Debug.LogError("Error opening file: " + fileToSend);
                    return;
                }

                //Connecting to destination client.
                Debug.Log("Connecting to destination client: " + address);
                var client = new TcpClient();
                try
                {
                    //Checking whether the send ip is our ip in order to send to local host.
                    Debug.Log("Connecting to " + address  + ":" + Server.properties.fileTransferPort);
                    client.Connect(address, Server.properties.fileTransferPort);
                }
                catch
                {
                    Debug.LogError("Error connecting to destination");
                    return;
                }
                
                //Getting client's NetworkStream.
                var ns = client.GetStream();

                //Sending FileInfo to the client.
                Debug.Log("Sending content info...");
                {
                    var contentInfoBytes = Encoding.ASCII.GetBytes(JsonUtility.ToJson(info));
                    var contentInfoLenghtBytes = BitConverter.GetBytes(contentInfoBytes.Length);
                    var fileLength = BitConverter.GetBytes(fileToSend.Length);
                    ns.Write(fileLength, 0, fileLength.Length);
                    ns.Write(contentInfoLenghtBytes, 0, contentInfoLenghtBytes.Length);
                    ns.Write(contentInfoBytes, 0, contentInfoBytes.Length);
                }

                //Getting permission for data transfer.
                Debug.Log("Getting permission...");
                {
                    var permission = new byte[1];
                    ns.Read(permission, 0, 1);
                    if (permission[0] != 1)
                    {
                        Debug.LogError("Permission denied, aborting the sending process and kicking player...");
                        //Disposing stream and closing connections.
                        fileStream.Dispose();
                        UnityThreadHelper.Dispatcher.Dispatch(() =>
                        {
                            player.networkIdentity.clientAuthorityOwner.Disconnect();
                        });
                        client.Close();
                        thrSend.Dispose();
                        return;
                    }
                }

                //Sending file data.
                Debug.Log("Sending file data...");
                int read;
                var buffer = new byte[32*1024]; // 32k chunks
                while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ns.Write(buffer, 0, read);
                }
                Debug.Log("Sending complete!");

                //Disposing stream and closing connections.
                fileStream.Dispose();
                client.Close();
                thrSend.Dispose();
            }, false);

            //Sending client file transfer pending message.
            player.networkIdentity.clientAuthorityOwner.Send(XMsgTypes.ServerMsgFileTransferPrepare, new EmptyMessage());

            //Starting the thread.
            thrSend.Start();
        }
    }
}