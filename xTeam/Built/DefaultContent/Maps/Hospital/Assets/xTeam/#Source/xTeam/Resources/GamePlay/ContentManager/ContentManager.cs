﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using xTeam.xTeam_Creator.Resources;
using xTeam.xTeam_Creator_Classes.Resources;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;
using XTeam.API;
using CodeCompiler = CSharpCompiler.CodeCompiler;

namespace XTeam.Resources.GamePlay.ContentManager
{
    /// <summary>
    /// Loads game contentInfo into the game.
    /// </summary>
    public static class ContentManager {
        /// <summary>
        /// List of all the currently loaded contentInfo.
        /// </summary>
        public static Dictionary<ContentPack, XContent> loadedContent = new Dictionary<ContentPack, XContent>();

        /// <summary>
        /// Loads a ContentPack located at the specified relativePath.
        /// Compiles the ContentPack and executes the OnEnable method on the main class.
        /// </summary>
        /// <param name="path">Path to the contentInfo.</param>
        /// <param name="executor">Executor of the contentInfo.</param>
        public static void LoadContentPack(string path, ContentExecutor executor, Action callback)
        {
            Debug.Log("Loading content from: " + path);
            //Checking if the relativePath is valid.
            if (path != null && Directory.Exists(path))
            {
                //Compiling all the script files in the directory to an assembly.
                Assembly compiledContent = CompileContentPack(path);
                var contentType = typeof(XContent);

                //Searching for XContent class in the assembly.
                if (compiledContent != null)
                {
                    var types = compiledContent.GetTypes();

                    foreach (var type in types)
                    {
                        if (!type.IsInterface && !type.IsAbstract)
                        {
                            if (type.IsSubclassOf(contentType))
                            {
                                //Caching the found contentInfo.
                                var content = (XContent)Activator.CreateInstance(type);

                                //Checking for content.xml
                                if (!ContentBrowser.CheckContentXml(path))
                                {
                                    CreateContentXml(path, content.GetInfo());
                                }
                                loadedContent.Add(new ContentPack
                                {
                                    info = content.GetInfo(),
                                    path = path
                                }, content);

                                //Setting the XContent properties.
                                content.runningOn = executor;
                                //Enabling the found contentInfo.
                                content.OnEnable();

                                //Invoking the callback.
                                if (callback == null) continue;
                                callback.Invoke();
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogError("An unknown error occured when loading content from: " + path);
                    //Invoking the callback.
                    if (callback != null)
                    {
                        callback.Invoke();
                    }
                }
            }
        }
        /// <summary>
        /// Unloads a specific contentInfo.
        /// </summary>
        /// <param name="contentInfo">Content to be unloaded.</param>
        public static void UnloadContentPack(ContentPack contentPack, Action<bool> callback)
        {
            XContent content;
            loadedContent.TryGetValue(contentPack, out content);
            content.OnDisable();
            loadedContent.Remove(contentPack);

            //Invoking the callback.
            if (callback != null && callback.Method.GetParameters()[0].IsDefined(typeof(bool), false))
            {
                callback(true);
            }
        }

        /// <summary>
        ///     Loads all content in the specified directory.
        /// </summary>
        public static void LoadAllContentInDir(string directory, ContentExecutor executor)
        {
            //Loading each of the plugins in plugins folder.
            foreach (var contentPack in Directory.GetDirectories(directory))
            {
                LoadContentPack(contentPack, executor, null);
            }
        }
        /// <summary>
        ///     Loads all content from the specified array of paths.
        /// </summary>
        public static void LoadAllContent(string[] paths, ContentExecutor executor)
        {
            //Loading each of the plugins in plugins folder.
            foreach (var contentPack in paths)
            {
                LoadContentPack(contentPack, executor, null);
            }
        }

        /// <summary>
        ///     Unloads all contentInfo in the contentInfo folder.
        /// </summary>
        public static void UnloadAllContent()
        {
            //Unloading each of the contentInfo in contentInfo folder.
            foreach (var content in loadedContent)
            {
                UnloadContentPack(content.Key, null);
            }
        }
        /// <summary>
        /// Creates a new content.xml file in the content directory.
        /// </summary>
        /// <param name="content"></param>
        public static void CreateContentXml(string path, ContentInfo info)
        {
            XmlUtil.Serialize(info).Save(path + "/" + "content.xml");
        }
        /// <summary>
        /// Compiles all the .cs files within a specified ContentPack.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>Compiled assembly</returns>
        public static Assembly CompileContentPack(string path)
        {
            //Setting the CompilerSettings.
            CompilerParameters parameters = new CompilerParameters
            {
                GenerateExecutable = false,
                GenerateInMemory = true
            };

            // Add ALL of the assembly references
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    parameters.ReferencedAssemblies.Add(assembly.Location);
                }
                catch (Exception) {}
            }

            // Add specific assembly references
            //parameters.ReferencedAssemblies.Add("System.dll");
            //parameters.ReferencedAssemblies.Add("CSharp.dll");
            //parameters.ReferencedAssemblies.Add("UnityEngine.dll");

            //Getting an array of scripts to add.
            var scriptsToCompile = CsSearch(path);

            Debug.Log("Compiling ContentPack at: " + path);
            foreach (var script in scriptsToCompile)
            {
                Debug.Log("Including: " + script);
            }
            //Compiling the .cs file.
            CodeCompiler codeCompiler = new CodeCompiler();
            CompilerResults results = codeCompiler.CompileAssemblyFromFileBatch(parameters, scriptsToCompile);

            //Checking for errors.
            if (results.Errors.Count != 0)
            {
                Debug.LogError("Errors while loading contentInfo!");
                foreach (var error in results.Errors)
                {
                    Debug.LogError(error.ToString());
                }
                return null;
            }

            //Returning the compiled contentInfo assembly.
            return results.CompiledAssembly;
        }

        /// <summary>
        /// Searches for .cs files in the ContentPack.
        /// </summary>
        /// <param name="contentPackPath"></param>
        /// <returns></returns>
        private static string[] CsSearch(string contentPackPath)
        {
            List<string> files = new List<string>();

            try
            {
                foreach (FileInfo f in new DirectoryInfo(contentPackPath).GetFiles())
                {
                    if (f.Name.StartsWith("#")) continue;
                    if(f.Extension != ".cs") continue;
                    files.Add(f.FullName);
                }
                foreach (var d in Directory.GetDirectories(contentPackPath))
                {
                    if(new DirectoryInfo(d).Name.StartsWith("#")) continue;
                    if(new DirectoryInfo(d).Name == "Editor") continue;
                    files.AddRange(CsSearch(d));
                }
            }
            catch (Exception excpt)
            {
                Debug.LogError(excpt.Message);
            }

            return files.ToArray();
        }
    }
}