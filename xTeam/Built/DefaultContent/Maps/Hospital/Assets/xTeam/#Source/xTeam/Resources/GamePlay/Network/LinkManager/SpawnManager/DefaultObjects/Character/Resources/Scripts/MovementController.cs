﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts.AvatarManager;
using XTeam.Resources.GamePlay.Network.Shared;
using XTeam.Resources.Utilities.InputManager.cMonkeys;
using XTeam.Resources.Utilities.UiManager;

namespace Assets.xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class MovementController : NetworkBehaviour
    {
        //Components
        public CapsuleCollider collider;
        public Rigidbody rigidbody;
        public Transform camera;
        public AvatarManager avatarManager;
        public bool controlsActive;

        //Positions settings.
        [SerializeField]
        public PositionSettings walkSettings = new PositionSettings();
        [SerializeField]
        public PositionSettings crouchSettings = new PositionSettings();
        [SerializeField]
        public PositionSettings flySettings = new PositionSettings();
        [SerializeField]
        public PositionSettings proneSettings = new PositionSettings();
        public bool canSprint = true;

        public float heightChangeTime = 0.5f;
        public float airControl = 0.5f;
        public float jumpForce = 2.0f;
        public float maxVelocityChange = 10.0f;

        public float slideLimit = 70f;
        public float speed = 10.0f;
        public float sprintMultiplier = 1.5f;

        //Input caching.
        //Flag will be triggered when the input is received.
        //As soon as the movement is processed these values will be reset.
        [SerializeField]
        private float horizontal;
        [SerializeField]
        private float vertical;
        [SerializeField]
        private bool sprint;
        [SerializeField]
        private bool jump;
        [SerializeField]
        private bool stand;
        [SerializeField]
        private bool crouch;
        [SerializeField]
        private bool prone;
        [SerializeField]
        private bool grounded;
        [SerializeField]
        private bool landed;
        [SyncVar]
        public PlayerPosition currentPlayerPosition;

        //Misc
        private Vector3 _contactPoint;
        [SerializeField]
        private float _currentRayDistance;

        #region Setup
        #endregion
        #region MovementProcessing
        /// <summary>
        /// Runs every game tick.
        /// </summary>
        private void FixedUpdate()
        {
            if (isServer || Client.unetClient == null || !Client.unetClient.isConnected)
            {
                //Performing cached actions.
                ProcessCachedActions();

                //Getting input from player cache.
                var horizontalInput = horizontal;
                var verticalInput = vertical;

                //Checking whether character is grounded.
                if (grounded)
                {
                    //Processing on ground movement.
                    ProcessMovement(horizontalInput, verticalInput);
                }
                else
                {
                    //Processing in air movement.
                    ProcessAirControl(horizontalInput, verticalInput);
                }
            }

            //Performing all the checks.
            //Checking current character height.
            CheckHeight();
        }
        /// <summary>
        /// Processes character on-ground movement.
        /// </summary>
        /// <param name="horizontalInput">Horizontal input</param>
        /// <param name="verticalInput">Vertical input</param>
        private void ProcessMovement(float horizontalInput, float verticalInput)
        {
            //Creating a currentSpeed variable.
            //Used to calculate appropriate speed for the character.
            var currentSpeed = ProcessSpeed(speed);

            //Calculating player slide.
            //If sliding, block all the player movement.
            if (CheckSliding())
            {
                return;
            }

            //Proccessing character's movement based on current position.
            switch (currentPlayerPosition)
            {
                case PlayerPosition.STANDING:
                    //Character is in a standing position.

                    //Checking if we're sprinting.
                    if (canSprint && sprint)
                    {
                        currentSpeed *= sprintMultiplier;
                    }

                    //Calculating the player speed.
                    horizontalInput *= currentSpeed;
                    verticalInput *= currentSpeed;

                    //Applying the calculated force to the character.
                    rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                        ForceMode.VelocityChange);
                    break;
                case PlayerPosition.CROUCHING:
                    //Character is in a crouching position.

                    //Calculating the player speed.
                    horizontalInput *= currentSpeed;
                    verticalInput *= currentSpeed;

                    //Applying the calculated force to the character.
                    rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                        ForceMode.VelocityChange);
                    break;
                case PlayerPosition.PRONING:
                    //Character is in a proning position.

                    //Calculating the player speed.
                    horizontalInput *= currentSpeed;
                    verticalInput *= currentSpeed;

                    //Applying the calculated force to the character.
                    rigidbody.AddForce(CalculateVelocityChange(horizontalInput, verticalInput),
                        ForceMode.VelocityChange);
                    break;
            }

        }
        /// <summary>
        /// Processes character air control.
        /// </summary>
        private void ProcessAirControl(float horizontalInput, float verticalInput)
        {
            //Creating a currentSpeed variable.
            //Used to calculate appropriate speed for the character.
            var currentSpeed = ProcessSpeed(speed);

            //Updating avatar animation settings.
            avatarManager.currentAvatar.UpdateAnimationValues(new AvatarAnimationValues
            {
                horizontal = horizontalInput,
                vertical = verticalInput,
                inAir = true,
                isJumping = false,
                isSprinting = false,
                playerPosition = currentPlayerPosition
            });

            //Set the movement input to be the force to apply to the player every frame
            horizontalInput = horizontalInput * currentSpeed * rigidbody.mass * airControl;
            verticalInput = verticalInput * currentSpeed * rigidbody.mass * airControl;
            //Applying the grounded force to the player.
            rigidbody.AddForce(CalculateAcceleration(horizontalInput, verticalInput), ForceMode.Acceleration);
        }
        /// <summary>
        /// Calculates speed value adequate to the current position.
        /// </summary>
        /// <param name="speedToProcess">Speed value to be processed.</param>
        /// <returns></returns>
        private float ProcessSpeed(float speedToProcess)
        {
            var resultSpeed = speedToProcess;

            switch (currentPlayerPosition)
            {
                case PlayerPosition.STANDING:
                    resultSpeed += walkSettings.movementMultiplier;
                    return resultSpeed;

                case PlayerPosition.CROUCHING:
                    resultSpeed *= crouchSettings.movementMultiplier;
                    return resultSpeed;

                case PlayerPosition.PRONING:
                    resultSpeed *= proneSettings.movementMultiplier;
                    return resultSpeed;

                case PlayerPosition.FLYING:
                    resultSpeed *= flySettings.movementMultiplier;
                    return resultSpeed;
            }

            return speedToProcess;
        }
        /// <summary>
        /// Processes previously cached player actions.
        /// </summary>
        private void ProcessCachedActions()
        {
            //Checking whether jump has been cached.
            if (jump)
            {
               Jump(); 
            }
            if (crouch)
            {
                Crouch();
            }
            if (prone)
            {
                Prone();
            }
            if (stand)
            {
                Stand();
            }
            if (landed)
            {
                OnLanded();
            }
        }
        /// <summary>
        /// Makes the character jump.
        /// </summary>
        private void Jump()
        {
            if (flySettings.canUse && grounded)
            {
                currentPlayerPosition = PlayerPosition.FLYING;
                rigidbody.AddForce(new Vector3(0, CalculateJumpVerticalSpeed(), 0), ForceMode.Impulse);

                //Updating avatar animation settings.
                avatarManager.currentAvatar.UpdateAnimationValues(new AvatarAnimationValues
                {
                    horizontal = horizontal,
                    vertical = vertical,
                    inAir = false,
                    isJumping = true,
                    isSprinting = false,
                    playerPosition = currentPlayerPosition
                });
            }

            //Setting jump flag to false.
            jump = false;
        }
        /// <summary>
        /// Makes the character stand.
        /// </summary>
        private void Stand()
        {
            if (walkSettings.canUse)
            {
                currentPlayerPosition = PlayerPosition.STANDING;
                _currentRayDistance = walkSettings.rayDistance;

                //Updating avatar animation settings.
                avatarManager.currentAvatar.UpdateAnimationValues(new AvatarAnimationValues
                {
                    horizontal = horizontal,
                    vertical = vertical,
                    inAir = false,
                    isJumping = false,
                    isSprinting = sprint,
                    playerPosition = currentPlayerPosition
                });
            }

            //Setting stand flag to false.
            stand = false;
        }
        /// <summary>
        /// Makes the character crouch.
        /// </summary>
        private void Crouch()
        {
            if (crouchSettings.canUse)
            {
                currentPlayerPosition = PlayerPosition.CROUCHING;
                _currentRayDistance = crouchSettings.rayDistance;

                //Updating avatar animation settings.
                avatarManager.currentAvatar.UpdateAnimationValues(new AvatarAnimationValues
                {
                    horizontal = horizontal,
                    vertical = vertical,
                    inAir = false,
                    isJumping = false,
                    isSprinting = false,
                    playerPosition = currentPlayerPosition
                });
            }

            //Setting crouch flag to false.
            crouch = false;
        }
        /// <summary>
        /// Makes the character prone.
        /// </summary>
        private void Prone()
        {
            if (proneSettings.canUse)
            {
                currentPlayerPosition = PlayerPosition.PRONING;
                _currentRayDistance = proneSettings.rayDistance;

                //Updating avatar animation settings.
                avatarManager.currentAvatar.UpdateAnimationValues(new AvatarAnimationValues
                {
                    horizontal = horizontal,
                    vertical = vertical,
                    inAir = false,
                    isJumping = false,
                    isSprinting = false,
                    playerPosition = currentPlayerPosition
                });
            }

            //Setting prone flag to false.
            prone = false;
        }
        #endregion
        #region ColliderMessages
        // Store point that we're in contact with for use in FixedUpdate if needed
        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            _contactPoint = hit.point;
        }
        #endregion
        #region InputProcessing
        /// <summary>
        /// Sending player input to the server.
        /// </summary>
        private void Update()
        {
            //Input getting works only locally on the client.
            if (controlsActive)
            {
                //Movement input caching.
                CacheMovement(CInput.GetAxis("Horizontal Movement"), CInput.GetAxis("Vertical Movement"));

                //Action caching.
                CacheJump(CInput.GetButton("Jump"));
                CacheSprint(CInput.GetButton("Sprint"));
                CacheCrouch(CInput.GetButton("Crouch"));
                CacheProne(CInput.GetButton("Prone"));
                CacheStand(!CInput.GetButtonDown("Jump") && !CInput.GetButton("Crouch") &&
                              !CInput.GetButton("Prone"));

                if (Client.unetClient != null && Client.unetClient.isConnected)
                {
                    //Movement input caching.
                    CmdCacheMovement(CInput.GetAxis("Horizontal Movement"), CInput.GetAxis("Vertical Movement"));

                    //Action caching.
                    CmdCacheJump(CInput.GetButton("Jump"));
                    CmdCacheSprint(CInput.GetButton("Sprint"));
                    CmdCacheCrouch(CInput.GetButton("Crouch"));
                    CmdCacheProne(CInput.GetButton("Prone"));
                    CmdCacheStand(!CInput.GetButtonDown("Jump") && !CInput.GetButton("Crouch") &&
                                  !CInput.GetButton("Prone"));
                }
            }
        }
        #endregion
        #region InputCaching
        /// <summary>
        /// Caches player movement input for futher processing.
        /// </summary>
        [Command]
        private void CmdCacheMovement(float horizontalInput, float verticalInput)
        {
            horizontal = horizontalInput;
            vertical = verticalInput;
        }
        /// <summary>
        /// Caches a player jump input.
        /// </summary>
        [Command]
        private void CmdCacheJump(bool jump)
        {
            this.jump = jump;
        }
        /// <summary>
        /// Caches a player crouch input.
        /// </summary>
        [Command]
        private void CmdCacheCrouch(bool crouch)
        {
            this.crouch = crouch;
        }
        /// <summary>
        /// Caches a player prone input.
        /// </summary>
        [Command]
        private void CmdCacheProne(bool prone)
        {
            this.prone = prone;
        }
        /// <summary>
        /// Caches a player sprint input.
        /// </summary>
        /// <param name="sprint"></param>
        [Command]
        private void CmdCacheSprint(bool sprint)
        {
            if (canSprint)
            {
                this.sprint = sprint;
            }
        }
        /// <summary>
        /// Caches a player stand input.
        /// </summary>
        /// <param name="stand"></param>
        [Command]
        private void CmdCacheStand(bool stand)
        {
            this.stand = stand;
        }

        /// <summary>
        /// Caches player movement input for futher processing.
        /// LOCAL
        /// </summary>
        private void CacheMovement(float horizontalInput, float verticalInput)
        {
            horizontal = horizontalInput;
            vertical = verticalInput;
        }
        /// <summary>
        /// Caches a player jump input.
        /// LOCAL
        /// </summary>
        private void CacheJump(bool jump)
        {
            this.jump = jump;
        }
        /// <summary>
        /// Caches a player crouch input.
        /// LOCAL
        /// </summary>
        private void CacheCrouch(bool crouch)
        {
            this.crouch = crouch;
        }
        /// <summary>
        /// Caches a player prone input.
        /// LOCAL
        /// </summary>
        private void CacheProne(bool prone)
        {
            this.prone = prone;
        }
        /// <summary>
        /// Caches a player sprint input.
        /// LOCAL
        /// </summary>
        /// <param name="sprint"></param>
        private void CacheSprint(bool sprint)
        {
            if (canSprint)
            {
                this.sprint = sprint;
            }
        }
        /// <summary>
        /// Caches a player stand input.
        /// LOCAL
        /// </summary>
        /// <param name="stand"></param>
        private void CacheStand(bool stand)
        {
            this.stand = stand;
        }
        #endregion
        #region Checks
        /// <summary>
        /// Checks whether the controler should slide.
        /// We use this normally rather than a ControllerColliderHit point, because that interferes with step climbing amongst other annoyances.
        /// </summary>
        /// <returns></returns>
        private bool CheckSliding()
        {
            if (currentPlayerPosition == PlayerPosition.STANDING || currentPlayerPosition == PlayerPosition.CROUCHING)
            {
                //See if surface immediately below should be slid down.
                RaycastHit hit;
                if (Physics.Raycast(collider.center, -Vector3.up, out hit, _currentRayDistance))
                {
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    {
                        return true;
                    }
                }
                //However, just raycasting straight down from the center can fail when on steep slopes
                //So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
                else
                {
                    Physics.Raycast(_contactPoint + Vector3.up, -Vector3.up, out hit);
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Checking whether character is grounded.
        /// </summary>
        /// <param name="col"></param>
        internal void OnCollisionEnter(Collision col)
        {
            grounded = CheckGrounded();
            if (grounded)
            {
                landed = true;
            }
        }
        /// <summary>
        /// Checking whether character lost his ground.
        /// </summary>
        /// <param name="col"></param>
        internal void OnCollisionExit(Collision col)
        {
            grounded = CheckGrounded();
        }
        /// <summary>
        /// Called when the collider lands on the ground.
        /// Applying current position land multipliers.
        /// </summary>
        private void OnLanded()
        {
            switch (currentPlayerPosition)
            {
                case PlayerPosition.STANDING:
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x * walkSettings.landVelocityMultiplier, 0, rigidbody.velocity.z * walkSettings.landVelocityMultiplier);
                    break;
                case PlayerPosition.CROUCHING:
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x * crouchSettings.landVelocityMultiplier, 0, rigidbody.velocity.z * crouchSettings.landVelocityMultiplier);
                    break;
                case PlayerPosition.PRONING:
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x * proneSettings.landVelocityMultiplier, 0, rigidbody.velocity.z * proneSettings.landVelocityMultiplier);
                    break;
            }

            //Cancling the landed flag.
            landed = false;
        }
        /// <summary>
        /// Checks whether the controler is grounded.
        /// </summary>
        /// <returns></returns>
        private bool CheckGrounded()
        {
            var hits = Physics.RaycastAll(collider.center + transform.position, -Vector3.up, _currentRayDistance);

            return hits.Any(hit => hit.collider.tag != "Character");
        }
        /// <summary>
        /// Checking whether the current height of the controler corresponds to his position.
        /// </summary>
        private void CheckHeight()
        {
            switch (currentPlayerPosition)
            {
                case PlayerPosition.STANDING:
                    if (collider.height != walkSettings.characterColliderHeight)
                    {
                        //Changing collider height.
                        StartCoroutine(UiManager.Smooth(collider.height, walkSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        //Changing collider center.
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, 0, collider.center.z), heightChangeTime,
                            value => collider.center = value));
                        //Changing collider radius.
                        StartCoroutine(UiManager.Smooth(collider.radius, walkSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 1;
                        //Camera height.
                        StartCoroutine(UiManager.Smooth(camera.transform.localPosition.y, walkSettings.characterCameraHeight,
                            heightChangeTime, value =>
                            {
                                camera.localPosition = new Vector3(camera.localPosition.x, value, camera.localPosition.z);
                            }));
                    }
                    break;
                case PlayerPosition.CROUCHING:
                    if (collider.height != crouchSettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, crouchSettings.characterColliderCenter, collider.center.z),
                            heightChangeTime, value => collider.center = value));
                        //Changing collider center.
                        StartCoroutine(UiManager.Smooth(collider.height, crouchSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        //Changing collider radius.
                        StartCoroutine(UiManager.Smooth(collider.radius, crouchSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 1;
                        //Camera height.
                        StartCoroutine(UiManager.Smooth(camera.transform.localPosition.y, crouchSettings.characterCameraHeight,
                            heightChangeTime, value =>
                            {
                                camera.localPosition = new Vector3(camera.localPosition.x, value, camera.localPosition.z);
                            }));
                    }
                    break;
                case PlayerPosition.PRONING:
                    if (collider.height != proneSettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.center,
                            new Vector3(collider.center.x, proneSettings.characterColliderCenter, collider.center.z),
                            heightChangeTime, value => collider.center = value));
                        //Changing collider center.
                        StartCoroutine(UiManager.Smooth(collider.height, proneSettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        //Changing collider radius.
                        StartCoroutine(UiManager.Smooth(collider.radius, proneSettings.characterColliderRadius,
                            heightChangeTime, value => collider.radius = value));
                        collider.direction = 2;
                        //Camera height.
                        StartCoroutine(UiManager.Smooth(camera.transform.localPosition.y, proneSettings.characterCameraHeight,
                            heightChangeTime, value =>
                            {
                                camera.localPosition = new Vector3(camera.localPosition.x, value, camera.localPosition.z);
                            }));
                    }
                    break;
                case PlayerPosition.FLYING:
                    if (collider.height != flySettings.characterColliderHeight)
                    {
                        //Starting height changing.
                        StartCoroutine(UiManager.Smooth(collider.height, flySettings.characterColliderHeight,
                            heightChangeTime, value => collider.height = value));
                        //Camera height.
                        StartCoroutine(UiManager.Smooth(camera.transform.localPosition.y, flySettings.characterCameraHeight,
                            heightChangeTime, value =>
                            {
                                camera.localPosition = new Vector3(camera.localPosition.x, value, camera.localPosition.z);
                            }));
                    }
                    break;
            }
        }
        /// <summary>
        /// Checks whether there is enough space for player to change position.
        /// </summary>
        /// <param name="newPosition"></param>
        /// <returns></returns>
        private bool CheckSpaceForPosition(PlayerPosition newPosition)
        {
            RaycastHit[] hits;
            Vector3 p1;
            Vector3 p2;

            switch (newPosition)
            {
                case PlayerPosition.STANDING:
                    p1 = transform.position + new Vector3(0, walkSettings.characterColliderCenter, 0) + Vector3.up * -walkSettings.characterColliderHeight * 0.5F;
                    p2 = p1 + Vector3.up * walkSettings.characterColliderHeight;
                    hits = Physics.CapsuleCastAll(p1, p2, walkSettings.characterColliderRadius, transform.forward);

                    if (hits.Any(hit => hit.collider.tag != "Character")) {
                        return false;
                    }
                    return true;
                case PlayerPosition.CROUCHING:
                    p1 = transform.position + new Vector3(0, crouchSettings.characterColliderCenter, 0) + Vector3.up * -crouchSettings.characterColliderHeight * 0.5F;
                    p2 = p1 + Vector3.up * crouchSettings.characterColliderHeight;
                    hits = Physics.CapsuleCastAll(p1, p2, crouchSettings.characterColliderRadius, transform.forward);

                    if (hits.Any(hit => hit.collider.tag != "Character"))
                    {
                        return false;
                    }
                    return true;
            }
            return true;
        }
        private float CalculateJumpVerticalSpeed()
        {
            // From the jump height and gravity we deduce the upwards speed 
            // for the character to reach at the apex.
            return Mathf.Sqrt(2 * jumpForce * -Physics.gravity.y);
        }

        private Vector3 CalculateVelocityChange(float horizontalInput, float verticalInput)
        {
            var horizontal = transform.rotation * Vector3.right * horizontalInput;
            var vertical = transform.rotation * Vector3.forward * verticalInput;

            //Calculating the velocity change.
            var targetVelocity = horizontal + vertical;
            var velocity = rigidbody.velocity;
            var velocityChange = targetVelocity - velocity;
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;

            return velocityChange;
        }

        private Vector3 CalculateAcceleration(float horizontalInput, float verticalInput)
        {
            var horizontal = transform.rotation * Vector3.right * horizontalInput;
            var vertical = transform.rotation * Vector3.forward * verticalInput;

            //Calculating the velocity change.
            var targetVelocity = horizontal + vertical;
            var velocity = rigidbody.velocity;
            var velocityChange = targetVelocity - velocity;
            velocityChange.y = 0;

            return velocityChange;
        }
        #endregion
    }

    /// <summary>
    /// All the possible player positions.
    /// </summary>
    public enum PlayerPosition
    {
        STANDING,
        CROUCHING,
        PRONING,
        FLYING
    }

    /// <summary>
    ///     Class for storing different per-position settings.
    /// </summary>
    [Serializable]
    public class PositionSettings
    {
        public bool canUse;
        public float characterColliderCenter;
        public float characterColliderHeight;
        public float characterColliderRadius;
        public float characterCameraHeight;
        public float movementMultiplier;
        public float landVelocityMultiplier;
        public float rayDistance;
    }
}