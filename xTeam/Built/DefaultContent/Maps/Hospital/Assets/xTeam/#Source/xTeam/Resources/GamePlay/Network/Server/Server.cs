﻿using System.Collections.Generic;
using Open.Nat;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.LinkManager.PlayerManager;
using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;
using XTeam.Resources.GamePlay.Network.Shared;
using XTeam.Resources.Utilities.FileManager;
using Object = UnityEngine.Object;

namespace XTeam.Resources.GamePlay.Network.Server
{
    /// <summary>
    ///     Class for starting up the server.
    /// </summary>
    public class Server : MonoBehaviour
    {
        /// <summary>
        ///     General properties of the server.
        /// </summary>
        [SerializeField]
        public static ServerProperties properties;

        /// <summary>
        ///     Connection properties of the server.
        /// </summary>
        public static ConnectionConfig connectionConfig;

        /// <summary>
        /// Content packs that should be enabled at the server startup.
        /// If empty, all the contentpacks will be loaded.
        /// </summary>
        public List<ContentPack> enabledContentPacks = new List<ContentPack>();

        /// <summary>
        ///     Instance of the server.
        /// </summary>
        public static Server instance;
        /// <summary>
        /// 	A network discovery component.
        /// </summary>
        public NetworkDiscovery networkDiscovery;
        /// <summary>
        /// Prefab of the LinkManager.
        /// </summary>
        public Object linkManagerPrefab;

        #region MonoBehaviour

        private void Awake()
        {
            //Creating default connectionConfig.
            CreateConnectionProperties();
			//Creating default serverProperties.
			properties = new ServerProperties();

            //Setting the instance variable.
            instance = this;
        }

        #endregion

        #region SetupMethods

        /// <summary>
        ///     Starts server based on the ServerProperties.
        /// </summary>
        internal static void StartServer()
        {
            Debug.Log("Starting a server on " + properties.serverAddress + ":" + properties.serverPort);
            //Loading empty scene.
            //instance.StartCoroutine(LevelLoader.LoadScene("Empty", LoadSceneMode.Single, null));

            //Setting the connection properties of the server.
            NetworkManager.singleton.networkPort = properties.serverPort;
            NetworkManager.singleton.networkAddress = properties.serverAddress;

            //Opening the required server ports.
            OpenServerPorts();

            //Starting the server.
            UnityEngine.Network.InitializeSecurity();
            NetworkServer.Configure(connectionConfig, properties.serverSize);
            NetworkServer.Listen(properties.serverPort);
            RegisterHandlers();

            //Spawning the LinkManager.
            GameObject linkManager = (GameObject)Instantiate(instance.linkManagerPrefab);
            NetworkServer.Spawn(linkManager);
        }
        /// <summary>
        /// Stops the currently running server.
        /// </summary>
        public static void StopServer()
        {
            if (!NetworkServer.active)
            {
                Debug.Log("Server is not running!");
                return;
            }
            Debug.Log("Stopping the server...");

            //Stopping the MatchMaker.
            MatchUpdater.StopUpdating();
            //Stopping the ConnectionBridge.
            ServerInfoSender.StopListeningForConnections();
            //Stopping the FTP connection
            FileSender.thrSend.Exit();
            //Stopping the UNETServer.
            NetworkManager.singleton.StopServer();

            Debug.Log("Server stopped.");

        }
        /// <summary>
        /// Registers default server handlers.
        /// </summary>
        private static void RegisterHandlers()
        {
            //Connect handler.
            NetworkServer.RegisterHandler(MsgType.Connect, msg =>
            {
                Debug.Log("A client connected...");
            });
            //Disconnect handler.
            NetworkServer.RegisterHandler(MsgType.Disconnect, PlayerManager.OnClientDisconnectedFromServer);
            //AddPlayer handler.
            NetworkServer.RegisterHandler(MsgType.AddPlayer, PlayerManager.OnAddPlayerForConnection);
        }
        /// <summary>
        /// Opens server ports on UPnP compatible router.
        /// </summary>
        private static void OpenServerPorts()
        {
            Debug.Log("Opening ports on a router...");
            //Starting the network discovery.
            var discoverer = new NatDiscoverer();
            var networkDiscovery = discoverer.DiscoverDeviceAsync();

            networkDiscovery.ContinueWith(task =>
            {
                //A router was found on the network.
                //Getting the found router.
                var device = task.Result;

                //Registering server ports.
                device.CreatePortMapAsync(new Mapping(Protocol.Udp, properties.serverPort, properties.serverPort, 0, "X-Team (GamePlay)"))
                    .RunSynchronously();
            });

            networkDiscovery.ContinueWith(task =>
            {
                //A router was found on the network.
                //Getting the found router.
                var device = task.Result;

                //Registering server ports.
                device.CreatePortMapAsync(new Mapping(Protocol.Udp, properties.serverPort, properties.fileTransferPort, 0, "X-Team (FTP)"))
                    .RunSynchronously();
            });

            networkDiscovery.ContinueWith(task =>
            {
                //A router was found on the network.
                //Getting the found router.
                var device = task.Result;

                //Registering server ports.
                device.CreatePortMapAsync(new Mapping(Protocol.Tcp, properties.connectionBridgePort, properties.connectionBridgePort, 0, "X-Team (Bridge)")).RunSynchronously();
            });
        }
        /// <summary>
        ///     Creates default connection properties.
        /// </summary>
        private static void CreateConnectionProperties()
        {
            //Creating connectionConfig object.
            connectionConfig = new ConnectionConfig
            {
                PacketSize = 1250,
                MaxSentMessageQueueSize = ushort.MaxValue,
                FragmentSize = 1100
            };

            //Setting properties.

            //Adding channels.
            connectionConfig.AddChannel(QosType.UnreliableSequenced); //Unreliable.
            connectionConfig.AddChannel(QosType.Reliable); //Reliable.
            connectionConfig.AddChannel(QosType.ReliableSequenced); //FTP.
        }

        #endregion
    }
    /// <summary>
    /// Properties of a server.
    /// </summary>
    public class ServerProperties
    {
        /// <summary>
        /// Name of the server.
        /// </summary>
        public string serverName = "Unnamed X-Team Server";
        /// <summary>
        /// Description of the server.
        /// </summary>
        public string serverDescription = "Default description";
        /// <summary>
        /// Password of the server.
        /// </summary>
        public string serverPassword = "";
        /// <summary>
        /// Size of the server.
        /// </summary>
        public int serverSize = 16;

        /// <summary>
        /// Gameplay address of the server.
        /// </summary>
        public string serverAddress = "localhost";
        /// <summary>
        /// Gameplay port of the server.
        /// </summary>
        public int serverPort = 42069;
        /// <summary>
        /// FTP port of the server.
        /// </summary>
        public int fileTransferPort
        {
            get { return serverPort + 1; }
        }
        /// <summary>
        /// Connection bridge port of the server.
        /// </summary>
        public int connectionBridgePort
        {
            get { return serverPort + 2; }
        }

        /// <summary>
        /// Whether a server is dedicated.
        /// </summary>
        public bool isDedicated = false;
        /// <summary>
        /// Whether the server should be registered to the matchmaker.
        /// </summary>
        public bool useMatchmaker = true;
        
        /// <summary>
        /// List of custom content paths to load.
        /// </summary>
        public List<string> contentToLoad = new List<string>();
        /// <summary>
        /// Whether the server should load all the content from the content folder.
        /// </summary>
        public bool loadContentFromFolder = true;

		/// <summary>
		/// The server tick rate.
		/// </summary>
        public float serverTickRate = 64;
		/// <summary>
		/// The server send rate.
		/// </summary>
		public float serverSendRate = 30;

        /// <summary>
        ///     Checks whether all the required data is present.
        /// </summary>
        /// <returns></returns>
        public bool IsAllRequiredDataPresent()
        {
			if (serverName != null && serverDescription != null && serverSize != 0 && serverAddress != "" && serverPort != 0)
            {
                //All of the required data is present.
                return true;
            }
            return false;
        }
    }
    /// <summary>
    /// IDs of server registered channels.
    /// </summary>
    public static class ServerChannels
    {
        public const short Reliable = 0;
        public const short Unreliable = 1;
        public const short FTP = 2;
    }
}