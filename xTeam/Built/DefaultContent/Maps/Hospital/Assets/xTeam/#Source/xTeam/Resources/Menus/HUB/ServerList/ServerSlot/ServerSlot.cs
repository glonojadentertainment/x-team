﻿using UnityEngine;
using XTeam.Resources.GamePlay.Network.Server.Modules.MatchMaker;
using XTeam.Resources.GamePlay.Network.Shared;

namespace XTeam.Resources.Menus.ServerList.ServerSlot
{
    /// <summary>
    /// Class managing server list slot.
    /// </summary>
    public class ServerSlot : MonoBehaviour
    {
        /// <summary>
        /// Server represented by this ServerSlot.
        /// </summary>
        public ServerInfo representedServer;
        /// <summary>
        /// Type of this server slot.
        /// </summary>
        public ServerNetworkType serverNetworkType;

        /// <summary>
        /// Connects to the represented server.
        /// </summary>
        public void Connect() {
            switch (serverNetworkType)
            {
                case ServerNetworkType.LAN:
                    Client.Connect(representedServer.localAddress, representedServer.port);
                    break;
                case ServerNetworkType.PUBLIC:
                    Client.Connect(representedServer.publicAddress, representedServer.port);
                    break;
            }
        }
    }
}
