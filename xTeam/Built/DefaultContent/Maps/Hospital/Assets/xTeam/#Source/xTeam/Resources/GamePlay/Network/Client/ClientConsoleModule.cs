﻿using Assets.xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager;
using UnityEngine;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager;
using XTeam.API.Console;

namespace XTeam.Resources.GamePlay.Network.Client
{
    /// <summary>
    /// Console module for basic client management.
    /// </summary>
    public class ClientConsoleModule : ConsoleModule {
        public override string GetName()
        {
            return "Client Basic Commands";
        }

        public override string[] GetBaseCommands()
        {
            return new string[] {"client", "cl"};
        }

        public override bool OnCommandRun(ConsoleCommand command)
        {
            //Client connect command.
            //Checking command args.
            if (command.arguments.Length == 0)
            {
                //There are no arguments on the command.
                //Showing server status.
                Debug.Log("Client status:");
                //UNET
                if (Shared.Client.unetClient.connection == null)
                {
                    Debug.Log(" UNET Active: " + false);
                }
                else
                {
                    Debug.Log(" UNET Active: " + true);
                }

                //Bridge
                if (Shared.Client.connThr == null)
                {
                    Debug.Log(" Bridge Active: " + null);
                }
                else
                {
                    Debug.Log(" Bridge Active: " + Shared.Client.connThr.IsAlive);
                }
                return true;
            }

            switch (command.arguments[0]) {
                case "connect":
                    Shared.Client.Connect(command.arguments[1], int.Parse(command.arguments[2]));
                    return true;
                case "disconnect":
                    Shared.Client.Disconnect();
                    return true;
                case "spawnlocal":
                    if (command.arguments[1] != null)
                    {
                        SpawnManager.SpawnLocally(command.arguments[1], Vector3.zero, Quaternion.identity);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

            return false;
        }
    }
}
