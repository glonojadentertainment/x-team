﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using Ionic.Zip;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.xTeam_Creator.Classes.MapCreator;
using xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup;
using xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Required.GameSetup.AutomaticallyGenerated;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;

namespace xTeam.xTeam_Creator.MapExporter.Editor
{
    public class MapExporter : MonoBehaviour
    {
        public static void ExportCurrentMap()
        {
            //Getting the maps directory relativePath of the game.
            var mapSettings = FindObjectOfType<MapSettings>();
            if (mapSettings == null)
            {
                mapSettings = new GameObject("MapSettings", new Type[]
                {
                    typeof(MapSettings)
                }).GetComponent<MapSettings>();
            }
            if (string.IsNullOrEmpty(mapSettings.mapName))
            {
                mapSettings.mapName = SceneManager.GetActiveScene().name;
            }

            var exportDirectory = GetMapExportDirectory();
            var mapExportFile = exportDirectory + "/" + mapSettings.mapName + ".xtma";
            var iconFilePath = EditorUtility.OpenFilePanel("X-Team - Select map icon", "", "png");

            //Checking whether export was cancelled.
            if (iconFilePath == "" || exportDirectory == "")
            {
                Debug.Log("Export cancelled.");
            }

            //Begining the export process...
            Debug.Log("Exporting map " + mapSettings.mapName + " to " + exportDirectory);

            //Cleaning up!
            CleanUp();

            //Saving the scene.
            EditorSceneManager.SaveOpenScenes();

            //Moving the saved scene to the default assets level relativePath.
            string defaultLevelAssetsPath = "Assets/" + mapSettings.mapName + ".unity";

            if (SceneManager.GetActiveScene().path != defaultLevelAssetsPath)
            {
                //Moving saved scene to a default maps directory.
                FileUtil.ReplaceFile(SceneManager.GetActiveScene().path, defaultLevelAssetsPath);
            }

            //Clear the assetbundle build database.
            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                print("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            //Exporting...
            AssetImporter assetImporter = AssetImporter.GetAtPath(defaultLevelAssetsPath);
            assetImporter.assetBundleName = mapSettings.mapName;
            BuildPipeline.BuildAssetBundles(exportDirectory, BuildAssetBundleOptions.ForceRebuildAssetBundle, BuildTarget.StandaloneWindows);
            //Getting rid of CustomLevelSettings.
            var cls = FindObjectOfType<CustomLevelSettings>().gameObject;
            if (cls != null)
            {
                DestroyImmediate(cls.gameObject);
            }
            
            //Clear the assetbundle build database.
            names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                print("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            try
            {
                using (var zip = new ZipFile())
                {
                    //Adding map file to the zip.
                    Debug.Log("Adding map file to map zip.");
                    var mapFileZipEntry = zip.AddEntry("mapData.xtma", File.ReadAllBytes(mapExportFile));
                    mapFileZipEntry.Comment = "The map file itself. Can be opened directly through X-Team";

                    //Adding map info document to the zip.
                    var mapInfo = CreateMapInfoDocument(mapSettings);
                    var mapInfoPath = exportDirectory + "/mapInfo.xml";
                    mapInfo.Save(mapInfoPath);
                    var mapInfoZipEntry = zip.AddEntry("mapInfo.xml",
                        File.ReadAllBytes(exportDirectory + "/mapInfo.xml"));
                    mapInfoZipEntry.Comment = "Document containing all information about the map.";

                    //Adding map icon file to the zip.
                    var mapIconZipEntry = zip.AddEntry("mapIcon.png", File.ReadAllBytes(iconFilePath));
                    mapIconZipEntry.Comment = "Map icon image.";

                    zip.Comment = "X-Team map " + mapSettings.mapName + " file.";

                    zip.Save(exportDirectory + "/" + mapSettings.mapName + ".xtm");

                    //Cleaning up the non-xtmed files.
                    File.Delete(mapExportFile);
                    File.Delete(exportDirectory + "/mapInfo.xml");
                    File.Delete(exportDirectory + "/contentInfo.xml");
                    File.Delete(defaultLevelAssetsPath);
                }
            }
            catch (Exception ex1)
            {
                Console.Error.WriteLine("Exception creating a ZIP file: " + ex1);
            }

            //Exporting completed!
            print("Exporting complete!");
        }

        [MenuItem("Assets/Bake map for xTeam", true)]
        public bool ValidateMap()
        {
            if (AssetDatabase.GetAssetOrScenePath(Selection.activeObject).Contains(".unity"))
            {
                return true;
            }
            return false;
        }

        private static void CleanUp()
        {
            //Creating new CustomLevelSettings object and setting its values.
            var cls = FindObjectOfType<CustomLevelSettings>();

            if (cls != null)
            {
                cls.SetSettings();
            }
            else
            {
                var clsgo = new GameObject { name = "CustomLevelSettings" };
                cls = clsgo.AddComponent<CustomLevelSettings>();
                cls.SetSettings();
            }

            //Removing unnecesery cameras.
            var cams = FindObjectsOfType<Camera>();
            var camCount = 0;

            foreach (var cam in cams.Where(cam => cam.enabled).Where(cam => cam.targetTexture == null))
            {
                cam.enabled = false;
                camCount++;
            }
            if (camCount > 0)
            {
                print("You had " + camCount + " extra cameras enabled, they were disabled for export.");
            }

            //Removing unnecesery audiolisteners.
            var listeners = FindObjectsOfType<AudioListener>();
            var listenerCount = 0;

            foreach (var t in listeners.Where(t => t.enabled))
            {
                t.enabled = false;
                listenerCount++;
            }
            if (listenerCount > 0)
                print("You had " + listenerCount + " extra AudioListeners enabled, they were disabled for export.");
        }

        /// <summary>
        ///     Asks user to choose map export directory.
        /// </summary>
        /// <returns></returns>
        private static string GetMapExportDirectory()
        {
            return new FileInfo(SceneManager.GetActiveScene().path).Directory.FullName;
        }

        private static XmlDocument CreateMapInfoDocument(MapSettings mapSettings)
        {
            Map mapInfo = new Map
            {
                name = mapSettings.mapName,
                description = mapSettings.mapDescription,
                type = mapSettings.mapType,
                version = mapSettings.mapVersion
            };

            return XmlUtil.Serialize(mapInfo);
        }
    }
}