﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.Server.Creator.MapList.MapSlot;

namespace xTeam.Resources.GamePlay.Network.Server.Creator.ContentList
{
    public class ContentPackListManager : MonoBehaviour {
        /// <summary>
        /// Prefab of single contentPack slot.
        /// </summary>
        public Object contentPackSlotPrefab;
        /// <summary>
        /// Urls of the online contentPack list.
        /// </summary>
        public string officialContentPacksListUrl;
        public string communityContentPacksListUrl;
        /// <summary>
        /// List of all currently registered contentPack slots.
        /// </summary>
        List<GameObject> _contentPackListSlots = new List<GameObject>();
        /// <summary>
        /// Mode of contentPack listing.
        /// </summary>
        public ContentPackListMode contentPackListMode;
	
        // Use this for initialization
        void Start () {
            //Refreshing the list for the first time.
            StartCoroutine(Refresh ());
        }
        /// <summary>
        /// Refreshes the contentPack list.
        /// </summary>
        /// <returns></returns>
        IEnumerator Refresh() {
            ClearContentPackList();
            WWW www;
            switch (contentPackListMode)
            {
                case ContentPackListMode.Community:
                    www = new WWW(communityContentPacksListUrl);
                    yield return www;
                    if (www.error != null)
                    {
                        //There was an error downloading the contentPack list file.
                        Debug.LogError("There was an error downloading the contentPack list file. " + www.error);
                        //Cooldown.
                        yield return new WaitForSeconds(1f);
                        //Strating the download process again.
                        StartCoroutine(Refresh());
                        yield break;
                    }
                    else
                    {
                        //Successfully downloaded .txt file!
                        //Spliting it to one-contentPack fragments.
                        OnOnlineContentPacksList(www.text);
                    }
                    break;
                case ContentPackListMode.Official:
                    www = new WWW(officialContentPacksListUrl);
                    yield return www;
                    if (www.error != null)
                    {
                        //There was an error downloading the contentPack list file.
                        Debug.LogError("There was an error downloading the contentPack list file. " + www.error);
                        //Cooldown.
                        yield return new WaitForSeconds(1f);
                        //Strating the download process again.
                        StartCoroutine(Refresh());
                        yield break;
                    }
                    else
                    {
                        //Successfully downloaded .txt file!
                        //Spliting it to one-contentPack fragments.
                        OnOnlineContentPacksList(www.text);
                    }
                    break;
                case ContentPackListMode.Downloaded:
                    //Getting maps from local storage.
                    ContentBrowser.GetAllContentPacksAsync(OnLocalContentPacksList);
                    break;
            }
        }
        /// <summary>
        /// Is called when the online ContentPack list has been fetched.
        /// </summary>
        /// <param name="contentPackListString"></param>
        public void OnOnlineContentPacksList(string contentPackListString) {
            //Fetched the content list. Checking if the list isn't empty.
            if(contentPackListString != null) {
                XmlDocument mapList = new XmlDocument();
                mapList.LoadXml(contentPackListString);
                XmlNodeList nodeList = mapList.DocumentElement.SelectNodes("/maps/contentPack");

                foreach (XmlNode node in nodeList)
                {
                    //Spawning a ContentPack slot.
                    GameObject processedMapSlot = (GameObject)Instantiate(contentPackSlotPrefab);
                    processedMapSlot.transform.SetParent(transform, false);

                    //Creating a contentPack object.
                    ContentPack fetchedContentPack = XmlUtil.Deserialize(node, typeof(ContentPack)) as ContentPack;
                    processedMapSlot.GetComponent<ContentPackSlot>().Setup(fetchedContentPack, ContentSlotType.ONLINE_CONTENT);

                    //Adding contentPack slot to contentPack slots list.
                    _contentPackListSlots.Add(processedMapSlot);
                }	
            }
        }
        /// <summary>
        /// Is called when the local contentPack list has been fetched.
        /// </summary>
        /// <param name="mapListString"></param>
        public void OnLocalContentPacksList(ContentPack[] contentPacks)
        {
            foreach (ContentPack contentPack in contentPacks)
            {
                Debug.Log("Setting the ContentPack slot for: " + contentPack.info.name);
                //Spawning a ContentPack slot.
                GameObject contentPackSlot = (GameObject)Instantiate(contentPackSlotPrefab);
                contentPackSlot.transform.SetParent(transform, false);

                //Creating a contentPack object.
                contentPackSlot.GetComponent<ContentPackSlot>().Setup(contentPack, ContentSlotType.LOCAL_CONTENT);

                //Adding contentPack slot to contentPack slots list.
                _contentPackListSlots.Add(contentPackSlot);
            }
        }
        /// <summary>
        /// Clears contentPack list.
        /// </summary>
        public void ClearContentPackList()
        {
            foreach(Transform t in transform)
            {
                Destroy(t.gameObject);
            }
        }
        #region ModeSettings
        public void ListOfficial()
        {
            contentPackListMode = ContentPackListMode.Official;
            StartCoroutine(Refresh());
        }
        public void ListCommunity()
        {
            contentPackListMode = ContentPackListMode.Community;
            StartCoroutine(Refresh());
        }
        public void ListDownloaded()
        {
            contentPackListMode = ContentPackListMode.Downloaded;
            StartCoroutine(Refresh());
        }
        #endregion
    }
    public enum ContentPackListMode
    {
        Official, Community, Downloaded
    }
}