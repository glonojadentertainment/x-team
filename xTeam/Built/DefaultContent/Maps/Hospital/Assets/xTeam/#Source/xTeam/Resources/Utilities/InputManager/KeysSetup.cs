﻿using UnityEngine;
using XTeam.Resources.Utilities.InputManager.cMonkeys;

namespace XTeam.Resources.Utilities.InputManager
{
    public class KeysSetup : MonoBehaviour {

        // Use this for initialization
        void Start () {
            //Movement keys.
            CInput.SetKey("Forward", Keys.W, Keys.UpArrow);
            CInput.SetKey("Backward", Keys.S, Keys.DownArrow);
            CInput.SetKey("Left", Keys.A, Keys.LeftArrow);
            CInput.SetKey("Right", Keys.D, Keys.RightArrow);

            CInput.SetKey("Jump", Keys.Space);
            CInput.SetKey("Sprint", Keys.LeftShift);
            CInput.SetKey("Crouch", Keys.LeftControl, Keys.RightControl);
            CInput.SetKey("Prone", Keys.LeftAlt);

            CInput.SetKey("Lean Left", Keys.Q);
            CInput.SetKey("Lean Right", Keys.E);

            //Direction axis.
            CInput.SetAxis("Vertical Movement", "Backward", "Forward");
            CInput.SetAxis("Horizontal Movement", "Left", "Right");
            CInput.SetAxis("Horizontal Leaning", "Lean Left", "Lean Right");

            //Function keys.
            //Interact.
            CInput.SetKey("Interact", Keys.X);
            CInput.SetKey("Drop", Keys.Mouse2);
            CInput.SetKey("Grab", Keys.Mouse0, null, Keys.Mouse1);

            //Inventory.
            CInput.SetKey("NextItem", Keys.MouseWheelDown);
            CInput.SetKey("PreviousItem", Keys.MouseWheelUp);
            CInput.SetKey("Reload", Keys.R);

            //Voicechat
            CInput.SetKey("Talk", Keys.F);
        }
    }
}
