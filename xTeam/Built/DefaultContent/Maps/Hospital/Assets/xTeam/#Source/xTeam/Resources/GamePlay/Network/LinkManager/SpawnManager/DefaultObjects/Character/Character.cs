﻿using Assets.xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts;
using UnityEngine;
using UnityEngine.Networking;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character.Resources.Scripts;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Characters.StandardCharacter.Scripts;
using XTeam.API.Objects.Entities.Living;
using XTeam.Resources.GamePlay.Network.Client.Player;
using XTeam.Resources.GamePlay.Network.LinkManager.PlayerManager;
using XTeam.Resources.GamePlay.Network.Shared;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects.Character
{
    /// <summary>
    ///     Class for synchronizing character information.
    /// </summary>
    public class Character : NetworkBehaviour, ILivingEntity
    {
        [Tooltip("Owner of this character.")]
        public Player owner;
        [Tooltip("Reference to the main camera of the character.")]
        public Camera characterCamera;
        [Tooltip("Health component of this living entity.")]
        public Health health;
        [Tooltip("Inventory component of this living entity.")]
        public Inventory inventory;
        [Tooltip("Movement controler of this character.")]
        public MovementController movementController;

        #region Setup
        /// <summary>
        /// Sets a specified player as an owner of this character.
        /// Owner can move the character.
        /// </summary>
        /// <param name="p"></param>
        [Command]
        public void CmdSetOwner(PlayerInfo p)
        {
            owner = PlayerManager.GetPlayer(p.guid);
            RpcEnableComponents(p);
        }
        /// <summary>
        /// Enables components of this character for a specified player.
        /// </summary>
        /// <param name="p"></param>
        [ClientRpc]
        public void RpcEnableComponents(PlayerInfo p)
        {
            if (Client.guid == p.guid)
            {
                EnableComponents();
            }
        }
        /// <summary>
        ///     Enabling required componenets on the character.
        /// </summary>
        public void EnableComponents()
        {
            Debug.Log("Enabling character components...");
            movementController.controlsActive = true;
            characterCamera.gameObject.SetActive(true);
        }
        #endregion
        #region Gets
        /// <summary>
        /// Gets the name of the character.
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            if (owner != null)
            {
                return owner.playerName;
            }
            else
            {
                return "NO-NAME";
            }
        }
        /// <summary>
        /// Gets health component of the character.
        /// </summary>
        /// <returns></returns>
        public Health GetHealth()
        {
            return health;
        }
        /// <summary>
        /// Gets inventory component of the character.
        /// </summary>
        /// <returns></returns>
        public Inventory GetInventory()
        {
            return inventory;
        }
        #endregion

        #region MonoBehaviour
        /// <summary>
        ///     Checkes whether the character was spawned on a server or a single player world.
        /// </summary>
        private void Start()
        {
            if (Client.unetClient == null || !Client.unetClient.isConnected)
            {
                EnableComponents();
            }
        }
        #endregion
    }
}