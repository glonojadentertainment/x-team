﻿using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.API;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.LinkManager.LinkCallbackManager;
using XTeam.Resources.GamePlay.Network.Shared;
using XTeam.Resources.Utilities.FileManager;
using Object = UnityEngine.Object;

namespace XTeam.Resources.GamePlay.Network.LinkManager.LinkManagerSetup
{
    /// <summary>
    ///     Class for spawning and performing startup setup for LinkManageron the server.
    /// </summary>
    internal class LinkManagerServerSetup : MonoBehaviour
    {
        #region Prefabs

        /// <summary>
        ///     Prefab of the Server Settings object.
        /// </summary>
        public Object serverSettingsPrefab;

        #endregion

        /// <summary>
        ///     Sets up the LinkManager.
        /// </summary>
        internal void SetupLinkManagerServer()
        {
            Debug.Log("Setting up the server LinkManager...");

            //Spawning default GameObjects.
            SpawnLinkManagerModules();
            //Applying server settings.
            ApplyServerSettings();
            //Registering handlers.
            RegisterServerLinkManagerHandlers();
            //Registering LinkContentManager handlers.
            NetworkContentManager.RegisterServerHandlers();

            //Starting ServerConnectionBridge...
            ServerInfoSender.StartListeningForConnections();

            //Loading server content.
            if (Server.Server.properties.loadContentFromFolder)
            {
                //Adding all the content from the Content folder to the contentToLoad.
                foreach (var contentPack in Directory.GetDirectories(FileManager.GetApplicationContentPath()))
                {
                    Server.Server.properties.contentToLoad.Add(contentPack);
                }
            }
            ContentManager.ContentManager.LoadAllContent(Server.Server.properties.contentToLoad.ToArray(), ContentExecutor.SERVER);

            //Calling OnServerReady.
            LinkManager.instance.linkServerCallbackManager.OnServerReady();
        }

        /// <summary>
        ///     Spawns all the LinkManager modules.
        /// </summary>
        private void SpawnLinkManagerModules()
        {
            Debug.Log("Spawning the LinkServerCallbackManager...");
            LinkManager.instance.linkServerCallbackManager = gameObject.AddComponent<LinkServerCallbackManager>();

            Debug.Log("Spawning the Server Settings object...");
            var serverSettingsGameObject = (GameObject) Instantiate(serverSettingsPrefab);
            LinkManager.serverSettings = serverSettingsGameObject.GetComponent<ServerSettings.ServerSettings>();
            LinkManager.serverSettings.CopyFrom(Server.Server.properties);
            NetworkServer.Spawn(serverSettingsGameObject);
        }

        /// <summary>
        ///     Applies server settings.
        /// </summary>
        private void ApplyServerSettings()
        {
            //Setting the server tickrate.
            Time.fixedDeltaTime = 1/LinkManager.serverSettings.serverTickRate;
			//Setting the server sendrate.
            UnityEngine.Network.sendRate = LinkManager.serverSettings.serverSendRate;
        }

        /// <summary>
        ///     Registers server handlers.
        /// </summary>
        private void RegisterServerLinkManagerHandlers()
        {
            Debug.Log("Registering server handlers...");
            //General handlers.
            NetworkServer.RegisterHandler(XMsgTypes.ClientMsgLogin, PlayerManager.PlayerManager.OnClientLogin);
            NetworkServer.RegisterHandler(XMsgTypes.ClientMsgReady, PlayerManager.PlayerManager.OnClientReady);
        }
    }
}