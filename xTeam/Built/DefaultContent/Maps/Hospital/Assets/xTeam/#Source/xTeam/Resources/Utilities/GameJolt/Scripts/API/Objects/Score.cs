﻿using System;
using GameJolt.External.SimpleJSON;

namespace GameJolt.API.Objects
{
	public class Score : Base
	{
		#region Fields & Properties
		public int Value { get; set; }
		public string Text { get; set; }
		public string Extra { get; set; }
		public string Time { get; set; }

		public int UserID { get; set; }
		public string UserName { get; set; }
		public string GuestName { get; set; }
		public string PlayerName
		{
			get
			{
				return UserName != null && UserName != string.Empty ? UserName : GuestName;
			}
		}
		#endregion Fields & Properties
		
		#region Constructors
		public Score(int value, string text, string guestName = "", string extra = "")
		{
			Value = value;
			Text = text;
			GuestName = guestName;
			Extra = extra;
		}

		public Score(JSONClass data)
		{
			PopulateFromJSON(data);
		}
		#endregion Constructors
		
		#region Update Attributes
		protected override void PopulateFromJSON(JSONClass data)
		{
			Value = data["sort"].AsInt;
			Text = data["score"].Value;
			Extra = data["extra_data"].Value;
			Time = data["stored"].Value;

			UserID = data["user_id"].AsInt;
			UserName = data["user"].Value;
			GuestName = data["guest"].Value;
		}
		#endregion Update Attributes

		#region Interface
		public void Add(int table = 0, Action<bool> callback = null)
		{
			Scores.Add(this, table, callback);
		}
		#endregion Interface
		
		public override string ToString()
		{
			return string.Format("GameJolt.API.Objects.Score: {0} - {1}", PlayerName, Value);
		}
	}
}