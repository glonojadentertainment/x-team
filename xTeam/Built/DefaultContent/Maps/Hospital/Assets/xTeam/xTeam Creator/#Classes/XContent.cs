﻿using xTeam.xTeam_Creator_Classes.Resources;

namespace XTeam.API
{
    public abstract class XContent
    {
        /// <summary>
        /// Determines whether content is ran on server or client.
        /// </summary>
        public ContentExecutor runningOn;
        /// <summary>
        /// Gets ContentInfo of this XContent.
        /// </summary>
        /// <returns></returns>
        public abstract ContentInfo GetInfo();
        /// <summary>
        /// Called when the content is enabled.
        /// </summary>
        /// <returns>Whether the content was enabled successfully. If not, an error message will be displayed.</returns>
        public virtual bool OnEnable()
        {
            //Default to false.
            return false;
        }
        /// <summary>
        /// Called when the content is disabled.
        /// </summary>
        public virtual void OnDisable()
        {
            
        }
    }
    /// <summary>
    /// Entity executing the XContent.
    /// </summary>
    public enum ContentExecutor
    {
        CLIENT, SERVER
    }
}
