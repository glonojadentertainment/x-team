﻿using UnityEngine;
using UnityEngine.Networking;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Optional.Objects.ObjectAttributes
{
    [RequireComponent (typeof (Rigidbody))]
    public class GrabableObject : NetworkBehaviour {
        [Tooltip ("How long can object be grabbed. 0 = infinity.")]
        public float maxGrabDuration;
    }
}