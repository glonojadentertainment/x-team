﻿using UnityEngine;

namespace xTeam.xTeam_Creator.Classes.MapCreator.Prefabs.Optional.Objects.DroppedItems
{
    public class DroppedItemSpawnpoint : MonoBehaviour {

        [Tooltip ("Name of the item, that will be spawned. It should be already filled in by the prefab.")]
        public string itemName;
        [Tooltip ("Name of the item, that will replace the original one.")]
        public string customItemName;
        [Tooltip ("A point in which item will be spawned. It should be already filled in by the prefab.")]
        public GameObject spawnPoint;
    }
}
