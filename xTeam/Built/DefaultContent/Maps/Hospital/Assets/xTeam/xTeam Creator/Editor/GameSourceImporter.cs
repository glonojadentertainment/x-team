﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace xTeam.xTeam_Creator.Core.Editor
{
    public static class GameSourceImporter
    {
        /// <summary>
        /// Folder for game assemblies inside the assets folder.
        /// </summary>
        public static string projectAssemblyFolder;
        /// <summary>
        /// Imports game assemblies from the specified path.
        /// </summary>
        [MenuItem("xTeam/Import source")]
        internal static void ImportGameSource()
        {
            Debug.Log("Importing xTeam source code...");

            //Deleting old source folder.
            if (new DirectoryInfo(GetSourceProjectPath()).Exists)
            {
                new DirectoryInfo(GetSourceProjectPath()).Delete(true);
            }
            //Copying the source code.
            DirectoryCopy(GetGameSourcePath(), GetSourceProjectPath(), true);

            //Refreshing asset database.
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }
        /// <summary>
        /// Copies a specified directory.
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if(dir.Name == "xTeam Creator") return;

            if (!dir.Exists)
            {
                Directory.CreateDirectory(dir.FullName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        /// <summary>
        /// Gets game path where all the source code is located.
        /// </summary>
        /// <returns>Game assembly path.</returns>
        internal static string GetGameSourcePath()
        {
            return EditorUtility.OpenFolderPanel("Choose xTeam directory", Application.dataPath, "") + "/" + "xTeam_Data" + "/" + "Source";
        }
        /// <summary>
        /// Gets the path where assemblies imported from the game are stored.
        /// </summary>
        /// <returns>Path where assemblies imported from the game are stored</returns>
        internal static string GetSourceProjectPath()
        {
            return Application.dataPath + "/xTeam/#Source";
        }
    }
}
