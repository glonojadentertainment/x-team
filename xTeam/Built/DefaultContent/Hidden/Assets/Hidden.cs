﻿using UnityEngine;
using xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager;
using xTeam.xTeam_Creator_Classes.Resources;
using XTeam.API;
using XTeam.Resources.GamePlay.ContentManager;

public class Hidden : XContent
{
    public override ContentInfo GetInfo()
    {
        return new ContentInfo
        {
            name = "Hidden:xTeam",
            version = 1
        };
    }

    public override bool OnEnable()
    {
        Debug.Log("Enabled Hidden:xTeam");

        if (runningOn == ContentExecutor.SERVER)
        {
            SpawnManager.Spawn(ContentBrowser.GetContentPackPath(GetInfo().name, runningOn) + "/Objects/" + "TestObj.xto", Vector3.zero, Quaternion.identity,
                o =>
                {
                    Debug.Log("Test object spawned!");
                });
        }

        return true;
    }

    public override void OnDisable()
    {
        Debug.Log("Disabled Hidden:xTeam");
    }
}