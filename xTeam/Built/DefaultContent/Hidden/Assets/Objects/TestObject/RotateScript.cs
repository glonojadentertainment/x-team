﻿using UnityEngine;

namespace Objects.TestObject
{
    public class RotateScript : MonoBehaviour
    {
        [SerializeField]
        private float rotationsPerMinute = 10f;

        private void Update()
        {
            transform.Rotate(0, 6f * rotationsPerMinute * Time.deltaTime, 0);
        }
    }
}
