﻿using UnityEngine;
using UnityEngine.Networking;

namespace XTeam.Resources.GamePlay.Network.Client.Player
{
    /// <summary>
    /// Representation of a player.
    /// </summary>
    public class Player : NetworkBehaviour
    {
        /// <summary>
        /// Name of the player.
        /// </summary>
        [SyncVar]
        public string playerName;
        /// <summary>
        /// Info of the player.
        /// </summary>
        [SyncVar] public PlayerInfo playerInfo;
        /// <summary>
        /// Ready flag of the player.
        /// </summary>
        [SyncVar]
        public bool isReady;
        /// <summary>
        /// Connection ID of the player.
        /// </summary>
        [SyncVar]
        public int connectionID;
        /// <summary>
        /// Custom attributes of the player.
        /// </summary>
        public SyncListPlayerAttribute playerAttributes = new SyncListPlayerAttribute();
        /// <summary>
        /// NetworkIdentity of the player.
        /// </summary>
        public NetworkIdentity networkIdentity;

        /// <summary>
        /// Local network IP of the player.
        /// </summary>
        [SyncVar]
        private string _localIp;
        /// <summary>
        /// Internet IP of the player.
        /// </summary>
        [SyncVar]
        private string _publicIp;

        /// <summary>
        /// Sets up the player variables based on a PlayerInfo.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="connectionID"></param>
        [Server]
        internal void SetupVariables(PlayerInfo info, int connectionID)
        {
            Debug.Log("Setting up the player " + info.name);

            playerName = info.name;
            playerInfo = info;
            _localIp = info.localIP;
            _publicIp = info.publicIP;
            this.connectionID = connectionID;
        }
        public override void OnStartClient()
        {
            if (playerInfo.guid == Shared.Client.guid)
            {
                Shared.Client.player = this;
            }
        }

        /// <summary>
        /// Gets connection address of the player.
        /// </summary>
        /// <returns>PlayerInfo connection address.</returns>
        public string GetConnectionAddress()
        {
            if (_publicIp == IPUtility.GetPublicIp().ToString())
            {
                return _localIp;
            }
            else
            {
                return _publicIp;
            }
        }
    }
    /// <summary>
    /// A custom player attribute.
    /// </summary>
    public struct PlayerAttribute
    {
        /// <summary>
        /// Name of the attribute.
        /// </summary>
        public string name;
        /// <summary>
        /// Value of the attrribute.
        /// </summary>
        public string value;
    }
    /// <summary>
    /// Synced list of player attributes.
    /// </summary>
    public class SyncListPlayerAttribute : SyncListStruct<PlayerAttribute> {}
}