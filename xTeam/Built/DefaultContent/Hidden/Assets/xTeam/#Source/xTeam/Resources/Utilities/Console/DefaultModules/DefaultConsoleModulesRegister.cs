﻿using UnityEngine;
using XTeam.API.Console;
using XTeam.Resources.GamePlay.Network.Client;

namespace XTeam.Resources.Utilities.Console.DefaultModules
{
    /// <summary>
    /// Class for registering the default Console Modules.
    /// </summary>
    public class DefaultConsoleModulesRegister : MonoBehaviour {
        /// <summary>
        /// Registering default console modules.
        /// </summary>
        void Start()
        {
            ConsoleHandler.RegisterModule(new ServerConsoleModule());
            ConsoleHandler.RegisterModule(new ClientConsoleModule());
        }
    }
}
