﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace XTeam.API.Console
{
    /// <summary>
    /// Class handling the console commands.
    /// </summary>
    public class ConsoleHandler
    {
        /// <summary>
        /// List of all registered console modules.
        /// </summary>
        private static List<ConsoleModule> _registeredConsoleModules = new List<ConsoleModule>();
        /// <summary>
        /// Whether logging is paused.
        /// </summary>
        public static bool loggingPaused = false;
        /// <summary>
        /// List of methods to which every command will be forwarded.
        /// </summary>
        public static List<Action<ConsoleCommand>> independentCommandReceivers = new List<Action<ConsoleCommand>>();

        /// <summary>
        /// Runs a specified command as a console.
        /// </summary>
        /// <param name="command">Command to run.</param>
        public static void Dispatch(ConsoleCommand command)
        {
            DispatchOnModule(command);
        }
        private static void DispatchOnModule(ConsoleCommand command)
        {
            //Getting the target ConsoleModule of the specified command.
            ConsoleModule module = GetConsoleModuleByCommand(command);
            if (module == null)
            {
                return;
            }

            //Calling the OnCommandDispatch method on the ConsoleModule and checking whether error accours.
            if (!module.OnCommandRun(command))
            {
                Debug.LogError("There was an error running the command: " + command.baseCommand);
            }
        }
        /// <summary>
        /// Returns a registered console module using the specified command.
        /// </summary>
        /// <param name="command">Command of the wanted module.</param>
        /// <returns></returns>
        public static ConsoleModule GetConsoleModuleByCommand(ConsoleCommand command)
        {
            foreach (ConsoleModule module in _registeredConsoleModules)
            {
                foreach(string baseCommand in module.GetBaseCommands())
                {
                    if(baseCommand == command.baseCommand)
                    {
                        return module;
                    }
                    if(baseCommand == "ANY")
                    {
                        return module;
                    }
                }
            }

            Debug.LogWarning("Console module: " + command.baseCommand + " could not be found.");
            return null;
        }
        /// <summary>
        /// Registers a Console Module.
        /// </summary>
        /// <param name="module">Module to register.</param>
        public static void RegisterModule(ConsoleModule module)
        {
            //Adding the specified console module to the registered console modules list.
            _registeredConsoleModules.Add(module);

            Debug.Log("Registered console module: " + module.GetName());
        }
        /// <summary>
        /// Unregisters a Console Module.
        /// </summary>
        /// <param name="module">Module to unregister.</param>
        public static void UnRegisterModule(ConsoleModule module)
        {
            //Adding the specified console module to the registered console modules list.
            _registeredConsoleModules.Remove(module);

            Debug.Log("Unregistered console module: " + module.GetName());
        }
    } 
}
