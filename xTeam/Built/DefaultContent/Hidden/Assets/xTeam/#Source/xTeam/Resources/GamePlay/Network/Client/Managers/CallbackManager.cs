﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;
using XTeam.Resources.GamePlay.Network.LinkManager.LinkManagerSetup;
using XTeam.Resources.Utilities.LevelLoader;

namespace XTeam.Resources.GamePlay.Network.Client.Managers
{
    /// <summary>
    /// Class managing Client callbacks.
    /// </summary>
    internal class CallbackManager : MonoBehaviour {
        /// <summary>
        /// Called when client connects to the server.
        /// </summary>
        internal static void OnClientConnect(NetworkMessage msg)
        {
            //Loading empty scene.
            if (!SceneManager.GetSceneByName("Untitled").IsValid())
            {
                SceneManager.CreateScene("Untitled");
                new GameObject("Camera").AddComponent<Camera>();
            }
            SceneManager.LoadSceneAsync("Untitled", LoadSceneMode.Single);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Untitled"));

            //Logging in to the server.
            LinkManagerClientSetup.LoginToServer();
        }
        /// <summary>
        /// Called when client disconnects from the server.
        /// </summary>
        /// <param name="msg">NetworkMessage details.</param>
        internal static void OnClientDisconnect(NetworkMessage msg)
        {
            Debug.Log("Disconnected from server.");
            Server.Server.instance.StartCoroutine(LevelLoader.LoadScene("HUB", LoadSceneMode.Single));
        }

        /// <summary>
        /// Called when an error accours when connecting to the server.
        /// </summary>
        /// <param name="msg">NetworkMessage details.</param>
        internal static void OnErrorConnecting(NetworkMessage msg)
        {
            Debug.LogError("There was an error connecting to the server.");
            Server.Server.instance.StartCoroutine(LevelLoader.LoadScene("ServerList", LoadSceneMode.Single));
        }
        /// <summary>
        /// Called when the player has been kicked from the server.
        /// </summary>
        /// <param name="msg"></param>
        internal static void OnClientKick(NetworkMessage msg)
        {
            
        }
        /// <summary>
        /// Called when guid info has been received from the server.
        /// </summary>
        /// <param name="msg"></param>
        internal static void OnGuidInfoGetFromServer(NetworkMessage msg)
        {
            //Setting Client's GUID.
            Shared.Client.guid = msg.ReadMessage<StringMessage>().value;
        }
        /// <summary>
        /// Called when all of the server data has been loaded.
        /// </summary>
        /// <param name="msg"></param>
        internal static void OnClientLoadServerData(NetworkMessage msg)
        {
            //All data successfully loaded from the server.
            //Setting client as ready.
            ClientScene.AddPlayer(Shared.Client.unetClient.connection, 0);
        }
    }
}
