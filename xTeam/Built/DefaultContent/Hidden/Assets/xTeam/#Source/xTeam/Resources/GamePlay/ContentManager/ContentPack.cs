﻿using xTeam.xTeam_Creator_Classes.Resources;

namespace XTeam.Resources.GamePlay.ContentManager
{
    /// <summary>
    /// Class holding information about a downloaded contentpack.
    /// </summary>
    public class ContentPack
    {
        public ContentInfo info;
        public string path;
    }
}
