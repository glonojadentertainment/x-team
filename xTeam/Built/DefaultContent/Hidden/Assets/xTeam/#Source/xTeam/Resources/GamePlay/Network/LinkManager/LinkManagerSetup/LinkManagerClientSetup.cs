﻿using UnityEngine;
using XTeam.Resources.GamePlay.Network.Client.Player;
using XTeam.Resources.GamePlay.Network.LinkManager.LinkCallbackManager;
using XTeam.Resources.Utilities;

namespace XTeam.Resources.GamePlay.Network.LinkManager.LinkManagerSetup
{
    /// <summary>
    /// Class for spawning and performing startup setup for LinkManager on the client.
    /// </summary>
    internal class LinkManagerClientSetup : MonoBehaviour
    {
        ///<summary>
        /// Sets up the linkManager.
        /// </summary>
        internal void SetupLinkManagerClient()
        {
            Debug.Log("Setting up the client LinkManager...");
            //Spawning LinkManager modules.
            SpawnLinkManagerModules();
            //Caching spawned players.
            CacheSpawnedPlayers();
        }
        /// <summary>
        /// Spawns all the non-networked LinkManager modules.
        /// </summary>
        private void SpawnLinkManagerModules()
        {
            Debug.Log("Spawning the LinkClientCallbackManager...");
            LinkManager.instance.linkClientCallbackManager = gameObject.AddComponent<LinkClientCallbackManager>();

            LinkManager.serverSettings = FindObjectOfType<ServerSettings.ServerSettings>();
        }
        /// <summary>
        /// Logs in into the currently connected server.
        /// </summary>
        internal static void LoginToServer()
        {
            Debug.Log("Logging in to the server...");

            var playerInfo = new PlayerInfo
            {
                name = PlayerIdentity.playerName,
                avatarUrl = PlayerIdentity.playerAvatarUrl,
                localIP = IPUtility.GetLocalIp().ToString(),
                publicIP = IPUtility.GetPublicIp().ToString()
            };

            Shared.Client.unetClient.Send(XMsgTypes.ClientMsgLogin, new PlayerManager.PlayerManager.LoginMessage()
            {
                connectionAddress = Shared.Client.unetClient.serverIp,
                playerInfo = playerInfo
            });
        }
        /// <summary>
        /// Caches all currently spawned players.
        /// </summary>
        private static void CacheSpawnedPlayers()
        {
            Debug.Log("Caching spawned players");

            foreach (var p in FindObjectsOfType<Player>())
            {
                p.transform.SetParent(PlayerManager.PlayerManager.instance.playersHolder.transform);
            }
        }
    }
}