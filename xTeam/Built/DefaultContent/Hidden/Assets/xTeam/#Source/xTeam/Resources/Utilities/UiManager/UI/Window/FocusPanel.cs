﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace XTeam.Resources.Utilities.UiManager.UI.Window
{
    public class FocusPanel : MonoBehaviour, IPointerDownHandler
    {

        private RectTransform _panel;

        void Awake()
        {
            _panel = GetComponent<RectTransform>();
        }

        public void OnPointerDown(PointerEventData data)
        {
            _panel.SetAsLastSibling();
        }

    }
}