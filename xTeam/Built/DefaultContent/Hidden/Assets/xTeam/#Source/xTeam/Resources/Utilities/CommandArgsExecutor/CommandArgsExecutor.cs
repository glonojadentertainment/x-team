﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.Network.Server;

namespace xTeam.Resources.Utilities.CommandArgsExecutor
{
    internal class CommandArgsExecutor : MonoBehaviour {
        /// <summary>
        /// Checks whether there were any command line arguments passed to the game.
        /// </summary>
        private void Start () {
            //Searching for specific command args.
            foreach (var arg in Environment.GetCommandLineArgs())
            {
                if (arg.Contains("loadcontent"))
                {
                    //Adding the specified ContentPack to the customContentToLoad.
                    Server.properties.loadContentFromFolder = false;
                    Server.properties.contentToLoad.Add(string.Join(":", arg.Split(':'), 1, 2));
                    //Starting server if not running.
                    if (!NetworkServer.active)
                    {
                        Server.StartServer();
                    }
                }
            }
        }
    }
}
