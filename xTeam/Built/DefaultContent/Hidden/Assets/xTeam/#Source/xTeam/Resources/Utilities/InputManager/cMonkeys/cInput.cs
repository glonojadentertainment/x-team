﻿#define Use_cInputGUI // Comment out this line to use your own GUI instead of cInput's built-in GUI.

#region Namespaces

using System;
using System.Collections.Generic;
using UnityEngine;

#endregion

/***********************************************************************
 *  cInput 2.7.5 by cMonkeys (Ward Dewaele and Deozaan)
 *  This script is NOT free, unlike Custom Inputmanager 1.x.
 *  Therefore the use of this script is strictly personal and 
 *  may not be spread without permission.
 *  
 *  Any technical or license questions can be mailed
 *  to ward.dewaele@pandora.be, but read the 
 *  included help documents first please.
 ***********************************************************************/

namespace XTeam.Resources.Utilities.InputManager.cMonkeys
{
    public class CInput : MonoBehaviour {

        #region cInput Variables and Properties

#if Use_cInputGUI

        [Obsolete("Please use cGUI.cSkin.")]
        public static GUISkin CSkin {
            get {
                Debug.LogWarning("cInput.cSkin has been deprecated. Please use cGUI.cSkin.");
                return CGui.cSkin;
            }
            set {
                Debug.LogWarning("cInput.cSkin has been deprecated. Please use cGUI.cSkin.");
                CGui.cSkin = value;
            }
        } // cSkin is DEPRECATED!

#endif

        public static float gravity = 3;
        public static float sensitivity = 3;
        public static float deadzone = 0.001f;

        public static bool Scanning { get { return _scanning; } } // scanning is read-only
        public static int Length {
            get {
                _cInputInit(); // if cInput doesn't exist, create it
                return _inputLength + 1;
            }
        } // length is read-only
        public static bool AllowDuplicates {
            get {
                _cInputInit(); // if cInput doesn't exist, create it
                return _allowDuplicates;
            }
            set {
                _allowDuplicates = value;
                PlayerPrefs.SetString("cInput_dubl", value.ToString());
                _exAllowDuplicates = value.ToString();
            }
        }

        // Private variables
        private static bool _allowDuplicates = false;
        private static string[,] _defaultStrings = new string[99, 5];
        private static string[] _inputName = new string[99]; // name of the input action (e.g., "Jump")
        private static KeyCode[] _inputPrimary = new KeyCode[99]; // primary input assigned to action (e.g., "Space")
        private static KeyCode[] _modifierUsedPrimary = new KeyCode[99]; // modfier used on primary input
        private static KeyCode[] _inputSecondary = new KeyCode[99]; // secondary input assigned to action
        private static KeyCode[] _modifierUsedSecondary = new KeyCode[99]; // modfier used on secondary input
        private static List<KeyCode> _modifiers = new List<KeyCode>(); // list that holds the allowed modifiers
        private static List<int> _markedAsAxis = new List<int>(); // list that keeps track of which actions are used to make axis
        private static string[] _axisName = new string[99];
        private static string[] _axisPrimary = new string[99];
        private static string[] _axisSecondary = new string[99];
        private static float[] _individualAxisSens = new float[99]; // individual axis sensitivity settings
        private static float[] _individualAxisGrav = new float[99]; // individual axis gravity settings
        private static float[] _individualAxisDead = new float[99]; // individual axis gravity settings
        private static bool[] _invertAxis = new bool[99];
        private static int[,] _makeAxis = new int[99, 2];
        private static int _inputLength = -1;
        private static int _axisLength = -1;
        private static List<KeyCode> _forbiddenKeys = new List<KeyCode>();

        private static bool[] _getKeyArray = new bool[99]; // values stored for GetKey function
        private static bool[] _getKeyDownArray = new bool[99]; // values stored for GetKeyDown
        private static bool[] _getKeyUpArray = new bool[99]; // values stored for GetKeyUp
        private static bool[] _axisTriggerArrayPrimary = new bool[99]; // values that help to check if an axis is up or down
        private static bool[] _axisTriggerArraySecondary = new bool[99]; // values that help to check if secondary input for an axis is up or down
        private static float[] _getAxis = new float[99];
        private static float[] _getAxisRaw = new float[99];
        private static float[] _getAxisArray = new float[99];
        private static float[] _getAxisArrayRaw = new float[99];

        // which types of inputs to allow when assigning inputs to actions
        private static bool _allowMouseAxis = false;
        private static bool _allowMouseButtons = true;
        private static bool _allowJoystickButtons = true;
        private static bool _allowJoystickAxis = true;
        private static bool _allowKeyboard = true;

        private static int _numGamepads = 4; // number of gamepads supported by built-in Input Manager settings

        // these strings are set by ShowMenu() to customize the look of cInput's menu
        private static string _menuHeaderString = "label";
        private static string _menuActionsString = "box";
        private static string _menuInputsString = "box";
        private static string _menuButtonsString = "button";

        /// <summary>Are we scanning inputs to make a new assignment?</summary>
        private static bool _scanning;
        /// <summary>Which index number of the array for inputs to scan for</summary>
        private static int _cScanIndex;
        /// <summary>Which input are we scanning for (primary (1) or secondary (2))?</summary>
        private static int _cScanInput;
        /// <summary>A reference to the cInput instance.</summary>
        private static CInput _cObject;
#if Use_cInputGUI
        /// <summary>A reference to the cInputGUI instance.</summary>
        private static CInputGui _cInputGuiObject;
#endif
        private static bool _cKeysLoaded;
        /// <summary>This is used to store all axis raw values so we can see if they changed since scanning began</summary>
        private static Dictionary<string, float> _axisRawValues = new Dictionary<string, float>();

        // External saving related variables
        private static string _exAllowDuplicates;
        private static string _exAxis;
        private static string _exAxisInverted;
        private static string _exDefaults;
        private static string _exInputs;
        private static string _exCalibrations;
        private static string _exCalibrationValues;
        private static bool _externalSaving = false;

        private static Dictionary<string, KeyCode> _string2Key = new Dictionary<string, KeyCode>();

        private static int[] _axisType = new int[10 * _numGamepads];
        /// <summary>The value that should be considered 0 for this axis</summary>
        private static Dictionary<string, float> _axisCalibrationOffset = new Dictionary<string, float>();
        // Note: this wastes one slot since we're ignoring gamepad 0
        private static string[,] _joyStrings = new string[_numGamepads + 1, 11];
        private static string[,] _joyStringsPos = new string[_numGamepads + 1, 11];
        private static string[,] _joyStringsNeg = new string[_numGamepads + 1, 11];

        #endregion // cInput Variables and Properties

        #region Awake/Start/Update functions

        void Awake() {
            DontDestroyOnLoad(this); // Keep this thing from getting destroyed if we change levels.

            // set values to global values
            for (int n = 0; n < 99; n++) {
                _individualAxisSens[n] = -99;
                _individualAxisGrav[n] = -99;
                _individualAxisDead[n] = -99;
            }
        }

        void Start() {
            _CreateDictionary();
            if (_externalSaving) {
                _LoadExternalInputs();
                //Debug.Log("cInput loaded inputs from external source.");
            } else {
                _LoadInputs();
                //Debug.Log("cInput settings loaded inputs from PlayerPrefs.");
            }

            AddModifier(KeyCode.None); // we need to initialize the modifiers with this one
        }

        void Update() {
            if (_scanning) {
                if (_cScanInput != 0) {
                    // scan for a button press to assign a key (using the GUI)
                    _InputScans();
                } else {
                    // this is the part where a button is actually assigned after scanning is complete
                    string prim;
                    string sec;

                    if (string.IsNullOrEmpty(_axisPrimary[_cScanIndex])) {
                        prim = _inputPrimary[_cScanIndex].ToString();
                    } else {
                        prim = _axisPrimary[_cScanIndex];
                    }

                    if (string.IsNullOrEmpty(_axisSecondary[_cScanIndex])) {
                        sec = _inputSecondary[_cScanIndex].ToString();
                    } else {
                        sec = _axisSecondary[_cScanIndex];
                    }

                    _ChangeKey(_cScanIndex, _inputName[_cScanIndex], prim, sec);
                    _scanning = false;
                }
            } else {
                // update values for all inputs
                _CheckInputs();
            }
        }

        #endregion

        public static void Init() {
            _cInputInit(); // if cInput doesn't exist, create it
        }

        private static void _CreateDictionary() {
            if (_string2Key.Count == 0) { // don't create the dictionary more than once
                for (int i = (int)KeyCode.None; i <= (int)KeyCode.Joystick4Button19; i++) {
                    KeyCode key = (KeyCode)i;
                    _string2Key.Add(key.ToString(), key);
                }

                // Create joystrings dictionaries
                for (int i = 1; i <= _numGamepads; i++) {
                    for (int j = 1; j <= 10; j++) {
                        _joyStrings[i, j] = "Joy" + i + " Axis " + j;
                        _joyStringsPos[i, j] = "Joy" + i + " Axis " + j + "+";
                        _joyStringsNeg[i, j] = "Joy" + i + " Axis " + j + "-";
                    }
                }
            }
        }

        public static void ForbidKey(KeyCode key) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_forbiddenKeys.Contains(key)) {
                _forbiddenKeys.Add(key);
            }
        }

        public static void ForbidKey(string keyString) {
            _cInputInit(); // if cInput doesn't exist, create it
            KeyCode key = _ConvertString2Key(keyString);
            ForbidKey(key);
        }

        #region AddModifier and RemoveModifier functions

        /// <summary>Designates a key for use as a modifier.</summary>
        /// <param name="modifierKey">The KeyCode for the key to be used as a modifier.</param>
        public static void AddModifier(KeyCode modifierKey) {
            _cInputInit(); // if cInput doesn't exist, create it
            _modifiers.Add(modifierKey);
        }

        /// <summary>Designates a key for use as a modifier.</summary>
        /// <param name="modifier">The string name of the key to be used as a modifier.</param>
        public static void AddModifier(string modifier) {
            _cInputInit(); // if cInput doesn't exist, create it
            AddModifier(_ConvertString2Key(modifier));
        }

        /// <summary>Removes a key for use as a modifier.</summary>
        /// <param name="modifierKey">The KeyCode for the key which should no longer be used as a modifier.</param>
        public static void RemoveModifier(KeyCode modifierKey) {
            _cInputInit(); // if cInput doesn't exist, create it
            _modifiers.Remove(modifierKey);
        }

        /// <summary>Removes a key for use as a modifier.</summary>
        /// <param name="modifier">The string name of the key which should no longer be used as a modifier.</param>
        public static void RemoveModifier(string modifier) {
            _cInputInit(); // if cInput doesn't exist, create it
            RemoveModifier(_ConvertString2Key(modifier));
        }

        #endregion

        private static KeyCode _ConvertString2Key(string str) {
            if (String.IsNullOrEmpty(str)) { return KeyCode.None; }
            if (_string2Key.Count == 0) { _CreateDictionary(); }

            if (_string2Key.ContainsKey(str)) {
                KeyCode key = _string2Key[str];
                return key;
            } else {
                if (!_IsAxisValid(str)) {
                    Debug.Log("cInput error: " + str + " is not a valid input.");
                }

                return KeyCode.None;
            }
        }

        #region SetKey functions

        #region SetKey Overloaded Functions
        // this is for compatibility with UnityScript which doesn't accept default parameters
        public static void SetKey(string action, string primary) {
            // note that Keys.None is used here for secondary and secondaryModifier because there is no secondary input
            SetKey(action, primary, Keys.None, primary, Keys.None);
        }

        public static void SetKey(string action, string primary, string secondary) {
            SetKey(action, primary, secondary, primary, secondary);
        }

        // Defines a Key with a modifier on the primary input
        public static void SetKey(string action, string primary, string secondary, string primaryModifier) {
            SetKey(action, primary, secondary, primaryModifier, secondary);
        }

        #endregion //SetKey Overloaded Functions

        // Defines a Key with modifiers
        public static void SetKey(string action, string primary, string secondary, string primaryModifier, string secondaryModifier) {
            _cInputInit(); // if cInput doesn't exist, create it

            // make sure we pass valid values for the modifiers
            if (String.IsNullOrEmpty(primaryModifier) || primaryModifier == Keys.None) { primaryModifier = primary; }
            if (String.IsNullOrEmpty(secondaryModifier) || secondaryModifier == Keys.None) { secondaryModifier = secondary; }

            int index = _FindKeyByDescription(action);

            // make sure this key hasn't already been set
            if (index == -1) {
                int num = _inputLength + 1;
                // actually set the key
                _SetDefaultKey(num, action, primary, secondary, primaryModifier, secondaryModifier);
            } else {
#if UNITY_EDITOR
                // skip this warning if an input with the same settings already exists
                int pStringHash = (primaryModifier != primary) ? (primaryModifier + " + " + primary).GetHashCode() : primary.GetHashCode();
                int sStringHash = (secondaryModifier != secondary) ? (secondaryModifier + " + " + secondary).GetHashCode() : secondary.GetHashCode();
                if (pStringHash == GetText(index, 1).GetHashCode() && sStringHash == GetText(index, 2).GetHashCode()) { return; }

                // also skip this warning if we loaded from an external source or we already created the cInput object
                if (_externalSaving == false || _cObject == null) {
                    // Whoops! Key with this name already exists!
                    Debug.LogWarning("A key with the name of " + action + " already exists. You should use ChangeKey() if you want to change an existing key!\n" +
                                     "This message will only be shown in the editor and is safe to ignore if you're reloading a scene/script that initializes the " + action + " input.");
                }
#endif
            }
        }

        private static void _SetDefaultKey(int num, string _name, string input1, string input2, string pMod, string sMod) {
            _defaultStrings[num, 0] = _name;
            _defaultStrings[num, 1] = input1;
            _defaultStrings[num, 2] = (string.IsNullOrEmpty(input2)) ? KeyCode.None.ToString() : input2;
            _defaultStrings[num, 3] = string.IsNullOrEmpty(pMod) ? input1 : pMod;
            _defaultStrings[num, 4] = string.IsNullOrEmpty(sMod) ? input2 : sMod;

            if (num > _inputLength) { _inputLength = num; }

            _modifierUsedPrimary[num] = _ConvertString2Key(_defaultStrings[num, 3]);
            _modifierUsedSecondary[num] = _ConvertString2Key(_defaultStrings[num, 4]);
            _SetKey(num, _name, input1, input2);
            _SaveDefaults();
        }

        private static void _SetKey(int num, string _name, string input1, string input2) {
            // input description 
            _inputName[num] = _name;
            _axisPrimary[num] = "";

            if (_string2Key.Count == 0) { return; }

            if (!string.IsNullOrEmpty(input1)) {
                // enter keyboard input in the input array
                KeyCode keyCode1 = _ConvertString2Key(input1);
                _inputPrimary[num] = keyCode1;

                // enter mouse and gamepad axis inputs in the inputstring array
                string axisName = _ChangeStringToAxisName(input1);
                if (input1 != axisName) {
                    _axisPrimary[num] = input1;
                }
            }

            _axisSecondary[num] = "";

            if (!string.IsNullOrEmpty(input2)) {
                // enter input in the alt input array
                KeyCode keyCode2 = _ConvertString2Key(input2);
                _inputSecondary[num] = keyCode2;

                // enter mouse and gamepad axis inputs in the inputstring array
                string axisName = _ChangeStringToAxisName(input2);
                if (input2 != axisName) {
                    _axisSecondary[num] = input2;
                }
            }
        }

        #endregion

        #region SetAxis and SetAxisSensitivity & related functions

        #region Overloaded SetAxis Functions

        // overload method to allow you to set an axis with two inputs
        public static void SetAxis(string description, string negativeInput, string positiveInput) {
            SetAxis(description, negativeInput, positiveInput, sensitivity, gravity, deadzone);
        }

        // overload method to allow you to set the sensitivity of the axis
        public static void SetAxis(string description, string negativeInput, string positiveInput, float axisSensitivity) {
            SetAxis(description, negativeInput, positiveInput, axisSensitivity, gravity, deadzone);
        }

        // overload method to allow you to set the sensitivity and the gravity an the axis
        public static void SetAxis(string description, string negativeInput, string positiveInput, float axisSensitivity, float axisGravity) {
            SetAxis(description, negativeInput, positiveInput, axisSensitivity, axisGravity, deadzone);
        }

        // overload method to allow you to set an axis with only one input
        public static void SetAxis(string description, string input) {
            SetAxis(description, input, "-1", sensitivity, gravity, deadzone);
        }

        // overload method to allow you to set an axis with only one input, and set sensitivity
        public static void SetAxis(string description, string input, float axisSensitivity) {
            SetAxis(description, input, "-1", axisSensitivity, gravity, deadzone);
        }

        // overload method to allow you to set an axis with only one input, and set sensitivity and gravity
        public static void SetAxis(string description, string input, float axisSensitivity, float axisGravity) {
            SetAxis(description, input, "-1", axisSensitivity, axisGravity, deadzone);
        }

        // overload method to allow you to set an axis with only one input, and set sensitivity, gravity and deadzone
        public static void SetAxis(string description, string input, float axisSensitivity, float axisGravity, float axisDeadzone) {
            SetAxis(description, input, "-1", axisSensitivity, axisGravity, axisDeadzone);
        }

        #endregion

        // This is the function that all other SetAxis overload methods call to actually set the axis
        public static void SetAxis(string description, string negativeInput, string positiveInput, float axisSensitivity, float axisGravity, float axisDeadzone) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (IsKeyDefined(negativeInput)) {
                int num = _FindAxisByDescription(description); // overwrite existing axis of same name
                if (num == -1) {
                    // this axis doesn't exist, so make a new one
                    num = _axisLength + 1;
                }

                int posInput = -1; // -1 by default, which means no input.
                int negInput = _FindKeyByDescription(negativeInput);

                if (IsKeyDefined(positiveInput)) {
                    posInput = _FindKeyByDescription(positiveInput);
                    _markedAsAxis.Add(_FindKeyByDescription(positiveInput)); // add the actions in the marked list
                    _markedAsAxis.Add(negInput);
                } else if (positiveInput != "-1") {
                    // the key isn't defined and we're not passing in -1 as a value, so there's a problem
                    Debug.LogError("Can't define Axis named: " + description + ". Please define '" + positiveInput + "' with SetKey() first.");
                    return; // break out of this function without trying to assign the axis
                }

                _SetAxis(num, description, negInput, posInput);
                _individualAxisSens[negInput] = axisSensitivity;
                _individualAxisGrav[negInput] = axisGravity;
                _individualAxisDead[negInput] = axisDeadzone;
                if (posInput >= 0) {
                    _individualAxisSens[posInput] = axisSensitivity;
                    _individualAxisGrav[posInput] = axisGravity;
                    _individualAxisDead[posInput] = axisDeadzone;
                }
            } else {
                Debug.LogError("Can't define Axis named: " + description + ". Please define '" + negativeInput + "' with SetKey() first.");
            }
        }

        private static void _SetAxis(int num, string description, int negative, int positive) {
            if (num > _axisLength) {
                _axisLength = num;
            }

            _axisName[num] = description;
            _makeAxis[num, 0] = negative;
            _makeAxis[num, 1] = positive;
            _SaveAxis();
        }

        // this allows you to set the axis sensitivity directly (after the axis has been defined)
        public static void SetAxisSensitivity(string axisName, float sensitivity) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot set sensitivity of " + axisName + ". Have you defined this axis with SetAxis() yet?");
            } else {
                // axis has been defined
                _individualAxisSens[_makeAxis[axis, 0]] = sensitivity;
                _individualAxisSens[_makeAxis[axis, 1]] = sensitivity;
            }
        }

        // this allows you to set the axis gravity directly (after the axis has been defined)
        public static void SetAxisGravity(string axisName, float gravity) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot set gravity of " + axisName + ". Have you defined this axis with SetAxis() yet?");
            } else {
                // axis has been defined
                _individualAxisGrav[_makeAxis[axis, 0]] = gravity;
                _individualAxisGrav[_makeAxis[axis, 1]] = gravity;
            }
        }

        // this allows you to set the axis deadzone directly (after the axis has been defined)
        public static void SetAxisDeadzone(string axisName, float deadzone) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot set deadzone of " + axisName + ". Have you defined this axis with SetAxis() yet?");
            } else {
                // axis has been defined
                _individualAxisDead[_makeAxis[axis, 0]] = deadzone;
                _individualAxisDead[_makeAxis[axis, 1]] = deadzone;
            }
        }

        #endregion

        #region GetAxisSensitivity/Gravity/Deadzone functions

        /// <summary>Retrieve the sensitivity value of the axis.</summary>
        /// <param name="axisName">The axis you want the sensitivity value for.</param>
        public static float GetAxisSensitivity(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot get sensitivity of " + axisName + ". Have you defined this axis with SetAxis() yet?");
                return -1;
            } else {
                // axis has been defined
                return _individualAxisSens[_makeAxis[axis, 0]];
            }
        }

        /// <summary>Retrieve the gravity value of the axis.</summary>
        /// <param name="axisName">The axis you want the gravity value for.</param>
        public static float GetAxisGravity(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot get gravity of " + axisName + ". Have you defined this axis with SetAxis() yet?");
                return -1;
            } else {
                // axis has been defined
                return _individualAxisGrav[_makeAxis[axis, 0]];
            }
        }

        /// <summary>Retrieve the deadzone value of the axis.</summary>
        /// <param name="axisName">The axis you want the deadzone value for.</param>
        public static float GetAxisDeadzone(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            int axis = _FindAxisByDescription(axisName);
            if (axis == -1) {
                // axis not defined!
                Debug.LogError("Cannot get deadzone of " + axisName + ". Have you defined this axis with SetAxis() yet?");
                return -1;
            } else {
                // axis has been defined
                return _individualAxisDead[_makeAxis[axis, 0]];
            }
        }

        #endregion //GetAxisSensitivity/Gravity/Deadzone functions

        #region Calibration functions

        public static void Calibrate() {
            _cInputInit(); // if cInput doesn't exist, create it
            string saveCals = "";
            _axisCalibrationOffset = _GetAxisRawValues();
            PlayerPrefs.SetString("cInput_calsVals", _CalibrationValuesToString());
            for (int joyNum = 1; joyNum <= _numGamepads; joyNum++) {
                for (int axisNum = 1; axisNum <= 10; axisNum++) {
                    int index = 10 * (joyNum - 1) + (axisNum - 1);
                    string joystring = _joyStrings[joyNum, axisNum];
                    float axisRaw = Input.GetAxisRaw(joystring);
                    _axisType[index] = (axisRaw < -deadzone) ? 1 : // axis is negative by default
                        (axisRaw > deadzone) ? -1 : // axis is positive by default
                            0; // axis is 0 by default
                    saveCals += _axisType[index] + "*";
                    PlayerPrefs.SetString("cInput_saveCals", saveCals);
                    _exCalibrations = saveCals;
                }
            }
        }

        private static string _CalibrationValuesToString() {
            string calVals = "";
            foreach (KeyValuePair<string, float> kvp in _axisCalibrationOffset) {
                calVals += kvp.Key + "*" + kvp.Value.ToString() + "#";
            }

            return calVals;
        }

        private static void _CalibrationValuesFromString(string calVals) {
            _axisCalibrationOffset.Clear(); // start with a clean slate
            string[] kvps = calVals.Split('#');
            for (int i = 0; i < kvps.Length - 1; i++) {
                string[] kvp = kvps[i].Split('*');
                _axisCalibrationOffset.Add(kvp[0], float.Parse(kvp[1]));
            }
        }

        private static float _GetCalibratedAxisInput(string description) {
            float rawValue = Input.GetAxisRaw(_ChangeStringToAxisName(description));

            switch (description) {
                case "Mouse Left":
                case "Mouse Right":
                case "Mouse Up":
                case "Mouse Down":
                case "Mouse Wheel Up":
                case "Mouse Wheel Down": { return rawValue; }
            }

            for (int joyNum = 1; joyNum <= _numGamepads; joyNum++) {
                for (int axisNum = 1; axisNum <= 10; axisNum++) {
                    string joyNeg = _joyStringsNeg[joyNum, axisNum];
                    string joyPos = _joyStringsPos[joyNum, axisNum];

                    if (description == joyNeg || description == joyPos) {
                        int index = 10 * (joyNum - 1) + (axisNum - 1);
                        switch (_axisType[index]) {
                            default:
                            case 0: {
                                return rawValue;
                            }
                            case 1: {
                                return (rawValue + 1) / 2;
                            }
                            case -1: {
                                return (rawValue - 1) / 2;
                            }
                        }
                    }
                }
            }

            Debug.LogWarning("No server found for " + description + " (" + _ChangeStringToAxisName(description) +
                             "). This should never happen, in theory. Returning value of " + rawValue);
            return rawValue;
        }

        #endregion

        #region ChangeKey functions

        #region ChangeKey (wait for input) functions

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        /// <param name="allowGamepadButtons">Allow a gamepad button to be bound? Default is true.</param>
        /// <param name="allowKeyboard">Allow keyboard keys to be bound? Default is true.</param>
        public static void ChangeKey(string action, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis, bool allowGamepadButtons, bool allowKeyboard) {
            _cInputInit(); // if cInput doesn't exist, create it
            int num = _FindKeyByDescription(action);
            _ScanForNewKey(num, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, allowGamepadButtons, allowKeyboard);
        }

        #region overloaded ChangeKey(string) functions for UnityScript compatibility

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        public static void ChangeKey(string action) {
            ChangeKey(action, 1, _allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        public static void ChangeKey(string action, int input) {
            ChangeKey(action, input, _allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        public static void ChangeKey(string action, int input, bool allowMouseAxis) {
            ChangeKey(action, input, allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        public static void ChangeKey(string action, int input, bool allowMouseAxis, bool allowMouseButtons) {
            ChangeKey(action, input, allowMouseAxis, allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        public static void ChangeKey(string action, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis) {
            ChangeKey(action, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="action">The string name of the key/action you want to change.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        /// <param name="allowGamepadButtons">Allow a gamepad button to be bound? Default is true.</param>
        public static void ChangeKey(string action, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis, bool allowGamepadButtons) {
            ChangeKey(action, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, allowGamepadButtons, _allowKeyboard);
        }

        #endregion //overloaded ChangeKey(string) functions for UnityScript compatibility

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        /// <param name="allowGamepadButtons">Allow a gamepad button to be bound? Default is true.</param>
        /// <param name="allowKeyboard">Allow keyboard keys to be bound? Default is true.</param>
        public static void ChangeKey(int index, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis, bool allowGamepadButtons, bool allowKeyboard) {
            _cInputInit(); // if cInput doesn't exist, create it
            _ScanForNewKey(index, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, allowGamepadButtons, allowKeyboard);
        }

        #region overloaded ChangeKey(int) functions for UnityScript compatibility

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        public static void ChangeKey(int index) {
            ChangeKey(index, 1, _allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        public static void ChangeKey(int index, int input) {
            ChangeKey(index, input, _allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        public static void ChangeKey(int index, int input, bool allowMouseAxis) {
            ChangeKey(index, input, allowMouseAxis, _allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        public static void ChangeKey(int index, int input, bool allowMouseAxis, bool allowMouseButtons) {
            ChangeKey(index, input, allowMouseAxis, allowMouseButtons, _allowJoystickAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        public static void ChangeKey(int index, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis) {
            ChangeKey(index, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, _allowJoystickButtons, _allowKeyboard);
        }

        /// <summary>cInput will wait for the user to press a button, then bind that button to this key. Also see cInput.scanning.</summary>
        /// <param name="index">The input array index of the key you want to change. Useful in for loops for GUI.</param>
        /// <param name="input">Primary = 1, Secondary = 2</param>
        /// <param name="allowMouseAxis">Allow a mouse axis to be bound? Default is false.</param>
        /// <param name="allowMouseButtons">Allow a mouse button to be bound? Default is true.</param>
        /// <param name="allowGamepadAxis">Allow a gamepad axis to be bound? Default is true.</param>
        /// <param name="allowGamepadButtons">Allow a gamepad button to be bound? Default is true.</param>
        public static void ChangeKey(int index, int input, bool allowMouseAxis, bool allowMouseButtons, bool allowGamepadAxis, bool allowGamepadButtons) {
            ChangeKey(index, input, allowMouseAxis, allowMouseButtons, allowGamepadAxis, allowGamepadButtons, _allowKeyboard);
        }

        #endregion //overloaded ChangeKey(int) functions for UnityScript compatibility

        #endregion //ChangeKey (wait for input) functions

        #region ChangeKey set via script (don't wait for input)

        // this lets the dev directly change the key without waiting for the player to push buttons.
        public static void ChangeKey(string action, string primary, string secondary, string primaryModifier, string secondaryModifier) {
            _cInputInit(); // if cInput doesn't exist, create it
            int num = _FindKeyByDescription(action);

            // set modifiers
            if (String.IsNullOrEmpty(primaryModifier)) {
                primaryModifier = primary;
            }

            if (String.IsNullOrEmpty(secondaryModifier)) {
                secondaryModifier = secondary;
            }

            _modifierUsedPrimary[num] = _ConvertString2Key(primaryModifier);
            _modifierUsedSecondary[num] = _ConvertString2Key(secondaryModifier);

            _ChangeKey(num, action, primary, secondary);
        }

        #region overloaded ChangeKey(string, primary, secondary) function for UnityScript compatibility)

        public static void ChangeKey(string action, string primary) {
            int num = _FindKeyByDescription(action);
            ChangeKey(action, primary, "", primary, _modifierUsedSecondary[num].ToString());
        }

        public static void ChangeKey(string action, string primary, string secondary) {
            ChangeKey(action, primary, secondary, primary, secondary);
        }

        public static void ChangeKey(string action, string primary, string secondary, string primaryModifier) {
            ChangeKey(action, primary, secondary, primaryModifier, secondary);
        }

        #endregion

        #endregion //ChangeKey set via script (don't wait for input)

        /// <summary>This starts the process of scanning for a new key (using the GUI to assign an input).</summary>
        /// <param name="num">The index of the input array</param>
        /// <param name="input">Primary or secondary input</param>
        /// <param name="mouseAx">Allow mouse axis (mouse ball) to be bound?</param>
        /// <param name="mouseBut">Allow mouse buttons to be bound?</param>
        /// <param name="joyAx">Allow joystick axes to be bound?</param>
        /// <param name="joyBut">Allow joystick buttons to be bound?</param>
        /// <param name="keyb">Allow keys from the keyboard to be bound?</param>
        private static void _ScanForNewKey(int num, int input, bool mouseAx, bool mouseBut, bool joyAx, bool joyBut, bool keyb) {
            _allowMouseAxis = mouseAx;
            _allowMouseButtons = mouseBut;
            _allowJoystickButtons = joyBut;
            _allowJoystickAxis = joyAx;
            _allowKeyboard = keyb;

            _cScanInput = input;
            _cScanIndex = num;
            _scanning = true;

            _axisRawValues = _GetAxisRawValues(); // get current axis values to make sure they change while scanning
        }

        /// <summary>Iterates through all of the axes in the Input Manager and saves the values in a dictionary.</summary>
        private static Dictionary<string, float> _GetAxisRawValues() {
            Dictionary<string, float> arv = new Dictionary<string, float>(); // arv means Axis Raw Values

            // these are all manually taken from the Axes that cInput installs in the Input Manager
            arv.Add("Horizontal", Input.GetAxisRaw("Horizontal"));
            arv.Add("Vertical", Input.GetAxisRaw("Vertical"));
            arv.Add("Fire1", Input.GetAxisRaw("Fire1"));
            arv.Add("Fire2", Input.GetAxisRaw("Fire2"));
            arv.Add("Fire3", Input.GetAxisRaw("Fire3"));
            arv.Add("Jump", Input.GetAxisRaw("Jump"));
            arv.Add("Mouse X", Input.GetAxisRaw("Mouse X"));
            arv.Add("Mouse Y", Input.GetAxisRaw("Mouse Y"));
            arv.Add("Mouse Horizontal", Input.GetAxisRaw("Mouse Horizontal"));
            arv.Add("Mouse Vertical", Input.GetAxisRaw("Mouse Vertical"));
            arv.Add("Mouse ScrollWheel", Input.GetAxisRaw("Mouse ScrollWheel"));
            arv.Add("Mouse Wheel", Input.GetAxisRaw("Mouse Wheel"));
            arv.Add("Window Shake X", Input.GetAxisRaw("Window Shake X"));
            arv.Add("Window Shake Y", Input.GetAxisRaw("Window Shake Y"));
            arv.Add("Shift", Input.GetAxisRaw("Shift"));

            string gpString = "";
            for (int gamePad = 1; gamePad <= _numGamepads; gamePad++) {
                for (int axis = 1; axis <= 10; axis++) {
                    gpString = "Joy" + gamePad + " Axis " + axis;
                    arv.Add(gpString, Input.GetAxis(gpString));
                }
            }

            return arv;
        }

        private static void _ChangeKey(int num, string action, string primary, string secondary) {
            _SetKey(num, action, primary, secondary);
            _SaveInputs();
        }

        #endregion

        #region _DefaultsExist, IsKeyDefined, and IsAxisDefined functions

        private static bool _DefaultsExist() {
            return (_defaultStrings.Length > 0) ? true : false;
        }

        public static bool IsKeyDefined(string keyName) {
            _cInputInit(); // if cInput doesn't exist, create it
            return (_FindKeyByDescription(keyName) >= 0) ? true : false;
        }

        public static bool IsAxisDefined(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            return (_FindAxisByDescription(axisName) >= 0) ? true : false;
        }

        #endregion

        #region CheckInputs function

        /// <summary>This is the magic that updates the values for all the inputs in cInput</summary>
        private void _CheckInputs() {
            bool inputPrimary = false; // a digital button/key; true if it's currently being pushed down
            bool inputSecondary = false; // a digital button/key; true if it's currently being pushed down
            bool axisPrimaryDefined = false; // whether or not an axis has a primary input defined for this input
            bool axisSecondaryDefined = false; // whether or not an axis has a secondary input defined for this input
            float axisPrimaryValue = 0f; // the value of the primary input for this element
            float axisSecondaryValue = 0f; // the value of the secondary input for this element

            #region Update input values

            for (int n = 0; n < _inputLength + 1; n++) {

                #region Handle cInput Keys/Buttons

                inputPrimary = Input.GetKey(_inputPrimary[n]);
                inputSecondary = Input.GetKey(_inputSecondary[n]);

                bool pModPressed = false; // is the primary modifier currently being pressed?
                bool sModPressed = false; // is the secondary modifier currently being pressed?
                bool modifierPressed = false; // is any modifier currently being pressed?

                for (int i = 0; i < _modifiers.Count; i++) {
                    if (Input.GetKey(_modifiers[i])) {
                        modifierPressed = true; // at least one modifier is active
                        if (!pModPressed && _modifiers[i] == _modifierUsedPrimary[n]) { pModPressed = true; }
                        if (!sModPressed && _modifiers[i] == _modifierUsedSecondary[n]) { sModPressed = true; }
                    }
                }

                /* These next two lines are realy ugly, so here's an explanation of the parts:
			 * (_modifierUsedPrimary[n] == _inputPrimary[n]) <-- means there is NO modifier for this input
			 * (!_modifierPressed) <-- means there was NO modifier key pushed
			 * (_modifierUsedPrimary[n] != _inputPrimary[n]) <-- means there IS a modifier for this input
			 * (_pModPressed) <-- means the modifier for this input HAS been pushed.
			 * 
			 * So what this does is checks two things:
			 * If there's no modifier AND no modifier keys are being pressed, we're good to go.
			 * OR
			 * If there is a modifier AND the modifier key is being pressed, we're good to go.
			 * */
                // These bools are used to determine if this key's modifier (if any) is being pushed.
                bool primaryModifierPassed = (((_modifierUsedPrimary[n] == _inputPrimary[n]) && !modifierPressed) || ((_modifierUsedPrimary[n] != _inputPrimary[n]) && pModPressed));
                bool secondaryModifierPassed = (((_modifierUsedSecondary[n] == _inputSecondary[n]) && !modifierPressed) || (_modifierUsedSecondary[n] != _inputSecondary[n] && sModPressed));

                if (!string.IsNullOrEmpty(_axisPrimary[n])) {
                    axisPrimaryDefined = true; // this is an axis
                    axisPrimaryValue = _GetCalibratedAxisInput(_axisPrimary[n]) * _PosOrNeg(_axisPrimary[n]);
                } else {
                    axisPrimaryDefined = false; // this isn't an axis
                    // set the value to 1 if the key is being pushed down, otherwise it's zero
                    axisPrimaryValue = inputPrimary ? 1f : 0f;
                }

                if (!string.IsNullOrEmpty(_axisSecondary[n])) {
                    axisSecondaryDefined = true;
                    axisSecondaryValue = _GetCalibratedAxisInput(_axisSecondary[n]) * _PosOrNeg(_axisSecondary[n]);
                } else {
                    axisSecondaryDefined = false; // this isn't an axis
                    // set the value to 1 if the key is being pushed down, otherwise it's zero
                    axisSecondaryValue = inputSecondary ? 1f : 0f;
                }

                #region GetKey
                if ((inputPrimary && primaryModifierPassed) || (inputSecondary && secondaryModifierPassed) || (axisPrimaryDefined && axisPrimaryValue > deadzone) || (axisSecondaryDefined && axisSecondaryValue > deadzone)) {
                    _getKeyArray[n] = true;
                } else {
                    _getKeyArray[n] = false;
                }
                #endregion //GetKey

                #region GetKeyDown
                if ((primaryModifierPassed && Input.GetKeyDown(_inputPrimary[n])) || (secondaryModifierPassed && Input.GetKeyDown(_inputSecondary[n]))) {
                    _getKeyDownArray[n] = true;
                } else {
                    bool doOnce = false;
                    if (axisPrimaryDefined && axisPrimaryValue > deadzone && !_axisTriggerArrayPrimary[n]) {
                        _axisTriggerArrayPrimary[n] = true;
                        doOnce = true;
                    }
                    if (axisSecondaryDefined && axisSecondaryValue > deadzone && !_axisTriggerArraySecondary[n]) {
                        _axisTriggerArraySecondary[n] = true;
                        doOnce = true;
                    }

                    _getKeyDownArray[n] = ((_axisTriggerArrayPrimary[n] || _axisTriggerArraySecondary[n]) && doOnce);
                }
                #endregion //GetKeyDown

                #region GetKeyUp
                if ((Input.GetKeyUp(_inputPrimary[n]) && primaryModifierPassed) || (Input.GetKeyUp(_inputSecondary[n]) && secondaryModifierPassed)) {
                    _getKeyUpArray[n] = true;
                } else {
                    bool doOnce = false;
                    if (axisPrimaryDefined && axisPrimaryValue <= deadzone && _axisTriggerArrayPrimary[n]) {
                        _axisTriggerArrayPrimary[n] = false;
                        doOnce = true;
                    }

                    if (axisSecondaryDefined && axisSecondaryValue <= deadzone && _axisTriggerArraySecondary[n]) {
                        _axisTriggerArraySecondary[n] = false;
                        doOnce = true;
                    }

                    _getKeyUpArray[n] = ((!_axisTriggerArrayPrimary[n] || !_axisTriggerArraySecondary[n]) && doOnce);
                }
                #endregion //GetKeyUp

                #endregion //Handle cInput Keys/Buttons

                #region Handle cInput Axes

                // Store global sensitivity, gravity and deadzone so we can change them and restore them later.
                // I know it seems silly to do this every iteration of the loop, but for some reason if we don't, it breaks things.
                float defaultSens = sensitivity;
                float defaultGrav = gravity;
                float defaultDead = deadzone;
                // Set individual sensitivity, gravity and deadzone
                sensitivity = (_individualAxisSens[n] != -99) ? _individualAxisSens[n] : defaultSens;
                gravity = (_individualAxisGrav[n] != -99) ? _individualAxisGrav[n] : defaultGrav;
                deadzone = (_individualAxisDead[n] != -99) ? _individualAxisDead[n] : defaultDead;

                // this keeps input working even if Time.deltaTime is 0
                float fauxDeltaTime = (Time.deltaTime == 0) ? 0.012f : Time.deltaTime;

                // gets the axis value(s) and apply smoothing (sensitivity/gravity) for non-raw value
                if (axisPrimaryValue > deadzone || axisSecondaryValue > deadzone) {
                    // for the raw value, just take the highest value from the primary or secondary input
                    _getAxisRaw[n] = Mathf.Max(axisPrimaryValue, axisSecondaryValue);

                    // use sensitivity settings to gradually bring the non-raw value up to 1 if not already there
                    if (_getAxis[n] < _getAxisRaw[n]) { _getAxis[n] = Mathf.Min(_getAxis[n] + sensitivity * fauxDeltaTime, _getAxisRaw[n]); }
                    // use gravity settings to gradually bring the non-raw value back down to zero if analog input decreases
                    if (_getAxis[n] > _getAxisRaw[n]) { _getAxis[n] = Mathf.Max(_getAxisRaw[n], _getAxis[n] - gravity * fauxDeltaTime); }
                } else {
                    // both inputs are less than or equal to deadzone cutoff
                    _getAxisRaw[n] = 0; //pretend you're not getting any value at all on the raw axis

                    // use gravity settings to gradually bring the non-raw value back down to zero if not already there
                    if (_getAxis[n] > 0) { _getAxis[n] = Mathf.Max(0, _getAxis[n] - gravity * fauxDeltaTime); }
                }

                // Restore global sensitivity, gravity and deadzone.
                // I know it seems silly to do this every iteration of the loop, but for some reason if we don't, it breaks things.
                sensitivity = defaultSens;
                gravity = defaultGrav;
                deadzone = defaultDead;

                #endregion //Handle cInput Axes

            }

            #endregion //Update input values

            /*
		 * NO LONGER IN THE FOR LOOP ABOVE WHICH GETS THE VALUES OF THE INPUTS!
		*/

            // compile the virtual axes (negative and positive)
            for (int n = 0; n <= _axisLength; n++) {
                int neg = _makeAxis[n, 0];
                int pos = _makeAxis[n, 1];
                if (_makeAxis[n, 1] == -1) {
                    // This axis has no "positive" input defined, so use the "negative" axis as the default value
                    _getAxisArray[n] = _getAxis[neg];
                    _getAxisArrayRaw[n] = _getAxisRaw[neg];
                } else {
                    // This axis has both a negative and positive input defined, so combine them for the result
                    _getAxisArray[n] = _getAxis[pos] - _getAxis[neg];
                    _getAxisArrayRaw[n] = _getAxisRaw[pos] - _getAxisRaw[neg];
                }
            }
        }

        #endregion

        #region GetKey, GetAxis, GetText, and related functions

        #region GetKey functions

        // returns -1 only if there was an error
        private static int _FindKeyByDescription(string description) {
            int hash = description.GetHashCode();

            for (int i = 0; i < _inputName.Length; i++) {
                if (String.IsNullOrEmpty(_inputName[i])) {
                    break;
                } else if (_inputName[i].GetHashCode() == hash) {
                    return i;
                }
            }

            // uh oh, the string didn't server!
            return -1;
        }

        /// <summary>Returns true every frame the input is being pressed</summary>
        public static bool GetKey(string description) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_DefaultsExist()) {
                Debug.LogError("No default inputs found. Please setup your default inputs with SetKey first.");
                return false;
            }

            if (!_cKeysLoaded) { return false; } // make sure we've saved/loaded keys before trying to access them.
            int index = _FindKeyByDescription(description);

            if (index > -1) {
                return _getKeyArray[index];
            } else {
                // if we got this far then the string didn't server and there's a problem
                Debug.LogError("Couldn't find a key server for " + description + ". Is it possible you typed it wrong or forgot to setup your defaults after making changes?");
                return false;
            }
        }

        /// <summary>Returns true just once when the input is first pressed down</summary>
        public static bool GetKeyDown(string description) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_DefaultsExist()) {
                Debug.LogError("No default inputs found. Please setup your default inputs with SetKey first.");
                return false;
            }

            if (!_cKeysLoaded) { return false; } // make sure we've saved/loaded keys before trying to access them.
            int index = _FindKeyByDescription(description);

            if (index > -1) {
                return _getKeyDownArray[index];
            } else {
                // if we got this far then the string didn't server and there's a problem
                Debug.LogError("Couldn't find a key server for " + description + ". Is it possible you typed it wrong or forgot to setup your defaults after making changes?");
                return false;
            }
        }

        /// <summary>Returns true just once when the input is released</summary>
        public static bool GetKeyUp(string description) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_DefaultsExist()) {
                Debug.LogError("No default inputs found. Please setup your default inputs with SetKey first.");
                return false;
            }

            if (!_cKeysLoaded) { return false; } // make sure we've saved/loaded keys before trying to access them.
            int index = _FindKeyByDescription(description);

            if (index > -1) {
                return _getKeyUpArray[index];
            } else {
                // if we got this far then the string didn't server and there's a problem
                Debug.LogError("Couldn't find a key server for " + description + ". Is it possible you typed it wrong or forgot to setup your defaults after making changes?");
                return false;
            }
        }

        #region GetButton functions -- they just call GetKey functions

        /// <summary>Returns true every frame the input is being pressed</summary>
        public static bool GetButton(string description) {
            return GetKey(description);
        }

        /// <summary>Returns true just once when the input is first pressed down</summary>
        public static bool GetButtonDown(string description) {
            return GetKeyDown(description);
        }

        /// <summary>Returns true just once when the input is released</summary>
        public static bool GetButtonUp(string description) {
            return GetKeyUp(description);
        }

        #endregion //GetButton functions -- they just call GetKey functions

        #endregion //GetKey functions

        #region GetAxis and related functions

        private static int _FindAxisByDescription(string description) {
            int hash = description.GetHashCode();

            for (int i = 0; i < _axisName.Length; i++) {
                if (String.IsNullOrEmpty(_axisName[i])) {
                    break;
                } else if (_axisName[i].GetHashCode() == hash) {
                    return i;
                }
            }

            return -1; // uh oh, the string didn't server!
        }

        public static float GetAxis(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_DefaultsExist()) {
                Debug.LogError("No default inputs found. Please setup your default inputs with SetKey first.");
                return 0;
            }

            int index = _FindAxisByDescription(axisName);
            if (index > -1) {
                if (_invertAxis[index]) {
                    // this axis should be inverted, so invert the value!
                    return _getAxisArray[index] * -1;
                } else {
                    // this axis is normal, return the normal value
                    return _getAxisArray[index];
                }
            }

            // if we got this far then the string didn't server and there's a problem
            Debug.LogError("Couldn't find an axis server for " + axisName + ". Is it possible you typed it wrong?");
            return 0;
        }

        public static float GetAxisRaw(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            if (!_DefaultsExist()) {
                Debug.LogError("No default inputs found. Please setup your default inputs with SetKey first.");
                return 0;
            }

            int index = _FindAxisByDescription(axisName);
            if (index > -1) {
                if (_invertAxis[index]) {
                    // this axis should be inverted, so invert the value!
                    return _getAxisArrayRaw[index] * -1;
                } else {
                    // this axis is normal, return the normal value
                    return _getAxisArrayRaw[index];
                }
            }

            // if we got this far then the string didn't server and there's a problem
            Debug.LogError("Couldn't find an axis server for " + axisName + ". Is it possible you typed it wrong?");
            return 0;
        }

        #endregion //GetAxis and related functions

        #region GetText, _ChangeStringToAxisName, _PosOrNeg functions

        #region Overloaded GetText(string) and GetText(int) functions for UnityScript compatibility

        public static string GetText(string action) {
            return GetText(action, 1);
        }

        public static string GetText(int index) {
            return GetText(index, 0);
        }

        #endregion //Overloaded GetText(string) and GetText(int) functions for UnityScript compatibility

        public static string GetText(string action, int input) {
            int index = _FindKeyByDescription(action);
            return GetText(index, input);
        }

        /// <summary>Get the name of an input using an int. Useful in for loops for GUIs.</summary>
        /// <param name="index">The index of the input.</param>
        /// <param name="input">Label, Primary, or Secondary. (0, 1, 2)</param>
        public static string GetText(int index, int input) {
            _cInputInit(); // if cInput doesn't exist, create it
            // make sure a valid value is passed in
            if (input < 0 || input > 2) {
                Debug.LogWarning("Can't look for text #" + input + " for " + _inputName[index] + " input. Only 0, 1, or 2 is acceptable. Clamping to this range.");
                input = Mathf.Clamp(input, 0, 2);
            }

            string name;

            if (input == 1) {
                if (!string.IsNullOrEmpty(_axisPrimary[index])) {
                    name = _axisPrimary[index];
                } else {
                    string prefix = "";
                    // if modifier is not empty and isn't the same as the key, and the key isn't empty
                    if (_modifierUsedPrimary[index] != KeyCode.None && _modifierUsedPrimary[index] != _inputPrimary[index] && _inputPrimary[index] != KeyCode.None) {
                        prefix = _modifierUsedPrimary[index].ToString() + " + ";
                    }
                    name = prefix + _inputPrimary[index].ToString();
                }
            } else if (input == 2) {
                if (!string.IsNullOrEmpty(_axisSecondary[index])) {
                    name = _axisSecondary[index];
                } else {
                    string prefix = "";
                    // if modifier is not empty and isn't the same as the key, and the key isn't empty
                    if (_modifierUsedSecondary[index] != KeyCode.None && _modifierUsedSecondary[index] != _inputSecondary[index] && _inputSecondary[index] != KeyCode.None) {
                        prefix = _modifierUsedSecondary[index].ToString() + " + ";
                    }
                    name = prefix + _inputSecondary[index].ToString();
                }
            } else {
                name = _inputName[index];
                return name;
            }

            // check to see if this key is currently waiting to be reassigned
            if (_scanning && (index == _cScanIndex) && (input == _cScanInput)) {
                name = ". . .";
            }

            return name;
        }

        private static string _ChangeStringToAxisName(string description) {
            // First we need to change the name of some of these things. . .
            switch (description) {
                case "Mouse Left": { return "Mouse Horizontal"; }
                case "Mouse Right": { return "Mouse Horizontal"; }
                case "Mouse Up": { return "Mouse Vertical"; }
                case "Mouse Down": { return "Mouse Vertical"; }
                case "Mouse Wheel Up": { return "Mouse Wheel"; }
                case "Mouse Wheel Down": { return "Mouse Wheel"; }
            }

            string joystring = _FindJoystringByDescription(description);
            if (joystring != null) {
                return joystring;
            }

            return description;
        }

        private static string _FindJoystringByDescription(string description) {
            for (int i = 1; i <= _numGamepads; i++) {
                for (int j = 1; j <= 10; j++) {
                    string joyPos = _joyStringsPos[i, j];
                    string joyNeg = _joyStringsNeg[i, j];
                    if (description == joyPos || description == joyNeg) {
                        return _joyStrings[i, j];
                    }
                }
            }

            return null;
        }

        private static bool _IsAxisValid(string axis) {
            switch (axis) {
                case "Mouse Left": { return true; }
                case "Mouse Right": { return true; }
                case "Mouse Up": { return true; }
                case "Mouse Down": { return true; }
                case "Mouse Wheel Up": { return true; }
                case "Mouse Wheel Down": { return true; }
            }

            bool state = false;
            for (int i = 1; i <= _numGamepads; i++) {
                for (int j = 1; j <= 10; j++) {
                    string joyPos = _joyStringsPos[i, j];
                    string joyNeg = _joyStringsNeg[i, j];
                    if (axis == joyPos || axis == joyNeg) {
                        state = true;
                    }
                }
            }

            return state;
        }

        // This function returns -1 for negative axes
        private static int _PosOrNeg(string description) {
            int posneg = 1;

            switch (description) {
                case "Mouse Left": { return -1; }
                case "Mouse Right": { return 1; }
                case "Mouse Up": { return 1; }
                case "Mouse Down": { return -1; }
                case "Mouse Wheel Up": { return 1; }
                case "Mouse Wheel Down": { return -1; }
            }

            for (int i = 1; i <= _numGamepads; i++) {
                for (int j = 1; j < 10; j++) {
                    string joyPos = _joyStringsPos[i, j];
                    string joyNeg = _joyStringsNeg[i, j];
                    if (description == joyPos) {
                        return 1;
                    } else if (description == joyNeg) {
                        return -1;
                    }
                }
            }

            return posneg;
        }

        #endregion //GetText, _ChangeStringToAxisName, _PosOrNeg functions

        #endregion //GetKey, GetAxis, GetText, and related functions

        #region Save, Load, Reset & Clear functions

        private static void _SaveAxis() {
            int num = _axisLength + 1;
            string axName = "";
            string axNeg = "";
            string axPos = "";
            string indAxSens = "";
            string indAxGrav = "";
            string indAxDead = "";
            for (int n = 0; n < num; n++) {
                axName += _axisName[n] + "*";
                axNeg += _makeAxis[n, 0] + "*";
                axPos += _makeAxis[n, 1] + "*";
                indAxSens += _individualAxisSens[n] + "*";
                indAxGrav += _individualAxisGrav[n] + "*";
                indAxDead += _individualAxisDead[n] + "*";
            }

            string axis = axName + "#" + axNeg + "#" + axPos + "#" + num;
            PlayerPrefs.SetString("cInput_axis", axis);
            PlayerPrefs.SetString("cInput_indAxSens", indAxSens);
            PlayerPrefs.SetString("cInput_indAxGrav", indAxGrav);
            PlayerPrefs.SetString("cInput_indAxDead", indAxDead);
            _exAxis = axis + "¿" + indAxSens + "¿" + indAxGrav + "¿" + indAxDead;
        }

        private static void _SaveAxInverted() {
            int num = _axisLength + 1;
            string axInv = "";

            for (int n = 0; n < num; n++) {
                axInv += _invertAxis[n] + "*";
            }

            PlayerPrefs.SetString("cInput_axInv", axInv);
            _exAxisInverted = axInv;
        }

        private static void _SaveDefaults() {
            // saving default inputs
            int num = _inputLength + 1;
            string defName = "";
            string def1 = "";
            string def2 = "";
            string defmod1 = "";
            string defmod2 = "";
            for (int n = 0; n < num; n++) {

                defName += _defaultStrings[n, 0] + "*";
                def1 += _defaultStrings[n, 1] + "*";
                def2 += _defaultStrings[n, 2] + "*";
                defmod1 += _defaultStrings[n, 3] + "*";
                defmod2 += _defaultStrings[n, 4] + "*";
            }

            string _Default = defName + "#" + def1 + "#" + def2 + "#" + defmod1 + "#" + defmod2;
            PlayerPrefs.SetInt("cInput_count", num);
            PlayerPrefs.SetString("cInput_defaults", _Default);
            _exDefaults = num + "¿" + _Default;
        }

        private static void _SaveInputs() {
            int num = _inputLength + 1;
            // *** save input configuration ***
            string descr = "";
            string inp = "";
            string altInp = "";
            string inpStr = "";
            string altInpStr = "";
            string modifierStr = "";
            string altModifierStr = "";

            for (int n = 0; n < num; n++) {
                // make the strings
                descr += _inputName[n] + "*";
                inp += _inputPrimary[n] + "*";
                altInp += _inputSecondary[n] + "*";
                inpStr += _axisPrimary[n] + "*";
                altInpStr += _axisSecondary[n] + "*";
                modifierStr += _modifierUsedPrimary[n] + "*";
                altModifierStr += _modifierUsedSecondary[n] + "*";
            }

            // save the strings to the PlayerPrefs
            PlayerPrefs.SetString("cInput_descr", descr);
            PlayerPrefs.SetString("cInput_inp", inp);
            PlayerPrefs.SetString("cInput_alt_inp", altInp);
            PlayerPrefs.SetString("cInput_inpStr", inpStr);
            PlayerPrefs.SetString("cInput_alt_inpStr", altInpStr);
            PlayerPrefs.SetString("cInput_modifierStr", modifierStr);
            PlayerPrefs.SetString("cInput_alt_modifierStr", altModifierStr);
            _exInputs = descr + "¿" + inp + "¿" + altInp + "¿" + inpStr + "¿" + altInpStr + "¿" + modifierStr + "¿" + altModifierStr;
        }

        public static string ExternalInputs {
            get {
                return _exAllowDuplicates + "æ" + _exAxis + "æ" + _exAxisInverted + "æ" + _exDefaults + "æ" + _exInputs +
                       "æ" + _exCalibrations + "æ" + _exCalibrationValues;
            }
        }

        public static void LoadExternal(string externString) {
            _cInputInit(); // if cInput doesn't exist, create it
            string[] tmpExternalStrings = externString.Split('æ');
            _exAllowDuplicates = tmpExternalStrings[0];
            _exAxis = tmpExternalStrings[1];
            _exAxisInverted = tmpExternalStrings[2];
            _exDefaults = tmpExternalStrings[3];
            _exInputs = tmpExternalStrings[4];
            _exCalibrations = tmpExternalStrings[5];
            _exCalibrationValues = tmpExternalStrings[6];
            _LoadExternalInputs();
        }

        private static void _LoadInputs() {
            if (!PlayerPrefs.HasKey("cInput_count")) {
                // there is nothing to load
                _cKeysLoaded = true;
                return;
            }

            if (PlayerPrefs.HasKey("cInput_dubl")) {
                if (PlayerPrefs.GetString("cInput_dubl") == "True") {
                    AllowDuplicates = true;
                } else {
                    AllowDuplicates = false;
                }
            }

            int count = PlayerPrefs.GetInt("cInput_count");
            _inputLength = count - 1;

            string defaults = PlayerPrefs.GetString("cInput_defaults");
            string[] arDefs = defaults.Split('#');
            string[] arDefName = arDefs[0].Split('*');
            string[] arDefPrime = arDefs[1].Split('*');
            string[] arDefSec = arDefs[2].Split('*');
            string[] arModPrime = arDefs[3].Split('*');
            string[] arModSec = arDefs[4].Split('*');

            for (int n = 0; n < arDefName.Length - 1; n++) {
                _SetDefaultKey(n, arDefName[n], arDefPrime[n], arDefSec[n], arModPrime[n], arModSec[n]);
            }

            if (PlayerPrefs.HasKey("cInput_inp")) {
                string descr = PlayerPrefs.GetString("cInput_descr");
                string inp = PlayerPrefs.GetString("cInput_inp");
                string altInp = PlayerPrefs.GetString("cInput_alt_inp");
                string inpStr = PlayerPrefs.GetString("cInput_inpStr");
                string altInpStr = PlayerPrefs.GetString("cInput_alt_inpStr");
                string modifierStr = PlayerPrefs.GetString("cInput_modifierStr");
                string altModifierStr = PlayerPrefs.GetString("cInput_alt_modifierStr");

                string[] arDescr = descr.Split('*');
                string[] arInp = inp.Split('*');
                string[] arAltInp = altInp.Split('*');
                string[] arInpStr = inpStr.Split('*');
                string[] arAltInpStr = altInpStr.Split('*');
                string[] arModifierStr = modifierStr.Split('*');
                string[] arAltModifierStr = altModifierStr.Split('*');

                for (int n = 0; n < arDescr.Length - 1; n++) {
                    if (arDescr[n] == _defaultStrings[n, 0]) {
                        _inputName[n] = arDescr[n];
                        _inputPrimary[n] = _ConvertString2Key(arInp[n]);
                        _inputSecondary[n] = _ConvertString2Key(arAltInp[n]);
                        _axisPrimary[n] = arInpStr[n];
                        _axisSecondary[n] = arAltInpStr[n];
                        _modifierUsedPrimary[n] = _ConvertString2Key(arModifierStr[n]);
                        _modifierUsedSecondary[n] = _ConvertString2Key(arAltModifierStr[n]);
                    }
                }

                // fixes inputs when defaults are being changed
                for (int m = 0; m < arDefName.Length - 1; m++) {
                    for (int n = 0; n < arDescr.Length - 1; n++) {
                        if (arDescr[n] == _defaultStrings[m, 0]) {
                            _inputName[m] = arDescr[n];
                            _inputPrimary[m] = _ConvertString2Key(arInp[n]);
                            _inputSecondary[m] = _ConvertString2Key(arAltInp[n]);
                            _axisPrimary[m] = arInpStr[n];
                            _axisSecondary[m] = arAltInpStr[n];
                            _modifierUsedPrimary[n] = _ConvertString2Key(arModifierStr[n]);
                            _modifierUsedSecondary[n] = _ConvertString2Key(arAltModifierStr[n]);
                        }
                    }
                }
            }

            if (PlayerPrefs.HasKey("cInput_axis")) {
                string ax = PlayerPrefs.GetString("cInput_axis");

                string[] axis = ax.Split('#');
                string[] axName = axis[0].Split('*');
                string[] axNeg = axis[1].Split('*');
                string[] axPos = axis[2].Split('*');

                int axCount = int.Parse(axis[3]);
                for (int n = 0; n < axCount; n++) {
                    int neg = int.Parse(axNeg[n]);
                    int pos = int.Parse(axPos[n]);
                    _SetAxis(n, axName[n], neg, pos);
                }
            }

            if (PlayerPrefs.HasKey("cInput_axInv")) {
                string invAx = PlayerPrefs.GetString("cInput_axInv");
                string[] axInv = invAx.Split('*');

                for (int n = 0; n < axInv.Length; n++) {
                    if (axInv[n] == "True") {
                        _invertAxis[n] = true;
                    } else {
                        _invertAxis[n] = false;
                    }
                }
            }

            if (PlayerPrefs.HasKey("cInput_indAxSens")) {
                string tmpAxisSens = PlayerPrefs.GetString("cInput_indAxSens");
                string[] arrAxisSens = tmpAxisSens.Split('*');
                for (int n = 0; n < arrAxisSens.Length - 1; n++) {
                    _individualAxisSens[n] = float.Parse(arrAxisSens[n]);
                }
            }

            if (PlayerPrefs.HasKey("cInput_indAxGrav")) {
                string tmpAxisGrav = PlayerPrefs.GetString("cInput_indAxGrav");
                string[] arrAxisGrav = tmpAxisGrav.Split('*');
                for (int n = 0; n < arrAxisGrav.Length - 1; n++) {
                    _individualAxisGrav[n] = float.Parse(arrAxisGrav[n]);
                }
            }

            if (PlayerPrefs.HasKey("cInput_indAxDead")) {
                string tmpAxisDead = PlayerPrefs.GetString("cInput_indAxDead");
                string[] arrAxisDead = tmpAxisDead.Split('*');
                for (int n = 0; n < arrAxisDead.Length - 1; n++) {
                    _individualAxisDead[n] = float.Parse(arrAxisDead[n]);
                }
            }

            // calibration loading
            if (PlayerPrefs.HasKey("cInput_saveCals")) {
                string saveCals = PlayerPrefs.GetString("cInput_saveCals");
                string[] saveCalsArr = saveCals.Split('*');
                for (int n = 0; n < saveCalsArr.Length - 1; n++) {
                    _axisType[n] = int.Parse(saveCalsArr[n]);
                }
            }

            if (PlayerPrefs.HasKey("cInput_calsVals")) {
                string calsVals = PlayerPrefs.GetString("cInput_calsVals");
                _CalibrationValuesFromString(calsVals);
            }

            _cKeysLoaded = true;
        }

        private static void _LoadExternalInputs() {
            _externalSaving = true;
            // splitting the external strings
            string[] externalStringAxes = _exAxis.Split('¿');
            string[] externalStringDefaults = _exDefaults.Split('¿');
            string[] externalStringInputs = _exInputs.Split('¿');

            AllowDuplicates = (_exAllowDuplicates == "True") ? true : false;

            int count = int.Parse(externalStringDefaults[0]);
            _inputLength = count - 1;

            string defaults = externalStringDefaults[1];
            string[] arDefs = defaults.Split('#');
            string[] arDefName = arDefs[0].Split('*');
            string[] arDefPrime = arDefs[1].Split('*');
            string[] arDefSec = arDefs[2].Split('*');
            string[] arModPrime = arDefs[3].Split('*');
            string[] arModSec = arDefs[4].Split('*');

            for (int n = 0; n < arDefName.Length - 1; n++) {
                _SetDefaultKey(n, arDefName[n], arDefPrime[n], arDefSec[n], arModPrime[n], arModSec[n]);
            }

            if (!string.IsNullOrEmpty(externalStringInputs[0])) {
                string descr = externalStringInputs[0];
                string inp = externalStringInputs[1];
                string altInp = externalStringInputs[2];
                string inpStr = externalStringInputs[3];
                string altInpStr = externalStringInputs[4];
                string modifierStr = externalStringInputs[5];
                string altModifierStr = externalStringInputs[6];

                string[] arDescr = descr.Split('*');
                string[] arInp = inp.Split('*');
                string[] arAltInp = altInp.Split('*');
                string[] arInpStr = inpStr.Split('*');
                string[] arAltInpStr = altInpStr.Split('*');
                string[] arModifierStr = modifierStr.Split('*');
                string[] arAltModifierStr = altModifierStr.Split('*');

                for (int n = 0; n < arDescr.Length - 1; n++) {
                    if (arDescr[n] == _defaultStrings[n, 0]) {
                        _inputName[n] = arDescr[n];
                        _inputPrimary[n] = _ConvertString2Key(arInp[n]);
                        _inputSecondary[n] = _ConvertString2Key(arAltInp[n]);
                        _axisPrimary[n] = arInpStr[n];
                        _axisSecondary[n] = arAltInpStr[n];
                        _modifierUsedPrimary[n] = _ConvertString2Key(arModifierStr[n]);
                        _modifierUsedSecondary[n] = _ConvertString2Key(arAltModifierStr[n]);
                    }
                }

                // fixes inputs when defaults are being changed
                for (int m = 0; m < arDefName.Length - 1; m++) {
                    for (int n = 0; n < arDescr.Length - 1; n++) {
                        if (arDescr[n] == _defaultStrings[m, 0]) {
                            _inputName[m] = arDescr[n];
                            _inputPrimary[m] = _ConvertString2Key(arInp[n]);
                            _inputSecondary[m] = _ConvertString2Key(arAltInp[n]);
                            _axisPrimary[m] = arInpStr[n];
                            _axisSecondary[m] = arAltInpStr[n];
                            _modifierUsedPrimary[n] = _ConvertString2Key(arModifierStr[n]);
                            _modifierUsedSecondary[n] = _ConvertString2Key(arAltModifierStr[n]);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(externalStringAxes[0])) {
                string invAx = _exAxisInverted;
                string[] axInv = (!string.IsNullOrEmpty(invAx)) ? invAx.Split('*') : null;
                string ax = externalStringAxes[0];

                string[] axis = ax.Split('#');
                string[] axName = axis[0].Split('*');
                string[] axNeg = axis[1].Split('*');
                string[] axPos = axis[2].Split('*');

                int axCount = int.Parse(axis[3]);
                for (int n = 0; n < axCount; n++) {
                    int neg = int.Parse(axNeg[n]);
                    int pos = int.Parse(axPos[n]);
                    _SetAxis(n, axName[n], neg, pos);

                    if (!string.IsNullOrEmpty(invAx)) {
                        if (axInv[n] == "True") {
                            _invertAxis[n] = true;
                        } else {
                            _invertAxis[n] = false;
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(externalStringAxes[1])) {
                string tmpAxisSens = externalStringAxes[1];
                string[] arrAxisSens = tmpAxisSens.Split('*');
                for (int n = 0; n < arrAxisSens.Length - 1; n++) {
                    _individualAxisSens[n] = float.Parse(arrAxisSens[n]);
                }
            }

            if (!string.IsNullOrEmpty(externalStringAxes[2])) {
                string tmpAxisGrav = externalStringAxes[2];
                string[] arrAxisGrav = tmpAxisGrav.Split('*');
                for (int n = 0; n < arrAxisGrav.Length - 1; n++) {
                    _individualAxisGrav[n] = float.Parse(arrAxisGrav[n]);
                }
            }

            if (!string.IsNullOrEmpty(externalStringAxes[3])) {
                string tmpAxisDead = externalStringAxes[3];
                string[] arrAxisDead = tmpAxisDead.Split('*');
                for (int n = 0; n < arrAxisDead.Length - 1; n++) {
                    _individualAxisDead[n] = float.Parse(arrAxisDead[n]);
                }
            }

            // calibration loading
            if (!string.IsNullOrEmpty(_exCalibrations)) {
                string saveCals = _exCalibrations;
                string[] saveCalsArr = saveCals.Split('*');
                for (int n = 1; n <= saveCalsArr.Length - 2; n++) {
                    _axisType[n] = int.Parse(saveCalsArr[n]);
                }
            }

            if (!string.IsNullOrEmpty(_exCalibrationValues)) {
                _CalibrationValuesFromString(_exCalibrationValues);
            }

            _cKeysLoaded = true;
        }

        public static void ResetInputs() {
            _cInputInit(); // if cInput doesn't exist, create it
            // reset inputs to default values
            for (int n = 0; n < _inputLength + 1; n++) {
                _SetKey(n, _defaultStrings[n, 0], _defaultStrings[n, 1], _defaultStrings[n, 2]);
                _modifierUsedPrimary[n] = _ConvertString2Key(_defaultStrings[n, 3]);
                _modifierUsedSecondary[n] = _ConvertString2Key(_defaultStrings[n, 4]);
            }

            for (int n = 0; n < _axisLength; n++) {
                _invertAxis[n] = false;
            }

            Clear();
            _SaveDefaults();
            _SaveInputs();
            _SaveAxInverted();
        }

        public static void Clear() {
            _cInputInit(); // if cInput doesn't exist, create it
            Debug.LogWarning("Clearing out all cInput related values from PlayerPrefs");
            PlayerPrefs.DeleteKey("cInput_axInv");
            PlayerPrefs.DeleteKey("cInput_axis");
            PlayerPrefs.DeleteKey("cInput_indAxSens");
            PlayerPrefs.DeleteKey("cInput_indAxGrav");
            PlayerPrefs.DeleteKey("cInput_indAxDead");
            PlayerPrefs.DeleteKey("cInput_count");
            PlayerPrefs.DeleteKey("cInput_defaults");
            PlayerPrefs.DeleteKey("cInput_descr");
            PlayerPrefs.DeleteKey("cInput_inp");
            PlayerPrefs.DeleteKey("cInput_alt_inp");
            PlayerPrefs.DeleteKey("cInput_inpStr");
            PlayerPrefs.DeleteKey("cInput_alt_inpStr");
            PlayerPrefs.DeleteKey("cInput_dubl");
            PlayerPrefs.DeleteKey("cInput_saveCals");
            PlayerPrefs.DeleteKey("cInput_calsVals");
            PlayerPrefs.DeleteKey("cInput_modifierStr");
            PlayerPrefs.DeleteKey("cInput_alt_modifierStr");
        }

        #endregion //Save, Load, Reset & Clear functions

        #region InvertAxis and IsAxisInverted functions

        // this sets the inversion of axisName to invertedStatus
        public static bool AxisInverted(string axisName, bool invertedStatus) {
            _cInputInit(); // if cInput doesn't exist, create it
            int index = _FindAxisByDescription(axisName);
            if (index > -1) {
                _invertAxis[index] = invertedStatus;
                _SaveAxInverted();
                return invertedStatus;
            }

            // if we got this far then the string didn't server and there's a problem.
            Debug.LogWarning("Couldn't find an axis server for " + axisName + " while trying to set inversion status. Is it possible you typed it wrong?");
            return false;
        }

        // this just returns inversion status of axisName
        public static bool AxisInverted(string axisName) {
            _cInputInit(); // if cInput doesn't exist, create it
            int index = _FindAxisByDescription(axisName);
            if (index > -1) {
                return _invertAxis[index];
            }

            // if we got this far then the string didn't server and there's a problem.
            Debug.LogWarning("Couldn't find an axis server for " + axisName + " while trying to get inversion status. Is it possible you typed it wrong?");
            return false;
        }

        #endregion

        #region ShowMenu functions

        public static bool ShowMenu() {
            _cInputInit(); // if cInput doesn't exist, create it
            Debug.LogError("cInput.ShowMenu() has been deprecated. Please use the appropriate cGUI variable, such as cGUI.showingAnyGUI");
            return false;
        }

        #region overloaded ShowMenu functions

        public static void ShowMenu(bool state) {
            ShowMenu(state, _menuHeaderString, _menuActionsString, _menuInputsString, _menuButtonsString);
        }

        public static void ShowMenu(bool state, string menuHeader) {
            ShowMenu(state, menuHeader, _menuActionsString, _menuInputsString, _menuButtonsString);
        }

        public static void ShowMenu(bool state, string menuHeader, string menuActions) {
            ShowMenu(state, menuHeader, menuActions, _menuInputsString, _menuButtonsString);
        }

        public static void ShowMenu(bool state, string menuHeader, string menuActions, string menuInputs) {
            ShowMenu(state, menuHeader, menuActions, menuInputs, _menuButtonsString);
        }

        #endregion overloaded showMenu functions

        // this is an old method of showing the menu, it's just in for backwards compatibility - 
        public static void ShowMenu(bool state, string menuHeader, string menuActions, string menuInputs, string menuButtons) {
            _cInputInit(); // if cInput doesn't exist, create it
            Debug.LogError("cInput.ShowMenu() has been deprecated. Please use the appropriate cGUI function, such as cGUI.ToggleGUI()");
        }

        #endregion

        private static void _cInputInit() {
            if (_cObject == null) {
                GameObject cObject = GameObject.Find("cObject");
                if (!cObject) {
                    // We need to create a GameObject named cObject
                    cObject = new GameObject();
                    cObject.name = "cObject";
                }

                // make sure the GameObject also has the cInput component attached
                if (cObject.GetComponent<CInput>() == null) {
                    _cObject = cObject.AddComponent<CInput>();
                }
            }

#if Use_cInputGUI

            // make sure the GameObject also has the cInputGUI component attached
            if (!_cInputGuiObject) {
                // get a reference to the component if it already exists
                _cInputGuiObject = _cObject.GetComponent<CInputGui>();
                if (!_cInputGuiObject) {
                    // if there's still no reference, the component needs to be added
                    _cObject.gameObject.AddComponent<CInputGui>();
                }
            }

#endif

        }

        private void _CheckingDuplicates(int num, int count) {
            if (AllowDuplicates) { return; }

            for (int n = 0; n < Length; n++) {
                if (count == 1) {
                    if (num != n && _inputPrimary[num] == _inputPrimary[n] && _modifierUsedPrimary[num] == _modifierUsedPrimary[n]) {
                        _inputPrimary[n] = KeyCode.None;
                    }

                    if (_inputPrimary[num] == _inputSecondary[n] && _modifierUsedPrimary[num] == _modifierUsedSecondary[n]) {
                        _inputSecondary[n] = KeyCode.None;
                    }
                }

                if (count == 2) {
                    if (_inputSecondary[num] == _inputPrimary[n] && _modifierUsedSecondary[num] == _modifierUsedPrimary[n]) {
                        _inputPrimary[n] = KeyCode.None;
                    }

                    if (num != n && _inputSecondary[num] == _inputSecondary[n] && _modifierUsedSecondary[num] == _modifierUsedSecondary[n]) {
                        _inputSecondary[n] = KeyCode.None;
                    }
                }
            }
        }

        private void _CheckingDuplicateStrings(int num, int count) {
            if (AllowDuplicates) { return; }

            for (int n = 0; n < Length; n++) {
                if (count == 1) {
                    if (num != n && _axisPrimary[num] == _axisPrimary[n]) {
                        _axisPrimary[n] = "";
                        _inputPrimary[n] = KeyCode.None;
                    }

                    if (_axisPrimary[num] == _axisSecondary[n]) {
                        _axisSecondary[n] = "";
                        _inputSecondary[n] = KeyCode.None;
                    }
                }

                if (count == 2) {
                    if (_axisSecondary[num] == _axisPrimary[n]) {
                        _axisPrimary[n] = "";
                        _inputPrimary[n] = KeyCode.None;
                    }

                    if (num != n && _axisSecondary[num] == _axisSecondary[n]) {
                        _axisSecondary[n] = "";
                        _inputSecondary[n] = KeyCode.None;
                    }
                }
            }
        }

        /// <summary>This is where we detect what input is being pressed to assign inputs using the GUI</summary>
        private void _InputScans() {
            KeyCode tmpModifier = KeyCode.None;
            if (Input.GetKey(KeyCode.Escape)) {
                if (_cScanInput == 1) {
                    _inputPrimary[_cScanIndex] = KeyCode.None;
                    _axisPrimary[_cScanIndex] = "";
                    _cScanInput = 0;
                }

                if (_cScanInput == 2) {
                    _inputSecondary[_cScanIndex] = KeyCode.None;
                    _axisSecondary[_cScanIndex] = "";
                    _cScanInput = 0;
                }
            }

            #region keyboard + mouse + joystick button scanning

            if (_scanning && Input.anyKeyDown && !Input.GetKey(KeyCode.Escape)) {
                KeyCode key = KeyCode.None;

                for (int i = (int)KeyCode.None; i < 450; i++) {
                    KeyCode ckey = (KeyCode)i;
                    if (ckey.ToString().StartsWith("Mouse")) {
                        if (!_allowMouseButtons) {
                            continue;
                        }
                    } else if (ckey.ToString().StartsWith("Joystick")) {
                        if (!_allowJoystickButtons) {
                            continue;
                        }
                    } else if (!_allowKeyboard) {
                        continue;
                    }

                    // loop through modifier list and set the input key
                    for (int n = 0; n < _modifiers.Count; n++) {
                        for (int m = 0; m < _modifiers.Count; m++) {
                            if (Input.GetKeyDown(_modifiers[n])) {
                                return;
                            }
                        }

                        if (Input.GetKeyDown(ckey)) {
                            key = ckey;
                            tmpModifier = ckey; // if this doesn't change it means there is no modifier used to set this input
                            bool markedAsAxis = false; // has this key been marked as an axis?
                            for (int m = 0; m < _markedAsAxis.Count; m++) {
                                if (_cScanIndex == _markedAsAxis[m]) {
                                    markedAsAxis = true;
                                    break; // no need to loop through the rest
                                }
                            }

                            // check if modifier is been pressed and that the inputs aren't part of an axis
                            if (Input.GetKey(_modifiers[n]) && !markedAsAxis) {
                                tmpModifier = _modifiers[n]; // if this is being set here it means we have a modifier being pressed down
                                break;
                            }
                        }
                    }
                }

                if (key != KeyCode.None) {
                    bool keyCleared = true;
                    // check if the entered key is forbidden
                    for (int b = 0; b < _forbiddenKeys.Count; b++) {
                        if (key == _forbiddenKeys[b]) {
                            keyCleared = false;
                        }
                    }

                    if (keyCleared) {
                        if (_cScanInput == 1) {
                            _inputPrimary[_cScanIndex] = key;
                            _modifierUsedPrimary[_cScanIndex] = tmpModifier; // set the modifier being used 
                            _axisPrimary[_cScanIndex] = "";
                            _CheckingDuplicates(_cScanIndex, _cScanInput);
                        }

                        if (_cScanInput == 2) {
                            _inputSecondary[_cScanIndex] = key;
                            _modifierUsedSecondary[_cScanIndex] = tmpModifier; // set the modifier being used
                            _axisSecondary[_cScanIndex] = "";
                            _CheckingDuplicates(_cScanIndex, _cScanInput);
                        }
                    }

                    _cScanInput = 0;
                }
            }

            #region mouse scroll wheel scanning (considered to be a mousebutton)

            if (_allowMouseButtons) {
                //if (!Mathf.Approximately(_axisRawValues["Mouse Wheel"], Input.GetAxisRaw("Mouse Wheel"))) {
                if (Input.GetAxis("Mouse Wheel") > 0 && !Input.GetKey(KeyCode.Escape)) {
                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Wheel Up";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Wheel Up";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                } else if (Input.GetAxis("Mouse Wheel") < 0 && !Input.GetKey(KeyCode.Escape)) {
                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Wheel Down";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Wheel Down";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                }
                //}
            }

            #endregion //mouse scroll wheel scanning (considered to be a mousebutton)

            #endregion // keyboard + mouse + joystick button scanning

            #region mouse axis scanning

            if (_allowMouseAxis) {
                //if (!Mathf.Approximately(_axisRawValues["Mouse Horizontal"], Input.GetAxisRaw("Mouse Horizontal"))) {
                if (Input.GetAxis("Mouse Horizontal") < -deadzone && !Input.GetKey(KeyCode.Escape)) {

                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Left";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Left";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                } else if (Input.GetAxis("Mouse Horizontal") > deadzone && !Input.GetKey(KeyCode.Escape)) {
                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Right";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Right";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                }
                //}

                //if (!Mathf.Approximately(_axisRawValues["Mouse Vertical"], Input.GetAxisRaw("Mouse Vertical"))) {
                if (Input.GetAxis("Mouse Vertical") > deadzone && !Input.GetKey(KeyCode.Escape)) {
                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Up";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Up";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                } else if (Input.GetAxis("Mouse Vertical") < -deadzone && !Input.GetKey(KeyCode.Escape)) {
                    if (_cScanInput == 1) {
                        _axisPrimary[_cScanIndex] = "Mouse Down";
                    }

                    if (_cScanInput == 2) {
                        _axisSecondary[_cScanIndex] = "Mouse Down";
                    }

                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                    _cScanInput = 0;
                }
                //}
            }

            #endregion // mouse axis scanning

            #region joystick axis scanning

            if (_allowJoystickAxis) {
                float scanningDeadzone = 0.25f;
                for (int i = 1; i <= _numGamepads; i++) {
                    for (int j = 1; j <= 10; j++) {
                        string joystring = _joyStrings[i, j];
                        string joystringPos = _joyStringsPos[i, j];
                        string joystringNeg = _joyStringsNeg[i, j];

                        float axisRaw = Input.GetAxisRaw(joystring);

                        if (!Mathf.Approximately(_axisRawValues[joystring], axisRaw)) {

                            #region Special Xbox gamepad trigger handling
                            // Xbox triggers are bound both to axis 3 (+/-) and axes 9 (+) and 10 (+)
                            // the problem with letting them bind to axis 3 is if both are pressed,
                            // it returns -1 + 1 which is 0, which is the same as neither of them being pressed
                            // this prevents them from being bound to the same axis, so they can both be
                            // pressed without interfering with each other.
                            if (j == 3) {
                                // if this is the gamepad's 3rd axis we want to check if either
                                // axis 9 or 10 is also returning a value

                                string lTrigger = _joyStringsPos[i, 9];
                                string rTrigger = _joyStringsPos[i, 10];

                                // if axis 9 or 10 has a positive value above scanningDeadzone, use that axis instead of axis 3
                                if (_GetCalibratedAxisInput(lTrigger) > scanningDeadzone) {
                                    joystringPos = lTrigger;
                                    joystringNeg = lTrigger;
                                } else if (_GetCalibratedAxisInput(rTrigger) > scanningDeadzone) {
                                    joystringPos = rTrigger;
                                    joystringNeg = rTrigger;
                                }
                            }
                            #endregion //Special Xbox gamepad trigger handling

                            float axis = (axisRaw < 0) ? // if the raw value is negative
                                _GetCalibratedAxisInput(joystringNeg) : // axis is the calibrated input of the negative axis
                                _GetCalibratedAxisInput(joystringPos); // else it's the calibrated input of the positive axis

                            if (_scanning && Mathf.Abs(axis) > scanningDeadzone && !Input.GetKey(KeyCode.Escape)) {
                                //Debug.Log("Calibrated value: " + axis + ". Raw value: " + Input.GetAxisRaw(_joystring));
                                if (_cScanInput == 1) {
                                    if (axis > scanningDeadzone) {
                                        _axisPrimary[_cScanIndex] = joystringPos;
                                    } else if (axis < -scanningDeadzone) {
                                        _axisPrimary[_cScanIndex] = joystringNeg;
                                    }

                                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                                    _cScanInput = 0;
                                    break;
                                } else if (_cScanInput == 2) {
                                    if (axis > scanningDeadzone) {
                                        _axisSecondary[_cScanIndex] = joystringPos;
                                    } else if (axis < -scanningDeadzone) {
                                        _axisSecondary[_cScanIndex] = joystringNeg;
                                    }

                                    _CheckingDuplicateStrings(_cScanIndex, _cScanInput);
                                    _cScanInput = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            #endregion // joystick axis scanning

        }
    }
}
