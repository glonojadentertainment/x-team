﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.Resources.Utilities.FileManager.XLoad;
using XTeam.Resources.GamePlay.Network.Server;
using XTeam.Resources.Utilities.UiManager.ProgressBar;
using XTeam.Resources.Utilities.UiManager.SplashScreen;

namespace XTeam.Resources.Utilities.LevelLoader
{
    /// <summary>
    ///     Class for loading already downloaded maps and scenes.
    /// </summary>
    public class LevelLoader : MonoBehaviour
    {
        /// <summary>
        /// Loads the specified scene.
        /// </summary>
        /// <param name="levelName">Scene name to be loaded.</param>
        /// <param name="loadMode">LoadMode to be used.</param>
        /// <param name="onSceneLoaded">Called when the scene has finished loading.</param>
        /// <returns></returns>
        public static IEnumerator LoadScene(string levelName, LoadSceneMode loadMode, params Action[] onSceneLoaded)
        {
            Debug.Log("Loading level " + levelName);

            //Starting the loading of the level.
            var loadingProcess = SceneManager.LoadSceneAsync(levelName, loadMode);
            //Showing the loading indicator of the level.
            var levelLoadingProgressBar = ProgressbarManager.NewProgressBar(loadingProcess);
            var levelLoadingSplashScreen = SplashScreenManager.ShowSplashScreen("XTeamLogo");
            yield return loadingProcess.isDone;

            //The level successfuly loaded.
            Debug.Log("Level " + levelName + " was successfully loaded.");
            //Hiding the loading indicator.
            levelLoadingProgressBar.Destroy();
            levelLoadingSplashScreen.Destroy();

            //Invoking the maploaded event.
            if (onSceneLoaded == null || onSceneLoaded.Length == 0) yield break;
            foreach (var action in onSceneLoaded)
            {
                action.Invoke();
            }
        }
        /// <summary>
        /// Loads a specific contentPack.
        /// </summary>
        /// <param name="mapPath">Path to contentPack to be loaded.</param>
        /// <param name="onMapLoaded">Called when the contentPack has finished loading.</param>
        /// <returns></returns>
        public static void LoadMap(string mapPath, params Action[] onMapLoaded)
        {
            Debug.Log("Loading map from disk: " + mapPath);

            //Loading the map from the specified mapPath.
            Server.instance.StartCoroutine(LoadScene(Load.SceneFromFile(mapPath), LoadSceneMode.Single, onMapLoaded));

            //Unloading usused assets to free up memory.
            UnityEngine.Resources.UnloadUnusedAssets();
            //Performing a garbage collection to free up some memory after loading.
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
        /// <summary>
        /// Unloads a specific contentPack.
        /// </summary>
        /// <param name="mapName">Map to be unloaded.</param>
        /// <param name="onMapUnloaded">Called when the contentPack has finished unloading.</param>
        /// <returns></returns>
        public static void UnloadMap(string mapName, params Action[] onMapUnloaded)
        {
            Debug.Log("Unloading contentPack: " + mapName);

            var sceneToUnload = SceneManager.GetSceneByName(mapName);
            if (sceneToUnload.isLoaded)
            {
                //Unloading the specified scene.
                SceneManager.UnloadScene(sceneToUnload.name);
                //Unloading usused assets to free up memory.
                UnityEngine.Resources.UnloadUnusedAssets();
                //Performing a garbage collection to free up some memory after unloading.
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                Debug.Log("Map " + mapName + " was successfully unloaded.");

                //Invoking the mapunloaded events.
                if (onMapUnloaded == null || onMapUnloaded.Length == 0) return;
                foreach (var action in onMapUnloaded)
                {
                    action();
                }
            }
        }
    }
}