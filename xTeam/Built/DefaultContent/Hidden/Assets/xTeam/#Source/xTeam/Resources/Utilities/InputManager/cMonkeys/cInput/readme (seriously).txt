How to test the Demo
--------------------

- Use the "Edit -> Project Settings -> cInput -> Replace InputManager.asset file" menu command in the Unity Editor to create an InputManager.asset file designed to work with cInput.
- Open the C# or JS scene of your choice in the "cMonkeys/cInput/Demo/Scenes/" folder.
- Play around with it.



How to use cInput in your own project
-------------------------------------

You only need 2 files for cInput to work, these are "InputManager.asset" (created using the "Edit -> Project Settings -> cInput -> Replace InputManager.asset file" menu option in the editor) & "cInput.cs". Once these two files are in the appropriate locations, all other files included in this package can safely be removed from your project (unless you want the optional features mentioned below).

- Use the "Edit -> Project Settings -> cInput -> Replace InputManager.asset file" menu command in the Unity Editor to create an InputManager.asset file designed to work with cInput.
- Place cInput.cs in the "< your project >/Assets/Plugins" folder.
- OPTIONAL: If you want to use the Keys class or the included GUI, place cKeys.cs, cInputGUI.cs, and cGUI.cs files in the "< your project >/Assets/Plugins" folder.



Read the reference manual if you don't know what to do. :)

We have video tutorials to help you get started on our website:

http://cinput2.weebly.com/



Questions?

ward.dewaele@pandora.be
