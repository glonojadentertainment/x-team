﻿using UnityEngine;

namespace XTeam.Resources.Utilities.InputManager.cMonkeys
{
    [RequireComponent(typeof(CGui))]
    public class CInputGui : MonoBehaviour {
        Rect _windowRect;
        float _wHeight = CGui.WindowMaxSize.y;
        float _wWidth = CGui.WindowMaxSize.x;
        bool _showPopUp;
        private Vector2 _scrollPosition;
        private float _clickDelay = 0f;

        private string _label2, _box2, _popwindow, _smallbutton;

        void Start() {
            CGui.cInputExists = true;
        }

        #region OnGUI

        void OnGui() {
            if (CGui.ShowingInputGui) {

                if (CInput.Scanning) {
                    _clickDelay = Time.realtimeSinceStartup + 0.15f;
                }

                GUI.skin = CGui.cSkin;
                CGui.UpdateGuiColors();

                if (Screen.height - CGui.WindowMaxSize.y < 0) { _wHeight = Screen.height; } else { _wHeight = CGui.WindowMaxSize.y; }
                if (Screen.width - CGui.WindowMaxSize.x < 0) { _wWidth = Screen.width; } else { _wWidth = CGui.WindowMaxSize.x; }
                _windowRect = new Rect((Screen.width - _wWidth) / 2, (Screen.height - _wHeight) / 2, _wWidth, _wHeight);
                //windowRect = GUILayout.Window(0, windowRect, MenuWindow, "");
                GUI.Window(0, _windowRect, MenuWindow, "");
                if (_showPopUp) {
                    GUI.Window(1, new Rect((Screen.width - 512) / 2, (Screen.height - 
                        0) / 2, 512, 350), PopUp, "", _popwindow);
                    GUI.BringWindowToFront(1);
                    GUI.FocusWindow(1);
                }
            }
        }

        void PopUp(int windowId) {
            GUI.FocusWindow(1);
            GUI.TextField(new Rect(40, 100, 450, 95), "Please leave all analog inputs in their neutral positions.\n\nClick on OK when ready.");

            Rect buttonRect = new Rect(150, 284, 200, 35);
            GUI.Button(buttonRect, "OK", _smallbutton);
            if (buttonRect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0) && _showPopUp) {
                CInput.Calibrate();
                _showPopUp = false;
            }
        }

        void MenuWindow(int windowId) {
            if (!_showPopUp) { GUI.FocusWindow(0); }
            GUI.backgroundColor = CGui.BgColor;
            #region left menu ---------------------------------------------

            float buttonWidth = 128;
            float buttonHeight = 35;
            float lbuttonStartH = _wWidth / 26.5f;
            int strvert = 7;
            if (CGui.cSkin && CGui.cSkin.name == "cGUISkin Dark") { strvert = 6; }
            float lbuttonStartV = _wHeight / strvert;

            float lbuttonEnd = _wHeight - (_wHeight / 6);
            float lbuttonSpace = 50;
            int showInt = 0;

            GUI.SetNextControlName("textarea"); // set button to active mode
            if (GUI.Button(new Rect(lbuttonStartH, lbuttonStartV + (lbuttonSpace * showInt++), buttonWidth, buttonHeight), "  INPUTS")) {
                // don't actually do anything because this one is already activated
            }

            GUI.FocusControl("textarea");

            if (CGui.cAudioExists) {
                if (GUI.Button(new Rect(lbuttonStartH, lbuttonStartV + (lbuttonSpace * showInt++), buttonWidth, buttonHeight), "  AUDIO")) {
                    CGui.ShowAudioGui();
                }
            }

            if (CGui.cVideoExists) {
                if (GUI.Button(new Rect(lbuttonStartH, lbuttonStartV + (lbuttonSpace * showInt++), buttonWidth, buttonHeight), "  VIDEO")) {
                    CGui.ShowVideoGui();
                }
            }


            if (GUI.Button(new Rect(lbuttonStartH, lbuttonEnd - (lbuttonSpace * 2), buttonWidth, buttonHeight), "  CALIBRATE")) {
                _showPopUp = true;
            }

            if (GUI.Button(new Rect(lbuttonStartH, lbuttonEnd - (lbuttonSpace), buttonWidth, buttonHeight), "  DEFAULTS")) {
                CInput.ResetInputs(); // reset cInput to defaults
            }

            if (GUI.Button(new Rect(lbuttonStartH, lbuttonEnd, buttonWidth, buttonHeight), "  EXIT")) {
                CGui.ToggleGui();
            }

            #endregion // left menu ---------------------------------------

            #region right menu (cInput) -----------------------------------

            float vWidthStartH1 = _windowRect.width / 3;
            float vWidthStartH3 = _windowRect.width / 1.8f;
            float vWidthStartV2 = _windowRect.height / 9f;
            float vSpace = 60;

            if (CGui.cSkin) {
                _label2 = "label";
                _box2 = "box2";
                _popwindow = "popwindow";
                _smallbutton = "smallbutton";
            } else {
                _label2 = "label";
                _box2 = "box";
                _popwindow = "window";
                _smallbutton = "button";
            }

            Rect rightSideRect = new Rect(_windowRect.width / 3.6f, vWidthStartV2 * 2.2f, _windowRect.width * 0.67f, _windowRect.height);

            // input settings
            GUI.Label(new Rect(rightSideRect.x + (rightSideRect.width / 2) - (buttonWidth / 2), _windowRect.height / 6.5f, buttonWidth, buttonHeight), "INPUT SETTINGS", _label2);
            // GUI.Label(new Rect(windowRect.width * 0.57f, v_widthStartV1, _buttonWidth, _buttonHeight), "INPUT SETTINGS", label2);

            GUI.Label(new Rect(rightSideRect.x + (rightSideRect.width * 0.2f) - (buttonWidth * 1.2f / 2), rightSideRect.y, buttonWidth * 1.2f, buttonHeight), "ACTION", _label2);
            GUI.Label(new Rect(rightSideRect.x + (rightSideRect.width * 0.5f) - (buttonWidth * 1.2f / 2), rightSideRect.y, buttonWidth * 1.2f, buttonHeight), "PRIMARY", _label2);
            GUI.Label(new Rect(rightSideRect.x + (rightSideRect.width * 0.8f) - (buttonWidth * 1.2f / 2), rightSideRect.y, buttonWidth * 1.2f, buttonHeight), "SECONDARY", _label2);

            // scroll 
            _scrollPosition = GUI.BeginScrollView(new Rect(vWidthStartH1, vWidthStartV2 * 3.0f, vWidthStartH3 * 1.1f, _windowRect.height * 0.58f), _scrollPosition, new Rect(vWidthStartH1, 0, vWidthStartH3, vSpace * (CInput.Length - 5)));

            for (int n = 0; n < CInput.Length; n++) {
                GUI.Label(new Rect(rightSideRect.x + (rightSideRect.width * 0.2f) - (buttonWidth * 1.2f / 2), 0 + (buttonHeight * n), buttonWidth * 1.2f, buttonHeight), CInput.GetText(n, 0), "label"); // name of input
                if (GUI.Button(new Rect(rightSideRect.x + (rightSideRect.width * 0.5f) - (buttonWidth * 1.2f / 2), 0 + (buttonHeight * n), buttonWidth * 1.2f, buttonHeight), CInput.GetText(n, 1), _box2) && Input.GetMouseButtonUp(0)) {
                    if (Time.realtimeSinceStartup > _clickDelay) {
                        CInput.ChangeKey(n, 1);
                    }
                }

                if (GUI.Button(new Rect(rightSideRect.x + (rightSideRect.width * 0.8f) - (buttonWidth * 1.2f / 2), 0 + (buttonHeight * n), buttonWidth * 1.2f, buttonHeight), CInput.GetText(n, 2), _box2)) {
                    if (Time.realtimeSinceStartup > _clickDelay) {
                        CInput.ChangeKey(n, 2);
                    }
                }
            }

            GUI.EndScrollView();

            #endregion //right menu ---------------------------------------
        }

        #endregion //OnGUI

    }
}
