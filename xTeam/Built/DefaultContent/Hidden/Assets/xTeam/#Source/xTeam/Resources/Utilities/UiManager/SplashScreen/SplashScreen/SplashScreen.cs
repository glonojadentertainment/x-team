﻿using UnityEngine;

namespace XTeam.Resources.Utilities.UiManager.SplashScreen.SplashScreen
{
    public class SplashScreen : MonoBehaviour
    {
        [Tooltip("Name of the splash screen.")]
        public string screenName;
        [Tooltip("Canvas group of this splash screen.")]
        public CanvasGroup canvasGroup;

        public void Destroy()
        {
            //Making the splash screen invisible.
            StartCoroutine(UiManager.Smooth(1, 0f,
                0.2f, value => canvasGroup.alpha = value));

            //Destroying the splashscreen.
            Destroy(gameObject);
        }
    }
}
