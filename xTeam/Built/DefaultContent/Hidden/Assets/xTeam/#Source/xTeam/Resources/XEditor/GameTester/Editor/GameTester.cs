﻿using System.Diagnostics;
using System.IO;
using UnityEngine;
using xTeam.Resources.XEditor.Editor;

namespace xTeam.Resources.XEditor.GameTester.Editor
{
    public class GameTester : MonoBehaviour
    {
        /// <summary>
        /// Runs current server info in the game.
        /// </summary>
        public static void RunContentInGame()
        {
            var xTeamApplicationPath = "";
    #if UNITY_EDITOR_WIN
            xTeamApplicationPath = XEditorCore.gameDir + "/" + "xTeam.exe";
    #endif
    #if UNITY_EDITOR_OSX
            xTeamApplicationPath = XEditorCore.gameDir + "/" + "xTeam.app";
    #endif

            if (string.IsNullOrEmpty(xTeamApplicationPath))
            {
                UnityEngine.Debug.LogError("Couldn't launch xTeam!");
                return;
            }
            UnityEngine.Debug.Log("About to launch " + xTeamApplicationPath);

            var xTeam = new Process();
            xTeam.StartInfo.FileName = xTeamApplicationPath;
#if UNITY_EDITOR_OSX
            xTeam.StartInfo.Arguments = "--args loadcontent:" + Application.dataPath;
#endif
#if UNITY_EDITOR_WIN
            xTeam.StartInfo.Arguments = "loadcontent:" + new DirectoryInfo(Application.dataPath).Parent.FullName;
#endif

            xTeam.Start();

            UnityEngine.Debug.Log("Arguments: " + xTeam.StartInfo.Arguments);
        }
    }
}
