﻿using System;
using GameJolt.API.Objects;
using UnityEngine;

namespace XTeam.Resources.Utilities
{
    internal class PlayerIdentity : MonoBehaviour {
        /// <summary>
        /// Instance of this class.
        /// </summary>
        public static PlayerIdentity instance;

        /// <summary>
        /// PlayerInfo's name.
        /// </summary>
        public static string playerName = "default-name";
        /// <summary>
        /// Type of the player.
        /// </summary>
        public static UserType playerType;
        /// <summary>
        /// PlayerInfo's avatar url.
        /// </summary>
        public static string playerAvatarUrl;
        /// <summary>
        /// PlayerInfo's avatar.
        /// </summary>
        public static Texture2D playerAvatar;
        /// <summary>
        /// Called when player's identity changes.
        /// </summary>
        public static Action OnPlayerIdentityChange;

        void Start()
        {
            instance = this;
            GenerateRandomIdentity();
        }

        public static void GenerateRandomIdentity()
        {
            playerName = "Anonymous-" + Guid.NewGuid();
            playerType = UserType.User;
            playerAvatarUrl = "http://www.sixiemesens-lemag.fr/wp-content/uploads/2013/11/blank_avatar_220.png";

            OnPlayerIdentityChange();
        }

        public static void OnGameJoltLogin(bool success)
        {
            if (success)
            {
                //Setting player identity to server the GameJolt identity.
                playerName = GameJolt.API.Manager.Instance.CurrentUser.Name;
                playerType = GameJolt.API.Manager.Instance.CurrentUser.Type;
                playerAvatarUrl = GameJolt.API.Manager.Instance.CurrentUser.AvatarURL;

                OnPlayerIdentityChange();
            }
        }
    }
}
