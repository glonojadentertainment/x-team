﻿using UnityEngine.Networking;
using xTeam.xTeam_Creator_Classes.Resources;

namespace xTeam.Resources.XEditor.MapExporter
{
    public class Map : MessageBase {
        public string name;
        public string description;
        public string type;	
        public int version;	
        public string downloadUrl;
        public string iconUrl;
        public ContentInfo contentInfo;
    }
}