﻿using System;
using System.Collections.Generic;

namespace xTeam.API.Event
{
    /// <summary>
    /// Class for managing the events.
    /// </summary>
    public class EventManager
    {
        /// <summary>
        /// List of all registered event handlers.
        /// </summary>
        private static List<EventHandler> _registeredEventHandlers = new List<EventHandler>();

        /// <summary>
        /// Calls the specified event.
        /// </summary>
        /// <param name="eventToCall">Event to call.</param>
        /// <param name="callback">Called when the event has been called on all the handlers. This is where all the code logic should be implemented.</param>
        public static void CallEvent(Event eventToCall, Action<Event> callback)
        {
            foreach (EventHandler handler in _registeredEventHandlers)
            {
                if (handler.handledEventType.GetType() == eventToCall.GetType())
                {
                    //Executing the handler callback.
                    handler.callback(eventToCall);
                }
            }

            //Checking whether the event was cancelled.
            if (eventToCall.GetType().IsAssignableFrom(typeof(ICancellable)) && (eventToCall as ICancellable).IsCanceled()) {}
            else
            {
                //Calling a callback with the executed event.
                callback(eventToCall);
            }
        }

        /// <summary>
        /// Registers a new event handler for a specified event.
        /// </summary>
        /// <param name="handler">Handler to be registered.</param>
        public static void RegisterHandler(Type eventTypeToHandle, Action<Event> callback)
        {
            //Adding event to the registered event handlers.
            _registeredEventHandlers.Add(new EventHandler(eventTypeToHandle, callback));
        }

        /// <summary>
        /// Unregisters a specified event handler.
        /// </summary>
        /// <param name="callback"></param>
        public static void UnRegisterHandler(Action<Event> callback)
        {
            foreach (EventHandler handler in _registeredEventHandlers)
            {
                if (handler.callback.GetType() == callback.GetType())
                {
                    _registeredEventHandlers.Remove(handler);
                }
            }
        }
    }
}