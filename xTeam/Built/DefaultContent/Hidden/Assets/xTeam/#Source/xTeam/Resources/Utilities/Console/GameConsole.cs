﻿using UnityEngine;
using System;
using XTeam.API.Console;

namespace XTeam.Resources.Utilities.Console
{
    public class GameConsole : MonoBehaviour
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

        ConsoleWindow _console = new ConsoleWindow();
        ConsoleInput _input = new ConsoleInput();

        string _strInput;

        /// <summary>
        /// Creates a new console window and registers the output.
        /// </summary>
        void Awake()
        {
            Debug.Log("Initializing the console...");

            _console.Initialize();
            _console.SetTitle("X-Team Console");

            _input.OnInputText += OnInputText;

            Application.logMessageReceivedThreaded += HandleLog;

            Debug.Log("Console Started");
        }

        /// <summary>
        /// Running the entered command.
        /// </summary>
        /// <param name="obj">The entered command.</param>
        void OnInputText(string obj)
        {
            ConsoleHandler.Dispatch(new ConsoleCommand(obj));
        }

        /// <summary>
        /// Debug.Log handling.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stackTrace"></param>
        /// <param name="type"></param>
        void HandleLog(string message, string stackTrace, LogType type)
        {
            if (type == LogType.Warning)
                System.Console.ForegroundColor = ConsoleColor.Yellow;
            else if (type == LogType.Error)
                System.Console.ForegroundColor = ConsoleColor.Red;
            else
                System.Console.ForegroundColor = ConsoleColor.Gray;

            // We're half way through typing something, so clear this line ..
            if (System.Console.CursorLeft != 0)
                _input.ClearLine();

            string messageToSend = message;

            if(type != LogType.Assert)
            {
                messageToSend = FormatMessage(messageToSend, type); 
            }

            if(!ConsoleHandler.loggingPaused)
            {
                WriteLine(messageToSend);
            }
        }
        /// <summary>
        /// Update the input every frame
        /// This gets new key input and calls the OnInputText callback
        /// </summary>
        void Update()
        {
            _input.Update();
        }

        /// <summary>
        /// It's important to call console.ShutDown in OnDestroy
        /// because compiling will error out in the editor if you don't
        /// because we redirected output. This sets it back to normal.
        /// </summary>
        void OnDestroy()
        {
            _console.Shutdown();
        }
        /// <summary>
        /// Formats a message.
        /// </summary>
        /// <param name="message">Message to be formated.</param>
        /// <returns></returns>
        string FormatMessage(string message, LogType type)
        {
            return "<" + DateTime.Now.ToString("HH:mm:ss") + "> [" + type + "] " + message;
        }
        /// <summary>
        /// Writes a specified string to the console.
        /// </summary>
        /// <param name="message">String to write.</param>
        void WriteLine(string message)
        {
            //Writing the message to the console.
            System.Console.WriteLine(message);
            _input.ClearLine();
            // If we were typing something re-add it.
            _input.RedrawInputLine();
        }
#endif
    } 
}