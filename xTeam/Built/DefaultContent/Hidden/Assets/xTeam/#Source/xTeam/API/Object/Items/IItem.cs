﻿using UnityEngine;

namespace XTeam.API.Objects.Items
{
    public interface IItem {
        string GetItemName();
        string GetCustomItemName();
        string GetItemOwnersGuid();

        GameObject GetGameObject();

        //Use methods.
        void Use1();
        void Use2();
        //Throw methods.
        void ThrowingStart();
        void ThrowingAbort();

        //Set methods.
        void SetCustomName(string name);
    }
    /// <summary>
    /// Holds all information about an item.
    /// </summary>
    [System.Serializable]
    public struct ItemInfo
    {
        public string name;
        public string ownersGuid;
        public string customName;
    }
    /// <summary>
    /// Holds all objects associated with specific item.
    /// </summary>
    public class ItemObjectHolder
    {
        public Object useableItem;
        public Object droppedItem;
    }
}