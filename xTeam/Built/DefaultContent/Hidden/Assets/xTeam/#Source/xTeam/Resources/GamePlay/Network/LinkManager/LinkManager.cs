﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using XTeam.Resources.GamePlay.ContentManager;
using XTeam.Resources.GamePlay.Network.LinkManager.LinkCallbackManager;
using XTeam.Resources.GamePlay.Network.LinkManager.LinkManagerSetup;

namespace XTeam.Resources.GamePlay.Network.LinkManager
{
    /// <summary>
    /// Class managing all the network functionalities.
    /// </summary>
    public class LinkManager : NetworkBehaviour
    {
        /// <summary>
        ///     Instance of this class.
        /// </summary>
        public static LinkManager instance;

        //LinkManager modules.
        /// <summary>
        /// ServerSettings module of LinkManager.
        /// </summary>
        public static ServerSettings.ServerSettings serverSettings;
        /// <summary>
        /// LinkClientCallbackManager module of the LinkManager.
        /// </summary>
        [HideInInspector]
        internal LinkClientCallbackManager linkClientCallbackManager;
        /// <summary>
        /// LinkServerCallbackManager module of the LinkManager.
        /// </summary>
        [HideInInspector]
        internal LinkServerCallbackManager linkServerCallbackManager;
        /// <summary>
        /// LinkManagerClientSetup module of the LinkManager.
        /// </summary>
        [SerializeField]
        private LinkManagerClientSetup linkManagerClientSetup;
        /// <summary>
        /// LinkManagerServerSetup module of the LinkManager.
        /// </summary>
        [SerializeField]
        private LinkManagerServerSetup linkManagerServerSetup;
        /// <summary>
        /// NetworkIdentity of the LinkManager.
        /// </summary>
        public NetworkIdentity linkManagerIdentity;

        #region NetworkBehaviour
        /// <summary>
        /// Sets the instance of LinkManager.
        /// </summary>
        void Awake()
        {
            instance = this;
        }
        /// <summary>
        /// Called when the server starts.
        /// </summary>
        public override void OnStartServer()
        {
            linkManagerServerSetup.SetupLinkManagerServer();
        }
        /// <summary>
        /// Called when the client starts.
        /// </summary>
        public override void OnStartClient()
        {
            linkManagerClientSetup.SetupLinkManagerClient();
        }
        #endregion

        #region ServerMessages
        [Server]
        public static void SendGameInfo(NetworkConnection playerConnection, string playerGuid)
        {
            Debug.Log("Sending game info to: " + playerConnection.connectionId);
            //Setting guid info to player.
            SendGuidInfo(playerConnection, playerGuid);
            //Sending addon information to the player.
            NetworkContentManager.ContentTransferManager.SendContentInfos(playerConnection);
        }
        [Server]
        public static void SendGuidInfo(NetworkConnection playerConnection, string guid)
        {
            var gi = new StringMessage(guid);

            playerConnection.Send(XMsgTypes.ServerMsgGuid, gi);
        }
        #endregion
    }
    /// <summary>
    /// Message types used in the game.
    /// </summary>
    public static class XMsgTypes
    {
        //General messages.
        public const short ClientMsgLogin = 1001;
        public const short ServerMsgGuid = 1002;
        public const short ClientMsgReady = 1003;
        public const short ServerMsgKick = 1004;
        //SpawnManager messages.
        public const short ServerMsgSpawnedObj = 2001;
        //FileTransfer messages.
        public const short ServerMsgFileTransferPrepare = 5000;
        //FileContentManager messages.
        public const short ServerMsgContentInfo = 6000;
        public const short ClientMsgContentRequest = 6001;
        public const short ServerMsgLoadAllContent = 6002;
        public const short ClientMsgAllContentLoaded = 6003;
        public const short ServerMsgTotalContentCount = 6004;
        public const short ClientMsgAllContentDownloaded = 6005;
        public const short ServerMsgAllLoaded = 6006;
        //Character messages.
        public const short ServerMsgEnableComponents = 7000;
    }
}