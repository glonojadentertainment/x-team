﻿using System;

namespace XTeam.API.Console
{
    public class ConsoleCommand
    {
        /// <summary>
        /// Base command of this command.
        /// </summary>
        public string baseCommand;
        /// <summary>
        /// Arguments passed with this command.
        /// </summary>
        public string[] arguments;
        /// <summary>
        /// Constructs a new ConsoleCommand from a command string.
        /// </summary>
        /// <param name="command">String to construct a ConsoleCommand from.</param>
        public ConsoleCommand(string command)
        {
            //Splitting the command.
            string[] commandSplit = command.Split(' ');

            //Getting the baseCommand.
            baseCommand = commandSplit[0];

            //Getting the arguments.
            arguments = new string[commandSplit.Length - 1];
            Array.Copy(commandSplit, 1, arguments, 0, arguments.Length);
        }
    } 
}
