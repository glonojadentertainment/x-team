﻿namespace XTeam.API.Console
{
    /// <summary>
    /// Base class for all console modules.
    /// </summary>
    public class ConsoleModule
    {
        /// <summary>
        /// Returns this module's name.
        /// </summary>
        /// <returns>Name of the module.</returns>
        public virtual string GetName()
        {
            return null;
        }
        /// <summary>
        /// Gets this module's base console command.
        /// </summary>
        /// <returns>Base command of the module.</returns>
        public virtual string[] GetBaseCommands()
        {
            return null;
        }
        /// <summary>
        /// Method run when the base command of the module is dispatched.
        /// </summary>
        /// <param name="command">Command that has been dispatched.</param>
        public virtual bool OnCommandRun(ConsoleCommand command)
        {
            //Since there is no behaviour on this module, the success indicator will return false.
            return false; //Whether the method succeded.
        }
    } 
}