﻿namespace xTeam.API.Event.PlayerEvent
{
    public abstract class PlayerEvent : Event
    {
        /// <summary>
        /// Constructs a player event.
        /// </summary>
        /// <param name="player"></param>
        public PlayerEvent(XTeam.Resources.GamePlay.Network.Client.Player.Player player)
        {
            this.player = player;
        }

        /// <summary>
        /// PlayerInfo involved in this event.
        /// </summary>
        protected XTeam.Resources.GamePlay.Network.Client.Player.Player player;

        /// <summary>
        /// Returns the player involved in this event.
        /// </summary>
        /// <returns></returns>
        public virtual XTeam.Resources.GamePlay.Network.Client.Player.Player GetPlayer()
        {
            return player;
        }
    }
}
