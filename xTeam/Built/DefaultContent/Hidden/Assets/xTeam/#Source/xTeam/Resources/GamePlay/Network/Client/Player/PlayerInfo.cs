﻿//Player holds whole-game data.

using UnityEngine.Networking;

namespace XTeam.Resources.GamePlay.Network.Client.Player
{
    /// <summary>
    /// Message holding player information.
    /// </summary>
    public class PlayerInfo : MessageBase
    {
        /// <summary>
        /// The name of the player.
        /// </summary>
        public string name;
        /// <summary>
        /// The guid of the player.
        /// </summary>
        public string guid;
        /// <summary>
        /// Avatar url of the player.
        /// </summary>
        public string avatarUrl;
        /// <summary>
        /// Local IP of the player.
        /// </summary>
        public string localIP;
        /// <summary>
        /// Public IP of the player.
        /// </summary>
        public string publicIP;
    }
}