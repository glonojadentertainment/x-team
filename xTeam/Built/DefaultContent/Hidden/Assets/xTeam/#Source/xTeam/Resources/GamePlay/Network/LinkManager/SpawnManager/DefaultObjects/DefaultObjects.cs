﻿using UnityEngine;

namespace xTeam.Resources.GamePlay.Network.LinkManager.SpawnManager.DefaultObjects
{
    public static class DefaultObjects
    {
        public static string characterResourcePath =
            "Assets/xTeam/Resources/GamePlay/Network/LinkManager/SpawnManager/DefaultObjects/Character/Resources";

        public static Object GetDefaultObject(DefaultObjectType type)
        {
            switch (type)
            {
                case DefaultObjectType.CHARACTER:
                    return UnityEngine.Resources.Load(characterResourcePath);
                    break;
            }

            return null;
        }
        public enum DefaultObjectType
        {
            CHARACTER
        }
    }
}
