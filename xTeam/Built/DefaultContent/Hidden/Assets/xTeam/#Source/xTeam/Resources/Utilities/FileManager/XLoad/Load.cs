﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using Ionic.Zip;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using xTeam.Resources.XEditor.MapExporter;
using xTeam.xTeam_Creator_Classes.Resources.XmlUtil;
using XTeam.Resources.GamePlay.Network.LinkManager;

namespace xTeam.Resources.Utilities.FileManager.XLoad
{
    public class Load : MonoBehaviour
    {
        /// <summary>
        /// Loads a GameObject from the specified file.
        /// </summary>
        /// <param name="path"></param>
        public static void GameObjectFromFile(string path, Vector3 position, Quaternion rotation, Action<GameObject> callback)
        {
            //Loading the object bundle.
            #if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX
                var spawned = Instantiate(LoadAsset(LoadAssetBundle(path))) as GameObject;
                spawned.name = new FileInfo(path).Name;
                callback(spawned);
                return;
#endif
            LinkManager.instance.StartCoroutine(LoadAssetBundle(path, assetBundle =>
            {
                if(assetBundle == null) return; //Failed loading the object.
                //Loading the main asset of the bundle.
                LinkManager.instance.StartCoroutine(LoadAsset(assetBundle, asset =>
                {
                    var instantiated = Instantiate(asset, position, rotation) as GameObject;
                    instantiated.name = new FileInfo(path).Name;
                    callback(instantiated);
                }));
            }));
        }
        /// <summary>
        /// Loads a Scene from the specified file.
        /// Returns the name of the loaded scene.
        /// </summary>
        /// <param name="path"></param>
        public static string SceneFromFile(string path)
        {
            //Extracting specified map file .xtm to cache.
            using (var mapContainer = ZipFile.Read(path))
            {
                //Getting required paths.
                var cacheDirectory = new DirectoryInfo(Application.dataPath);
#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX
                cacheDirectory = new DirectoryInfo(Application.temporaryCachePath);
#endif
                if (cacheDirectory.FullName == Application.dataPath)
                {
                    cacheDirectory = new DirectoryInfo(XTeam.Resources.Utilities.FileManager.FileManager.GetApplicationCachePath());
                }

                Guid tempGuid = Guid.NewGuid();
                var mapCacheDirectory = new DirectoryInfo(cacheDirectory + "/" + tempGuid + "Cache");
                if (!mapCacheDirectory.Exists)
                {
                    mapCacheDirectory = cacheDirectory.CreateSubdirectory(tempGuid + "Cache");
                }

                //Extracting the map file to the cache folder.
                mapContainer.ExtractAll(mapCacheDirectory.FullName, ExtractExistingFileAction.OverwriteSilently);

                //Deserializng the mapInfo.xml file.
                var mapInfoXml = new XmlDocument();
                mapInfoXml.Load(mapCacheDirectory.FullName + "/mapInfo.xml");
                var mapInfo = XmlUtil.Deserialize(mapInfoXml, typeof(Map)) as Map;

                //Load compressed scene.
                var mapFile = new FileInfo(mapCacheDirectory.FullName + "/mapData.xto");
                Debug.Log("Loading map: " + mapInfo.name + " from: " + mapFile.FullName);
                AssetBundle.LoadFromFile(mapFile.FullName).LoadAllAssets();

                return mapInfo.name;
            }
        }
        private static IEnumerator LoadAssetBundle(string path, Action<AssetBundle> loadedAssetBundle)
        {
            //Loading the AssetBundle.
            var loadRequest = AssetBundle.LoadFromFileAsync(path);
            yield return loadRequest;

            //Returning the assetbundle.
            if(loadRequest.assetBundle == null) Debug.LogError("Failed to load asset pack at: " + path);
            loadedAssetBundle(loadRequest.assetBundle);
        }
        private static AssetBundle LoadAssetBundle(string path)
        {
            //Loading the AssetBundle.
            return AssetBundle.LoadFromFile(path);
        }

        private static IEnumerator LoadAsset(AssetBundle bundle, Action<UnityEngine.Object> loadedAsset)
        {
            //Loading the Asset.
            var loadRequest = bundle.LoadAllAssetsAsync();
            yield return loadRequest;

            //Returning the asset.
            if(loadRequest.allAssets == null || loadRequest.allAssets.Length == 0) Debug.LogError("Failed to load asset from: " + bundle.name);
            loadedAsset(bundle.mainAsset);
        }
        private static UnityEngine.Object LoadAsset(AssetBundle bundle)
        {
            //Loading the Asset.
            var objects = bundle.LoadAllAssets();
            return objects[0];
        }
    }
}
