﻿using System;
using UnityEngine;

namespace XTeam.API.Console
{
    /// <summary>
    /// Represents a console poll.
    /// </summary>
    public class ConsolePoll : ConsoleModule
    {
        public string title;
        public string description;
        public string[] choices;
        public Action<string> onPollChoiceSelect;

        /// <summary>
        /// Creates a new poll on the console with specified choices.
        /// </summary>
        /// <param name="title">Title of the poll.</param>
        /// <param name="choices">Choices to list on the poll.</param>
        /// <param name="onPollChoiceSelect">Callback when one of the choices is selected.</param>
        public ConsolePoll(string title, string description, string[] choices, Action<string> onPollChoiceSelect)
        {
            //Setting the properties.
            this.title = title;
            this.description = description;
            this.choices = choices;
            this.onPollChoiceSelect = onPollChoiceSelect;

            //Writing the poll.
            WritePoll();
            //Pausing the logging queue.
            ConsoleHandler.loggingPaused = true;

            //Registering the console module.
            ConsoleHandler.RegisterModule(this);
        }
        /// <summary>
        /// Writes the poll to the console.
        /// </summary>
        private void WritePoll()
        {
            //Writing the title.
            Debug.LogAssertion("");
            Debug.LogAssertion("");
            Debug.LogAssertion("[Poll] " + title);
            //Writing the description.
            if (description != null)
            {
                Debug.LogAssertion(" " + description);
            }
            else
            {
                Debug.LogAssertion(" " + "Choose one of the following.");
            }

            //Writing the choices.
            int choiceNumber = 0;
            foreach(string choice in choices)
            {
                Debug.LogAssertion(" [" + choiceNumber + "] " + choice);
                choiceNumber++;
            }

            //Writing the ending.
            Debug.LogAssertion("");
            Debug.LogAssertion("Enter desired choice number to select it.");
            Debug.LogAssertion("");
            Debug.LogAssertion("");
        }
        public override string[] GetBaseCommands()
        {
            return choices;
        }
        public override string GetName()
        {
            return "Console Poll: " + title;
        }
        public override bool OnCommandRun(ConsoleCommand command)
        {
            ConsoleHandler.loggingPaused = false;
            onPollChoiceSelect(command.baseCommand);
            //Unregistering the console module.
            ConsoleHandler.UnRegisterModule(this);
            return true;
        }
    }
    /// <summary>
    /// Represents a console input field.
    /// </summary>
    public class ConsoleInputField : ConsoleModule
    {
        public string title;
        public string description;
        public Action<string> onInputFieldSubmit;

        /// <summary>
        /// Creates a new poll on the console with specified choices.
        /// </summary>
        /// <param name="title">Title of the poll.</param>
        /// <param name="choices">Choices to list on the poll.</param>
        /// <param name="OnPollChoiceSelect">Callback when one of the choices is selected.</param>
        public ConsoleInputField(string title, string description, Action<string> onInputFieldSubmit)
        {
            //Setting the properties.
            this.title = title;
            this.description = description;
            this.onInputFieldSubmit = onInputFieldSubmit;

            //Writing the poll.
            WriteInputField();
            //Pausing the logging queue.
            ConsoleHandler.loggingPaused = true;

            //Registering the console module.
            ConsoleHandler.RegisterModule(this);
        }
        /// <summary>
        /// Writes the poll to the console.
        /// </summary>
        private void WriteInputField()
        {
            //Writing the title.
            Debug.LogAssertion("");
            Debug.LogAssertion("");
            Debug.LogAssertion("[InputField] " + title);
            //Writing the description.
            if (description != null)
            {
                Debug.LogAssertion(" " + description);
            }
            else
            {
                Debug.LogAssertion(" " + "Enter a value.");
            }

            Debug.LogAssertion("");
            Debug.LogAssertion("");
        }
        public override string[] GetBaseCommands()
        {
            return new string[] { "ANY" };
        }
        public override string GetName()
        {
            return "Console Input Field: " + title;
        }
        public override bool OnCommandRun(ConsoleCommand command)
        {
            ConsoleHandler.loggingPaused = false;
            onInputFieldSubmit(command.baseCommand);
            //Unregistering the console module.
            ConsoleHandler.UnRegisterModule(this);
            return true;
        }
    }
}