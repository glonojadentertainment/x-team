﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using xTeam.API.Event;
using xTeam.API.Event.PlayerEvent;
using XTeam.Resources.GamePlay.Network.Client.Player;
using Object = UnityEngine.Object;

namespace XTeam.Resources.GamePlay.Network.LinkManager.PlayerManager
{
    /// <summary>
    /// Class managing Players on the Server.
    /// </summary>
    public class PlayerManager : NetworkBehaviour
    {
        /// <summary>
        ///     Prefab of the player object.
        /// </summary>
        public Object playerPrefab;

        /// <summary>
        ///     Holder of the player objects.
        /// </summary>
        public GameObject playersHolder;
        
        /// <summary>
        /// Instance of PlayerManager.
        /// </summary>
        public static PlayerManager instance;

        #region NetworkBehaviour
        /// <summary>
        /// Setting the instance.
        /// </summary>
        private void Awake()
        {
            instance = this;
        }
        #endregion
        #region GetPlayer
        /// <summary>
        ///     Searches for specific player.
        /// </summary>
        /// <returns>
        ///     Specific player class.
        /// </returns>
        public static Player GetPlayer(int connectionId)
        {
            return instance.playersHolder.transform.GetComponentsInChildren<Player>().FirstOrDefault(player => player.connectionID == connectionId);
        }
        /// <summary>
        ///     Searches for specific player.
        /// </summary>
        /// <returns>
        ///     Specific player class.
        /// </returns>
        public static Player GetPlayer(string playerGuid)
        {
            return GetPlayers().FirstOrDefault(p => p.playerInfo.guid == playerGuid);
        }
        /// <summary>
        ///     Searches for specific player.
        /// </summary>
        /// <returns>
        ///     Specific player class.
        /// </returns>
        public static Player GetPlayerByName(string playerName)
        {
            return GetPlayers().FirstOrDefault(p => p.playerName == playerName);
        }

        /// <returns>
        ///     Array of all player's classes.
        /// </returns>
        public static IEnumerable<Player> GetPlayers()
        {
            return instance.playersHolder.GetComponentsInChildren<Player>();
        }
        #endregion
        #region KickPlayer
        /// <summary>
        /// Kicks a specified player without a reason.
        /// </summary>
        public static void KickPlayer(Player p)
        {
            //Sending the reason message to the player.
            p.networkIdentity.clientAuthorityOwner.Send(XMsgTypes.ServerMsgKick, new KickMessage
            {
                reason = "You were kicked from the server"
            });
            //Disconnecting the player.
            p.networkIdentity.clientAuthorityOwner.Disconnect();
        }
        /// <summary>
        /// Kicks a specified player with a reason message.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="reason"></param>
        public static void KickPlayer(Player p, string reason)
        {
            //Sending the reason message to the player.
            p.networkIdentity.clientAuthorityOwner.Send(XMsgTypes.ServerMsgKick, new KickMessage
            {
                reason = reason
            });
            //Disconnecting the player.
            p.networkIdentity.clientAuthorityOwner.Disconnect();
        }

        #endregion
        /// <summary>
        ///     Searches for a NetworkConnection with a specified connectionID.
        /// </summary>
        /// <param name="connectionId">Connection id of the requested connection.</param>
        /// <returns>Found NetworkConnection.</returns>
        public static NetworkConnection GetConnection(int connectionId)
        {
            return
                NetworkServer.connections.Where(conn => conn != null)
                    .FirstOrDefault(conn => conn.connectionId == connectionId);
        }

        #region Callbacks
        public static void OnClientDisconnectedFromServer(NetworkMessage msg)
        {
            //PlayerInfo left a server.
            var p = GetPlayer(msg.conn.connectionId);
            Debug.Log(p.playerName + " disconnected from the server.");

            //Removing player from the list.
            NetworkServer.Destroy(p.gameObject);
        }
        /// <summary>
        /// Called when a player logs in to the server.
        /// </summary>
        /// <param name="msg"></param>
        internal static void OnClientLogin(NetworkMessage msg)
        {
            //Fetched player's login data.
            //Deserializing PlayerInfo.
            LoginMessage loginMessage = msg.ReadMessage<LoginMessage>();
            PlayerInfo playerInfo = loginMessage.playerInfo;

            Debug.Log(playerInfo.name + " logged in");
            //Generating a new GUID for player.
            playerInfo.guid = Guid.NewGuid().ToString();

            //Getting player's object.
            var playerGameObject = (GameObject)Instantiate(instance.playerPrefab);
            var player = playerGameObject.GetComponent<Player>();
            player.SetupVariables(playerInfo, msg.conn.connectionId);
            player.name = playerInfo.name;
            //Adding player to the players holder.
            playerGameObject.transform.SetParent(instance.playersHolder.transform);
            //Spawning the player's object.
            NetworkServer.SpawnWithClientAuthority(playerGameObject, msg.conn);

            EventManager.CallEvent(new PlayerLoginEvent(player, loginMessage.connectionAddress, player.name + " joined the game"), calledEvent =>
            {
                PlayerLoginEvent playerLoginEvent = calledEvent as PlayerLoginEvent;

                switch (playerLoginEvent.GetResult())
                {
                        case PlayerLoginEvent.Result.ALLOWED:
                        //Sending player game info.
                        LinkManager.SendGameInfo(msg.conn, playerInfo.guid);
                        break;
                        case PlayerLoginEvent.Result.KICK_BANNED:
                        KickPlayer(playerLoginEvent.GetPlayer());
                        //TODO: Reason
                        break;
                        case PlayerLoginEvent.Result.KICK_FULL:
                        KickPlayer(playerLoginEvent.GetPlayer());
                        //TODO: Reason
                        break;
                        case PlayerLoginEvent.Result.KICK_OTHER:
                        KickPlayer(playerLoginEvent.GetPlayer());
                        //TODO: Reason
                        break;
                        case PlayerLoginEvent.Result.KICK_WHITELIST:
                        KickPlayer(playerLoginEvent.GetPlayer());
                        //TODO: Reason
                        break;
                        default:
                        KickPlayer(playerLoginEvent.GetPlayer());
                        break;
                }
            });
        }

        internal static void OnAddPlayerForConnection(NetworkMessage msg)
        {
            //This spawns the new player on all clients
            NetworkServer.AddPlayerForConnection(msg.conn, GetPlayer(msg.conn.connectionId).gameObject, 0);
            //Giving player authority.
            GetPlayer(msg.conn.connectionId).networkIdentity.AssignClientAuthority(msg.conn);
        }

        public static void OnClientReady(NetworkMessage msg)
        {
            //Setting player as ready.
            GetPlayer(msg.conn.connectionId).isReady = true;
        }

        #endregion

        #region Messages

        /// <summary>
        /// Message used to login to the server.
        /// </summary>
        internal class LoginMessage : MessageBase
        {
            /// <summary>
            /// Player info of the logging in player.
            /// </summary>
            public PlayerInfo playerInfo;

            /// <summary>
            /// Address used to connect to the server.
            /// </summary>
            public string connectionAddress;
        }

        /// <summary>
        /// Message used to kick players.
        /// </summary>
        internal class KickMessage : MessageBase
        {
            /// <summary>
            /// Reson for the kick.
            /// </summary>
            public string reason;
        }

        #endregion
    }
}
