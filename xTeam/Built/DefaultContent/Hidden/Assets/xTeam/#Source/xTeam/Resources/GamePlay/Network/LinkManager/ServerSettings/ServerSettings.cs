﻿using UnityEngine;
using UnityEngine.Networking;
using XTeam.Resources.GamePlay.Network.Server;

namespace XTeam.Resources.GamePlay.Network.LinkManager.ServerSettings
{
    public class ServerSettings : NetworkBehaviour {
        //Settings.
        [Tooltip("Name of the server.")] [SyncVar]
        public string serverName;
        [Tooltip("Description of the server.")] [SyncVar]
        public string serverDescription;
        [Tooltip ("Size of the server.")] [SyncVar]
        public int serverSize;
        [Tooltip ("Password of the server.")] [SyncVar]
        public string serverPassword;
        [Tooltip ("Port of the server.")] [SyncVar]
        public int serverPort;

        /// <summary>
        /// File transfer port of the server.
        /// </summary>
        public int fileTransferPort
        {
            get { return serverPort + 1; }
        }
        /// <summary>
        /// Connection bridge port of the server.
        /// </summary>
        public int connectionBridgePort
        {
            get { return serverPort + 2; }
        }
        [Tooltip ("Whether server is dedicated.")] [SyncVar]
        public bool isDedicated = false;
        [Tooltip ("Whether server uses matchmaker.")] [SyncVar]
        public bool useMatchmaker = true;
        [Tooltip("The tickrate of the server.")] [SyncVar]
        public float serverTickRate;
		[Tooltip("The sendrate of the server.")] [SyncVar]
		public float serverSendRate;

        /// <summary>
        /// Copies server settings from the ServerProperties class.
        /// </summary>
        /// <param name="properties">ServerProperties to copy data from.</param>
        [Server]
        public void CopyFrom(ServerProperties properties)
        {
            serverName = properties.serverName;
            serverDescription = properties.serverDescription;
            serverSize = properties.serverSize;
            serverPassword = properties.serverPassword;
            serverPort = properties.serverPort;
            isDedicated = properties.isDedicated;
            useMatchmaker = properties.useMatchmaker;
            serverTickRate = properties.serverTickRate;
            serverSendRate = properties.serverSendRate;
        }
    }
}
